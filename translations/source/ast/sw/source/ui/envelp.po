#. extracted from sw/source/ui/envelp
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2013-06-02 22:13+0000\n"
"Last-Translator: Xuacu <xuacusk8@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1370211189.0\n"

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_DATABASE_NOT_OPENED\n"
"string.text"
msgid "Database could not be opened."
msgstr "Nun se pudo abrir la base de datos."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_NO_DRIVERS\n"
"string.text"
msgid "No database drivers installed."
msgstr "Nun hai instalaos los controladores de la base de datos."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_BTN_NEWDOC\n"
"string.text"
msgid "~New Doc."
msgstr "~Nuevu doc."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_SENDER_TOKENS\n"
"string.text"
msgid "COMPANY;CR;FIRSTNAME; ;LASTNAME;CR;ADDRESS;CR;CITY; ;STATEPROV; ;POSTALCODE;CR;COUNTRY;CR;"
msgstr "COMPANY;CR;FIRSTNAME; ;LASTNAME;CR;ADDRESS;CR;CITY; ;STATEPROV; ;POSTALCODE;CR;COUNTRY;CR;"

#: label.src
msgctxt ""
"label.src\n"
"STR_CUSTOM\n"
"string.text"
msgid "[User]"
msgstr "[Usuariu]"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_HDIST\n"
"string.text"
msgid "H. Pitch"
msgstr "Dist. horizontal"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_VDIST\n"
"string.text"
msgid "V. Pitch"
msgstr "Dist. vertical"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_WIDTH\n"
"string.text"
msgid "Width"
msgstr "Anchor"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_HEIGHT\n"
"string.text"
msgid "Height"
msgstr "Altor"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_LEFT\n"
"string.text"
msgid "Left margin"
msgstr "Marxe esquierdu"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_UPPER\n"
"string.text"
msgid "Top margin"
msgstr "Marxe superior"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_COLS\n"
"string.text"
msgid "Columns"
msgstr "Columnes"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_ROWS\n"
"string.text"
msgid "Rows"
msgstr "Fileres"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_PWIDTH\n"
"string.text"
msgid "Page Width"
msgstr "Anchor de páxina"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_PHEIGHT\n"
"string.text"
msgid "Page Height"
msgstr "Altor de páxina"
