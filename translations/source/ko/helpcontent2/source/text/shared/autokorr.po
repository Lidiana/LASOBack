#. extracted from helpcontent2/source/text/shared/autokorr
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:02+0100\n"
"PO-Revision-Date: 2011-04-06 00:13+0200\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: 01000000.xhp
msgctxt ""
"01000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 01000000.xhp
msgctxt ""
"01000000.xhp\n"
"hd_id3148410\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 01000000.xhp
msgctxt ""
"01000000.xhp\n"
"hd_id3146946\n"
"2\n"
"help.text"
msgid "TWo INitial CApitals have been corrected"
msgstr "단어 첫 부분에 있는 두 개의 대문자가 수정되었습니다."

#: 01000000.xhp
msgctxt ""
"01000000.xhp\n"
"par_id3158397\n"
"3\n"
"help.text"
msgid "Typing errors such as \"WOrd\" have been corrected and replaced by the <link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> function to \"Word\"."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link> 기능에서는 \"WOrd\"와 같은 입력 오류를 \"Word\"로 수정합니다."

#: 02000000.xhp
msgctxt ""
"02000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 02000000.xhp
msgctxt ""
"02000000.xhp\n"
"hd_id3155354\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 02000000.xhp
msgctxt ""
"02000000.xhp\n"
"hd_id3150502\n"
"2\n"
"help.text"
msgid "Start each sentence with a capital letter"
msgstr "모든 문장을 대문자로 시작"

#: 02000000.xhp
msgctxt ""
"02000000.xhp\n"
"par_id3158397\n"
"3\n"
"help.text"
msgid "Your text was corrected with <link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> so that the current word began with a capital letter. AutoCorrect changes words at the beginning of a paragraph, and words after the character at the end of a sentence (period, exclamation point, question mark)."
msgstr "현재 단어가 대문자로 시작되도록 <link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>을 통해 텍스트가 수정되었습니다. 자동 고침은 단락의 처음에 나오는 단어와 문장 끝에 있는 문자(마침표, 느낌표, 물음표) 다음에 나오는 단어를 변경합니다."

#: 03000000.xhp
msgctxt ""
"03000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 03000000.xhp
msgctxt ""
"03000000.xhp\n"
"hd_id3152459\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 03000000.xhp
msgctxt ""
"03000000.xhp\n"
"hd_id3146946\n"
"2\n"
"help.text"
msgid "Two capital letters at the beginning of a word and a sentence have been corrected to one capital letter"
msgstr "단어 처음에 있는 두 개의 대문자와 대문자가 있는 문장 첫 부분이 수정됨"

#: 03000000.xhp
msgctxt ""
"03000000.xhp\n"
"par_id3158397\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has modified your text so that a word beginning with two capital letters at the beginning of a sentence now starts with one capital letter."
msgstr "사용자의 텍스트는 <link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>을 통하여 단어 첫 부분에 있는 두 개의 대문자와 대문자로 시작하는 문장 첫 부분이 수정되었습니다."

#: 04000000.xhp
msgctxt ""
"04000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 04000000.xhp
msgctxt ""
"04000000.xhp\n"
"hd_id3154283\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 04000000.xhp
msgctxt ""
"04000000.xhp\n"
"hd_id3154812\n"
"2\n"
"help.text"
msgid "A replacement has been carried out"
msgstr "자동 고침 (바꾸기)이 실행되었습니다"

#: 04000000.xhp
msgctxt ""
"04000000.xhp\n"
"par_id3159241\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has replaced a word."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>에서는 단어 단위로 바뀝니다."

#: 05000000.xhp
msgctxt ""
"05000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 05000000.xhp
msgctxt ""
"05000000.xhp\n"
"hd_id3155354\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 05000000.xhp
msgctxt ""
"05000000.xhp\n"
"hd_id3156418\n"
"2\n"
"help.text"
msgid "AutoCorrect has performed a replacement. The beginning of the sentence now starts with a capital letter"
msgstr "자동 고침 (바꾸기)이 실행되었으며 문장 첫 부분이 대문자로 시작되었습니다"

#: 05000000.xhp
msgctxt ""
"05000000.xhp\n"
"par_id3153341\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has performed a replacement, and the beginning of the sentence now starts with a capital letter."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>에서는 바꾸기를 수행하여 문장의 시작 부분을 대문자로 시작합니다."

#: 06000000.xhp
msgctxt ""
"06000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 06000000.xhp
msgctxt ""
"06000000.xhp\n"
"hd_id3148932\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 06000000.xhp
msgctxt ""
"06000000.xhp\n"
"hd_id3158421\n"
"2\n"
"help.text"
msgid "Double quotation marks (\") have been replaced"
msgstr "큰 따옴표가 바뀌었습니다"

#: 06000000.xhp
msgctxt ""
"06000000.xhp\n"
"par_id3146060\n"
"3\n"
"help.text"
msgid "Your text was corrected by <link href=\"text/shared/01/06040000.xhp\" name=\"Autocorrect\">Autocorrect</link> so that double quotation marks were replaced by <link href=\"text/shared/01/06040400.xhp\" name=\"typographical quotation marks\">typographical quotation marks</link>."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>에서 텍스트의 큰 따옴표를 <link href=\"text/shared/01/06040400.xhp\" name=\"인쇄체 인용 부호\">인쇄체 인용 부호</link>로 바꾸도록 수정되었습니다."

#: 07000000.xhp
msgctxt ""
"07000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 07000000.xhp
msgctxt ""
"07000000.xhp\n"
"hd_id3153629\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 07000000.xhp
msgctxt ""
"07000000.xhp\n"
"hd_id3149987\n"
"2\n"
"help.text"
msgid "Single quotes have been replaced"
msgstr "작은 따옴표가 바뀌었습니다"

#: 07000000.xhp
msgctxt ""
"07000000.xhp\n"
"par_id3154688\n"
"3\n"
"help.text"
msgid "Your text was corrected by <link href=\"text/shared/01/06040000.xhp\" name=\"Autocorrect\">Autocorrect</link> so that single quotation marks were replaced by <link href=\"text/shared/01/06040400.xhp\" name=\"typographical quotation marks\">typographical quotation marks</link>."
msgstr ""

#: 08000000.xhp
msgctxt ""
"08000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 08000000.xhp
msgctxt ""
"08000000.xhp\n"
"hd_id3147240\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 08000000.xhp
msgctxt ""
"08000000.xhp\n"
"hd_id3152823\n"
"2\n"
"help.text"
msgid "An URL has been detected and a hyperlink attribute has been set"
msgstr "URL을 탐지하여 하이퍼링크 속성을 설정합니다."

#: 08000000.xhp
msgctxt ""
"08000000.xhp\n"
"par_id3150278\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has modified your text. A string has been detected as an URL and is now shown as a hyperlink."
msgstr "문자열이 URL로 인식되고 하이퍼링크로 표시되도록 <link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>을 통하여 사용자의 텍스트가 수정되었습니다."

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"hd_id3149976\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"hd_id3147543\n"
"2\n"
"help.text"
msgid "Double spaces have been ignored"
msgstr "이중 공백이 무시되었습니다"

#: 09000000.xhp
msgctxt ""
"09000000.xhp\n"
"par_id3149297\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has corrected your text so that the multiple spaces you have entered have now been reduced to one single space."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>에서는 텍스트에 있는 여러 개의 공백을 하나의 공백으로 수정합니다."

#: 10000000.xhp
msgctxt ""
"10000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 10000000.xhp
msgctxt ""
"10000000.xhp\n"
"hd_id3147446\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 10000000.xhp
msgctxt ""
"10000000.xhp\n"
"hd_id3155577\n"
"2\n"
"help.text"
msgid "Bold and underline attributes have been recognized and applied"
msgstr "굵게 표시 및 밑줄 속성을 인식하고 적용합니다."

#: 10000000.xhp
msgctxt ""
"10000000.xhp\n"
"par_id3156014\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has modified your text, and the bold and/or underline text attributes have been automatically applied."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>에서는 텍스트를 수정하여 굵게 및/또는 밑줄 텍스트 속성을 자동으로 적용합니다."

#: 12000000.xhp
msgctxt ""
"12000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 12000000.xhp
msgctxt ""
"12000000.xhp\n"
"hd_id3153116\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 12000000.xhp
msgctxt ""
"12000000.xhp\n"
"hd_id3149551\n"
"2\n"
"help.text"
msgid "Minus signs have been replaced"
msgstr "음수 기호가 바뀌었습니다"

#: 12000000.xhp
msgctxt ""
"12000000.xhp\n"
"par_id3148932\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has modified your text, and minus signs have been replaced with dashes."
msgstr "빼기 기호로부터 대시가 되도록 <link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>을 통하여 사용자의 텍스트가 수정되었습니다."

#: 13000000.xhp
msgctxt ""
"13000000.xhp\n"
"tit\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 13000000.xhp
msgctxt ""
"13000000.xhp\n"
"hd_id3149513\n"
"1\n"
"help.text"
msgid "AutoCorrect has been activated"
msgstr "자동 고침 기능이 활성화되었습니다."

#: 13000000.xhp
msgctxt ""
"13000000.xhp\n"
"hd_id3147090\n"
"2\n"
"help.text"
msgid "1st ... has been replaced with 1st ..."
msgstr "1st ...는 1st ...로 바뀝니다."

#: 13000000.xhp
msgctxt ""
"13000000.xhp\n"
"par_id3153220\n"
"3\n"
"help.text"
msgid "<link href=\"text/shared/01/06040000.xhp\" name=\"AutoCorrect\">AutoCorrect</link> has corrected your text so that ordinal number suffixes have been superscripted."
msgstr "<link href=\"text/shared/01/06040000.xhp\" name=\"자동 고침\">자동 고침</link>에서는 텍스트를 수정하여 서수 접미사를 위 첨자로 표시합니다."
