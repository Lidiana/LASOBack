#. extracted from svx/source/accessibility
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:06+0200\n"
"PO-Revision-Date: 2011-04-05 11:56+0200\n"
"Last-Translator: arjunaraoc <arjunaraoc@googlemail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: te\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_3D_MATERIAL_COLOR\n"
"string.text"
msgid "3D material color"
msgstr "3Dపదార్ధము రంగు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_TEXT_COLOR\n"
"string.text"
msgid "Font color"
msgstr "అక్షరశైలి రంగు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_BACKGROUND_COLOR\n"
"string.text"
msgid "Background color"
msgstr "పూర్వరంగ రంగు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_NONE\n"
"string.text"
msgid "None"
msgstr "ఏదీ కాదు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_SOLID\n"
"string.text"
msgid "Solid"
msgstr "గట్టి"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_HATCH\n"
"string.text"
msgid "With hatching"
msgstr "పొదుగించు తో"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_GRADIENT\n"
"string.text"
msgid "Gradient"
msgstr "గ్రేడియంట్"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_FILLSTYLE_BITMAP\n"
"string.text"
msgid "Bitmap"
msgstr "బిట్ మాప్"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_WITH\n"
"string.text"
msgid "with"
msgstr "తో"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_STYLE\n"
"string.text"
msgid "Style"
msgstr "శైలి"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_A11Y_AND\n"
"string.text"
msgid "and"
msgstr "మరియు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_NAME\n"
"string.text"
msgid "Corner control"
msgstr "మూల నిర్మాణము"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CORN_DESCR\n"
"string.text"
msgid "Selection of a corner point."
msgstr "మూలస్థానం ఎంపిక"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_NAME\n"
"string.text"
msgid "Angle control"
msgstr "కోణాంశ  నియంత్రణ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_ANGL_DESCR\n"
"string.text"
msgid "Selection of a major angle."
msgstr "పెద్దదైన కోణాంశమును ఎంపిక చేయు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LT\n"
"string.text"
msgid "Top left"
msgstr "పై ఎడమ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MT\n"
"string.text"
msgid "Top middle"
msgstr "పై మధ్య"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RT\n"
"string.text"
msgid "Top right"
msgstr "పై కుడి"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LM\n"
"string.text"
msgid "Left center"
msgstr "ఎడమ మధ్యమ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MM\n"
"string.text"
msgid "Center"
msgstr "మధ్యలో"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RM\n"
"string.text"
msgid "Right center"
msgstr "కుడి మధ్యమ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_LB\n"
"string.text"
msgid "Bottom left"
msgstr "దిగువ ఎడమ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_MB\n"
"string.text"
msgid "Bottom middle"
msgstr "దిగువ మధ్య"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_RB\n"
"string.text"
msgid "Bottom right"
msgstr "దిగువ కుడి"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A000\n"
"string.text"
msgid "0 degrees"
msgstr "0 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A045\n"
"string.text"
msgid "45 degrees"
msgstr "45 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A090\n"
"string.text"
msgid "90 degrees"
msgstr "90 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A135\n"
"string.text"
msgid "135 degrees"
msgstr "135 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A180\n"
"string.text"
msgid "180 degrees"
msgstr "180 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A225\n"
"string.text"
msgid "225 degrees"
msgstr "225 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A270\n"
"string.text"
msgid "270 degrees"
msgstr "270 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_RECTCTL_ACC_CHLD_A315\n"
"string.text"
msgid "315 degrees"
msgstr "315 కోణాంశం"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_NAME\n"
"string.text"
msgid "Contour control"
msgstr "రూపురేఖల నియంత్రణ"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_GRAPHCTRL_ACC_DESCRIPTION\n"
"string.text"
msgid "This is where you can edit the contour."
msgstr "ఈ   ఆకారమును ఎక్కడైన సరిచేయు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_SELECTION\n"
"string.text"
msgid "Special character selection"
msgstr "ప్రత్యేకమైన అక్షరముల ఎంపిక"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHAR_SEL_DESC\n"
"string.text"
msgid "Select special characters in this area."
msgstr "ఈ స్థలంలో ప్రత్యేకమైన అక్షరములు ఎంపిక చేయు"

#: accessibility.src
msgctxt ""
"accessibility.src\n"
"RID_SVXSTR_CHARACTER_CODE\n"
"string.text"
msgid "Character code "
msgstr "అక్షరముల సంకేతం"
