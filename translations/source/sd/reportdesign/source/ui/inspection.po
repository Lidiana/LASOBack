#. extracted from reportdesign/source/ui/inspection
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2014-08-19 10:36+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1408444572.000000\n"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_PROPPAGE_DEFAULT\n"
"string.text"
msgid "General"
msgstr ""
"#-#-#-#-#  tabledesign.po (PACKAGE VERSION)  #-#-#-#-#\n"
"عام \n"
"#-#-#-#-#  dialog.po (PACKAGE VERSION)  #-#-#-#-#\n"
"عام \n"
"#-#-#-#-#  appl.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ساڌارڻ\n"
"#-#-#-#-#  dialogs.po (PACKAGE VERSION)  #-#-#-#-#\n"
"عام \n"
"#-#-#-#-#  propctrlr.po (PACKAGE VERSION)  #-#-#-#-#\n"
"عام\n"
"#-#-#-#-#  bibliography.po (PACKAGE VERSION)  #-#-#-#-#\n"
"عام "

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PROPPAGE_DATA\n"
"string.text"
msgid "Data"
msgstr "سامگري"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BOOL\n"
"No\n"
"itemlist.text"
msgid "No"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BOOL\n"
"Yes\n"
"itemlist.text"
msgid "Yes"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE\n"
"string.text"
msgid "Force New Page"
msgstr "نئون صفحو تي زور ڀريو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"None\n"
"itemlist.text"
msgid "None"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"Before Section\n"
"itemlist.text"
msgid "Before Section"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"After Section\n"
"itemlist.text"
msgid "After Section"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_FORCENEWPAGE_CONST\n"
"Before & After Section\n"
"itemlist.text"
msgid "Before & After Section"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_NEWROWORCOL\n"
"string.text"
msgid "New Row Or Column"
msgstr "نئين قطار يا نئون ڪالم"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER\n"
"string.text"
msgid "Keep Together"
msgstr "گڏ رکو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER_CONST\n"
"No\n"
"itemlist.text"
msgid "No"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER_CONST\n"
"Whole Group\n"
"itemlist.text"
msgid "Whole Group"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_KEEPTOGETHER_CONST\n"
"With First Detail\n"
"itemlist.text"
msgid "With First Detail"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CANGROW\n"
"string.text"
msgid "Can Grow"
msgstr "اُسري سگهي ٿو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CANSHRINK\n"
"string.text"
msgid "Can Shrink"
msgstr "سسي سگهي ٿو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPEATSECTION\n"
"string.text"
msgid "Repeat Section"
msgstr "وڀاڳ دهرايو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PRINTREPEATEDVALUES\n"
"string.text"
msgid "Print repeated values"
msgstr "دهرايل ملهَہ ڇاپيو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CONDITIONALPRINTEXPRESSION\n"
"string.text"
msgid "Conditional Print Expression"
msgstr "شرطي ڇپائيءَ جو اِظهار"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_STARTNEWCOLUMN\n"
"string.text"
msgid "Start new column"
msgstr "نئون ڪالم شروع ڪريو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_STARTNEWPAGE\n"
"string.text"
msgid "Start new page"
msgstr "نئون صفحو شروع ڪريو"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_RESETPAGENUMBER\n"
"string.text"
msgid "Reset page number"
msgstr "پهريون صفحو نمبر"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_CHARTTYPE\n"
"string.text"
msgid "Chart type"
msgstr "چارٽ جو قسم"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PRINTWHENGROUPCHANGE\n"
"string.text"
msgid "Print repeated value on group change"
msgstr ""

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_VISIBLE\n"
"string.text"
msgid "Visible"
msgstr "نظر ايندڙ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_GROUPKEEPTOGETHER\n"
"string.text"
msgid "Group keep together"
msgstr "گروپ گڏ ڪري رکو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_GROUPKEEPTOGETHER_CONST\n"
"Per Page\n"
"itemlist.text"
msgid "Per Page"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_GROUPKEEPTOGETHER_CONST\n"
"Per Column\n"
"itemlist.text"
msgid "Per Column"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SECTIONPAGEBREAK_CONST\n"
"None\n"
"itemlist.text"
msgid "None"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SECTIONPAGEBREAK_CONST\n"
"Section\n"
"itemlist.text"
msgid "Section"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SECTIONPAGEBREAK_CONST\n"
"Automatic\n"
"itemlist.text"
msgid "Automatic"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PAGEHEADEROPTION\n"
"string.text"
msgid "Page header"
msgstr "صفحي مٿان شرح"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PAGEFOOTEROPTION\n"
"string.text"
msgid "Page footer"
msgstr "صفحي هيٺان شرح"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"All Pages\n"
"itemlist.text"
msgid "All Pages"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"Not With Report Header\n"
"itemlist.text"
msgid "Not With Report Header"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"Not With Report Footer\n"
"itemlist.text"
msgid "Not With Report Footer"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_REPORTPRINTOPTION_CONST\n"
"Not With Report Header/Footer\n"
"itemlist.text"
msgid "Not With Report Header/Footer"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DEEPTRAVERSING\n"
"string.text"
msgid "Deep traversing"
msgstr "اونهو آر پار وڃڻ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PREEVALUATED\n"
"string.text"
msgid "Pre evaluation"
msgstr "اڳين ڪٿ"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_POSITIONX\n"
"string.text"
msgid "Position X"
msgstr "آسٿانX"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_POSITIONY\n"
"string.text"
msgid "Position Y"
msgstr "آسٿانY"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_WIDTH\n"
"string.text"
msgid "Width"
msgstr ""
"#-#-#-#-#  envelp.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ويڪرِ \n"
"#-#-#-#-#  table.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ويڪرِ\n"
"#-#-#-#-#  frmdlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ويڪر \n"
"#-#-#-#-#  propctrlr.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ويڪر\n"
"#-#-#-#-#  svdraw.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ويڪر "

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_HEIGHT\n"
"string.text"
msgid "Height"
msgstr ""
"#-#-#-#-#  envelp.po (PACKAGE VERSION)  #-#-#-#-#\n"
"اوچائي \n"
"#-#-#-#-#  table.po (PACKAGE VERSION)  #-#-#-#-#\n"
"اوچائي\n"
"#-#-#-#-#  propctrlr.po (PACKAGE VERSION)  #-#-#-#-#\n"
"اوُچائي\n"
"#-#-#-#-#  svdraw.po (PACKAGE VERSION)  #-#-#-#-#\n"
"اوچائي "

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_INITIALFORMULA\n"
"string.text"
msgid "Initial value"
msgstr "شروعاتي ملهُہ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PRESERVEIRI\n"
"string.text"
msgid "Preserve as Link"
msgstr "ڪڙيءَ جئان محفوظ رکو"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_FORMULA\n"
"string.text"
msgid "Formula"
msgstr ""
"#-#-#-#-#  utlui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"نسخو\n"
"#-#-#-#-#  source.po (PACKAGE VERSION)  #-#-#-#-#\n"
"نسخو\n"
"#-#-#-#-#  misc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"نسخو\n"
"#-#-#-#-#  items.po (PACKAGE VERSION)  #-#-#-#-#\n"
"نسخو "

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DATAFIELD\n"
"string.text"
msgid "Data field"
msgstr "سامگري کيتر"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_FONT\n"
"string.text"
msgid "Font"
msgstr ""
"#-#-#-#-#  config.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ \n"
"#-#-#-#-#  dlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ\n"
"#-#-#-#-#  styleui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ\n"
"#-#-#-#-#  animations.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ\n"
"#-#-#-#-#  inc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ \n"
"#-#-#-#-#  form.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ \n"
"#-#-#-#-#  items.po (PACKAGE VERSION)  #-#-#-#-#\n"
"فانٽ"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_BACKCOLOR\n"
"string.text"
msgid "Background color"
msgstr ""
"#-#-#-#-#  config.po (PACKAGE VERSION)  #-#-#-#-#\n"
"پس منظر جو رنگ \n"
"#-#-#-#-#  propctrlr.po (PACKAGE VERSION)  #-#-#-#-#\n"
"پس منظر جو رنگ\n"
"#-#-#-#-#  accessibility.po (PACKAGE VERSION)  #-#-#-#-#\n"
"پس منظر جو رنگ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_BACKTRANSPARENT\n"
"string.text"
msgid "Background Transparent"
msgstr "پس منظر شفاف"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_CONTROLBACKGROUNDTRANSPARENT\n"
"string.text"
msgid "Background Transparent"
msgstr "پس منظر شفاف"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_OVERLAP_OTHER_CONTROL\n"
"string.text"
msgid "This operation is not allowed. The control overlaps with another one."
msgstr "هن ڪارروائيءَ جي نٿي ڏني وڃي۔ ضانطو ٻئي سان هڪ ٻئي مٿان چڙهي ٿو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_ILLEGAL_POSITION\n"
"string.text"
msgid "This position can not be set. It is invalid."
msgstr "هيءَ بيهڪ سيٽ نٿي ٿي سگهي۔ اِها نامنظور آهي"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SCOPE_GROUP\n"
"string.text"
msgid "Group: %1"
msgstr "گروپ: %1"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_FORMULALIST\n"
"string.text"
msgid "Function"
msgstr "ڪاريَہ "

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_SCOPE\n"
"string.text"
msgid "Scope"
msgstr "گنجائش"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE\n"
"string.text"
msgid "Data Field Type"
msgstr "سامگريءَ جي کيتر جو قسم"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"Field or Formula\n"
"itemlist.text"
msgid "Field or Formula"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"Function\n"
"itemlist.text"
msgid "Function"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"Counter\n"
"itemlist.text"
msgid "Counter"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_TYPE_CONST\n"
"User defined Function\n"
"itemlist.text"
msgid "User defined Function"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_MASTERFIELDS\n"
"string.text"
msgid "Link master fields"
msgstr "مکيہ کيترن کي ڳنڍيو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_DETAILFIELDS\n"
"string.text"
msgid "Link slave fields"
msgstr "ماتحت کيترن کي ڳنڍيو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_EXPLANATION\n"
"string.text"
msgid "Charts can be used to display detailed data about the current record of the report. To do this, you can specify which columns in the chart match which columns in the report."
msgstr "رپورٽ جي مؤجودهہ رڪارڊ بابت تفصيلوار سامگري ڏيکارڻ لاءِ چارٽن جو اِستعمال ڪري سگهجي ٿو۔ ائين ڪرڻ لاءِ توهين ڄاڻائي سگهو تہ رپورٽ ۾ ڪهڙا ڪالم چارٽ جي ڪهڙن ڪالمن سان ميل کائين ٿا۔"

#: inspection.src
#, fuzzy
msgctxt ""
"inspection.src\n"
"RID_STR_DETAILLABEL\n"
"string.text"
msgid "Chart"
msgstr ""
"#-#-#-#-#  appl.po (PACKAGE VERSION)  #-#-#-#-#\n"
"چارٽ\n"
"#-#-#-#-#  UI.po (PACKAGE VERSION)  #-#-#-#-#\n"
"چارٽ\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"تختي"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_MASTERLABEL\n"
"string.text"
msgid "Report"
msgstr "رپورٽ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PREVIEW_COUNT\n"
"string.text"
msgid "Preview Row(s)"
msgstr "قطار (قطارون) اَڳ منظر ۾ آڻيو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_AREA\n"
"string.text"
msgid "Area"
msgstr "کيتر"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_MIMETYPE\n"
"string.text"
msgid "Report Output Format"
msgstr "آئوٽ پٽ رچنا جي رپورٽ موڪليو"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICALALIGN\n"
"string.text"
msgid "Vert. Alignment"
msgstr "ورٽ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICAL_ALIGN_CONST\n"
"Top\n"
"itemlist.text"
msgid "Top"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICAL_ALIGN_CONST\n"
"Middle\n"
"itemlist.text"
msgid "Middle"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_VERTICAL_ALIGN_CONST\n"
"Bottom\n"
"itemlist.text"
msgid "Bottom"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST\n"
"string.text"
msgid "Horz. Alignment"
msgstr "عمودي سڌائي"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Left\n"
"itemlist.text"
msgid "Left"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Right\n"
"itemlist.text"
msgid "Right"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Block\n"
"itemlist.text"
msgid "Block"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_PARAADJUST_CONST\n"
"Center\n"
"itemlist.text"
msgid "Center"
msgstr ""

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_COUNTER\n"
"string.text"
msgid "Counter"
msgstr "ڳڻپ ڪندڙ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_ACCUMULATION\n"
"string.text"
msgid "Accumulation"
msgstr "اَمبارُ"

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_MINIMUM\n"
"string.text"
msgid "Minimum"
msgstr "گهٽ ۾ گهٽ "

#: inspection.src
msgctxt ""
"inspection.src\n"
"RID_STR_F_MAXIMUM\n"
"string.text"
msgid "Maximum"
msgstr "وڌ ۾ وڌ "
