#. extracted from scp2/source/graphicfilter
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-11-20 13:01+0100\n"
"PO-Revision-Date: 2011-04-06 14:39+0200\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT\n"
"LngText.text"
msgid "Image Filters"
msgstr ""

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT\n"
"LngText.text"
msgid "Additional filters required to read alien image formats."
msgstr ""

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_ADOBE\n"
"LngText.text"
msgid "Adobe Photoshop Import Filter"
msgstr " آمد جو فلٽر Adobe Photoshop "

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_ADOBE\n"
"LngText.text"
msgid "Adobe Photoshop Import Filter"
msgstr " آمد جو فلٽر Adobe Photoshop "

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_TGA\n"
"LngText.text"
msgid "TGA Import"
msgstr "TGAجي آمد"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_TGA\n"
"LngText.text"
msgid "TGA TrueVision TARGA Import Filter"
msgstr "TGA TrueVision TARGA جي آمد جو فلٽر"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_EPS\n"
"LngText.text"
msgid "EPS Import/Export Filter"
msgstr "EPS آمد/ روانگيءَ جو فلٽر"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_EPS\n"
"LngText.text"
msgid "Encapsulated Postscript Import/Export Filter"
msgstr "تمام مختصر پسوٽر اِسڪرپٽ آمد/ روانگي فلٽر "

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_XPM\n"
"LngText.text"
msgid "XPM Export Filter"
msgstr "XPM جي روانگيءَ جو فلٽر"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_XPM\n"
"LngText.text"
msgid "XPM Export Filter"
msgstr "XPM جي روانگيءَ جو فلٽر"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_PBMP\n"
"LngText.text"
msgid "Portable Bitmap Import/Export"
msgstr "پورٽيبل بٽمپ جي آمد/ روانگي"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_PBMP\n"
"LngText.text"
msgid "Portable Bitmap Import/Export Filters"
msgstr "پورٽيبل بٽميپ جي آمد /روانگي"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_SUNRST\n"
"LngText.text"
msgid "SUN Rasterfile Import/Export"
msgstr "SUN راسٽرفائل جي امد / روانگي"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_SUNRST\n"
"LngText.text"
msgid "SUN Rasterfile Import/Export Filters"
msgstr "SUN رسٽرفائل جي آمد / روانگيءَ جا فلٽر"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_ACAD\n"
"LngText.text"
msgid "AutoCAD Import"
msgstr " جي آمد AutoCAD خودڪار"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_ACAD\n"
"LngText.text"
msgid "AutoCAD Import Filter"
msgstr " جي آمد جو فلٽر AutoCAD خودڪار"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_KODAC\n"
"LngText.text"
msgid "Kodak Photo-CD Import"
msgstr " جي آمد جو فلٽر  CD ڪوڊڪ فوٽو"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_KODAC\n"
"LngText.text"
msgid "Kodak Photo-CD Import Filter"
msgstr " جي آمد جو فلٽر  CD ڪوڊڪ فوٽو"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_MACPICT\n"
"LngText.text"
msgid "Mac-Pict Import/Export"
msgstr " جي آمد / روانگي Mac-Pict"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_MACPICT\n"
"LngText.text"
msgid "Mac-Pict Import/Export Filters"
msgstr " جي آمد / روانگيءَ جا فلٽر  Mac-Pict"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_OS2META\n"
"LngText.text"
msgid "OS/2 Metafile Import/Export"
msgstr " ميٽا فائل جي آمد/روانگي  OS/2"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_OS2META\n"
"LngText.text"
msgid "OS/2 Metafile Import/Export Filters"
msgstr " ميٽا فائل جي آمد/ روانگيءَ جا فلٽر  OS/2"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_PCX\n"
"LngText.text"
msgid "PCX Import"
msgstr " جي آمد  PCX"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_PCX\n"
"LngText.text"
msgid "Z-Soft PCX Import"
msgstr "جي آمد   Z-Soft PCX"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_TIFF\n"
"LngText.text"
msgid "TIFF Import/Export"
msgstr "  جي آمد/ روانگي TIFF"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_TIFF\n"
"LngText.text"
msgid "TIFF Import and Export Filter"
msgstr " جي آمد ۽ روانگيءَ جو فلٽر  TIFF"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_SVG\n"
"LngText.text"
msgid "SVG Export"
msgstr " روانگي SVG"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_SVG\n"
"LngText.text"
msgid "SVG Export Filter"
msgstr " روانگيءَ جو فلٽر SVG"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_FLASH\n"
"LngText.text"
msgid "Macromedia Flash (SWF)"
msgstr "Macromedia Flash (SWF)"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_FLASH\n"
"LngText.text"
msgid "Macromedia Flash (SWF) Export Filter"
msgstr "Macromedia Flash (SWF) روانگيءَ جو فلٽر"
