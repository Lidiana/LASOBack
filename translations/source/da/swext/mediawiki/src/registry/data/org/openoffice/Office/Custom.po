#. extracted from swext/mediawiki/src/registry/data/org/openoffice/Office/Custom
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:05+0200\n"
"PO-Revision-Date: 2011-04-05 10:53+0200\n"
"Last-Translator: Leif <leiflodahl@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"GeneralSendError\n"
"value.text"
msgid "The operation 'Send to MediaWiki' could not be completed successfully."
msgstr "Operation 'Send til MediaWiki' kunne ikke færdiggøres succesfuldt."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"NoWikiFilter\n"
"value.text"
msgid "The MediaWiki export filter cannot be found. Choose 'Tools-XML Filter Settings' to install the filter, or use the setup to install the component."
msgstr "MediaWiki eksportfilter kan ikke findes. Vælg 'Funktioner-Indstillinger for XML-filter' for at installere filteret, eller brug installationsprogrammet for at installere komponenten."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"NoConnectionToURL\n"
"value.text"
msgid "A connection to the MediaWiki system at '$ARG1' could not be created."
msgstr "En forbindelse til MediaWiki Systemet ved '$ARG1' kunne ikke oprettes."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"WrongLogin\n"
"value.text"
msgid "User name or password is incorrect. Please try again, or leave the fields blank for an anonymous connection."
msgstr "Brugernavn eller adgangskode er forkert. Prøv venligst igen eller efterlad felterne tomme for en anonym forbindelse."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"InvalidURL\n"
"value.text"
msgid "A connection could not be created because the URL is invalid."
msgstr "En forbindelse kunne ikke oprettes fordi URL'en er ugyldig."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"NoURL\n"
"value.text"
msgid "Specify a MediaWiki server by providing a URL."
msgstr "Angiv en MediaWiki-server ved at angive en URL."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"CancelSending\n"
"value.text"
msgid "The transfer has been interrupted. Please check the integrity of the wiki article."
msgstr "Overførslen er blevet afbrudt. Kontroller venligst integriteten af wiki artiklen."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditSetting_UrlLabel\n"
"value.text"
msgid "U~RL"
msgstr "U~RL"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditSetting_UsernameLabel\n"
"value.text"
msgid "~Username"
msgstr "~Brugernavn"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditSetting_PasswordLabel\n"
"value.text"
msgid "~Password"
msgstr "~Adgangskode"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendTitle\n"
"value.text"
msgid "Send to MediaWiki"
msgstr "Send til MediaWiki"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_WikiArticle\n"
"value.text"
msgid "Wiki article"
msgstr "Wikiartikel"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_No\n"
"value.text"
msgid "No"
msgstr "Nej"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_OK\n"
"value.text"
msgid "OK"
msgstr "OK"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_Yes\n"
"value.text"
msgid "Yes"
msgstr "Ja"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_AddButton\n"
"value.text"
msgid "~Add..."
msgstr "Ti~lføj..."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditButton\n"
"value.text"
msgid "~Edit..."
msgstr "~Rediger..."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendButton\n"
"value.text"
msgid "~Send"
msgstr "~Send"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_RemoveButton\n"
"value.text"
msgid "~Remove"
msgstr "~Fjern"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_NewWikiPage_Label1\n"
"value.text"
msgid "A wiki article with the title '$ARG1' does not exist yet. Do you want to create a new article with that name?"
msgstr "En wikiartikel med titlen '$ARG1' findes ikke endnu. Vil du oprette en ny artikel med det navn?"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendToMediaWiki_Label1\n"
"value.text"
msgid "Media~Wiki Server"
msgstr "Media~Wiki-server"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendToMediaWiki_Label2\n"
"value.text"
msgid "~Title"
msgstr "~Titel"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendToMediaWiki_Label3\n"
"value.text"
msgid "S~ummary"
msgstr "S~ammenfatning"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendToMediaWiki_MinorCheck\n"
"value.text"
msgid "This is a ~minor edit"
msgstr "Dette er en mindre redigering"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_SendToMediaWiki_BrowserCheck\n"
"value.text"
msgid "Show in web ~browser"
msgstr "Vis i web-~browser"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"UnknownCert\n"
"value.text"
msgid "The certificate of the selected site is unknown."
msgstr "Certifikatet fra det valgte sted er ukendt."

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_MediaWiki_Title\n"
"value.text"
msgid "MediaWiki"
msgstr "MediaWiki"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditSetting_AccountLine\n"
"value.text"
msgid "Account"
msgstr "Konto"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditSetting_WikiLine\n"
"value.text"
msgid "MediaWiki Server"
msgstr "MediaWiki-server"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_EditSetting_SaveBox\n"
"value.text"
msgid "~Save password"
msgstr "~Gem adgangskode"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_MediaWiki_Extension_String\n"
"value.text"
msgid "Wiki Publisher"
msgstr "Wiki Publisher"

#: WikiExtension.xcu
msgctxt ""
"WikiExtension.xcu\n"
".WikiExtension.Strings\n"
"Dlg_WikiPageExists_Label1\n"
"value.text"
msgid "A wiki article with the title '$ARG1' already exists.&#13;&#13;Do you want to replace the current article with your article?&#13;&#13;"
msgstr "En wiki-artikel med titlen '$ARG1' findes allerede.&#13;&#13;Vil du erstatte den aktuelle artikel med din artikel?&#13;&#13;"
