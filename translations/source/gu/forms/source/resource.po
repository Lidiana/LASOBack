#. extracted from forms/source/resource
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2014-10-14 09:56+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1413280600.000000\n"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_BASELISTBOX_ERROR_FILLLIST\n"
"string.text"
msgid "The contents of a combo box or list field could not be determined."
msgstr "કોમ્બો પેટી અથવા યાદીનાં ક્ષેત્રની સમાવિષ્ટ વસ્તુઓ નક્કી કરી શકાઇ નહિ."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_IMPORT_GRAPHIC\n"
"string.text"
msgid "Insert Image"
msgstr "ઇમેજ દાખલ કરો"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONTROL_SUBSTITUTED_NAME\n"
"string.text"
msgid "substituted"
msgstr "ઉપશીર્ષક"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONTROL_SUBSTITUTED_EPXPLAIN\n"
"string.text"
msgid "An error occurred while this control was being loaded. It was therefore replaced with a placeholder."
msgstr "નિયંત્રણ લાવતી વખતે ભૂલ દેખાઇ. તેથી આ જગ્યા રાખનાર વડે તેની જગ્યા બદલાઈ હતી."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_READERROR\n"
"string.text"
msgid "Error reading data from database"
msgstr "ડેટાબેઝમાંથી માહિતી વાંચવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONNECTERROR\n"
"string.text"
msgid "Connection failed"
msgstr "જોડાણ નિષ્ફળ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_ERR_LOADING_FORM\n"
"string.text"
msgid "The data content could not be loaded."
msgstr "માહિતીના સમાવિષ્ટ ભાગો લાવી શકાયા નહિ."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_ERR_REFRESHING_FORM\n"
"string.text"
msgid "The data content could not be updated"
msgstr "માહિતીના સમાવિષ્ટ ભાગો સુધારી શકાયા નહિ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERR_INSERTRECORD\n"
"string.text"
msgid "Error inserting the new record"
msgstr "નવો રેકોર્ડ ઉમેરવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERR_UPDATERECORD\n"
"string.text"
msgid "Error updating the current record"
msgstr "વર્તમાન રેકોર્ડ સુધારવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERR_DELETERECORD\n"
"string.text"
msgid "Error deleting the current record"
msgstr "વર્તમાન રેકોર્ડ દૂર કરવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERR_DELETERECORDS\n"
"string.text"
msgid "Error deleting the specified records"
msgstr "સ્પષ્ટ થયેલ રેકોર્ડ દૂર કરવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NEED_NON_NULL_OBJECT\n"
"string.text"
msgid "The object cannot be NULL."
msgstr "વસ્તુ NULL થઈ શકી નહિ."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_OPEN_GRAPHICS\n"
"string.text"
msgid "Insert Image from..."
msgstr "તેમાંથી ઇમેજ દાખલ કરો..."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CLEAR_GRAPHICS\n"
"string.text"
msgid "Remove Image"
msgstr "ઇમેજને દૂર કરો"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INVALIDSTREAM\n"
"string.text"
msgid "The given stream is invalid."
msgstr "આપેલ સ્ટ્રીમ અયોગ્ય છે."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_SYNTAXERROR\n"
"string.text"
msgid "Syntax error in query expression"
msgstr "પ્રશ્ર્ન સમીકરણમાં બંધારણની ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INCOMPATIBLE_TYPES\n"
"string.text"
msgid "The value types supported by the binding cannot be used for exchanging data with this control."
msgstr "બાઈન્ડીંગ દ્વારા આધારભૂત કિંમત પ્રકારો આ નિયંત્રક સાથે માહિતીના બદલવા માટે વાપરી શકાય છે."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_LABEL_RECORD\n"
"string.text"
msgid "Record"
msgstr "રેકોર્ડ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INVALID_VALIDATOR\n"
"string.text"
msgid "The control is connected to an external value binding, which at the same time acts as validator. You need to revoke the value binding, before you can set a new validator."
msgstr "નિયંત્રક એ બાહ્ય કિંમત બાઈન્ડીંગ સાથે જોડાયેલ છે, કે જે એક જ સમયે ચકાસનાર તરીકે કામ કરે છે. તમારે કિંમત બાઈન્ડીંગને જગાડવાની જરૂર છે, તમે નવો ચકાસનાર સુયોજિત કરી શકો તે પહેલાં."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_LABEL_OF\n"
"string.text"
msgid "of"
msgstr "નું"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_QUERY_SAVE_MODIFIED_ROW\n"
"string.text"
msgid ""
"The content of the current form has been modified.\n"
"Do you want to save your changes?"
msgstr ""
"વર્તમાન ફોર્મના સમાવિષ્ટો સુધારાઈ ગયેલ છે.\n"
"શું તમે તમારા ફેરફારો સંગ્રહવા માંગો છો?"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULD_NOT_SET_ORDER\n"
"string.text"
msgid "Error setting the sort criteria"
msgstr "ગોઠવવાનો ક્રમ સુયોજિત કરવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULD_NOT_SET_FILTER\n"
"string.text"
msgid "Error setting the filter criteria"
msgstr "ગાળક વિચારધારા સુયોજિત કરવામાં ભૂલ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FEATURE_REQUIRES_PARAMETERS\n"
"string.text"
msgid "To execute this function, parameters are needed."
msgstr "આ વિધેય ચલાવવા માટે, પરિમાણો જરૂરી છે."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FEATURE_NOT_EXECUTABLE\n"
"string.text"
msgid "This function cannot be executed, but is only for status queries."
msgstr "આ વિધેય ચલાવી શકાશે નહિં, પરંતુ તે માત્ર પરિસ્થિતિ પ્રશ્નો માટે જ છે."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FEATURE_UNKNOWN\n"
"string.text"
msgid "Unknown function."
msgstr "અજ્ઞાત વિધેય."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_NO_BINDING_EXPRESSION\n"
"string.text"
msgid "Please enter a binding expression."
msgstr "મહેરબાની કરીને બાઈન્ડીંગ સમીકરણ દાખલ કરો."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_INVALID_BINDING_EXPRESSION\n"
"string.text"
msgid "This is an invalid binding expression."
msgstr "આ અયોગ્ય બાઈન્ડીંગ સમીકરણ છે."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_INVALID_VALUE\n"
"string.text"
msgid "Value is invalid."
msgstr "કિંમત અયોગ્ય છે."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_REQUIRED\n"
"string.text"
msgid "A value is required."
msgstr "કિંમત જરૂરી છે."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_INVALID_CONSTRAINT\n"
"string.text"
msgid "The constraint '$1' not validated."
msgstr "પરિમાણ '$1' માન્ય થયેલ નથી."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_IS_NOT_A\n"
"string.text"
msgid "The value is not of the type '$2'."
msgstr "કિંમત '$2' પ્રકારની નથી."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_MAX_INCL\n"
"string.text"
msgid "The value must be smaller than or equal to $2."
msgstr "કિંમત $2 કરતાં નાની અથવા તેના જેટલી જ હોવી જોઈએ."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_MAX_EXCL\n"
"string.text"
msgid "The value must be smaller than $2."
msgstr "કિંમત $2 કરતાં મોટી હોવી જ જોઈએ."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_MIN_INCL\n"
"string.text"
msgid "The value must be greater than or equal to $2."
msgstr "કિંમત $2 કરતાં મોટી અથવા તેના જેટલી જ હોવી જોઈએ."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_MIN_EXCL\n"
"string.text"
msgid "The value must be greater than $2."
msgstr "કિંમત $2 કરતાં મોટી હોવી જ જોઈએ."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_PATTERN\n"
"string.text"
msgid "The value does not match the pattern '$2'."
msgstr "કિંમત '$2' બંધારણને મળતી આવતી નથી."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_TOTAL_DIGITS\n"
"string.text"
msgid "$2 digits allowed at most."
msgstr "મોટા ભાગે $2 અંકો માન્ય છે."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_FRACTION_DIGITS\n"
"string.text"
msgid "$2 fraction digits allowed at most."
msgstr "મોટા ભાગે $2 અપૂર્ણાંક અંકો માન્ય છે."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_LENGTH\n"
"string.text"
msgid "The string must be $2 characters long."
msgstr "શબ્દમાળા $2 અક્ષરો જેટલી લાંબી જ હોવી જોઈએ."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_MIN_LENGTH\n"
"string.text"
msgid "The string must be at least $2 characters long."
msgstr "શબ્દમાળા ઓછામાં ઓછી $2 અક્ષરો જેટલી લાંબી હોવી જોઈએ."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_VALUE_MAX_LENGTH\n"
"string.text"
msgid "The string can only be $2 characters long at most."
msgstr "શબ્દમાળા મોટે ભાગે માત્ર $2 અક્ષરો જેટલી લાંબી હોઈ શકે."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_STRING\n"
"string.text"
msgid "String"
msgstr "શબ્દમાળા"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_URL\n"
"string.text"
msgid "Hyperlink"
msgstr "હાઇપરલિંક"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_BOOLEAN\n"
"string.text"
msgid "True/False (Boolean)"
msgstr "ખરું/ખોટું (બુલિયન)"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_DECIMAL\n"
"string.text"
msgid "Decimal"
msgstr "દશાંશ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_FLOAT\n"
"string.text"
msgid "Floating point"
msgstr "અપૂર્ણાંક બિંદુ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_DOUBLE\n"
"string.text"
msgid "Double"
msgstr "બેવડુ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_DATE\n"
"string.text"
msgid "Date"
msgstr "તારીખ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_TIME\n"
"string.text"
msgid "Time"
msgstr "સમય"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_DATETIME\n"
"string.text"
msgid "Date and Time"
msgstr "તારીખ અને સમય"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_YEARMONTH\n"
"string.text"
msgid "Month and year"
msgstr "મહિનો અને વર્ષ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_YEAR\n"
"string.text"
msgid "Year"
msgstr "વર્ષ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_MONTHDAY\n"
"string.text"
msgid "Month and day"
msgstr "મહિનો અને દિવસ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_MONTH\n"
"string.text"
msgid "Month"
msgstr "મહિનો"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_DATATYPE_DAY\n"
"string.text"
msgid "Day"
msgstr "દિવસ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_CANT_EVALUATE\n"
"string.text"
msgid "Error during evaluation"
msgstr "મૂલ્યાંકન દરમ્યાન ભૂલ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_PATTERN_DOESNT_MATCH\n"
"string.text"
msgid "The string '$1' does not match the required regular expression '$2'."
msgstr "શબ્દમાળા '$1' જરૂરી નિયમિત સમીકરણ '$2' ને મળતી આવતી નથી."

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_BINDING_UI_NAME\n"
"string.text"
msgid "Binding"
msgstr "બાઈન્ડીંગ"

#: xforms.src
msgctxt ""
"xforms.src\n"
"RID_STR_XFORMS_CANT_REMOVE_TYPE\n"
"string.text"
msgid "This is a built-in type and cannot be removed."
msgstr ""
