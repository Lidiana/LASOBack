#. extracted from svx/source/form
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-02 00:23+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: kok\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1462148591.000000\n"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_MODEL\n"
"string.text"
msgid ""
"Deleting the model '$MODELNAME' affects all controls currently bound to this model.\n"
"Do you really want to delete this model?"
msgstr ""
" '$MODELNAME' नमुनो काडून उडयल्यार ह्या नमुन्याकडेन संबंदित सगळ्या नियंत्रणांचेर परिणाम जातलो.\n"
"तुमी खरेच हो नमुनो काडून उडोवपाक सोदतात ? "

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_INSTANCE\n"
"string.text"
msgid ""
"Deleting the instance '$INSTANCENAME' affects all controls currently bound to this instance.\n"
"Do you really want to delete this instance?"
msgstr ""
" '$INSTANCENAME' देख काडून उडयल्यार ह्या देखीकडेन संबंदित सगळ्या नियंत्रणांचेर परिणाम जांव येता.\n"
"तुमी खरेच ही देख काडून उडोवपाक सोदतात? "

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_ELEMENT\n"
"string.text"
msgid ""
"Deleting the element '$ELEMENTNAME' affects all controls currently bound to this element.\n"
"Do you really want to delete this element?"
msgstr ""
" '$ELEMENTNAME' हे मूळद्रव्य काडून उडयल्यार ताचेकडेन संबंदित सगळ्या नियंत्रणांचेर परिणाम जांव येता.\n"
"तुमकां खरेंच ही मूळद्रव्यां काडून उडोवपाची आसात? "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_ATTRIBUTE\n"
"string.text"
msgid "Do you really want to delete the attribute '$ATTRIBUTENAME'?"
msgstr "तुमकां खरेच '$ATTRIBUTENAME' गुणधर्म काडून उडोवपाचे आसात?"

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_SUBMISSION\n"
"string.text"
msgid ""
"Deleting the submission '$SUBMISSIONNAME' affects all controls currently bound to this submission.\n"
"\n"
"Do you really want to delete this submission?"
msgstr ""
" '$SUBMISSIONNAME'  सादरीकरण काडून उडयल्यार ताचेकडेन संबंदित सगळ्या नियंत्रणांचेर परिणाम जांव येता.\n"
"तुमकां खरेच हे सादरीकरण काडून उडोवपाचे आसा? "

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_QRY_REMOVE_BINDING\n"
"string.text"
msgid ""
"Deleting the binding '$BINDINGNAME' affects all controls currently bound to this binding.\n"
"\n"
"Do you really want to delete this binding?"
msgstr ""
" '$BINDINGNAME' हे बायंडिंग काडून उडयले जाल्यार ताचेकडेन संबंदित सगळ्या नियंत्रणांचेर परिणाम जांव येता.\n"
"\n"
"\n"
"तुमकां खरेच हे बाइंडिंग काडून उडोवपाचे आसा?"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_INVALID_XMLNAME\n"
"string.text"
msgid "The name '%1' is not valid in XML. Please enter a different name."
msgstr "XML मजगतीचे '%1'  नाव अमान्य आसा. उपकार करून वेगळे नाव दियात."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_INVALID_XMLPREFIX\n"
"string.text"
msgid "The prefix '%1' is not valid in XML. Please enter a different prefix."
msgstr "XML मजगतीं '%1' उपसर्ग अमान्य आसा. उपकार करून दुसरो उपसर्ग घालात."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DOUBLE_MODELNAME\n"
"string.text"
msgid "The name '%1' already exists. Please enter a new name."
msgstr "नाव '%1' पयलीसावन् अस्तित्वांत आसा. उपकार करून नवे नाव दियात."

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_EMPTY_SUBMISSIONNAME\n"
"string.text"
msgid "The submission must have a name."
msgstr "नाव दिवपाकुच जाय"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_ADD\n"
"menuitem.text"
msgid "Add Item"
msgstr "वस्त जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_ADD_ELEMENT\n"
"menuitem.text"
msgid "Add Element"
msgstr "मूळद्रव्यां जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_ADD_ATTRIBUTE\n"
"menuitem.text"
msgid "Add Attribute"
msgstr " गुणधर्म जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_EDIT\n"
"menuitem.text"
msgid "Edit"
msgstr "सम्पादन"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_MENU_DATANAVIGATOR\n"
"TBI_ITEM_REMOVE\n"
"menuitem.text"
msgid "Delete"
msgstr "काडून उडयात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_METHOD_POST\n"
"string.text"
msgid "Post"
msgstr ""

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_METHOD_PUT\n"
"string.text"
msgid "Put"
msgstr ""

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_METHOD_GET\n"
"string.text"
msgid "Get"
msgstr ""

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_REPLACE_NONE\n"
"string.text"
msgid "None"
msgstr ""

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_REPLACE_INST\n"
"string.text"
msgid "Instance"
msgstr "देखी"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_REPLACE_DOC\n"
"string.text"
msgid "Document"
msgstr ""

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_PARENT\n"
"string.text"
msgid "Submission: "
msgstr "सुपूर्त: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_ID\n"
"string.text"
msgid "ID: "
msgstr "ID: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_BIND\n"
"string.text"
msgid "Binding: "
msgstr "बाइंडिंग : "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_REF\n"
"string.text"
msgid "Reference: "
msgstr "संदर्ब : "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_ACTION\n"
"string.text"
msgid "Action: "
msgstr "कृती: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_METHOD\n"
"string.text"
msgid "Method: "
msgstr "पद्दत: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_SUBM_REPLACE\n"
"string.text"
msgid "Replace: "
msgstr "बदलप: "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_ELEMENT\n"
"string.text"
msgid "Add Element"
msgstr "मूळद्रव्यां जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_ELEMENT\n"
"string.text"
msgid "Edit Element"
msgstr "मूलद्रव्य सम्पादन"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_ELEMENT\n"
"string.text"
msgid "Delete Element"
msgstr "मूलद्रव्य काडून उडयात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_ATTRIBUTE\n"
"string.text"
msgid "Add Attribute"
msgstr " गुणधर्म जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_ATTRIBUTE\n"
"string.text"
msgid "Edit Attribute"
msgstr "गुणधर्म सम्पादन"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_ATTRIBUTE\n"
"string.text"
msgid "Delete Attribute"
msgstr "गुणधर्म काडून उडयात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_BINDING\n"
"string.text"
msgid "Add Binding"
msgstr "बाइंडिंग जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_BINDING\n"
"string.text"
msgid "Edit Binding"
msgstr "बाइन्डिंग संपादन "

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_BINDING\n"
"string.text"
msgid "Delete Binding"
msgstr "बाइन्डिंग काडून उडयात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_ADD_SUBMISSION\n"
"string.text"
msgid "Add Submission"
msgstr "सादरीकरण जोडात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_EDIT_SUBMISSION\n"
"string.text"
msgid "Edit Submission"
msgstr "सादरीकरण सम्पादन"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_DATANAV_REMOVE_SUBMISSION\n"
"string.text"
msgid "Delete Submission"
msgstr "सादरीकरण काडून उडयात"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_ELEMENT\n"
"string.text"
msgid "Element"
msgstr ""

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_ATTRIBUTE\n"
"string.text"
msgid "Attribute"
msgstr ""

#: datanavi.src
#, fuzzy
msgctxt ""
"datanavi.src\n"
"RID_STR_BINDING\n"
"string.text"
msgid "Binding"
msgstr "बाइन्डिंग"

#: datanavi.src
msgctxt ""
"datanavi.src\n"
"RID_STR_BINDING_EXPR\n"
"string.text"
msgid "Binding expression"
msgstr ""

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "काडून उडयात"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_FILTER_EDIT\n"
"menuitem.text"
msgid "~Edit"
msgstr "सम्पादन"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_FILTER_IS_NULL\n"
"menuitem.text"
msgid "~Is Null"
msgstr "परिणाम शून्य"

#: filtnav.src
msgctxt ""
"filtnav.src\n"
"RID_FM_FILTER_MENU\n"
"SID_FM_FILTER_IS_NOT_NULL\n"
"menuitem.text"
msgid "I~s not Null"
msgstr "परिणाम शून्य ना"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU.SID_FM_NEW\n"
"SID_FM_NEW_FORM\n"
"menuitem.text"
msgid "Form"
msgstr "अर्जी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU.SID_FM_NEW\n"
"SID_FM_NEW_HIDDEN\n"
"menuitem.text"
msgid "Hidden Control"
msgstr "लिपिल्ले नियंत्रण"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_NEW\n"
"menuitem.text"
msgid "~New"
msgstr "नवीन"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_CHANGECONTROLTYPE\n"
"menuitem.text"
msgid "Replace with"
msgstr "पासून बदलात"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_CUT\n"
"menuitem.text"
msgid "Cu~t"
msgstr ""

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_PASTE\n"
"menuitem.text"
msgid "~Paste"
msgstr ""

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "काडून उडयात"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_TAB_DIALOG\n"
"menuitem.text"
msgid "Tab Order..."
msgstr "टॅब क्रम..."

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_RENAME_OBJECT\n"
"menuitem.text"
msgid "~Rename"
msgstr "नवे नाव दियात"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_SHOW_PROPERTY_BROWSER\n"
"menuitem.text"
msgid "Propert~ies"
msgstr "वैशिश्ट्यां"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_OPEN_READONLY\n"
"menuitem.text"
msgid "Open in Design Mode"
msgstr "आकृतीबंध स्थितींत उगडात"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMEXPLORER_POPUPMENU\n"
"SID_FM_AUTOCONTROLFOCUS\n"
"menuitem.text"
msgid "Automatic Control Focus"
msgstr "आपसूक नियंत्रण फोकस"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_EDIT\n"
"menuitem.text"
msgid "~Text Box"
msgstr "मजकूर पेटी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_BUTTON\n"
"menuitem.text"
msgid "~Button"
msgstr "कळ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_FIXEDTEXT\n"
"menuitem.text"
msgid "La~bel field"
msgstr "लेबल क्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_GROUPBOX\n"
"menuitem.text"
msgid "G~roup Box"
msgstr "गट पेटी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_LISTBOX\n"
"menuitem.text"
msgid "L~ist Box"
msgstr "वळेरी पेटी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_CHECKBOX\n"
"menuitem.text"
msgid "~Check Box"
msgstr "तपास  पेटी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_RADIOBUTTON\n"
"menuitem.text"
msgid "~Radio Button"
msgstr "रेडियो कळ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_COMBOBOX\n"
"menuitem.text"
msgid "Combo Bo~x"
msgstr "कॉम्बो पेटी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_IMAGEBUTTON\n"
"menuitem.text"
msgid "I~mage Button"
msgstr "प्रतिमा कळ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_FILECONTROL\n"
"menuitem.text"
msgid "~File Selection"
msgstr "धारिकेची निवड"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_DATE\n"
"menuitem.text"
msgid "~Date Field"
msgstr "तारीक प्रक्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_TIME\n"
"menuitem.text"
msgid "Tim~e Field"
msgstr "वेळ प्रक्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_NUMERIC\n"
"menuitem.text"
msgid "~Numerical Field"
msgstr "संख्यात्मक  प्रक्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_CURRENCY\n"
"menuitem.text"
msgid "C~urrency Field"
msgstr "चलन प्रक्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_PATTERN\n"
"menuitem.text"
msgid "~Pattern Field"
msgstr "नमुनो  प्रक्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_IMAGECONTROL\n"
"menuitem.text"
msgid "Ima~ge Control"
msgstr "प्रतिमा नियंत्रण"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_FORMATTED\n"
"menuitem.text"
msgid "Fo~rmatted Field"
msgstr "रचना प्रक्षेत्र"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_SCROLLBAR\n"
"menuitem.text"
msgid "Scroll bar"
msgstr "फिरवपाची पट्टी"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_SPINBUTTON\n"
"menuitem.text"
msgid "Spin Button"
msgstr "स्पिन कळ"

#: fmexpl.src
msgctxt ""
"fmexpl.src\n"
"RID_FMSHELL_CONVERSIONMENU\n"
"SID_FM_CONVERTTO_NAVIGATIONBAR\n"
"menuitem.text"
msgid "Navigation Bar"
msgstr "वाट दाखोवपी पट्टी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_CONTEXT_ADDFORM\n"
"string.text"
msgid "Error while creating form"
msgstr " अर्जी तयार करतास्तना त्रुटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_FIELDREQUIRED\n"
"string.text"
msgid "Input required in field '#'. Please enter a value."
msgstr "'#' या क्षेत्रांत आदान नोंद गरजेची. उपकार करून मोल दियात."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_DUPLICATE_NAME\n"
"string.text"
msgid ""
"Entry already exists.\n"
"Please choose another name."
msgstr ""
" नोंद पयलीसावन् आसा.\n"
"उपकार करून दुसरे नाव निवडात."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FORMS\n"
"string.text"
msgid "Forms"
msgstr "अर्जी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NO_PROPERTIES\n"
"string.text"
msgid "No control selected"
msgstr "नियंत्रण निवडिल्ले ना"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPERTIES_CONTROL\n"
"string.text"
msgid "Properties: "
msgstr "वैशिश्ट्यां"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPERTIES_FORM\n"
"string.text"
msgid "Form Properties"
msgstr "अर्जी वैशिश्ट्यां"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FMEXPLORER\n"
"string.text"
msgid "Form Navigator"
msgstr "अर्जी वाटदाखोवपी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FORM\n"
"string.text"
msgid "Form"
msgstr "अर्जी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_TABWIN_PREFIX\n"
"Table\n"
"itemlist.text"
msgid "Table"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_TABWIN_PREFIX\n"
"Query\n"
"itemlist.text"
msgid "Query"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_TABWIN_PREFIX\n"
"SQL\n"
"itemlist.text"
msgid "SQL"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_STDFORMNAME\n"
"string.text"
msgid "Form"
msgstr "अर्जी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_HIDDEN\n"
"string.text"
msgid "Hidden Control"
msgstr "लिपिल्ले नियंत्रण"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_CONTROL\n"
"string.text"
msgid "Control"
msgstr "नियन्त्रण"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_REC_TEXT\n"
"string.text"
msgid "Record"
msgstr "अभिलेख"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_REC_FROM_TEXT\n"
"string.text"
msgid "of"
msgstr "चो"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FIELDSELECTION\n"
"string.text"
msgid "Add field:"
msgstr "प्रक्षेत्र जोडात..."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_WRITEERROR\n"
"string.text"
msgid "Error writing data to database"
msgstr "म्हायतीकोशांत म्हायती  बरयतास्तना त्रुटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SYNTAXERROR\n"
"string.text"
msgid "Syntax error in query expression"
msgstr "शंका अभिव्यक्तींत व्याकरणाची त्रुटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DELETECONFIRM_RECORD\n"
"string.text"
msgid "You intend to delete 1 record."
msgstr "तुमीं अभिलेख १ काडून उडोवपाक सोदता."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DELETECONFIRM_RECORDS\n"
"string.text"
msgid "# records will be deleted."
msgstr "  # अभिलेख काडून उडयले वतले."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DELETECONFIRM\n"
"string.text"
msgid ""
"If you click Yes, you won't be able to undo this operation.\n"
"Do you want to continue anyway?"
msgstr ""
"तुमीं  जर हंय क्लिक केले, जाल्यार ही कारवाय रद्द करपाक शकचे ना!\n"
"तरीय तुमी चालू दवरुपाक सोदता?"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_ERR_NO_ELEMENT\n"
"string.text"
msgid "Choose an entry from the list or enter a text corresponding to one of the list items."
msgstr "वळेरेतल्यान एक नोंद निवडात वा वळेरेतल्या एका वस्तीकडेन संबंदित मजकूर भरात."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_GROUPBOX\n"
"string.text"
msgid "Frame element"
msgstr "चौकट मूलद्रव्यां"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NAVIGATION\n"
"string.text"
msgid "Navigation"
msgstr "मार्गक्रमण"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NAVIGATIONBAR\n"
"string.text"
msgid "Navigation bar"
msgstr "मार्गक्रमण पट्टी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_COLUMN\n"
"string.text"
msgid "Col"
msgstr "कॉ."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_PROPERTY\n"
"string.text"
msgid "Set property '#'"
msgstr " '#' वैशिश्ट्य स्थापित करात"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_INSERT\n"
"string.text"
msgid "Insert in container"
msgstr "डब्यान घालात"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_REMOVE\n"
"string.text"
msgid "Delete #"
msgstr "काडून उडयात  #"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_REMOVE_MULTIPLE\n"
"string.text"
msgid "Delete # objects"
msgstr "# वस्ती काडून उडयात"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_CONTAINER_REPLACE\n"
"string.text"
msgid "Replace a container element"
msgstr "डब्यातली मूलद्रव्यां बदलात"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_DELETE_LOGICAL\n"
"string.text"
msgid "Delete structure"
msgstr "संरचनेक काडून उडयात"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_UNDO_MODEL_REPLACE\n"
"string.text"
msgid "Replace Control"
msgstr "नियंत्रण बदलात"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DATE\n"
"string.text"
msgid "Date"
msgstr "तारीक"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_TIME\n"
"string.text"
msgid "Time"
msgstr "वेळ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_PUSHBUTTON\n"
"string.text"
msgid "Push Button"
msgstr "कळ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_RADIOBUTTON\n"
"string.text"
msgid "Option Button"
msgstr "पर्याय कळ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_CHECKBOX\n"
"string.text"
msgid "Check Box"
msgstr "तपास पेटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_FIXEDTEXT\n"
"string.text"
msgid "Label Field"
msgstr "लेबल प्रक्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_GROUPBOX\n"
"string.text"
msgid "Group Box"
msgstr "गट पेटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_EDIT\n"
"string.text"
msgid "Text Box"
msgstr "मजकूर पेटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_FORMATTED\n"
"string.text"
msgid "Formatted Field"
msgstr "रचिल्ले प्रक्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_LISTBOX\n"
"string.text"
msgid "List Box"
msgstr "वळेरी पेटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_COMBOBOX\n"
"string.text"
msgid "Combo Box"
msgstr "कॉम्बो पेटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_IMAGEBUTTON\n"
"string.text"
msgid "Image Button"
msgstr "प्रतिमा कळ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_IMAGECONTROL\n"
"string.text"
msgid "Image Control"
msgstr "प्रतिमा नियंत्रण"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_FILECONTROL\n"
"string.text"
msgid "File Selection"
msgstr "धारिकेची निवड"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_DATEFIELD\n"
"string.text"
msgid "Date Field"
msgstr "तारीक प्रक्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_TIMEFIELD\n"
"string.text"
msgid "Time Field"
msgstr "वेळ प्रक्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_NUMERICFIELD\n"
"string.text"
msgid "Numeric Field"
msgstr "क्रमांक  क्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_CURRENCYFIELD\n"
"string.text"
msgid "Currency Field"
msgstr "चलन प्रक्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_PATTERNFIELD\n"
"string.text"
msgid "Pattern Field"
msgstr "नमुनो प्रक्षेत्र"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_DBGRID\n"
"string.text"
msgid "Table Control "
msgstr "कोष्टक नियंत्रण"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_SCROLLBAR\n"
"string.text"
msgid "Scrollbar"
msgstr "हालती पट्टी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_SPINBUTTON\n"
"string.text"
msgid "Spin Button"
msgstr "स्पिन कळ"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_NAVBAR\n"
"string.text"
msgid "Navigation Bar"
msgstr "वाट दाखोवपी पट्टी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_PROPTITLE_MULTISELECT\n"
"string.text"
msgid "Multiselection"
msgstr "खूप निवड"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NODATACONTROLS\n"
"string.text"
msgid "No data-related controls in the current form!"
msgstr " चालू अर्जींत आकडेवारीकडेन संबंदित नियंत्रणां ना!"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_POSTFIX_DATE\n"
"string.text"
msgid " (Date)"
msgstr " (तारीक)"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_POSTFIX_TIME\n"
"string.text"
msgid " (Time)"
msgstr "(वेळ)"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FILTER_NAVIGATOR\n"
"string.text"
msgid "Filter navigator"
msgstr "गाळणी संचालन"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FILTER_FILTER_FOR\n"
"string.text"
msgid "Filter for"
msgstr "खातीर गाळणी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_FILTER_FILTER_OR\n"
"string.text"
msgid "Or"
msgstr "वा"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_NOCONTROLS_FOR_EXTERNALDISPLAY\n"
"string.text"
msgid "Valid bound controls which can be used in the table view do not exist in the current form."
msgstr "वैध नियंत्रणां जी कोष्टक दृश्यांत वापरूं  येतात ती चालू अर्जींत ना."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_AUTOFIELD\n"
"string.text"
msgid "<AutoField>"
msgstr "<आपसूक प्रक्षेत्र>"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"LIKE\n"
"itemlist.text"
msgid "LIKE"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"NOT\n"
"itemlist.text"
msgid "NOT"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"EMPTY\n"
"itemlist.text"
msgid "EMPTY"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"TRUE\n"
"itemlist.text"
msgid "TRUE"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"FALSE\n"
"itemlist.text"
msgid "FALSE"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"IS\n"
"itemlist.text"
msgid "IS"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"BETWEEN\n"
"itemlist.text"
msgid "BETWEEN"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"OR\n"
"itemlist.text"
msgid "OR"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"AND\n"
"itemlist.text"
msgid "AND"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Average\n"
"itemlist.text"
msgid "Average"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Count\n"
"itemlist.text"
msgid "Count"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Maximum\n"
"itemlist.text"
msgid "Maximum"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Minimum\n"
"itemlist.text"
msgid "Minimum"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Sum\n"
"itemlist.text"
msgid "Sum"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Every\n"
"itemlist.text"
msgid "Every"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Any\n"
"itemlist.text"
msgid "Any"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Some\n"
"itemlist.text"
msgid "Some"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"STDDEV_POP\n"
"itemlist.text"
msgid "STDDEV_POP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"STDDEV_SAMP\n"
"itemlist.text"
msgid "STDDEV_SAMP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"VAR_SAMP\n"
"itemlist.text"
msgid "VAR_SAMP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"VAR_POP\n"
"itemlist.text"
msgid "VAR_POP"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Collect\n"
"itemlist.text"
msgid "Collect"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Fusion\n"
"itemlist.text"
msgid "Fusion"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_RSC_SQL_INTERNATIONAL\n"
"Intersection\n"
"itemlist.text"
msgid "Intersection"
msgstr ""

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_ERROR\n"
"string.text"
msgid "Syntax error in SQL statement"
msgstr "SQL निवेदनांत व्याकरण त्रुटी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_VALUE_NO_LIKE\n"
"string.text"
msgid "The value #1 cannot be used with LIKE."
msgstr "LIKE सयत् मोल #1 वापरूंक शकना!"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_FIELD_NO_LIKE\n"
"string.text"
msgid "LIKE cannot be used with this field."
msgstr "ह्या प्रक्षेत्रासयत LIKE वापर करूंक शकना. "

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_ACCESS_DAT_NO_VALID\n"
"string.text"
msgid "The value entered is not a valid date. Please enter a date in a valid format, for example, MM/DD/YY."
msgstr "घातिल्ले मोल वैध तारीक न्ही. उपकार करून वैध स्थितींत तारीक घालात. देखीक  मम/दिदि/वव."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_INT_NO_VALID\n"
"string.text"
msgid "The field cannot be compared with an integer."
msgstr "प्रक्षेत्राची  इंटिजराकडेन तुळा जावपाक शकना."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_TABLE\n"
"string.text"
msgid "The database does not contain a table named \"#\"."
msgstr "म्हायतीकोशांत \"#\" नावाचे कोष्टक ना."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_TABLE_OR_QUERY\n"
"string.text"
msgid "The database does contain neither a table nor a query named \"#\"."
msgstr "म्हायतीकोशांत \"#\" नावाचे कोष्टक वा प्रस्न ना."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_TABLE_EXISTS\n"
"string.text"
msgid "The database already contains a table or view with name \"#\"."
msgstr "म्हायतीकोशांत  \"#\" नावाचे कोष्टक वा दृश्य पयलीसावन आसा."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_QUERY_EXISTS\n"
"string.text"
msgid "The database already contains a query with name \"#\"."
msgstr "म्हायतीकोशांत  \"#\" नावाचो प्रस्न पयलीसावन आसा."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_COLUMN\n"
"string.text"
msgid "The column \"#1\" is unknown in the table \"#2\"."
msgstr "कोष्टक \"#\"मजगती स्तंभ \"#\" खबर ना!"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_REAL_NO_VALID\n"
"string.text"
msgid "The field cannot be compared with a floating point number."
msgstr "प्रक्षेत्राची उफेत्या बिंदू क्रमांकाकडेन तुळा जावपाक शकना."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_SVT_SQL_SYNTAX_CRIT_NO_COMPARE\n"
"string.text"
msgid "The entered criterion cannot be compared with this field."
msgstr "दिल्ल्या निकशांची ह्या प्रक्षेत्राकडेन तुळा जावपाक शकना."

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_DATANAVIGATOR\n"
"string.text"
msgid "Data Navigator"
msgstr "म्हायती वाटदाखोवपी"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_READONLY_VIEW\n"
"string.text"
msgid " (read-only)"
msgstr " (फकत वाचपाखातीर)"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_ALREADYEXISTOVERWRITE\n"
"string.text"
msgid "The file already exists. Overwrite?"
msgstr "धारिका पयलीसावन आसा. वयर बरोवया?"

#: fmstring.src
msgctxt ""
"fmstring.src\n"
"RID_STR_OBJECT_LABEL\n"
"string.text"
msgid "#object# label"
msgstr "#वास्तीचें# नांव"
