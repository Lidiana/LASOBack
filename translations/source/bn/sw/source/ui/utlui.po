#. extracted from sw/source/ui/utlui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-25 12:11+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: Bengali <ankur-bd-l10n@googlegroups.com>\n"
"Language: bn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-Language: bn_BD\n"
"X-POOTLE-MTIME: 1464178284.000000\n"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_FOOTNOTE\n"
"string.text"
msgid "Footnote Characters"
msgstr "পাদটীকার অক্ষর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_PAGENO\n"
"string.text"
msgid "Page Number"
msgstr "পৃষ্ঠা নম্বর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_LABEL\n"
"string.text"
msgid "Caption Characters"
msgstr "ক্যাপশন অক্ষর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_DROPCAPS\n"
"string.text"
msgid "Drop Caps"
msgstr "ড্রপ ক্যাপ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_NUM_LEVEL\n"
"string.text"
msgid "Numbering Symbols"
msgstr "সংখ্যায়ন প্রতীক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_BUL_LEVEL\n"
"string.text"
msgid "Bullets"
msgstr "বুলেট"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_INET_NORMAL\n"
"string.text"
msgid "Internet Link"
msgstr "ইন্টারনেট লিংক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_INET_VISIT\n"
"string.text"
msgid "Visited Internet Link"
msgstr "পরিদর্শিত ইন্টারনেট লিংক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_JUMPEDIT\n"
"string.text"
msgid "Placeholder"
msgstr "স্থানধারক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_TOXJUMP\n"
"string.text"
msgid "Index Link"
msgstr "সূচির লিংক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_ENDNOTE\n"
"string.text"
msgid "Endnote Characters"
msgstr "প্রান্তটীকার অক্ষর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_LANDSCAPE\n"
"string.text"
msgid "Landscape"
msgstr "আড়াআড়ি"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_LINENUM\n"
"string.text"
msgid "Line Numbering"
msgstr "লাইন সংখ্যায়ন"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_IDX_MAIN_ENTRY\n"
"string.text"
msgid "Main Index Entry"
msgstr "প্রধান সূচির ভুক্তি"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_FOOTNOTE_ANCHOR\n"
"string.text"
msgid "Footnote Anchor"
msgstr "পাদটীকার নোঙ্গর"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_ENDNOTE_ANCHOR\n"
"string.text"
msgid "Endnote Anchor"
msgstr "প্রান্তটীকার নোঙ্গর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_EMPHASIS\n"
"string.text"
msgid "Emphasis"
msgstr "গুরুত্ব"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_CITIATION\n"
"string.text"
msgid "Quotation"
msgstr "উদ্ধৃতি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_STRONG\n"
"string.text"
msgid "Strong Emphasis"
msgstr "বিশেষ গুরুত্ব"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_CODE\n"
"string.text"
msgid "Source Text"
msgstr "সোর্স লেখা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_SAMPLE\n"
"string.text"
msgid "Example"
msgstr "উদাহরণ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_KEYBOARD\n"
"string.text"
msgid "User Entry"
msgstr "ব্যবহারকারী ভুক্তি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_VARIABLE\n"
"string.text"
msgid "Variable"
msgstr "চলক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_DEFINSTANCE\n"
"string.text"
msgid "Definition"
msgstr "সংজ্ঞা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_HTML_TELETYPE\n"
"string.text"
msgid "Teletype"
msgstr "টেলিটাইপ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_FRAME\n"
"string.text"
msgid "Frame"
msgstr "ফ্রেম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_GRAPHIC\n"
"string.text"
msgid "Graphics"
msgstr "গ্রাফিক্স"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_OLE\n"
"string.text"
msgid "OLE"
msgstr "OLE"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_FORMEL\n"
"string.text"
msgid "Formula"
msgstr "সূত্র"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_MARGINAL\n"
"string.text"
msgid "Marginalia"
msgstr "মার্জিনালিয়া"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_WATERSIGN\n"
"string.text"
msgid "Watermark"
msgstr "জলছাপ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLFRM_LABEL\n"
"string.text"
msgid "Labels"
msgstr "স্তর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_STANDARD\n"
"string.text"
msgid "Default Style"
msgstr ""

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT\n"
"string.text"
msgid "Text Body"
msgstr "লেখা বডি"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_IDENT\n"
"string.text"
msgid "First Line Indent"
msgstr "প্রথম লাইন ইন্ডেন্ট"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_NEGIDENT\n"
"string.text"
msgid "Hanging Indent"
msgstr "ঝুলন্ত ইন্ডেন্ট"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TEXT_MOVE\n"
"string.text"
msgid "Text Body Indent"
msgstr "লেখা বডি ইন্ডেন্ট"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_GREETING\n"
"string.text"
msgid "Complimentary Close"
msgstr "শ্রদ্ধাজ্ঞাপক সমাপ্তি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_SIGNATURE\n"
"string.text"
msgid "Signature"
msgstr "স্বাক্ষর"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE_BASE\n"
"string.text"
msgid "Heading"
msgstr "শিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUMBUL_BASE\n"
"string.text"
msgid "List"
msgstr "তালিকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_REGISTER_BASE\n"
"string.text"
msgid "Index"
msgstr "সূচি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_CONFRONTATION\n"
"string.text"
msgid "List Indent"
msgstr "তালিকা ইন্ডেন্ট"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_MARGINAL\n"
"string.text"
msgid "Marginalia"
msgstr "মার্জিনালিয়া"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE1\n"
"string.text"
msgid "Heading 1"
msgstr "শিরোনাম ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE2\n"
"string.text"
msgid "Heading 2"
msgstr "শিরোনাম ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE3\n"
"string.text"
msgid "Heading 3"
msgstr "শিরোনাম ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE4\n"
"string.text"
msgid "Heading 4"
msgstr "শিরোনাম ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE5\n"
"string.text"
msgid "Heading 5"
msgstr "শিরোনাম ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE6\n"
"string.text"
msgid "Heading 6"
msgstr "শিরোনাম ৬"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE7\n"
"string.text"
msgid "Heading 7"
msgstr "শিরোনাম ৭"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE8\n"
"string.text"
msgid "Heading 8"
msgstr "শিরোনাম ৮"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE9\n"
"string.text"
msgid "Heading 9"
msgstr "শিরোনাম ৯"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADLINE10\n"
"string.text"
msgid "Heading 10"
msgstr "শিরোনাম ১০"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1S\n"
"string.text"
msgid "Numbering 1 Start"
msgstr "সংখ্যায়ন ১ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1\n"
"string.text"
msgid "Numbering 1"
msgstr "সংখ্যায়ন ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL1E\n"
"string.text"
msgid "Numbering 1 End"
msgstr "সংখ্যায়ন ১ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM1\n"
"string.text"
msgid "Numbering 1 Cont."
msgstr "সংখ্যায়ন ১ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2S\n"
"string.text"
msgid "Numbering 2 Start"
msgstr "সংখ্যায়ন ২ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2\n"
"string.text"
msgid "Numbering 2"
msgstr "সংখ্যায়ন ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL2E\n"
"string.text"
msgid "Numbering 2 End"
msgstr "সংখ্যায়ন ২ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM2\n"
"string.text"
msgid "Numbering 2 Cont."
msgstr "সংখ্যায়ন ২ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3S\n"
"string.text"
msgid "Numbering 3 Start"
msgstr "সংখ্যায়ন ৩ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3\n"
"string.text"
msgid "Numbering 3"
msgstr "সংখ্যায়ন ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL3E\n"
"string.text"
msgid "Numbering 3 End"
msgstr "সংখ্যায়ন ৩ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM3\n"
"string.text"
msgid "Numbering 3 Cont."
msgstr "সংখ্যায়ন ৩ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4S\n"
"string.text"
msgid "Numbering 4 Start"
msgstr "সংখ্যায়ন ৪ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4\n"
"string.text"
msgid "Numbering 4"
msgstr "সংখ্যায়ন ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL4E\n"
"string.text"
msgid "Numbering 4 End"
msgstr "সংখ্যায়ন ৪ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM4\n"
"string.text"
msgid "Numbering 4 Cont."
msgstr "সংখ্যায়ন ৪ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5S\n"
"string.text"
msgid "Numbering 5 Start"
msgstr "সংখ্যায়ন ৫ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5\n"
"string.text"
msgid "Numbering 5"
msgstr "সংখ্যায়ন ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_LEVEL5E\n"
"string.text"
msgid "Numbering 5 End"
msgstr "সংখ্যায়ন ৫ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_NUM_NONUM5\n"
"string.text"
msgid "Numbering 5 Cont."
msgstr "সংখ্যায়ন ৫ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1S\n"
"string.text"
msgid "List 1 Start"
msgstr "তালিকা ১ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1\n"
"string.text"
msgid "List 1"
msgstr "তালিকা ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL1E\n"
"string.text"
msgid "List 1 End"
msgstr "তালিকা ১ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM1\n"
"string.text"
msgid "List 1 Cont."
msgstr "তালিকা ১ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2S\n"
"string.text"
msgid "List 2 Start"
msgstr "তালিকা ২ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2\n"
"string.text"
msgid "List 2"
msgstr "তালিকা ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL2E\n"
"string.text"
msgid "List 2 End"
msgstr "তালিকা ২ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM2\n"
"string.text"
msgid "List 2 Cont."
msgstr "তালিকা ২ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3S\n"
"string.text"
msgid "List 3 Start"
msgstr "তালিকা ৩ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3\n"
"string.text"
msgid "List 3"
msgstr "তালিকা ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL3E\n"
"string.text"
msgid "List 3 End"
msgstr "তালিকা ৩ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM3\n"
"string.text"
msgid "List 3 Cont."
msgstr "তালিকা ৩ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4S\n"
"string.text"
msgid "List 4 Start"
msgstr "তালিকা ৪ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4\n"
"string.text"
msgid "List 4"
msgstr "তালিকা ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL4E\n"
"string.text"
msgid "List 4 End"
msgstr "তালিকা ৪ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM4\n"
"string.text"
msgid "List 4 Cont."
msgstr "তালিকা ৪ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5S\n"
"string.text"
msgid "List 5 Start"
msgstr "তালিকা ৫ শুরু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5\n"
"string.text"
msgid "List 5"
msgstr "তালিকা ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_LEVEL5E\n"
"string.text"
msgid "List 5 End"
msgstr "তালিকা ৫ শেষ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_BUL_NONUM5\n"
"string.text"
msgid "List 5 Cont."
msgstr "তালিকা ৫ চলছে"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADER\n"
"string.text"
msgid "Header"
msgstr "শীর্ষচরণ"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADERL\n"
"string.text"
msgid "Header Left"
msgstr "বাম শীর্ষচরণ"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HEADERR\n"
"string.text"
msgid "Header Right"
msgstr "ডান শীর্ষচরণ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTER\n"
"string.text"
msgid "Footer"
msgstr "পাদচরণ"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTERL\n"
"string.text"
msgid "Footer Left"
msgstr "বাম পাদচরণ"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTERR\n"
"string.text"
msgid "Footer Right"
msgstr "ডান পাদচরণ"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TABLE\n"
"string.text"
msgid "Table Contents"
msgstr "সারণির বিষয়বস্তু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TABLE_HDLN\n"
"string.text"
msgid "Table Heading"
msgstr "সারণির শিরোনাম"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FRAME\n"
"string.text"
msgid "Frame Contents"
msgstr "ফ্রেমের বিষয়বস্তু"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_FOOTNOTE\n"
"string.text"
msgid "Footnote"
msgstr "পাদটীকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_ENDNOTE\n"
"string.text"
msgid "Endnote"
msgstr "প্রান্তটীকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL\n"
"string.text"
msgid "Caption"
msgstr "ক্যাপশন"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_ABB\n"
"string.text"
msgid "Illustration"
msgstr "চিত্র"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_TABLE\n"
"string.text"
msgid "Table"
msgstr "সারণি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_FRAME\n"
"string.text"
msgid "Text"
msgstr "লেখা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_LABEL_DRAWING\n"
"string.text"
msgid "Drawing"
msgstr "অঙ্কন"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_JAKETADRESS\n"
"string.text"
msgid "Addressee"
msgstr "প্রাপক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_SENDADRESS\n"
"string.text"
msgid "Sender"
msgstr "প্রেরক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDXH\n"
"string.text"
msgid "Index Heading"
msgstr "সূচির শিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX1\n"
"string.text"
msgid "Index 1"
msgstr "সূচি ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX2\n"
"string.text"
msgid "Index 2"
msgstr "সূচি ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDX3\n"
"string.text"
msgid "Index 3"
msgstr "সূচি ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_IDXBREAK\n"
"string.text"
msgid "Index Separator"
msgstr "সূচি বিভাজক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNTH\n"
"string.text"
msgid "Contents Heading"
msgstr "বিষয়বস্তুর শিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT1\n"
"string.text"
msgid "Contents 1"
msgstr "বিষয়বস্তু ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT2\n"
"string.text"
msgid "Contents 2"
msgstr "বিষয়বস্তু ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT3\n"
"string.text"
msgid "Contents 3"
msgstr "বিষয়বস্তু ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT4\n"
"string.text"
msgid "Contents 4"
msgstr "বিষয়বস্তু ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT5\n"
"string.text"
msgid "Contents 5"
msgstr "বিষয়বস্তু ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT6\n"
"string.text"
msgid "Contents 6"
msgstr "বিষয়বস্তু ৬"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT7\n"
"string.text"
msgid "Contents 7"
msgstr "বিষয়বস্তু ৭"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT8\n"
"string.text"
msgid "Contents 8"
msgstr "বিষয়বস্তু ৮"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT9\n"
"string.text"
msgid "Contents 9"
msgstr "বিষয়বস্তু ৯"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CNTNT10\n"
"string.text"
msgid "Contents 10"
msgstr "বিষয়বস্তু ১০"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USERH\n"
"string.text"
msgid "User Index Heading"
msgstr "ব্যবহারকারী সূচির শিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER1\n"
"string.text"
msgid "User Index 1"
msgstr "ব্যবহারকারী সূচি ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER2\n"
"string.text"
msgid "User Index 2"
msgstr "ব্যবহারকারী সূচি ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER3\n"
"string.text"
msgid "User Index 3"
msgstr "ব্যবহারকারী সূচি ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER4\n"
"string.text"
msgid "User Index 4"
msgstr "ব্যবহারকারী সূচি ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER5\n"
"string.text"
msgid "User Index 5"
msgstr "ব্যবহারকারী সূচি ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER6\n"
"string.text"
msgid "User Index 6"
msgstr "ব্যবহারকারী সূচি ৬"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER7\n"
"string.text"
msgid "User Index 7"
msgstr "ব্যবহারকারী সূচি ৭"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER8\n"
"string.text"
msgid "User Index 8"
msgstr "ব্যবহারকারী সূচি ৮"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER9\n"
"string.text"
msgid "User Index 9"
msgstr "ব্যবহারকারী সূচি ৯"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_USER10\n"
"string.text"
msgid "User Index 10"
msgstr "ব্যবহারকারী সূচি ১০"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_CITATION\n"
"string.text"
msgid "Citation"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_ILLUSH\n"
"string.text"
msgid "Illustration Index Heading"
msgstr "চিত্র সূচির শিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_ILLUS1\n"
"string.text"
msgid "Illustration Index 1"
msgstr "চিত্র সূচি ১"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_OBJECTH\n"
"string.text"
msgid "Object Index Heading"
msgstr "বস্তু সূচির শিরোনাম"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_OBJECT1\n"
"string.text"
msgid "Object Index 1"
msgstr "বস্তু সূচি ১"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_TABLESH\n"
"string.text"
msgid "Table Index Heading"
msgstr "সারণি সূচির শিরোনাম"

#: poolfmt.src
#, fuzzy
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_TABLES1\n"
"string.text"
msgid "Table Index 1"
msgstr "সারণি সূচি ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_AUTHORITIESH\n"
"string.text"
msgid "Bibliography Heading"
msgstr "তথ্যসূত্র শিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_TOX_AUTHORITIES1\n"
"string.text"
msgid "Bibliography 1"
msgstr "তথ্যসূত্র ১"

#. Document title style, not to be confused with Heading style
#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_DOC_TITEL\n"
"string.text"
msgid "Title"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_DOC_SUBTITEL\n"
"string.text"
msgid "Subtitle"
msgstr "উপশিরোনাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_BLOCKQUOTE\n"
"string.text"
msgid "Quotations"
msgstr "উদ্ধৃতি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_PRE\n"
"string.text"
msgid "Preformatted Text"
msgstr "পূর্ববিন্যাসিত লেখা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_HR\n"
"string.text"
msgid "Horizontal Line"
msgstr "অনুভূমিক লাইন"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_DD\n"
"string.text"
msgid "List Contents"
msgstr "বিষয়বস্তুর তালিকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCOLL_HTML_DT\n"
"string.text"
msgid "List Heading"
msgstr "শিরোনামের তালিকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_STANDARD\n"
"string.text"
msgid "Default Style"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_FIRST\n"
"string.text"
msgid "First Page"
msgstr "প্রথম পৃষ্ঠা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_LEFT\n"
"string.text"
msgid "Left Page"
msgstr "বাম পৃষ্ঠা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_RIGHT\n"
"string.text"
msgid "Right Page"
msgstr "ডান পৃষ্ঠা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_JAKET\n"
"string.text"
msgid "Envelope"
msgstr "খাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_REGISTER\n"
"string.text"
msgid "Index"
msgstr "সূচি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_HTML\n"
"string.text"
msgid "HTML"
msgstr "HTML"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_FOOTNOTE\n"
"string.text"
msgid "Footnote"
msgstr "পাদটীকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLPAGE_ENDNOTE\n"
"string.text"
msgid "Endnote"
msgstr "প্রান্তটীকা"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM1\n"
"string.text"
msgid "Numbering 1"
msgstr "সংখ্যায়ন ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM2\n"
"string.text"
msgid "Numbering 2"
msgstr "সংখ্যায়ন ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM3\n"
"string.text"
msgid "Numbering 3"
msgstr "সংখ্যায়ন ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM4\n"
"string.text"
msgid "Numbering 4"
msgstr "সংখ্যায়ন ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_NUM5\n"
"string.text"
msgid "Numbering 5"
msgstr "সংখ্যায়ন ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL1\n"
"string.text"
msgid "List 1"
msgstr "তালিকা ১"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL2\n"
"string.text"
msgid "List 2"
msgstr "তালিকা ২"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL3\n"
"string.text"
msgid "List 3"
msgstr "তালিকা ৩"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL4\n"
"string.text"
msgid "List 4"
msgstr "তালিকা ৪"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLNUMRULE_BUL5\n"
"string.text"
msgid "List 5"
msgstr "তালিকা ৫"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_RUBYTEXT\n"
"string.text"
msgid "Rubies"
msgstr "রুবি"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM0\n"
"string.text"
msgid "1 column"
msgstr "কলাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM1\n"
"string.text"
msgid "2 columns with equal size"
msgstr "সমান আকার সম্বলিত ২ কলাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM2\n"
"string.text"
msgid "3 columns with equal size"
msgstr "সমান আকার সম্বলিত ৩ কলাম"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM3\n"
"string.text"
msgid "2 columns with different size (left > right)"
msgstr "ভিন্ন আকার সম্বলিত ২ কলাম (বাম > ডান)"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_COLUMN_VALUESET_ITEM4\n"
"string.text"
msgid "2 columns with different size (left < right)"
msgstr "ভিন্ন আকার সম্বলিত ২ কলাম (বাম < ডান)"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_POOLCHR_VERT_NUM\n"
"string.text"
msgid "Vertical Numbering Symbols"
msgstr "উল্লম্ব সংখ্যায়ন প্রতীক"

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_DEFAULT\n"
"string.text"
msgid "Default Style"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_3D\n"
"string.text"
msgid "3D"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLACK1\n"
"string.text"
msgid "Black 1"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLACK2\n"
"string.text"
msgid "Black 2"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BLUE\n"
"string.text"
msgid "Blue"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_BROWN\n"
"string.text"
msgid "Brown"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY\n"
"string.text"
msgid "Currency"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_3D\n"
"string.text"
msgid "Currency 3D"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_GRAY\n"
"string.text"
msgid "Currency Gray"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_LAVENDER\n"
"string.text"
msgid "Currency Lavender"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_CURRENCY_TURQUOISE\n"
"string.text"
msgid "Currency Turquoise"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_GRAY\n"
"string.text"
msgid "Gray"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_GREEN\n"
"string.text"
msgid "Green"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_LAVENDER\n"
"string.text"
msgid "Lavender"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_RED\n"
"string.text"
msgid "Red"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_TURQUOISE\n"
"string.text"
msgid "Turquoise"
msgstr ""

#: poolfmt.src
msgctxt ""
"poolfmt.src\n"
"STR_TABSTYLE_YELLOW\n"
"string.text"
msgid "Yellow"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DEL_EMPTY_PARA+1\n"
"string.text"
msgid "Remove empty paragraphs"
msgstr "ফাঁকা অনুচ্ছেদ অপসারণ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_USE_REPLACE+1\n"
"string.text"
msgid "Use replacement table"
msgstr "প্রতিস্থাপিত সারণির ব্যবহার"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_CPTL_STT_WORD+1\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr "প্রারম্ভিক দুইটি বড় হাতের অক্ষর ঠিক করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_CPTL_STT_SENT+1\n"
"string.text"
msgid "Capitalize first letter of sentences"
msgstr "বাক্যের প্রথম বর্ণ বড় হাতের বর্ণে রূপান্তর"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_TYPO+1\n"
"string.text"
msgid "Replace \"standard\" quotes with %1 \\bcustom%2 quotes"
msgstr "\"standard\" উদ্ধ্বৃতি %1 \\\\bcustom%2 উদ্ধ্বৃতি দ্বারা প্রতিস্থাপন করুন"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_USER_STYLE+1\n"
"string.text"
msgid "Replace Custom Styles"
msgstr "স্বনির্বাচিত শৈলীর প্রতিস্থাপন"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_BULLET+1\n"
"string.text"
msgid "Bullets replaced"
msgstr "বুলেট প্রতিস্থাপিত"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_UNDER+1\n"
"string.text"
msgid "Automatic _underline_"
msgstr "স্বয়ংক্রিয় _নিম্নরেখা_"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_BOLD+1\n"
"string.text"
msgid "Automatic *bold*"
msgstr "স্বয়ংক্রিয় *গাঢ়*"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_FRACTION+1\n"
"string.text"
msgid "Replace 1/2 ... with ½ ..."
msgstr "½ ... দিয়ে 1/2 ... প্রতিস্থাপন করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DETECT_URL+1\n"
"string.text"
msgid "URL recognition"
msgstr "URL শনাক্তকরণ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DASH+1\n"
"string.text"
msgid "Replace dashes"
msgstr "ড্যাশ প্রতিস্থাপন"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_ORDINAL+1\n"
"string.text"
msgid "Replace 1st... with 1^st..."
msgstr "1^st... দিয়ে 1st... প্রতিস্থাপন করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_RIGHT_MARGIN+1\n"
"string.text"
msgid "Combine single line paragraphs"
msgstr "এক লাইণের অনুচ্ছেদ একত্রীকরণ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_TEXT +1\n"
"string.text"
msgid "Set \"Text body\" Style"
msgstr "\"লেখা বডি\" শৈলী নির্ধারণ করা"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_INDENT  +1\n"
"string.text"
msgid "Set \"Text body indent\" Style"
msgstr "\"লেখা বডির ইন্ডেন্ট\" শৈলী নির্ধারণ করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_NEG_INDENT  +1\n"
"string.text"
msgid "Set \"Hanging indent\" Style"
msgstr "\"ঝুলন্ত ইন্ডেন্ট\" শৈলী নির্ধারণ করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_TEXT_INDENT +1\n"
"string.text"
msgid "Set \"Text body indent\" Style"
msgstr "\"লেখা বডির ইন্ডেন্ট\" শৈলী নির্ধারণ করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_TMPL_HEADLINE +1\n"
"string.text"
msgid "Set \"Heading $(ARG1)\" Style"
msgstr "\"শিরোনাম $(ARG1)\" শৈলী নির্ধারণ করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_SET_NUMBULET +1\n"
"string.text"
msgid "Set \"Bullet\" or \"Numbering\" Style"
msgstr "\"বুলেট\" বা \"সংখ্যায়ন\" শৈলী নির্ধারণ করা"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_DEL_MORELINES +1\n"
"string.text"
msgid "Combine paragraphs"
msgstr "অনুচ্ছেদ একত্রীকরণ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_SHELLRES_AUTOFMTSTRS\n"
"STR_AUTOFMTREDL_NON_BREAK_SPACE +1\n"
"string.text"
msgid "Add non breaking space"
msgstr "টানা ফাঁকা জায়গা যোগ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_OBJECT_SELECT\n"
"string.text"
msgid "Click object"
msgstr "বস্তুতে ক্লিক করুন"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_START_INS_GLOSSARY\n"
"string.text"
msgid "Before inserting AutoText"
msgstr "স্বয়ংক্রিয়-লেখা প্রবেশের পূর্বে"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_END_INS_GLOSSARY\n"
"string.text"
msgid "After inserting AutoText"
msgstr "স্বয়ংক্রিয়-লেখা প্রবেশের পরে"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSEOVER_OBJECT\n"
"string.text"
msgid "Mouse over object"
msgstr "বস্তুর উপর মাউস"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSECLICK_OBJECT\n"
"string.text"
msgid "Trigger hyperlink"
msgstr "হাইপারলিংক ট্রিগার করা হবে"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_MOUSEOUT_OBJECT\n"
"string.text"
msgid "Mouse leaves object"
msgstr "মাউসের বস্তু থেকে সরে যায়"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_LOAD\n"
"string.text"
msgid "Image loaded successfully"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_ABORT\n"
"string.text"
msgid "Image loading terminated"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_IMAGE_ERROR\n"
"string.text"
msgid "Could not load image"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_KEYINPUT_A\n"
"string.text"
msgid "Input of alphanumeric characters"
msgstr "আলফানিউমেরিক অক্ষরের ইনপুট"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_KEYINPUT_NOA\n"
"string.text"
msgid "Input of non-alphanumeric characters"
msgstr "আলফানিউমেরিক ব্যতীত অক্ষরের ইনপুট"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_RESIZE\n"
"string.text"
msgid "Resize frame"
msgstr "ফ্রেমের আকার পরিবর্তন"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_EVENT_FRM_MOVE\n"
"string.text"
msgid "Move frame"
msgstr "ফ্রেম সরানো"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_OUTLINE\n"
"string.text"
msgid "Headings"
msgstr "শিরোনাম"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_TABLE\n"
"string.text"
msgid "Tables"
msgstr "সারণি"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_FRAME\n"
"string.text"
msgid "Text frames"
msgstr "লেখা ফ্রেম"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_GRAPHIC\n"
"string.text"
msgid "Images"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_OLE\n"
"string.text"
msgid "OLE objects"
msgstr "OLE অবজেক্ট"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_BOOKMARK\n"
"string.text"
msgid "Bookmarks"
msgstr "বুকমার্ক"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_REGION\n"
"string.text"
msgid "Sections"
msgstr "বিভাগ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_URLFIELD\n"
"string.text"
msgid "Hyperlinks"
msgstr "হাইপারলিংক"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_REFERENCE\n"
"string.text"
msgid "References"
msgstr "রেফারেন্স"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_INDEX\n"
"string.text"
msgid "Indexes"
msgstr "সূচিপত্র"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_DRAWOBJECT\n"
"string.text"
msgid "Drawing objects"
msgstr "অঙ্কন বস্তু"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_POSTIT\n"
"string.text"
msgid "Comments"
msgstr "মন্তব্য"

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING1\n"
"string.text"
msgid "Heading 1"
msgstr "শিরোনাম ১"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY1\n"
"string.text"
msgid "This is the content from the first chapter. This is a user directory entry."
msgstr ""

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING11\n"
"string.text"
msgid "Heading 1.1"
msgstr "শিরোনাম ১"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY11\n"
"string.text"
msgid "This is the content from chapter 1.1. This is the entry for the table of contents."
msgstr ""

#: utlui.src
#, fuzzy
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_HEADING12\n"
"string.text"
msgid "Heading 1.2"
msgstr "শিরোনাম ১"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_ENTRY12\n"
"string.text"
msgid "This is the content from chapter 1.2. This keyword is a main entry."
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_TABLE1\n"
"string.text"
msgid "Table 1: This is table 1"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_IDXEXAMPLE_IDXTXT_IMAGE1\n"
"string.text"
msgid "Image 1: This is image 1"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_OUTLINE\n"
"string.text"
msgid "Heading"
msgstr "শিরোনাম"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_TABLE\n"
"string.text"
msgid "Table"
msgstr "সারণি"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_FRAME\n"
"string.text"
msgid "Text frame"
msgstr "লেখা ফ্রেম"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_GRAPHIC\n"
"string.text"
msgid "Image"
msgstr ""

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_OLE\n"
"string.text"
msgid "OLE object"
msgstr "OLE অবজেক্ট"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_BOOKMARK\n"
"string.text"
msgid "Bookmark"
msgstr "বুকমার্ক"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_REGION\n"
"string.text"
msgid "Section"
msgstr "বিভাগ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_URLFIELD\n"
"string.text"
msgid "Hyperlink"
msgstr "হাইপারলিংক"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_REFERENCE\n"
"string.text"
msgid "Reference"
msgstr "রেফারেন্স"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_INDEX\n"
"string.text"
msgid "Index"
msgstr "সূচি"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_POSTIT\n"
"string.text"
msgid "Comment"
msgstr "মন্তব্য"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_CONTENT_TYPE_SINGLE_DRAWOBJECT\n"
"string.text"
msgid "Draw object"
msgstr "অঙ্কন বস্তু"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_DEFINE_NUMBERFORMAT\n"
"string.text"
msgid "Additional formats..."
msgstr "অতিরিক্ত বিন্যাস..."

#: utlui.src
msgctxt ""
"utlui.src\n"
"RID_STR_SYSTEM\n"
"string.text"
msgid "[System]"
msgstr "[সিস্টেম]"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_MULT_INTERACT_SPELL_WARN\n"
"string.text"
msgid ""
"The interactive spellcheck is already active\n"
"in a different document"
msgstr ""
"একটি ভিন্ন নথিতে\n"
"মিথস্ক্রিয়া বানান-পরীক্ষক ইতোমধ্যে সক্রিয়"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_MULT_INTERACT_HYPH_WARN\n"
"string.text"
msgid ""
"The interactive hyphenation is already active\n"
"in a different document"
msgstr ""
"একটি ভিন্ন নথিতে\n"
"মিথস্ক্রিয়া হাইফেন প্রয়োগ ইতোমধ্যে সক্রিয়"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_SPELL_TITLE\n"
"string.text"
msgid "Spellcheck"
msgstr "বানান-পরীক্ষণ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPH_TITLE\n"
"string.text"
msgid "Hyphenation"
msgstr "হাইফেন প্রয়োগ"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPERCTRL_SEL\n"
"string.text"
msgid "SEL"
msgstr "SEL"

#: utlui.src
msgctxt ""
"utlui.src\n"
"STR_HYPERCTRL_HYP\n"
"string.text"
msgid "HYP"
msgstr "HYP"
