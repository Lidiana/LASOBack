#. extracted from sw/source/core/unocore
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-08-25 18:38+0000\n"
"Last-Translator: வே. இளஞ்செழியன் (Ve. Elanjelian) <tamiliam@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1472150297.000000\n"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_CHART2_ROW_LABEL_TEXT\n"
"string.text"
msgid "Row %ROWNUMBER"
msgstr "வரிசை %ROWNUMBER"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_CHART2_COL_LABEL_TEXT\n"
"string.text"
msgid "Column %COLUMNLETTER"
msgstr "நிரல் %COLUMNLETTER"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_CHARACTER\n"
"string.text"
msgid "Character"
msgstr "எழுத்து"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_PARAGRAPH\n"
"string.text"
msgid "Paragraph"
msgstr "பத்தி"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_FRAME\n"
"string.text"
msgid "Frame"
msgstr "சட்டம்"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_PAGE\n"
"string.text"
msgid "Pages"
msgstr "பக்கங்கள்"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_NUMBERING\n"
"string.text"
msgid "Numbering"
msgstr "எண்ணிடல்"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_TABLE\n"
"string.text"
msgid "Table"
msgstr "அட்டவணை"

#: unocore.src
msgctxt ""
"unocore.src\n"
"STR_STYLE_FAMILY_CELL\n"
"string.text"
msgid "Cell"
msgstr ""
