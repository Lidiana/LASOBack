#. extracted from reportdesign/uiconfig/dbreport/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-12 20:22+0000\n"
"Last-Translator: Jean-Baptiste Faure <jbfaure@libreoffice.org>\n"
"Language-Team: ll.org\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-Project-Style: openoffice\n"
"X-POOTLE-MTIME: 1481574124.000000\n"

#: backgrounddialog.ui
msgctxt ""
"backgrounddialog.ui\n"
"BackgroundDialog\n"
"title\n"
"string.text"
msgid "Section Setup"
msgstr "Mise en page de la section"

#: backgrounddialog.ui
msgctxt ""
"backgrounddialog.ui\n"
"background\n"
"label\n"
"string.text"
msgid "Background"
msgstr "Arrière-plan"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"CharDialog\n"
"title\n"
"string.text"
msgid "Character Settings"
msgstr "Paramètres des caractères"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"font\n"
"label\n"
"string.text"
msgid "Font"
msgstr "Police"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"fonteffects\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "Effets de caractère"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"position\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Position"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"asianlayout\n"
"label\n"
"string.text"
msgid "Asian Layout"
msgstr "Mise en page asiatique"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"background\n"
"label\n"
"string.text"
msgid "Highlighting"
msgstr "Surlignage"

#: chardialog.ui
msgctxt ""
"chardialog.ui\n"
"alignment\n"
"label\n"
"string.text"
msgid "Alignment"
msgstr "Alignement"

#: condformatdialog.ui
msgctxt ""
"condformatdialog.ui\n"
"CondFormat\n"
"title\n"
"string.text"
msgid "Conditional Formatting"
msgstr "Formatage conditionnel"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"typeCombobox\n"
"0\n"
"stringlist.text"
msgid "Field Value Is"
msgstr "Valeur de champ est"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"typeCombobox\n"
"1\n"
"stringlist.text"
msgid "Expression Is"
msgstr "Expression est"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"0\n"
"stringlist.text"
msgid "between"
msgstr "entre"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"1\n"
"stringlist.text"
msgid "not between"
msgstr "non entre"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"2\n"
"stringlist.text"
msgid "equal to"
msgstr "égal à"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"3\n"
"stringlist.text"
msgid "not equal to"
msgstr "différent de"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"4\n"
"stringlist.text"
msgid "greater than"
msgstr "supérieur à"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"5\n"
"stringlist.text"
msgid "less than"
msgstr "inférieur à"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"6\n"
"stringlist.text"
msgid "greater than or equal to"
msgstr "supérieur ou égal à"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"opCombobox\n"
"7\n"
"stringlist.text"
msgid "less than or equal to"
msgstr "inférieur ou égal à"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"lhsButton\n"
"label\n"
"string.text"
msgid "..."
msgstr "..."

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"andLabel\n"
"label\n"
"string.text"
msgid "and"
msgstr "et"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"rhsButton\n"
"label\n"
"string.text"
msgid "..."
msgstr "..."

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem1\n"
"label\n"
"string.text"
msgid "Bold"
msgstr "Gras"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem2\n"
"label\n"
"string.text"
msgid "Italic"
msgstr "Italique"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem3\n"
"label\n"
"string.text"
msgid "Underline"
msgstr "Soulignage"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem4\n"
"label\n"
"string.text"
msgid "Background Color"
msgstr "Couleur d'arrière-plan"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem5\n"
"label\n"
"string.text"
msgid "Font Color"
msgstr "Couleur de police"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"ToolBoxItem6\n"
"label\n"
"string.text"
msgid "Character Formatting"
msgstr "Formatage des caractères"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"removeButton\n"
"label\n"
"string.text"
msgid "-"
msgstr "-"

#: conditionwin.ui
msgctxt ""
"conditionwin.ui\n"
"addButton\n"
"label\n"
"string.text"
msgid "+"
msgstr "+"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"DateTimeDialog\n"
"title\n"
"string.text"
msgid "Date and Time"
msgstr "Date et Heure"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"date\n"
"label\n"
"string.text"
msgid "_Include Date"
msgstr "_Inclure la date"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"datelistbox_label\n"
"label\n"
"string.text"
msgid "_Format:"
msgstr "_Formater :"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"time\n"
"label\n"
"string.text"
msgid "Include _Time"
msgstr "Inclure l'_heure"

#: datetimedialog.ui
msgctxt ""
"datetimedialog.ui\n"
"timelistbox_label\n"
"label\n"
"string.text"
msgid "Fo_rmat:"
msgstr "Fo_rmater :"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"FloatingField\n"
"title\n"
"string.text"
msgid "Sorting and Grouping"
msgstr "Trier et grouper"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"up\n"
"label\n"
"string.text"
msgid "Sort Ascending"
msgstr "Tri croissant"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"down\n"
"label\n"
"string.text"
msgid "Sort Descending"
msgstr "Tri décroissant"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"delete\n"
"label\n"
"string.text"
msgid "Remove sorting"
msgstr "Supprimer le tri"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"insert\n"
"label\n"
"string.text"
msgid "Insert"
msgstr "Insérer"

#: floatingfield.ui
msgctxt ""
"floatingfield.ui\n"
"helptext\n"
"label\n"
"string.text"
msgid "Highlight the fields to insert into the selected section of the template, then click Insert or press Enter."
msgstr "Mettre en évidence les champs à insérer dans la section du modèle sélectionnée, puis cliquer sur Insérer ou appuyer sur Entrée."

#: floatingnavigator.ui
msgctxt ""
"floatingnavigator.ui\n"
"FloatingNavigator\n"
"title\n"
"string.text"
msgid "Report navigator"
msgstr "Navigateur de rapport"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"FloatingSort\n"
"title\n"
"string.text"
msgid "Sorting and Grouping"
msgstr "Tri et groupe"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Group actions"
msgstr "Grouper les actions"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"up\n"
"label\n"
"string.text"
msgid "Move up"
msgstr "Déplace vers le haut"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"down\n"
"label\n"
"string.text"
msgid "Move down"
msgstr "Déplace vers le bas"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"delete\n"
"label\n"
"string.text"
msgid "Delete"
msgstr "Supprimer"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Groups"
msgstr "Groupes"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Sorting"
msgstr "Tri"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label7\n"
"label\n"
"string.text"
msgid "Group Header"
msgstr "En-tête de groupe"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label8\n"
"label\n"
"string.text"
msgid "Group Footer"
msgstr "Pied de page de groupe"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label9\n"
"label\n"
"string.text"
msgid "Group On"
msgstr "Grouper sur"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label10\n"
"label\n"
"string.text"
msgid "Group Interval"
msgstr "Grouper l'interval"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label11\n"
"label\n"
"string.text"
msgid "Keep Together"
msgstr "Conserver ensemble"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"sorting\n"
"0\n"
"stringlist.text"
msgid "Ascending"
msgstr "Croissant"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"sorting\n"
"1\n"
"stringlist.text"
msgid "Descending"
msgstr "Décroissant"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"header\n"
"0\n"
"stringlist.text"
msgid "Present"
msgstr "Présent"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"header\n"
"1\n"
"stringlist.text"
msgid "Not present"
msgstr "Non présent"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"keep\n"
"0\n"
"stringlist.text"
msgid "No"
msgstr "Non"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"keep\n"
"1\n"
"stringlist.text"
msgid "Whole Group"
msgstr "Le groupe entier"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"keep\n"
"2\n"
"stringlist.text"
msgid "With First Detail"
msgstr "Avec le premier détail"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"footer\n"
"0\n"
"stringlist.text"
msgid "Present"
msgstr "Présent"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"footer\n"
"1\n"
"stringlist.text"
msgid "Not present"
msgstr "Non présent"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"group\n"
"0\n"
"stringlist.text"
msgid "Each Value"
msgstr "Chaque valeur"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Properties"
msgstr "Propriétés"

#: floatingsort.ui
msgctxt ""
"floatingsort.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Help"
msgstr "Aide"

#: pagedialog.ui
msgctxt ""
"pagedialog.ui\n"
"PageDialog\n"
"title\n"
"string.text"
msgid "Page Setup"
msgstr "Mise en page"

#: pagedialog.ui
msgctxt ""
"pagedialog.ui\n"
"page\n"
"label\n"
"string.text"
msgid "Page"
msgstr "Page"

#: pagedialog.ui
msgctxt ""
"pagedialog.ui\n"
"background\n"
"label\n"
"string.text"
msgid "Background"
msgstr "Arrière-plan"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"PageNumberDialog\n"
"title\n"
"string.text"
msgid "Page Numbers"
msgstr "Numéros de page"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"pagen\n"
"label\n"
"string.text"
msgid "_Page N"
msgstr "_Page N"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"pagenofm\n"
"label\n"
"string.text"
msgid "Page _N of M"
msgstr "Page _N de M"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Format"
msgstr "Formater"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"toppage\n"
"label\n"
"string.text"
msgid "_Top of Page (Header)"
msgstr "_Haut de la page (en-tête)"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"bottompage\n"
"label\n"
"string.text"
msgid "_Bottom of Page (Footer)"
msgstr "_Bas de la page (pied de page)"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Position"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment\n"
"0\n"
"stringlist.text"
msgid "Left"
msgstr "Gauche"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment\n"
"1\n"
"stringlist.text"
msgid "Center"
msgstr "Centre"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment\n"
"2\n"
"stringlist.text"
msgid "Right"
msgstr "Droite"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"alignment_label\n"
"label\n"
"string.text"
msgid "_Alignment:"
msgstr "_Alignement :"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"shownumberonfirstpage\n"
"label\n"
"string.text"
msgid "Show Number on First Page"
msgstr "Afficher le numéro sur la première page"

#: pagenumberdialog.ui
msgctxt ""
"pagenumberdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "General"
msgstr "Général"
