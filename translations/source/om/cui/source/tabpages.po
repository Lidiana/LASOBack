#. extracted from cui/source/tabpages
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-12-11 18:30+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: om\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1449858640.000000\n"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_NONE\n"
"string.text"
msgid "Set No Borders"
msgstr "Daanga Hin Saraarin."

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_ONLYOUTER\n"
"string.text"
msgid "Set Outer Border Only"
msgstr "Daanga Ala Qofa Saraari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERHORI\n"
"string.text"
msgid "Set Outer Border and Horizontal Lines"
msgstr "Daanga Ala fi Sarara Ciisaa Saraari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERALL\n"
"string.text"
msgid "Set Outer Border and All Inner Lines"
msgstr "Daanga Ala fi Sararoota Keessa Qofa Saraari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERINNER\n"
"string.text"
msgid "Set Outer Border Without Changing Inner Lines"
msgstr "Daanga Ala Sarari osoo Sararoota Keessa hin Jijjiirin"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_DIAGONAL\n"
"string.text"
msgid "Set Diagonal Lines Only"
msgstr "Sarara Diyaagonalii Qofa Sireessi"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ALL\n"
"string.text"
msgid "Set All Four Borders"
msgstr "Dangaa Afranuu Saraari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_LEFTRIGHT\n"
"string.text"
msgid "Set Left and Right Borders Only"
msgstr "Daanga Bitaa fi Mirgaa qofa Sarari "

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_TOPBOTTOM\n"
"string.text"
msgid "Set Top and Bottom Borders Only"
msgstr "Daanga gubba fi Jalaa qofa Sarari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ONLYLEFT\n"
"string.text"
msgid "Set Left Border Only"
msgstr "Daanga Bitaa qofa Sarari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_HOR_PRESET_ONLYHOR\n"
"string.text"
msgid "Set Top and Bottom Borders, and All Inner Lines"
msgstr "Daanga gubba fi Jalaa, akkasumas Sararoota keessa Hunda Sarari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_VER_PRESET_ONLYVER\n"
"string.text"
msgid "Set Left and Right Borders, and All Inner Lines"
msgstr "Daanga Bitaa fi Mirgaa, akkasumas Sararoota keessa Hunda Sarari"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_NONE\n"
"string.text"
msgid "No Shadow"
msgstr "Gaaddidduu Malee"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMRIGHT\n"
"string.text"
msgid "Cast Shadow to Bottom Right"
msgstr "Gaaddidu gara Miil Mirgaa Jalatti godhii"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPRIGHT\n"
"string.text"
msgid "Cast Shadow to Top Right"
msgstr "Gaaddidu gara Miil Mirgaa Gubaatti godhii"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMLEFT\n"
"string.text"
msgid "Cast Shadow to Bottom Left"
msgstr "Gaaddidu gara Miil Bitaa Jalatti godhii"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPLEFT\n"
"string.text"
msgid "Cast Shadow to Top Left"
msgstr "Gaaddidu gara Miil Bitaa Gubaattii godhii"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_LTR\n"
"string.text"
msgid "Left-to-right (LTR)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_RTL\n"
"string.text"
msgid "Right-to-left (RTL)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_SUPER\n"
"string.text"
msgid "Use superordinate object settings"
msgstr "Qindaa'inoota wantaa ol'aanoo fayyadami"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_HORI\n"
"string.text"
msgid "Left-to-right (horizontal)"
msgstr "Bitaa-gara-mirgaa (Sardala)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_HORI\n"
"string.text"
msgid "Right-to-left (horizontal)"
msgstr "Mirgaa-gara-bitaa (Sardala)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_VERT\n"
"string.text"
msgid "Right-to-left (vertical)"
msgstr "Mirgaa-gara-bitaa (Sarjaa)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_VERT\n"
"string.text"
msgid "Left-to-right (vertical)"
msgstr "Bitaa-gara-mirgaa (Sarjaa)"

#: paragrph.src
#, fuzzy
msgctxt ""
"paragrph.src\n"
"STR_EXAMPLE\n"
"string.text"
msgid "Example"
msgstr "Fakkeenya"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_GRADIENT\n"
"string.text"
msgid "Please enter a name for the gradient:"
msgstr "maqaa gargartoo galchi:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_GRADIENT\n"
"string.text"
msgid ""
"The gradient was modified without saving. \n"
"Modify the selected gradient or add a new gradient."
msgstr ""
"Gargartoon osoo hin olkaa'amniin fooyyeeffameera. \n"
"Gargartoo filatame fooyyessi ykn gargaartoo haaraa dabali."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_BITMAP\n"
"string.text"
msgid "Please enter a name for the bitmap:"
msgstr "Suurxiqqoodhaaf maqaa galchi:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_EXT_BITMAP\n"
"string.text"
msgid "Please enter a name for the external bitmap:"
msgstr "Suurxiqqoo alaatiif maqaa galchi:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_PATTERN\n"
"string.text"
msgid "Please enter a name for the pattern:"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_BITMAP\n"
"string.text"
msgid ""
"The bitmap was modified without saving. \n"
"Modify the selected bitmap or add a new bitmap."
msgstr ""
"Suurxiqqoon osoo hin olkaa'amniin fooyyeeffameera. \n"
"Suurxiqqoo filatame fooyyessi ykn Suurxiqqoo haaraa dabali."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_PATTERN\n"
"string.text"
msgid ""
"The pattern was modified without saving. \n"
"Modify the selected pattern or add a new pattern"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINESTYLE\n"
"string.text"
msgid "Please enter a name for the line style:"
msgstr "Akkaataa sararaatiif maqaa galchi:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_LINESTYLE\n"
"string.text"
msgid ""
"The line style was modified without saving. \n"
"Modify the selected line style or add a new line style."
msgstr ""
"Akkaataan sararaa osoo hin olkaa'amniin fooyyeeffameera. \n"
"Akkaataa sararaa filatame fooyyessi ykn Akkaataa sararaa haaraa dabali."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_HATCH\n"
"string.text"
msgid "Please enter a name for the hatching:"
msgstr "Maaloo, Saphapheessuuf maqaa galchi"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_HATCH\n"
"string.text"
msgid ""
"The hatching type was modified but not saved. \n"
"Modify the selected hatching type or add a new hatching type."
msgstr ""
"Saphapheessuun fooyya'eraa garuu hin olkaa 'amne. \n"
"Akaakuu saphapheessuu filatame fooyyessi ykn akaakuu saphapheessuu haaraa dabali."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHANGE\n"
"string.text"
msgid "Modify"
msgstr "Fooyyessi"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ADD\n"
"string.text"
msgid "Add"
msgstr "Iida'i"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_COLOR\n"
"string.text"
msgid "Please enter a name for the new color:"
msgstr "Maaloo halluu haaraadhaaf maqaa galchi:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_COLOR\n"
"string.text"
msgid ""
"The color was modified without saving.\n"
"Modify the selected color or add a new color."
msgstr ""
"Halluun osoo hin olkaa'amin fooyyeeffame.\n"
"Halluu filatame fooyyessi ykn halluu haaraa dabali."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_TABLE\n"
"string.text"
msgid "Table"
msgstr "Gabatee"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINEEND\n"
"string.text"
msgid "Please enter a name for the new arrowhead:"
msgstr "Maaloo,mataa xiyyaatii haaraatiif maqaa galchi:"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_NOSTYLE\n"
"string.text"
msgid "No %1"
msgstr "Lakk %1 n hin jiru"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FAMILY\n"
"string.text"
msgid "Family"
msgstr "Maati"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FONT\n"
"string.text"
msgid "Font"
msgstr "Bocquu"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_STYLE\n"
"string.text"
msgid "Style"
msgstr "Akkaata"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_TYPEFACE\n"
"string.text"
msgid "Typeface"
msgstr "Barfuula"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_HIGHLIGHTING\n"
"string.text"
msgid "Highlight Color"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USE_REPLACE\n"
"string.text"
msgid "Use replacement table"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_WORD\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_SENT\n"
"string.text"
msgid "Capitalize first letter of every sentence"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BOLD_UNDER\n"
"string.text"
msgid "Automatic *bold* and _underline_"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NO_DBL_SPACES\n"
"string.text"
msgid "Ignore double spaces"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DETECT_URL\n"
"string.text"
msgid "URL Recognition"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DASH\n"
"string.text"
msgid "Replace dashes"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CORRECT_ACCIDENTAL_CAPS_LOCK\n"
"string.text"
msgid "Correct accidental use of cAPS LOCK key"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NON_BREAK_SPACE\n"
"string.text"
msgid "Add non-breaking space before specific punctuation marks in French text"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ORDINAL\n"
"string.text"
msgid "Format ordinal numbers suffixes (1st -> 1^st)"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_EMPTY_PARA\n"
"string.text"
msgid "Remove blank paragraphs"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USER_STYLE\n"
"string.text"
msgid "Replace Custom Styles"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BULLET\n"
"string.text"
msgid "Replace bullets with: "
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_RIGHT_MARGIN\n"
"string.text"
msgid "Combine single line paragraphs if length greater than"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NUM\n"
"string.text"
msgid "Bulleted and numbered lists. Bullet symbol: "
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BORDER\n"
"string.text"
msgid "Apply border"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CREATE_TABLE\n"
"string.text"
msgid "Create table"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_REPLACE_TEMPLATES\n"
"string.text"
msgid "Apply Styles"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_AT_STT_END\n"
"string.text"
msgid "Delete spaces and tabs at beginning and end of paragraph"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_BETWEEN_LINES\n"
"string.text"
msgid "Delete spaces and tabs at end and start of line"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CONNECTOR\n"
"string.text"
msgid "Connector"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DIMENSION_LINE\n"
"string.text"
msgid "Dimension line"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_READ_DATA_ERROR\n"
"string.text"
msgid "The file could not be loaded!"
msgstr "Faayiliin fe'amuu hin dandeenye!"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_LOAD_ERROR\n"
"string.text"
msgid "The selected module could not be loaded."
msgstr ""
