#. extracted from svtools/source/control
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-06-26 03:07+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: om\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1435288044.000000\n"

#: calendar.src
msgctxt ""
"calendar.src\n"
"STR_SVT_CALENDAR_DAY\n"
"string.text"
msgid "Day"
msgstr "Guyyaa"

#: calendar.src
msgctxt ""
"calendar.src\n"
"STR_SVT_CALENDAR_WEEK\n"
"string.text"
msgid "Week"
msgstr "Torban"

#: calendar.src
msgctxt ""
"calendar.src\n"
"STR_SVT_CALENDAR_TODAY\n"
"string.text"
msgid "Today"
msgstr "Har'a"

#: calendar.src
msgctxt ""
"calendar.src\n"
"STR_SVT_CALENDAR_NONE\n"
"string.text"
msgid "None"
msgstr "Homaa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_ALPHANUMERIC\n"
"string.text"
msgid "Alphanumeric"
msgstr "Katabbii"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_NORMAL\n"
"string.text"
msgid "Normal"
msgstr "Baratamoo"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_CHARSET\n"
"string.text"
msgid "Character set"
msgstr "Arfii qindaa'e"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_DICTIONARY\n"
"string.text"
msgid "Dictionary"
msgstr "Galmee jechootaa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_PINYIN\n"
"string.text"
msgid "Pinyin"
msgstr "Piiniyiin"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_STROKE\n"
"string.text"
msgid "Stroke"
msgstr "Dhawwiinsa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_RADICAL\n"
"string.text"
msgid "Radical"
msgstr "Ajaa'ibsiisaa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_UNICODE\n"
"string.text"
msgid "Unicode"
msgstr "Yuniikoodii"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_ZHUYIN\n"
"string.text"
msgid "Zhuyin"
msgstr "Zuhuuyiin"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_PHONEBOOK\n"
"string.text"
msgid "Phone book"
msgstr "Baafata bilbilaa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_PHONETIC_F\n"
"string.text"
msgid "Phonetic (alphanumeric first)"
msgstr "Xinsagalee(dura katabbii)"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_COLLATE_PHONETIC_L\n"
"string.text"
msgid "Phonetic (alphanumeric last)"
msgstr "Xinsagalee(dhuma katabbii)"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_ALPHANUMERIC\n"
"string.text"
msgid "Alphanumeric"
msgstr "Katabbii"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_DICTIONARY\n"
"string.text"
msgid "Dictionary"
msgstr "Galmee jechootaa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_PINYIN\n"
"string.text"
msgid "Pinyin"
msgstr "Piiniyiin"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_RADICAL\n"
"string.text"
msgid "Radical"
msgstr "Ajaa'ibsiisaa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_STROKE\n"
"string.text"
msgid "Stroke"
msgstr "Dhawwiinsa"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_ZHUYIN\n"
"string.text"
msgid "Zhuyin"
msgstr "Zuhuuyiin"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_PHONETIC_FS\n"
"string.text"
msgid "Phonetic (alphanumeric first, grouped by syllables)"
msgstr "Xinsagalee (dura katabbii birsagaaleen kan gurmaa'e)"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_PHONETIC_FC\n"
"string.text"
msgid "Phonetic (alphanumeric first, grouped by consonants)"
msgstr "Xinsagalee (dura katabbii dubbifamtootan kan gurmaa'e)"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_PHONETIC_LS\n"
"string.text"
msgid "Phonetic (alphanumeric last, grouped by syllables)"
msgstr "Xinsagalee (dhuma katabbii birsagaaleen kan gurmaa'e)"

#: ctrlbox.src
msgctxt ""
"ctrlbox.src\n"
"STR_SVT_INDEXENTRY_PHONETIC_LC\n"
"string.text"
msgid "Phonetic (alphanumeric last, grouped by consonants)"
msgstr "Xinsagalee (dhuma katabbii dubbifamtootan kan gurmaa'e)"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_LIGHT\n"
"string.text"
msgid "Light"
msgstr "Hafalee"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_LIGHT_ITALIC\n"
"string.text"
msgid "Light Italic"
msgstr "Mirgada hafalee"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_NORMAL\n"
"string.text"
msgid "Regular"
msgstr "Idilee"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_NORMAL_ITALIC\n"
"string.text"
msgid "Italic"
msgstr "Mirgada"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_BOLD\n"
"string.text"
msgid "Bold"
msgstr "Yabbuu"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_BOLD_ITALIC\n"
"string.text"
msgid "Bold Italic"
msgstr "Mirgada Yabbuu"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_BLACK\n"
"string.text"
msgid "Black"
msgstr "Gurraacha"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_STYLE_BLACK_ITALIC\n"
"string.text"
msgid "Black Italic"
msgstr "Mirgada Gurraacha"

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_FONTMAP_BOTH\n"
"string.text"
msgid "The same font will be used on both your printer and your screen."
msgstr "Maxxansaa fi argii keerratti bocquu walfakkaatuti itti fayyadama."

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_FONTMAP_PRINTERONLY\n"
"string.text"
msgid "This is a printer font. The screen image may differ."
msgstr "Kuni bocquu maxxansaati. Calaqqeen argii kee adda ta'uu mala."

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_FONTMAP_SCREENONLY\n"
"string.text"
msgid "This is a screen font. The printer image may differ."
msgstr "Kuni bocquu argiiti. Calaqqeen argii kee adda ta'uu mala."

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_FONTMAP_SIZENOTAVAILABLE\n"
"string.text"
msgid "This font size has not been installed. The closest available size will be used."
msgstr "Bocquun bal'ina hanganaa hin ijaaramne. Bocquu jiran keessaa kan daran itti aanee jiru itti fayyadama."

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_FONTMAP_STYLENOTAVAILABLE\n"
"string.text"
msgid "This font style will be simulated or the closest matching style will be used."
msgstr "Bocquun simulate ni ta'a ykn haalatini walsimu ni fudhatama."

#: ctrltool.src
msgctxt ""
"ctrltool.src\n"
"STR_SVT_FONTMAP_NOTAVAILABLE\n"
"string.text"
msgid "This font has not been installed. The closest available font will be used."
msgstr "Bocquun bal'ina hanganaa hin ijaaramne. Bocquu jiran keessaa kan daran itti aanee jiru itti fayyadama."

#: filectrl.src
msgctxt ""
"filectrl.src\n"
"STR_FILECTRL_BUTTONTEXT\n"
"string.text"
msgid "Browse..."
msgstr "Sakatta'i..."

#: filectrl.src
#, fuzzy
msgctxt ""
"filectrl.src\n"
"STR_TABBAR_PUSHBUTTON_MOVET0HOME\n"
"string.text"
msgid "Move To Home"
msgstr "Yaada Siqsi"

#: filectrl.src
msgctxt ""
"filectrl.src\n"
"STR_TABBAR_PUSHBUTTON_MOVELEFT\n"
"string.text"
msgid "Move Left"
msgstr "Bitaa Siqsi"

#: filectrl.src
msgctxt ""
"filectrl.src\n"
"STR_TABBAR_PUSHBUTTON_MOVERIGHT\n"
"string.text"
msgid "Move Right"
msgstr "Mirga Siqsi"

#: filectrl.src
#, fuzzy
msgctxt ""
"filectrl.src\n"
"STR_TABBAR_PUSHBUTTON_MOVETOEND\n"
"string.text"
msgid "Move To End"
msgstr ""
"#-#-#-#-#  dlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Gadi siiqsi\n"
"#-#-#-#-#  formwizard.po (PACKAGE VERSION)  #-#-#-#-#\n"
"Gadi siqsi"

#: filectrl.src
msgctxt ""
"filectrl.src\n"
"STR_TABBAR_PUSHBUTTON_ADDTAB\n"
"string.text"
msgid "Add"
msgstr ""

#: ruler.src
msgctxt ""
"ruler.src\n"
"STR_SVT_ACC_RULER_HORZ_NAME\n"
"string.text"
msgid "Horizontal Ruler"
msgstr ""

#: ruler.src
msgctxt ""
"ruler.src\n"
"STR_SVT_ACC_RULER_VERT_NAME\n"
"string.text"
msgid "Vertical Ruler"
msgstr ""
