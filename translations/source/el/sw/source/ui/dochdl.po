#. extracted from sw/source/ui/dochdl
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-08-25 12:34+0200\n"
"PO-Revision-Date: 2016-07-04 20:26+0000\n"
"Last-Translator: Dimitris Spingos <dmtrs32@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1467664017.000000\n"

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_NOGLOS\n"
"string.text"
msgid "AutoText for Shortcut '%1' not found."
msgstr "Το αυτόματο κείμενο για την συντόμευση '%1' δεν βρέθηκε."

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_NO_TABLE\n"
"string.text"
msgid "A table with no rows or no cells cannot be inserted"
msgstr "Δεν μπορεί να εισαχθεί πίνακας χωρίς γραμμές ή κελιά"

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_TABLE_TOO_LARGE\n"
"string.text"
msgid "The table cannot be inserted because it is too large"
msgstr "Ο πίνακας δεν μπορεί να εισαχθεί επειδή είναι υπερβολικά μεγάλος"

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_ERR_INSERT_GLOS\n"
"string.text"
msgid "AutoText could not be created."
msgstr "Αδύνατη η δημιουργία αυτόματου κειμένου."

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_CLPBRD_FORMAT_ERROR\n"
"string.text"
msgid "Requested clipboard format is not available."
msgstr "Η ζητούμενη μορφή προχείρου δεν είναι διαθέσιμη."

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_PRIVATETEXT\n"
"string.text"
msgid "%PRODUCTNAME Writer"
msgstr "%PRODUCTNAME Writer"

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_PRIVATEGRAPHIC\n"
"string.text"
msgid "Image [%PRODUCTNAME Writer]"
msgstr "Εικόνα του [%PRODUCTNAME Writer]"

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_PRIVATEOLE\n"
"string.text"
msgid "Object [%PRODUCTNAME Writer]"
msgstr "Αντικείμενο [%PRODUCTNAME Writer]"

#: dochdl.src
msgctxt ""
"dochdl.src\n"
"STR_DDEFORMAT\n"
"string.text"
msgid "DDE link"
msgstr "Σύνδεση DDE"
