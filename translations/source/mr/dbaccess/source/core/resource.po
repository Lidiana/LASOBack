#. extracted from dbaccess/source/core/resource
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-05-11 16:12+0200\n"
"PO-Revision-Date: 2012-08-23 11:08+0200\n"
"Last-Translator: Sandeep <sandeep.shedmake@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_TRIED_OPEN_TABLE\n"
"string.text"
msgid "Tried to open the table $name$."
msgstr "$name$ कोष्टक उघडण्याचा प्रयत्न केला."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONNECTION_INVALID\n"
"string.text"
msgid "No connection could be established."
msgstr "जुळणी प्रस्थापित होणे शक्य नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_TABLE_IS_FILTERED\n"
"string.text"
msgid "The table $name$ already exists. It is not visible because it has been filtered out."
msgstr "कोष्टक $name$ आधीपासूनच अस्तित्वात आहे. कोष्टक गाळणी केलेली असल्यामुळे ती अदृश्य आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NEED_CONFIG_WRITE_ACCESS\n"
"string.text"
msgid "You have no write access to the configuration data the object is based on."
msgstr "ज्या डाटा अंतर्गत रचनेवर वस्तू आधारित आहे त्यात आपण लेखनीय प्रवेश मिळवू शकत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULDNOTCONNECT_UNSPECIFIED\n"
"string.text"
msgid "The connection to the external data source could not be established. An unknown error occurred. The driver is probably defective."
msgstr "बाह्य डाटा स्त्रोताशी जुळणी प्रस्थापित होऊ शकत नाही. एक अज्ञात दोष आढळला. ड्राईवर मध्ये दोष असण्याची शक्यता आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULDNOTCONNECT_NODRIVER\n"
"string.text"
msgid "The connection to the external data source could not be established. No SDBC driver was found for the URL '$name$'."
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULDNOTLOAD_MANAGER\n"
"string.text"
msgid "The connection to the external data source could not be established. The SDBC driver manager could not be loaded."
msgstr "बाह्य डाटा स्त्रोताशी जुळणी प्रस्थापित होऊ शकत नाही. SDBC ड्राईवर व्यवस्थापक लोड होऊ शकला नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FORM\n"
"string.text"
msgid "Form"
msgstr "फॉर्म"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT\n"
"string.text"
msgid "Report"
msgstr "अहवाल"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_DATASOURCE_NOT_STORED\n"
"string.text"
msgid "The data source was not saved. Please use the interface XStorable to save the data source."
msgstr "डाटा स्त्रोत जतन झाला नाही. कृपया डाटा स्त्रोतास जतन करण्यासाठी इंटरफेस Xस्टोरेबलचा वापर करा."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ONLY_QUERY\n"
"string.text"
msgid ""
"The given command is not a SELECT statement.\n"
"Only queries are allowed."
msgstr ""
"दिलेला आदेश SELECT वाक्य नाही.\n"
"फक्त शंकाना परवानगी दिलेली आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_VALUE_CHANGED\n"
"string.text"
msgid "No values were modified."
msgstr "मूल्यांमध्ये सुधारणा आढळले नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_XROWUPDATE\n"
"string.text"
msgid "Values could not be inserted. The XRowUpdate interface is not supported by ResultSet."
msgstr "मूल्य अंर्तभूत होऊ शकले नाहीत. ResultSetला XRowUpdate इंटरफेस आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_XRESULTSETUPDATE\n"
"string.text"
msgid "Values could not be inserted. The XResultSetUpdate interface is not supported by ResultSet."
msgstr "मूल्य अंर्तभूत होऊ शकले नाही. ResultSetला XResultSetUpdate इंटरफेस आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_UPDATE_MISSING_CONDITION\n"
"string.text"
msgid "Values could not be modified, due to a missing condition statement."
msgstr "अट वाक्य गहाळ झाल्यामुळे मूल्यांमध्ये सुधारणा करता आली नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_COLUMN_ADD\n"
"string.text"
msgid "The adding of columns is not supported."
msgstr "स्तंभांचे समावेशनास आधार दिलेला नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_COLUMN_DROP\n"
"string.text"
msgid "The dropping of columns is not supported."
msgstr "स्तंभांचे सोडून देणे आधार दिलेला नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_CONDITION_FOR_PK\n"
"string.text"
msgid "The WHERE condition could not be created for the primary key."
msgstr "WHERE अट प्राथमिक कळीसाठी निर्माण केली जाऊ शकत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_UNKNOWN_PROP\n"
"string.text"
msgid "The column does not support the property '%value'."
msgstr "गुणधर्म '%value' ला स्तंभ आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_NOT_SEARCHABLE\n"
"string.text"
msgid "The column is not searchable!"
msgstr "स्तंभ शोधण्याजोगी नाही!"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NOT_SEQUENCE_INT8\n"
"string.text"
msgid "The value of the columns is not of the type Sequence<sal_Int8>."
msgstr "स्तंभांचे मूल्य हे <sal_Int8> क्रम प्रकाराचे नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_NOT_VALID\n"
"string.text"
msgid "The column is not valid."
msgstr "स्तंभ वैध नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_MUST_VISIBLE\n"
"string.text"
msgid "The column '%name' must be visible as a column."
msgstr "'%name' स्तंभ हा स्तंभासारखा दिसला पाहिजे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_XQUERIESSUPPLIER\n"
"string.text"
msgid "The interface XQueriesSupplier is not available."
msgstr "XQueriesSupplier इंटरफेस उपलब्ध नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NOT_SUPPORTED_BY_DRIVER\n"
"string.text"
msgid "The driver does not support this function."
msgstr "या कार्याला ड्राईवर आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_ABS_ZERO\n"
"string.text"
msgid "An 'absolute(0)' call is not allowed."
msgstr "'absolute(0)' कॉल अनुमतित नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_RELATIVE\n"
"string.text"
msgid "Relative positioning is not allowed in this state."
msgstr "या अवस्थेत संबधित स्थिती स परवानगी दिली नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_REFESH_AFTERLAST\n"
"string.text"
msgid "A row cannot be refreshed when the ResultSet is positioned after the last row."
msgstr "जर ResultSet ची स्थिती शेवटच्या पंक्तिनंतर असेल तर त्या पंक्तिस अद्ययावत करू शकत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_MOVETOINSERTROW_CALLED\n"
"string.text"
msgid "A new row cannot be inserted when the ResultSet is not first moved to the insert row."
msgstr "जर ResultSetला अगोदर ओळ अंतर्भुत करा मध्ये हलविण्यात आले नाही तर एक नविन ओळ अंर्तभूत होऊ शकत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_UPDATEROW\n"
"string.text"
msgid "A row cannot be modified in this state"
msgstr "या अवस्थेत ओळीत सुधारणा अशक्य"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETEROW\n"
"string.text"
msgid "A row cannot be deleted in this state."
msgstr "या अवस्थेत ओळ काढून टाकणे शक्य नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_TABLE_RENAME\n"
"string.text"
msgid "The driver does not support table renaming."
msgstr "कोष्टकाला पुन्हा नाव देण्यास ड्राईवर आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_ALTER_COLUMN_DEF\n"
"string.text"
msgid "The driver does not support the modification of column descriptions."
msgstr "स्तंभ वर्णनात बदल करण्यास ड्राईवर आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_ALTER_BY_NAME\n"
"string.text"
msgid "The driver does not support the modification of column descriptions by changing the name."
msgstr "स्तंभाचे नाव बदलून स्तंभ वर्णनात बदल करण्यास ड्राईवर आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_ALTER_BY_INDEX\n"
"string.text"
msgid "The driver does not support the modification of column descriptions by changing the index."
msgstr "अनुक्रमणिका बदलून स्तंभ वर्णनात बदल करण्यास ड्राईवर आधार देत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FILE_DOES_NOT_EXIST\n"
"string.text"
msgid "The file \"$file$\" does not exist."
msgstr "\"$file$\" फाइल अस्तित्वात नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_TABLE_DOES_NOT_EXIST\n"
"string.text"
msgid "There exists no table named \"$table$\"."
msgstr "\"$table$\" नावाचे कोष्टक अस्तित्वात नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_QUERY_DOES_NOT_EXIST\n"
"string.text"
msgid "There exists no query named \"$table$\"."
msgstr "\"$table$\" नावाचे कोष्टक अस्तित्वात नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONFLICTING_NAMES\n"
"string.text"
msgid "There are tables in the database whose names conflict with the names of existing queries. To make full use of all queries and tables, make sure they have distinct names."
msgstr "माहितीकोशात कोष्टक आहेत ज्यांची नावे अस्तित्वात असणारया शंकाच्या नावाना विरोध करतात. सर्व शंकांचे आणि कोष्टकांचा संपूर्ण वापर करण्यासाठी, त्यांच्याकडे निराळी नावे असल्याची खात्री करा."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COMMAND_LEADING_TO_ERROR\n"
"string.text"
msgid ""
"The SQL command leading to this error is:\n"
"\n"
"$command$"
msgstr ""
"या दोषात परिणामित झाल्याने SQL आदेश आहे:\n"
"\n"
"$command$"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_STATEMENT_WITHOUT_RESULT_SET\n"
"string.text"
msgid "The SQL command does not describe a result set."
msgstr "SQL आदेश अहवाल संचाचे वर्णन करत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NAME_MUST_NOT_BE_EMPTY\n"
"string.text"
msgid "The name must not be empty."
msgstr "नाव रिक्त असायला नको."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_NULL_OBJECTS_IN_CONTAINER\n"
"string.text"
msgid "The container cannot contain NULL objects."
msgstr "कन्टेनर NULL वस्तूंना समाविष्ट करू शकत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NAME_ALREADY_USED\n"
"string.text"
msgid "There already is an object with the given name."
msgstr "दिलेल्या नावाने वस्तू आधीच आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_OBJECT_CONTAINER_MISMATCH\n"
"string.text"
msgid "This object cannot be part of this container."
msgstr "ही वस्तू या कन्टेनरचा भाग असू शकत नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_OBJECT_ALREADY_CONTAINED\n"
"string.text"
msgid "The object already is, with a different name, part of the container."
msgstr "वस्तू दुसरया नावाने, कन्टेनरचा भाग आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NAME_NOT_FOUND\n"
"string.text"
msgid "Unable to find the document '$name$'."
msgstr "दस्तऐवज '$name$' आढळले नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERROR_WHILE_SAVING\n"
"string.text"
msgid ""
"Could not save the document to $location$:\n"
"$message$"
msgstr ""
"दस्तऐवजाला $location$ स्थाळावर साठवणे अशक्य:\n"
"$message$"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_NO_SUCH_DATA_SOURCE\n"
"string.text"
msgid ""
"Error accessing data source '$name$':\n"
"$error$"
msgstr ""
"डाटा स्रोत '$name$' करीता प्रवेशवेळी त्रुटी:\n"
"$error$"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_SUB_FOLDER\n"
"string.text"
msgid "There exists no folder named \"$folder$\"."
msgstr "\"$folder$\" नावाचे फोल्डर अस्तित्वात नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETE_BEFORE_AFTER\n"
"string.text"
msgid "Cannot delete the before-first or after-last row."
msgstr "पहिलेच्या-अगोदर किंवा पहिलेच्या-नंतर ओळ नष्ट करणे अशक्य."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETE_INSERT_ROW\n"
"string.text"
msgid "Cannot delete the insert-row."
msgstr "अंतर्भूतीत-ओळ नष्ट करणे अशक्य."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_RESULT_IS_READONLY\n"
"string.text"
msgid "Result set is read only."
msgstr "परिणाम संच फक्त वाचणीय आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETE_PRIVILEGE\n"
"string.text"
msgid "DELETE privilege not available."
msgstr "DELETE हक्क अनुपलब्ध."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ROW_ALREADY_DELETED\n"
"string.text"
msgid "Current row is already deleted."
msgstr "विद्यमान ओळ आधीपासूनच नष्ट केली आहे."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_UPDATE_FAILED\n"
"string.text"
msgid "Current row could not be updated."
msgstr "सध्याची ओळ सुधारीत करणे अशक्य."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_INSERT_PRIVILEGE\n"
"string.text"
msgid "INSERT privilege not available."
msgstr "INSERT हक्क अनुपलब्ध."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INTERNAL_ERROR\n"
"string.text"
msgid "Internal error: no statement object provided by the database driver."
msgstr "आंतरीक त्रुटी: डाटाबेस ड्राइव्हरद्वारे कोणतिही वाक्यरचना वस्तु पुरवली नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_EXPRESSION1\n"
"string.text"
msgid "Expression1"
msgstr "एक्सप्रेशन1"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_SQL_COMMAND\n"
"string.text"
msgid "No SQL command was provided."
msgstr "SQL आदेश पुरवले नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INVALID_INDEX\n"
"string.text"
msgid "Invalid column index."
msgstr "अवैध स्तंभ अनुक्रम."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INVALID_CURSOR_STATE\n"
"string.text"
msgid "Invalid cursor state."
msgstr "अवैध कर्सर स्तर."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CURSOR_BEFORE_OR_AFTER\n"
"string.text"
msgid "The cursor points to before the first or after the last row."
msgstr "पहिल्या किंवा शेवटच्या ओळकरीता कर्सर पॉईंट्स्."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_BOOKMARK_BEFORE_OR_AFTER\n"
"string.text"
msgid "The rows before the first and after the last row don't have a bookmark."
msgstr "पहिल्या ओळच्या आधी व शेवटच्या ओळ नंतर वाचनखूण आढळले नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_BOOKMARK_DELETED\n"
"string.text"
msgid "The current row is deleted, and thus doesn't have a bookmark."
msgstr "विद्यमान ओळ नष्ट केले, व म्हणूनच वाचनखूण आढळले नाही."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONNECTION_REQUEST\n"
"string.text"
msgid "A connection for the following URL was requested \"$name$\"."
msgstr "खालील URL \"$name$\" करीता जोडणीची विनंती केली."

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_MISSING_EXTENSION\n"
"string.text"
msgid "The extension is not installed."
msgstr "एक्सटेंशन प्रतिष्ठापीत नाही."
