#. extracted from wizards/source/importwizard
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-10-14 14:26+0000\n"
"Last-Translator: paz Ohhashi <paz.ohhashi@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1444832804.000000\n"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sHelpButton\n"
"string.text"
msgid "~Help"
msgstr "ヘルプ(~H)"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sCancelButton\n"
"string.text"
msgid "~Cancel"
msgstr "キャンセル(~C)"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sBackButton\n"
"string.text"
msgid "<< ~Back"
msgstr "<< 戻る(~B)"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sNextButton\n"
"string.text"
msgid "Ne~xt >>"
msgstr "次へ(~X) >>"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sBeginButton\n"
"string.text"
msgid "~Convert"
msgstr "変換(~C)"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sCloseButton\n"
"string.text"
msgid "~Close"
msgstr "閉じる(~C)"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sWelcometextLabel1\n"
"string.text"
msgid "This wizard convert legacy format documents to Open Document Format for Office Applications."
msgstr "このウィザードはオフィスアプリケーション用に古いフォーマットのドキュメントを オープンドキュメントフォーマット(ODF)に変換します。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sWelcometextLabel3\n"
"string.text"
msgid "Select the document type for conversion:"
msgstr "変換するドキュメントの種類を選択します。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSTemplateCheckbox_1_\n"
"string.text"
msgid "Word templates"
msgstr "Word テンプレート"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSTemplateCheckbox_2_\n"
"string.text"
msgid "Excel templates"
msgstr "Excel テンプレート"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSTemplateCheckbox_3_\n"
"string.text"
msgid "PowerPoint templates"
msgstr "PowerPoint テンプレート"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSDocumentCheckbox_1_\n"
"string.text"
msgid "Word documents"
msgstr "Word ドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSDocumentCheckbox_2_\n"
"string.text"
msgid "Excel documents"
msgstr "Excel ドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSDocumentCheckbox_3_\n"
"string.text"
msgid "PowerPoint/Publisher documents"
msgstr "PowerPoint/Publisher ドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSearchInSubDir\n"
"string.text"
msgid "Including subdirectories"
msgstr "下位ディレクトリも含む"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMSContainerName\n"
"string.text"
msgid "Microsoft Office"
msgstr "Microsoft Office"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSummaryHeader\n"
"string.text"
msgid "Summary:"
msgstr "概要:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sTextImportLabel\n"
"string.text"
msgid "Import from:"
msgstr "インポート元:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sTextExportLabel\n"
"string.text"
msgid "Save to:"
msgstr "保存先:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sGroupnameDefault\n"
"string.text"
msgid "Imported_Templates"
msgstr "インポートしたテンプレート"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressDesc1\n"
"string.text"
msgid "Progress: "
msgstr "進捗状況: "

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressOneDoc\n"
"string.text"
msgid "Document"
msgstr "ドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressMoreDocs\n"
"string.text"
msgid "Documents"
msgstr "ドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressMoreTemplates\n"
"string.text"
msgid "Templates"
msgstr "テンプレート"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sNoDirCreation\n"
"string.text"
msgid "The '%1' directory cannot be created: "
msgstr "ディレクトリ '%1' は格納できません。 "

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMsgDirNotThere\n"
"string.text"
msgid "The '%1' directory does not exist."
msgstr "ディレクトリ '%1' はありません。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sQueryfornewCreation\n"
"string.text"
msgid "Do you want to create it now?"
msgstr "作成しますか？"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sFileExists\n"
"string.text"
msgid "The '<1>' file already exists.<CR>Do you want to overwrite it?"
msgstr "ファイル '<1>' はすでにあります。<CR>上書きしますか？"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sMorePathsError3\n"
"string.text"
msgid "Directories do not exist"
msgstr "ディレクトリはありません"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sOverwriteallFiles\n"
"string.text"
msgid "Do you want to overwrite documents without being asked?"
msgstr "確認なしでドキュメントに上書きしますか？"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sreeditMacro\n"
"string.text"
msgid "Document macro has to be revised."
msgstr "ドキュメントマクロを編集しなおす必要があります。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"scouldNotsaveDocument\n"
"string.text"
msgid "Document '<1>' could not be saved."
msgstr "ドキュメント '<1>' は保存できませんでした。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"scouldNotopenDocument\n"
"string.text"
msgid "Document '<1>' could not be opened."
msgstr "ドキュメント '<1>' を開くことができません。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sConvertError1\n"
"string.text"
msgid "Do you really want to terminate conversion at this point?"
msgstr "変換をここで中断しますか？"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sConvertError2\n"
"string.text"
msgid "Cancel Wizard"
msgstr "ウィザードを中止"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sRTErrorDesc\n"
"string.text"
msgid "An unexpected error has occurred in the wizard."
msgstr "ウィザードで予期されないエラーが発生しました。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sRTErrorHeader\n"
"string.text"
msgid "Error"
msgstr "エラー"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sPathDialogMessage\n"
"string.text"
msgid "Select a directory"
msgstr "ディレクトリを選択します。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sDialogTitle\n"
"string.text"
msgid "Document Converter"
msgstr "ドキュメント変換"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressPage1\n"
"string.text"
msgid "Progress"
msgstr "進捗状況"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressPage2\n"
"string.text"
msgid "Retrieving the relevant documents:"
msgstr "関連するドキュメントをまとめています:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressPage3\n"
"string.text"
msgid "Converting the documents"
msgstr "ドキュメントの変換"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressFound\n"
"string.text"
msgid "Found:"
msgstr "見つかりました:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sProgressPage5\n"
"string.text"
msgid "%1 found"
msgstr "%1 が見つかりました。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sReady\n"
"string.text"
msgid "Finished"
msgstr "完了"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSourceDocuments\n"
"string.text"
msgid "Source documents"
msgstr "ソースドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sTargetDocuments\n"
"string.text"
msgid "Target documents"
msgstr "ターゲットドキュメント"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sCreateLogfile\n"
"string.text"
msgid "Create log file"
msgstr "ログファイルの作成"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sLogfileSummary\n"
"string.text"
msgid "<COUNT> documents converted"
msgstr "<COUNT> のドキュメントを変換しました"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sLogfileHelpText\n"
"string.text"
msgid "A log file will be created in your work directory"
msgstr "ワーキングディレクトリにログファイルが格納されます。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sshowLogfile\n"
"string.text"
msgid "Show log file"
msgstr "Log file を表示"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumInclusiveSubDir\n"
"string.text"
msgid "All subdirectories will be taken into account"
msgstr "すべての下位ディレクトリが対象になります。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumSaveDokumente\n"
"string.text"
msgid "These will be exported to the following directory:"
msgstr "次のディレクトリにエクスポートされます｡"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumMSTextDocuments\n"
"string.text"
msgid "All Word documents contained in the following directory will be imported:"
msgstr "すべての Word ドキュメントは次のディレクトリからインポートされます。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumMSTableDocuments\n"
"string.text"
msgid "All Excel documents contained in the following directory will be imported:"
msgstr "すべての Excel ドキュメントは次のディレクトリからインポートされます。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumMSDrawDocuments\n"
"string.text"
msgid "All PowerPoint/Publisher documents contained in the following directory will be imported:"
msgstr "すべての PowerPoint/Publisher ドキュメントは次のディレクトリからインポートされます。"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumMSTextTemplates\n"
"string.text"
msgid "All Word templates contained in the following directory will be imported:"
msgstr "すべての Word テンプレートは次のディレクトリからインポートされます:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumMSTableTemplates\n"
"string.text"
msgid "All Excel templates contained in the following directory will be imported:"
msgstr "すべての Excel テンプレートは次のディレクトリからインポートされます:"

#: importwi.src
msgctxt ""
"importwi.src\n"
"sSumMSDrawTemplates\n"
"string.text"
msgid "All PowerPoint templates contained in the following directory will be imported:"
msgstr "すべての PowerPoint テンプレートは次のディレクトリからインポートされます:"
