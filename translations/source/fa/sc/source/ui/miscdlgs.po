#. extracted from sc/source/ui/miscdlgs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-05-12 11:33+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1431430420.000000\n"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_INSERT_COLS\n"
"string.text"
msgid "Column inserted"
msgstr "ستون درج شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_INSERT_ROWS\n"
"string.text"
msgid "Row inserted "
msgstr "ردیف درج شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_INSERT_TABS\n"
"string.text"
msgid "Sheet inserted "
msgstr "برگه درج شد "

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_DELETE_COLS\n"
"string.text"
msgid "Column deleted"
msgstr "ستون حذف شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_DELETE_ROWS\n"
"string.text"
msgid "Row deleted"
msgstr "ردیف حذف شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_DELETE_TABS\n"
"string.text"
msgid "Sheet deleted"
msgstr "برگه حذف شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_MOVE\n"
"string.text"
msgid "Range moved"
msgstr "محدوده منتقل شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_CONTENT\n"
"string.text"
msgid "Changed contents"
msgstr "محتویات تغییر یافت"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_CONTENT_WITH_CHILD\n"
"string.text"
msgid "Changed contents"
msgstr "محتویات تغییر یافت"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_CHILD_CONTENT\n"
"string.text"
msgid "Changed to "
msgstr "تغییر به "

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_CHILD_ORGCONTENT\n"
"string.text"
msgid "Original"
msgstr "اصلی"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_REJECT\n"
"string.text"
msgid "Changes rejected"
msgstr "تغییرات رد شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_ACCEPTED\n"
"string.text"
msgid "Accepted"
msgstr "تأیید شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_REJECTED\n"
"string.text"
msgid "Rejected"
msgstr "رد شد"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_NO_ENTRY\n"
"string.text"
msgid "No Entry"
msgstr "بدون ورودی"

#: acredlin.src
#, fuzzy
msgctxt ""
"acredlin.src\n"
"STR_CHG_EMPTY\n"
"string.text"
msgid "<empty>"
msgstr "<خالی>"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES\n"
"SC_CHANGES_COMMENT\n"
"menuitem.text"
msgid "Edit Comment..."
msgstr "ویرایش توضیجات..."

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_ACTION\n"
"menuitem.text"
msgid "Action"
msgstr "عمل"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_POSITION\n"
"menuitem.text"
msgid "Position"
msgstr "موقعیت"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_AUTHOR\n"
"menuitem.text"
msgid "Author"
msgstr "مؤلف"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_DATE\n"
"menuitem.text"
msgid "Date"
msgstr "تاریخ"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES.SC_SUB_SORT\n"
"SC_SORT_COMMENT\n"
"menuitem.text"
msgid "Description"
msgstr "شرح"

#: acredlin.src
msgctxt ""
"acredlin.src\n"
"RID_POPUP_CHANGES\n"
"SC_SUB_SORT\n"
"menuitem.text"
msgid "Sorting"
msgstr "مرتب‌سازی"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_TITLE_CONFLICT\n"
"string.text"
msgid "Conflict"
msgstr "تعارض"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_TITLE_AUTHOR\n"
"string.text"
msgid "Author"
msgstr "مؤلف"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_TITLE_DATE\n"
"string.text"
msgid "Date"
msgstr "تاریخ"

#: conflictsdlg.src
msgctxt ""
"conflictsdlg.src\n"
"STR_UNKNOWN_USER_CONFLICT\n"
"string.text"
msgid "Unknown User"
msgstr "کاربر ناشناخته"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_NOT_PROTECTED\n"
"string.text"
msgid "Not protected"
msgstr "محافظت نشده"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_NOT_PASS_PROTECTED\n"
"string.text"
msgid "Not password-protected"
msgstr "محافظت نشده با گذرواژه"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_HASH_BAD\n"
"string.text"
msgid "Hash incompatible"
msgstr "درهم‌سازی ناسازگار"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_HASH_GOOD\n"
"string.text"
msgid "Hash compatible"
msgstr "درهم‌سازی سازگار"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_HASH_REGENERATED\n"
"string.text"
msgid "Hash re-generated"
msgstr "درهم‌سازی دوباره تولید شد"

#: retypepassdlg.src
msgctxt ""
"retypepassdlg.src\n"
"STR_RETYPE\n"
"string.text"
msgid "Re-type"
msgstr "نوشتن مجدد"
