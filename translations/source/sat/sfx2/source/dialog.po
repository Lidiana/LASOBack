#. extracted from sfx2/source/dialog
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-12 01:12+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sat\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457745173.000000\n"

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"STR_RESET\n"
"string.text"
msgid "~Reset"
msgstr ""
"#-#-#-#-#  config.po (PACKAGE VERSION)  #-#-#-#-#\n"
"रिसेट (~R)\n"
"#-#-#-#-#  classes.po (PACKAGE VERSION)  #-#-#-#-#\n"
"दोहड़ा साजाव (~R)"

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"STR_APPLY\n"
"string.text"
msgid "Apply"
msgstr ""
"#-#-#-#-#  fldui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"लागू करें\n"
"#-#-#-#-#  dlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"बाहाल\n"
"#-#-#-#-#  framework.po (PACKAGE VERSION)  #-#-#-#-#\n"
"बाहाल"

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_TABPAGE_INVALIDNAME\n"
"string.text"
msgid "This name is already in use."
msgstr "नोवा ञुतुम माड़ाङ खोन बेभार रे मेनाक् आ ."

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_TABPAGE_INVALIDSTYLE\n"
"string.text"
msgid "This Style does not exist."
msgstr "नोवा हुना़र माड़ाङ खोन बा़नुक् आ ."

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"STR_TABPAGE_INVALIDPARENT\n"
"string.text"
msgid ""
"This Style cannot be used as a base Style,\n"
"because it would result in a recursive reference."
msgstr ""
"नोवा हना़र वा़सा़व हुना़र लेका बेभार बाङ दाड़ेयाक् आ ,\n"
" चेदाक् जे उलटा कोचे जोनोड़ रे  नोवा कुड़ायोक् आ ."

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_POOL_STYLE_NAME\n"
"string.text"
msgid ""
"Name already exists as a default Style.\n"
"Please choose another name."
msgstr ""
"नोवा हुना़र वा़यसा़व हुना़र लेका बेभार बाङ दाड़ेयाक् आ .\n"
" चेदाक् जे उलटा कोचे जोनोड़ रे नोवा कुड़ायोक् आ  ."

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_DELETE_STYLE_USED\n"
"string.text"
msgid ""
"One or more of the selected styles is in use in this document.\n"
"If you delete these styles, text will revert to the parent style.\n"
"Do you still wish to delete these styles?\n"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_DELETE_STYLE\n"
"string.text"
msgid "Styles in use: "
msgstr "Styles in use: "

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"MN_CONTEXT_TEMPLDLG\n"
"ID_NEW\n"
"menuitem.text"
msgid "New..."
msgstr "नावा (~N)..."

#: dialog.src
msgctxt ""
"dialog.src\n"
"MN_CONTEXT_TEMPLDLG\n"
"ID_EDIT\n"
"menuitem.text"
msgid "Modify..."
msgstr "बोदोल"

#: dialog.src
msgctxt ""
"dialog.src\n"
"MN_CONTEXT_TEMPLDLG\n"
"ID_HIDE\n"
"menuitem.text"
msgid "Hide"
msgstr "उकू (~e)उकु "

#: dialog.src
msgctxt ""
"dialog.src\n"
"MN_CONTEXT_TEMPLDLG\n"
"ID_SHOW\n"
"menuitem.text"
msgid "Show"
msgstr "उदुक्उदुक् "

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"MN_CONTEXT_TEMPLDLG\n"
"ID_DELETE\n"
"menuitem.text"
msgid "Delete..."
msgstr "मेटाव (~D)..."

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"SID_NAVIGATOR\n"
"string.text"
msgid "Navigator"
msgstr ""
"#-#-#-#-#  utlui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"पानतेयाक्\n"
"#-#-#-#-#  navipi.po (PACKAGE VERSION)  #-#-#-#-#\n"
"पानतेयिच् (~N)"

#: dialog.src
msgctxt ""
"dialog.src\n"
"SID_SIDEBAR\n"
"string.text"
msgid "Sidebar"
msgstr "Sidebar"

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_ERROR_WRONG_CONFIRM\n"
"string.text"
msgid "Faulty password confirmation"
msgstr "एड़ेयाक् दानाङ साबाद गोटाएड़ेयाक् दानाङ साबाद गोटा "

#: dialog.src
#, fuzzy
msgctxt ""
"dialog.src\n"
"STR_PDF_EXPORT_SEND\n"
"string.text"
msgid "Send"
msgstr "भेजा(~e)"

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_FONT_TABPAGE\n"
"string.text"
msgid "Font"
msgstr "फॉन्ट"

#: dialog.src
msgctxt ""
"dialog.src\n"
"STR_PREVIEW_CHECKBOX\n"
"string.text"
msgid "Show Previews"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_OPENDOC\n"
"menuitem.text"
msgid "Open..."
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_SAVEDOC\n"
"menuitem.text"
msgid "Save"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_UNDO\n"
"menuitem.text"
msgid "Undo"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_REDO\n"
"menuitem.text"
msgid "Redo"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_PRINTDOC\n"
"menuitem.text"
msgid "Print"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_MENUBAR\n"
"menuitem.text"
msgid "Menubar"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_OPTIONS\n"
"menuitem.text"
msgid "Options"
msgstr ""

#: dialog.src
msgctxt ""
"dialog.src\n"
"RID_MENU_NOTEBOOKBAR\n"
"SID_CLOSEDOC\n"
"menuitem.text"
msgid "Close"
msgstr ""

#: dinfdlg.src
#, fuzzy
msgctxt ""
"dinfdlg.src\n"
"STR_SFX_NEWOFFICEDOC\n"
"string.text"
msgid "%PRODUCTNAME document"
msgstr "%PRODUCTNAME दोलिल"

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Checked by\n"
"itemlist.text"
msgid "Checked by"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Client\n"
"itemlist.text"
msgid "Client"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Date completed\n"
"itemlist.text"
msgid "Date completed"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Department\n"
"itemlist.text"
msgid "Department"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Destinations\n"
"itemlist.text"
msgid "Destinations"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Disposition\n"
"itemlist.text"
msgid "Disposition"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Division\n"
"itemlist.text"
msgid "Division"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Document number\n"
"itemlist.text"
msgid "Document number"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Editor\n"
"itemlist.text"
msgid "Editor"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"E-Mail\n"
"itemlist.text"
msgid "E-Mail"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Forward to\n"
"itemlist.text"
msgid "Forward to"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Group\n"
"itemlist.text"
msgid "Group"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Info\n"
"itemlist.text"
msgid "Info"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Language\n"
"itemlist.text"
msgid "Language"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Mailstop\n"
"itemlist.text"
msgid "Mailstop"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Matter\n"
"itemlist.text"
msgid "Matter"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Office\n"
"itemlist.text"
msgid "Office"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Owner\n"
"itemlist.text"
msgid "Owner"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Project\n"
"itemlist.text"
msgid "Project"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Publisher\n"
"itemlist.text"
msgid "Publisher"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Purpose\n"
"itemlist.text"
msgid "Purpose"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Received from\n"
"itemlist.text"
msgid "Received from"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Recorded by\n"
"itemlist.text"
msgid "Recorded by"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Recorded date\n"
"itemlist.text"
msgid "Recorded date"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Reference\n"
"itemlist.text"
msgid "Reference"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Source\n"
"itemlist.text"
msgid "Source"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Status\n"
"itemlist.text"
msgid "Status"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Telephone number\n"
"itemlist.text"
msgid "Telephone number"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"Typist\n"
"itemlist.text"
msgid "Typist"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_CB_PROPERTY_STRINGARRAY\n"
"URL\n"
"itemlist.text"
msgid "URL"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_LB_PROPERTY_STRINGARRAY\n"
"Text\n"
"itemlist.text"
msgid "Text"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_LB_PROPERTY_STRINGARRAY\n"
"DateTime\n"
"itemlist.text"
msgid "DateTime"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_LB_PROPERTY_STRINGARRAY\n"
"Date\n"
"itemlist.text"
msgid "Date"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_LB_PROPERTY_STRINGARRAY\n"
"Duration\n"
"itemlist.text"
msgid "Duration"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_LB_PROPERTY_STRINGARRAY\n"
"Number\n"
"itemlist.text"
msgid "Number"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_LB_PROPERTY_STRINGARRAY\n"
"Yes or no\n"
"itemlist.text"
msgid "Yes or no"
msgstr ""

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"SFX_ST_DURATION_FORMAT\n"
"string.text"
msgid " Y: %1 M: %2 D: %3 H: %4 M: %5 S: %6"
msgstr "Y: %1 M: %2 D: %3 H: %4 M: %5 S: %6"

#: dinfdlg.src
msgctxt ""
"dinfdlg.src\n"
"STR_SFX_REMOVE_PROPERTY\n"
"string.text"
msgid "Remove Property"
msgstr "गुन को साहाय मे"

#: dinfdlg.src
#, fuzzy
msgctxt ""
"dinfdlg.src\n"
"STR_SFX_QUERY_WRONG_TYPE\n"
"string.text"
msgid ""
"The value entered does not match the specified type.\n"
"The value will be stored as text."
msgstr ""
"गोनोङ आदेराक् गोटावाक् लेकान सांव बाय मेलोक् काना.\n"
" गोनोङ ओनोल लेका जोगाक् आ."

#: filedlghelper.src
msgctxt ""
"filedlghelper.src\n"
"STR_SFX_FILEDLG_ACTUALVERSION\n"
"string.text"
msgid "Current version"
msgstr "नितोक् हा़लियाक्नितो हा़लियाक् "

#: filedlghelper.src
#, fuzzy
msgctxt ""
"filedlghelper.src\n"
"STR_SFX_EXPLORERFILE_EXPORT\n"
"string.text"
msgid "Export"
msgstr ""
"#-#-#-#-#  doc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"बाहरे भेजा\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"बाहरे भेजा\n"
"#-#-#-#-#  xsltdialog.po (PACKAGE VERSION)  #-#-#-#-#\n"
"बाहरे भेजा"

#: filedlghelper.src
#, fuzzy
msgctxt ""
"filedlghelper.src\n"
"STR_SFX_EXPLORERFILE_INSERT\n"
"string.text"
msgid "Insert"
msgstr ""
"#-#-#-#-#  ribbar.po (PACKAGE VERSION)  #-#-#-#-#\n"
"सोगे\n"
"#-#-#-#-#  dialog.po (PACKAGE VERSION)  #-#-#-#-#\n"
"भितिर\n"
"#-#-#-#-#  fldui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"जोड़ें\n"
"#-#-#-#-#  appl.po (PACKAGE VERSION)  #-#-#-#-#\n"
"सोगे\n"
"#-#-#-#-#  formwizard.po (PACKAGE VERSION)  #-#-#-#-#\n"
"आदेर\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"सोगे\n"
"#-#-#-#-#  basicide.po (PACKAGE VERSION)  #-#-#-#-#\n"
"सोगे"

#: filedlghelper.src
#, fuzzy
msgctxt ""
"filedlghelper.src\n"
"STR_SFX_EXPLORERFILE_BUTTONINSERT\n"
"string.text"
msgid "~Insert"
msgstr ""
"#-#-#-#-#  envelp.po (PACKAGE VERSION)  #-#-#-#-#\n"
"सेलेद (~I)\n"
"#-#-#-#-#  index.po (PACKAGE VERSION)  #-#-#-#-#\n"
" आदेर (~I)\n"
"#-#-#-#-#  fldui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"जोड़ें (~I)\n"
"#-#-#-#-#  misc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"आदेर (~I)\n"
"#-#-#-#-#  schedule.po (PACKAGE VERSION)  #-#-#-#-#\n"
" आदेर (~I)"

#: filedlghelper.src
msgctxt ""
"filedlghelper.src\n"
"STR_SFX_IMPORT_ALL\n"
"string.text"
msgid "<All formats>"
msgstr "<जोतो तेयार को >"

#: filedlghelper.src
msgctxt ""
"filedlghelper.src\n"
"STR_PB_SAVEACOPY\n"
"string.text"
msgid "Save a Copy"
msgstr "Save a Copy"

#: newstyle.src
msgctxt ""
"newstyle.src\n"
"STR_QUERY_OVERWRITE\n"
"string.text"
msgid "Style already exists. Overwrite?"
msgstr "हुना़र माड़ाङ खोन मेनाक् आ ओल चेतान ओल ?"

#: recfloat.src
msgctxt ""
"recfloat.src\n"
"STR_MACRO_LOSS\n"
"string.text"
msgid "Do you really want to cancel the recording? Any steps recorded up to this point will be lost."
msgstr "चेत् आम सा़री गे रिकार्ड बा़डरा सानाम काना ? नोवा टुड़ाक् हा़विच जोतो रिकोर्ड आदोक् आ .चेत् आम सा़री गे रिकार्ड बा़डरा सानाम काना ? नोवा टुड़ाक् हा़विच जोतो रिकोर्ड आदोक् आ ."

#: recfloat.src
msgctxt ""
"recfloat.src\n"
"STR_CANCEL_RECORDING\n"
"string.text"
msgid "Cancel Recording"
msgstr "रेकोर्ड बा़डरारेकोर्ड बा़डरा "

#: taskpane.src
msgctxt ""
"taskpane.src\n"
"STR_SFX_DOCK\n"
"string.text"
msgid "Dock"
msgstr "टोपे"

#: taskpane.src
msgctxt ""
"taskpane.src\n"
"STR_SFX_UNDOCK\n"
"string.text"
msgid "Undock"
msgstr "टोपे ओचोक्"

#: templdlg.src
#, fuzzy
msgctxt ""
"templdlg.src\n"
"STR_STYLE_ELEMTLIST\n"
"string.text"
msgid "Style List"
msgstr ""
"#-#-#-#-#  index.po (PACKAGE VERSION)  #-#-#-#-#\n"
"हुना़र\n"
"#-#-#-#-#  doc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"हुना़र \n"
"#-#-#-#-#  dialog.po (PACKAGE VERSION)  #-#-#-#-#\n"
"शैलियाँ"

#: templdlg.src
msgctxt ""
"templdlg.src\n"
"STR_STYLE_FILTER_HIERARCHICAL\n"
"string.text"
msgid "Hierarchical"
msgstr "थार लेकाते (~w)"

#: versdlg.src
msgctxt ""
"versdlg.src\n"
"STR_VIEWVERSIONCOMMENT\n"
"string.text"
msgid "View Version Comment"
msgstr ""

#: versdlg.src
msgctxt ""
"versdlg.src\n"
"STR_NO_NAME_SET\n"
"string.text"
msgid "(no name set)"
msgstr ""
