#. extracted from chart2/source/controller/dialogs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-02 02:44+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sat\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1462157075.000000\n"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DLG_CHART_WIZARD\n"
"string.text"
msgid "Chart Wizard"
msgstr "चार्ट विजार्ड"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DLG_SMOOTH_LINE_PROPERTIES\n"
"string.text"
msgid "Smooth Lines"
msgstr "चिंकांड़ गार को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DLG_STEPPED_LINE_PROPERTIES\n"
"string.text"
msgid "Stepped Lines"
msgstr "Stepped Lines"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_CHARTTYPE\n"
"string.text"
msgid "Chart Type"
msgstr "चार्ट रेकोमचार्ट लेकान"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_DATA_RANGE\n"
"string.text"
msgid "Data Range"
msgstr "पासनाव सा़खिया़त पासनाव डाटा "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_CHART_ELEMENTS\n"
"string.text"
msgid "Chart Elements"
msgstr "चार्ट जिनिस को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_CHART_LOCATION\n"
"string.text"
msgid "Chart Location"
msgstr "चार्ट जायगा ठांव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_LINE\n"
"string.text"
msgid "Line"
msgstr "गार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_BORDER\n"
"string.text"
msgid "Borders"
msgstr "सीमा धारेसिमा़ धारे"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_AREA\n"
"string.text"
msgid "Area"
msgstr "जायगा, ठांवझायगा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_TRANSPARENCY\n"
"string.text"
msgid "Transparency"
msgstr "ञेल पारोमाक्ञेल पारोमाक्  "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_FONT\n"
"string.text"
msgid "Font"
msgstr "चिकी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_FONT_EFFECTS\n"
"string.text"
msgid "Font Effects"
msgstr "चिकी पोरभावफोन्ट पोरभाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_NUMBERS\n"
"string.text"
msgid "Numbers"
msgstr "लेखालेखा को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_POSITION\n"
"string.text"
msgid "Position"
msgstr "टेपओबोसता"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_LAYOUT\n"
"string.text"
msgid "Layout"
msgstr "ओ़डो़क् ञेलोक्उयहार लेका ते तेयार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_OPTIONS\n"
"string.text"
msgid "Options"
msgstr "एटागाक्आपनार मोने तेयाक्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_SCALE\n"
"string.text"
msgid "Scale"
msgstr "नापाक्नानापाक्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_POSITIONING\n"
"string.text"
msgid "Positioning"
msgstr "मोहोर रेयाक् टेपओबोसताक् आ"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_TRENDLINE_TYPE\n"
"string.text"
msgid "Type"
msgstr ""
"#-#-#-#-#  utlui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"नोकोल\n"
"#-#-#-#-#  config.po (PACKAGE VERSION)  #-#-#-#-#\n"
"लेकान\n"
"#-#-#-#-#  dbui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"नोकोल\n"
"#-#-#-#-#  index.po (PACKAGE VERSION)  #-#-#-#-#\n"
"नोकोल\n"
"#-#-#-#-#  fmtui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"लेकान\n"
"#-#-#-#-#  chrdlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"नोकोल\n"
"#-#-#-#-#  misc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"नोकोल\n"
"#-#-#-#-#  basicide.po (PACKAGE VERSION)  #-#-#-#-#\n"
"टाइप ओल\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"रोकोम\n"
"#-#-#-#-#  xsltdialog.po (PACKAGE VERSION)  #-#-#-#-#\n"
"रेकोम"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_XERROR_BARS\n"
"string.text"
msgid "X Error Bars"
msgstr "Y भुल बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_YERROR_BARS\n"
"string.text"
msgid "Y Error Bars"
msgstr "Y भुल बार"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_ZERROR_BARS\n"
"string.text"
msgid "Z Error Bars"
msgstr "Y भुल बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_ALIGNMENT\n"
"string.text"
msgid "Alignment"
msgstr "थार होचोसाजाव होचो "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_PERSPECTIVE\n"
"string.text"
msgid "Perspective"
msgstr "ञेनेल उमुल"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_APPEARANCE\n"
"string.text"
msgid "Appearance"
msgstr "ओडोक् ञेलओडोक ञेल"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_ILLUMINATION\n"
"string.text"
msgid "Illumination"
msgstr "चामकावचोमकाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_ASIAN\n"
"string.text"
msgid "Asian Typography"
msgstr "एसियान  चिता़र"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AVERAGE_LINE_WITH_PARAMETERS\n"
"string.text"
msgid "Mean value line with value %AVERAGE_VALUE and standard deviation %STD_DEVIATION"
msgstr "गोनोङ सांव बाराबारी गोनोङ गार %AVERAGE_VALUE आर एटाक् सेत् चालाव नाप %STD_DEVIATIONगोनोङ सांव बाराबारी गोनोङ गार %AVERAGE_VALUE आर एटाक् सेत् चालाव नाप %STD_DEVIATION"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS\n"
"string.text"
msgid "Axis"
msgstr "निघा़ गार को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS_X\n"
"string.text"
msgid "X Axis"
msgstr "X निघा़ गार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS_Y\n"
"string.text"
msgid "Y Axis"
msgstr "Y निघा़ गार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS_Z\n"
"string.text"
msgid "Z Axis"
msgstr "Z निघा़ गार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_SECONDARY_X_AXIS\n"
"string.text"
msgid "Secondary X Axis"
msgstr "दोसाराक् X धुरी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_SECONDARY_Y_AXIS\n"
"string.text"
msgid "Secondary Y Axis"
msgstr "दोसाराक् Y धुरी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXES\n"
"string.text"
msgid "Axes"
msgstr "निघा़ गार कोधुरी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRIDS\n"
"string.text"
msgid "Grids"
msgstr "जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID\n"
"string.text"
msgid "Grid"
msgstr "जांगलाजांगला "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MAJOR_X\n"
"string.text"
msgid "X Axis Major Grid"
msgstr "X निघा़ गार माराङ जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MAJOR_Y\n"
"string.text"
msgid "Y Axis Major Grid"
msgstr "Y निघा़ गार माराङ जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MAJOR_Z\n"
"string.text"
msgid "Z Axis Major Grid"
msgstr "Z निघा़ गार माराङ जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MINOR_X\n"
"string.text"
msgid "X Axis Minor Grid"
msgstr "X निघा़ गार हुडिञ जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MINOR_Y\n"
"string.text"
msgid "Y Axis Minor Grid"
msgstr "Y निघा़ गार हुडिञ जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MINOR_Z\n"
"string.text"
msgid "Z Axis Minor Grid"
msgstr "Z निघा़ गार हुडिञ जांगला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_LEGEND\n"
"string.text"
msgid "Legend"
msgstr "गामकाहनी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE\n"
"string.text"
msgid "Title"
msgstr "ञुतुमएम ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLES\n"
"string.text"
msgid "Titles"
msgstr "ञुतुममुल ञुतुम को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_MAIN\n"
"string.text"
msgid "Main Title"
msgstr "मुल ञुतुममुल ञुतुम "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_SUB\n"
"string.text"
msgid "Subtitle"
msgstr "एटागाक् ञुतुमखुंट ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_X_AXIS\n"
"string.text"
msgid "X Axis Title"
msgstr "X धुरी ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_Y_AXIS\n"
"string.text"
msgid "Y Axis Title"
msgstr "Y धुरी ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_Z_AXIS\n"
"string.text"
msgid "Z Axis Title"
msgstr "Z धुरी ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_SECONDARY_X_AXIS\n"
"string.text"
msgid "Secondary X Axis Title"
msgstr "दोसाराक् X निघा़ गार ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_SECONDARY_Y_AXIS\n"
"string.text"
msgid "Secondary Y Axis Title"
msgstr "दोसाराक् Y निघा़ गार ञुतुम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_LABEL\n"
"string.text"
msgid "Label"
msgstr "चिखना़"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATALABELS\n"
"string.text"
msgid "Data Labels"
msgstr "सा़खिया़त चिखना़ लागावडाटा चिखना़ लागाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATAPOINT\n"
"string.text"
msgid "Data Point"
msgstr "सा़खिया़त का़नीडाटा का़नी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATAPOINTS\n"
"string.text"
msgid "Data Points"
msgstr "सा़खिया़त का़नी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_LEGEND_SYMBOL\n"
"string.text"
msgid "Legend Key"
msgstr "गाम काहनी का़ठीगाम काहनी का़ठी "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATASERIES\n"
"string.text"
msgid "Data Series"
msgstr "सा़खिया़त लेताड़सा़खिया़त थार लेखा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATASERIES_PLURAL\n"
"string.text"
msgid "Data Series"
msgstr "सा़खिया़त लेताड़सा़खिया़त थार लेखा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVE\n"
"string.text"
msgid "Trend Line"
msgstr "गाम का़हनी गारगाम का़हनी गार "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVES\n"
"string.text"
msgid "Trend Lines"
msgstr "गाम का़हनी गार को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVE_WITH_PARAMETERS\n"
"string.text"
msgid "Trend line %FORMULA with accuracy R² = %RSQUARED"
msgstr "गाम काहनी गार  %FORMULA  सांव बेस उता़र R² = %RSQUARED"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_MOVING_AVERAGE_WITH_PARAMETERS\n"
"string.text"
msgid "Moving average trend line with period = %PERIOD"
msgstr ""

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AVERAGE_LINE\n"
"string.text"
msgid "Mean Value Line"
msgstr "बारबारी गोनोङ गार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVE_EQUATION\n"
"string.text"
msgid "Equation"
msgstr "सोमान सोमान होचो"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_ERROR_BARS_X\n"
"string.text"
msgid "X Error Bars"
msgstr "Y भुल बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_ERROR_BARS_Y\n"
"string.text"
msgid "Y Error Bars"
msgstr "Y भुल बार"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_ERROR_BARS_Z\n"
"string.text"
msgid "Z Error Bars"
msgstr "Y भुल बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_STOCK_LOSS\n"
"string.text"
msgid "Stock Loss"
msgstr "जोमा हान"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_STOCK_GAIN\n"
"string.text"
msgid "Stock Gain"
msgstr "जोमा लाब"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_PAGE\n"
"string.text"
msgid "Chart Area"
msgstr "चार्ट जायगा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DIAGRAM\n"
"string.text"
msgid "Chart"
msgstr "चार्टचार्ट रे"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DIAGRAM_WALL\n"
"string.text"
msgid "Chart Wall"
msgstr "चार्ट भित"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DIAGRAM_FLOOR\n"
"string.text"
msgid "Chart Floor"
msgstr "चार्ट सा़ड़िम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_SHAPE\n"
"string.text"
msgid "Drawing Object"
msgstr "चिता़र जिनिस "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATASERIES\n"
"string.text"
msgid "Data Series '%SERIESNAME'"
msgstr "डाटा थार  '%SERIESNAME'"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATAPOINT_INDEX\n"
"string.text"
msgid "Data Point %POINTNUMBER"
msgstr "डाटा का़नी %POINTNUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATAPOINT_VALUES\n"
"string.text"
msgid "Values: %POINTVALUES"
msgstr "गोनोङ : %POINTVALUES"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATAPOINT\n"
"string.text"
msgid "Data Point %POINTNUMBER, data series %SERIESNUMBER, values: %POINTVALUES"
msgstr "डाटा का़नी  %POINTNUMBER, डाटा थार  %SERIESNUMBER, गोनोङ को : %POINTVALUES"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_STATUS_DATAPOINT_MARKED\n"
"string.text"
msgid "Data point %POINTNUMBER in data series %SERIESNUMBER selected, values: %POINTVALUES"
msgstr "डाटा का़नी   %POINTNUMBER रे डाटा थार  %SERIESNUMBER बाछाव ,गानोङ : %POINTVALUESडाटा का़नी   %POINTNUMBER रे डाटा थार  %SERIESNUMBER बाछाव ,गानोङ : %POINTVALUES"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_STATUS_OBJECT_MARKED\n"
"string.text"
msgid "%OBJECTNAME selected"
msgstr "%OBJECTNAME बाछाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_STATUS_PIE_SEGMENT_EXPLODED\n"
"string.text"
msgid "Pie exploded by %PERCENTVALUE percent"
msgstr "%PERCENTVALUE सायाक् दाराय ते सोड़े पोसाक् एना"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_FOR_SERIES\n"
"string.text"
msgid "%OBJECTNAME for Data Series '%SERIESNAME'"
msgstr "%OBJECTNAME सा़खिया़त थार लेखा ला़गित् '%SERIESNAME'"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_FOR_ALL_SERIES\n"
"string.text"
msgid "%OBJECTNAME for all Data Series"
msgstr "%OBJECTNAME जोतो सा़खिया़त थार लेखा ला़गित्%OBJECTNAME जोतो सा़खिया़त थार लेखा ला़गित् "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_CHARTTYPE\n"
"string.text"
msgid "Edit chart type"
msgstr "चार्ट रेकोम सासापड़ाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_DATA_RANGES\n"
"string.text"
msgid "Edit data ranges"
msgstr "डाटा पासनाव को सासापड़ाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_3D_VIEW\n"
"string.text"
msgid "Edit 3D view"
msgstr "3D ञेनेल सासापड़ाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_CHART_DATA\n"
"string.text"
msgid "Edit chart data"
msgstr "चार्ट डाटा सासापड़ाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_TOGGLE_LEGEND\n"
"string.text"
msgid "Legend on/off"
msgstr "गाम काहनी एहोब/बोंद"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_TOGGLE_GRID_HORZ\n"
"string.text"
msgid "Horizontal grid major/major&minor/off"
msgstr ""

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_TOGGLE_GRID_VERTICAL\n"
"string.text"
msgid "Vertical grid major/major&minor/off"
msgstr ""

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_SCALE_TEXT\n"
"string.text"
msgid "Scale Text"
msgstr "ओनोल नापओनोल नाप "

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_REARRANGE_CHART\n"
"string.text"
msgid "Automatic Layout"
msgstr "आच् ते सालाक्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_NOTPOSSIBLE\n"
"string.text"
msgid "This function cannot be completed with the selected objects."
msgstr "नोवा का़मी बाङ चालाव दाड़ेयाक् बाछाव जिनिस सांव ते."

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_TEXT\n"
"string.text"
msgid "Edit text"
msgstr "ओनोल आदेर"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_COLUMN_LABEL\n"
"string.text"
msgid "Column %COLUMNNUMBER"
msgstr "कांधा %COLUMNNUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ROW_LABEL\n"
"string.text"
msgid "Row %ROWNUMBER"
msgstr "थार %ROWNUMBERथार  %ROWNUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_LABEL\n"
"string.text"
msgid "Name"
msgstr "ञुतुमञुतुम."

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X\n"
"string.text"
msgid "X-Values"
msgstr "गोनोङ"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y\n"
"string.text"
msgid "Y-Values"
msgstr "गोनोङ"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_SIZE\n"
"string.text"
msgid "Bubble Sizes"
msgstr "भुंबुक् माराङ तेत्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X_ERROR\n"
"string.text"
msgid "X-Error-Bars"
msgstr "भुल-बार(-E) (~a)"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X_ERROR_POSITIVE\n"
"string.text"
msgid "Positive X-Error-Bars"
msgstr "गानोगाक् X-भुल-बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X_ERROR_NEGATIVE\n"
"string.text"
msgid "Negative X-Error-Bars"
msgstr "बाङ गानोगाक्  भुल-बार को(-E)"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y_ERROR\n"
"string.text"
msgid "Y-Error-Bars"
msgstr "Y-भुल-बार (~i)"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y_ERROR_POSITIVE\n"
"string.text"
msgid "Positive Y-Error-Bars"
msgstr "गानोगाक् Y-भुल-बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y_ERROR_NEGATIVE\n"
"string.text"
msgid "Negative Y-Error-Bars"
msgstr "बाङ गानोगाक् Y-भुल-बार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_FIRST\n"
"string.text"
msgid "Open Values"
msgstr "झिज गोनोङ को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_LAST\n"
"string.text"
msgid "Close Values"
msgstr "बोंद गोनोङ को (~v)"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_MIN\n"
"string.text"
msgid "Low Values"
msgstr "लातार गोनोङ को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_MAX\n"
"string.text"
msgid "High Values"
msgstr "बाड़ती गोनोङ को"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_CATEGORIES\n"
"string.text"
msgid "Categories"
msgstr "हा़टिञ रे तेयारहा़टिञ को."

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_UNNAMED_SERIES\n"
"string.text"
msgid "Unnamed Series"
msgstr "बाङ ञुतुमाक् थार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_UNNAMED_SERIES_WITH_INDEX\n"
"string.text"
msgid "Unnamed Series %NUMBER"
msgstr "बाङ ञुतुमाक् थार %NUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_SELECT_RANGE_FOR_SERIES\n"
"string.text"
msgid "Select Range for %VALUETYPE of %SERIESNAME"
msgstr "%VALUETYPE रेयाक् %SERIESNAME ञुतुम ला़गित् पासनाव बाछाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_SELECT_RANGE_FOR_CATEGORIES\n"
"string.text"
msgid "Select Range for Categories"
msgstr "हा़टिञ को ला़गित् पासनाव बाछाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_SELECT_RANGE_FOR_DATALABELS\n"
"string.text"
msgid "Select Range for data labels"
msgstr "डाटा चिखना ला़गित् पासनाव बाछाव"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_EDITOR_INCORRECT_INPUT\n"
"string.text"
msgid ""
"Your last input is incorrect.\n"
"Ignore this change and close the dialog?"
msgstr ""
"आमाक् मुचा़त् आदेर बाङ सुहिया.\n"
" चेत् नोवा बोदोल आड़ाक् गिडी आर काथाम बोंदा?"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_TEXT_DIRECTION_LTR\n"
"string.text"
msgid "Left-to-right"
msgstr ""
"#-#-#-#-#  frmdlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"बांये से दायें\n"
"#-#-#-#-#  src.po (PACKAGE VERSION)  #-#-#-#-#\n"
"लेंगा-खोन-जोजोम"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_TEXT_DIRECTION_RTL\n"
"string.text"
msgid "Right-to-left"
msgstr ""
"#-#-#-#-#  table.po (PACKAGE VERSION)  #-#-#-#-#\n"
"जोजोम  खोन लेंगा\n"
"#-#-#-#-#  frmdlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"दायें से बांयें"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TEXT_DIRECTION_SUPER\n"
"string.text"
msgid "Use superordinate object settings"
msgstr "हातावक् ओसारमाराङ आरी जिनिस साजाव को बेभार"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PROPERTY_ROLE_FILLCOLOR\n"
"string.text"
msgid "Fill Color"
msgstr "रोङ पेरेच्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PROPERTY_ROLE_BORDERCOLOR\n"
"string.text"
msgid "Border Color"
msgstr "सीमा़ धारे रोङ"

#: Strings_AdditionalControls.src
msgctxt ""
"Strings_AdditionalControls.src\n"
"STR_TEXT_SEPARATOR\n"
"string.text"
msgid "Separator"
msgstr "भेगाराक्भेनेगार"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_COLUMN\n"
"string.text"
msgid "Column"
msgstr "कांधामित् कांधा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_BAR\n"
"string.text"
msgid "Bar"
msgstr "मित् बार"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_AREA\n"
"string.text"
msgid "Area"
msgstr "जायगा, ठांवझायगा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_PIE\n"
"string.text"
msgid "Pie"
msgstr "सोड़े (~u)सोड़े  "

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_PIE_EXPLODED\n"
"string.text"
msgid "Exploded Pie Chart"
msgstr "सोड़े चार्ट पोसाक्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_DONUT_EXPLODED\n"
"string.text"
msgid "Exploded Donut Chart"
msgstr "टोटे का़मी चार्ट पोसाक्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_DONUT\n"
"string.text"
msgid "Donut"
msgstr "टोटे का़मी"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_LINE\n"
"string.text"
msgid "Line"
msgstr "गार"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_XY\n"
"string.text"
msgid "XY (Scatter)"
msgstr "XY (छित् या़व )"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_POINTS_AND_LINES\n"
"string.text"
msgid "Points and Lines"
msgstr "टुडा़क् आर गार को"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_POINTS_ONLY\n"
"string.text"
msgid "Points Only"
msgstr "एकेन टुडा़क्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINES_ONLY\n"
"string.text"
msgid "Lines Only"
msgstr "गार को एसकार"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINES_3D\n"
"string.text"
msgid "3D Lines"
msgstr "3D गार को3D गार को "

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_COMBI_COLUMN_LINE\n"
"string.text"
msgid "Column and Line"
msgstr "कांधा आर गार"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINE_COLUMN\n"
"string.text"
msgid "Columns and Lines"
msgstr "कांधा आर गार को"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINE_STACKEDCOLUMN\n"
"string.text"
msgid "Stacked Columns and Lines"
msgstr "कांधा आर गार को डांग जारवा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_NET\n"
"string.text"
msgid "Net"
msgstr "रेडियो तोंतरो"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_STOCK\n"
"string.text"
msgid "Stock"
msgstr "लाठा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_1\n"
"string.text"
msgid "Stock Chart 1"
msgstr "जोमा चार्ट 1"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_2\n"
"string.text"
msgid "Stock Chart 2"
msgstr "जोमा चार्ट 2"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_3\n"
"string.text"
msgid "Stock Chart 3"
msgstr "जोमा चार्ट 3"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_4\n"
"string.text"
msgid "Stock Chart 4"
msgstr " जोमा चार्ट 4"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_NORMAL\n"
"string.text"
msgid "Normal"
msgstr "साधारोनसाधरोन"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STACKED\n"
"string.text"
msgid "Stacked"
msgstr "मुरा़यजोगाव जारवा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_PERCENT\n"
"string.text"
msgid "Percent Stacked"
msgstr "सायाक् बोझा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_DEEP\n"
"string.text"
msgid "Deep"
msgstr "गा़हिर"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_FILLED\n"
"string.text"
msgid "Filled"
msgstr "पेरेजाक्पेरेच् आकाना"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_BUBBLE\n"
"string.text"
msgid "Bubble"
msgstr "भुंबुक्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_BUBBLE_1\n"
"string.text"
msgid "Bubble Chart"
msgstr "भुंबुक् चार्ट"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_GL3D_BAR\n"
"string.text"
msgid "GL3D Bar"
msgstr ""

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_GL3D_BAR\n"
"string.text"
msgid "GL3D Bar Chart"
msgstr ""

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_INVALID_NUMBER\n"
"string.text"
msgid "Numbers are required. Check your input."
msgstr "लेखा को ला़कती. आमाक् आदेर ञेल मे."

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_STEP_GT_ZERO\n"
"string.text"
msgid "The major interval requires a positive number. Check your input."
msgstr "माराङ फांक सेलेदोगाक् लेखा ला़कती याक् आ. आमाक् आदेर ञेल मे."

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_BAD_LOGARITHM\n"
"string.text"
msgid "The logarithmic scale requires positive numbers. Check your input."
msgstr "हुडिञ एलेख नाप सेलेदोगाक् लेखा ला़कती याक् आ. आमाक् आदेर ञेल मे."

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_MIN_GREATER_MAX\n"
"string.text"
msgid "The minimum must be lower than the maximum. Check your input."
msgstr "कोम उता़राक् ढेर उता़राक् खोन कोम हुयुक् ला़कती. आमाक् आदेर ञेल मे."

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_INVALID_INTERVALS\n"
"string.text"
msgid "The major interval needs to be greater than the minor interval. Check your input."
msgstr "माराङ टांवहें हुडिञ टांवहें खोन माराङ हुयुक् ला़कती. आमाक् आदेर ञेल मे."

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_INVALID_TIME_UNIT\n"
"string.text"
msgid "The major and minor interval need to be greater or equal to the resolution. Check your input."
msgstr "माराङ आर हुडिञ टांवहें गोटा तेत् रे माराङ आर बाङ सोमान हुयुक् ला़कती. आमाक् आदेर ञेल मे.माराङ आर हुडिञ टांवहें गोटा तेत् रे माराङ आर बाङ सोमान हुयुक् ला़कती. आमाक् आदेर ञेल मे."

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_INDICATE_BOTH\n"
"string.text"
msgid "Negative and Positive"
msgstr "कोमोगाक् आर ढेरोगाक्"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_INDICATE_DOWN\n"
"string.text"
msgid "Negative"
msgstr "कोमोगाक्"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_INDICATE_UP\n"
"string.text"
msgid "Positive"
msgstr "टेप"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_CONTROLTEXT_ERROR_BARS_FROM_DATA\n"
"string.text"
msgid "From Data Table"
msgstr "डाटा टेबुल खोन"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_LINEAR\n"
"string.text"
msgid "Linear"
msgstr "Linear"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_LOG\n"
"string.text"
msgid "Logarithmic"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_EXP\n"
"string.text"
msgid "Exponential"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_POWER\n"
"string.text"
msgid "Power"
msgstr "बिजली (~i)"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_POLYNOMIAL\n"
"string.text"
msgid "Polynomial"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_MOVING_AVERAGE\n"
"string.text"
msgid "Moving average"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_MEAN\n"
"string.text"
msgid "Mean"
msgstr "ताला रेयाक्"
