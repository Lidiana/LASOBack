#. extracted from extensions/source/dbpilots
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2014-11-30 12:19+0000\n"
"Last-Translator: Alan <alan.monfort@free.fr>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: br\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1417349960.000000\n"

#: commonpagesdbp.src
msgctxt ""
"commonpagesdbp.src\n"
"RID_STR_OPTION_DB_FIELD_TITLE\n"
"string.text"
msgid "Database Field"
msgstr "Maezienn stlennvon"

#: commonpagesdbp.src
msgctxt ""
"commonpagesdbp.src\n"
"RID_STR_TYPE_TABLE\n"
"string.text"
msgid "Table"
msgstr "Taolenn"

#: commonpagesdbp.src
msgctxt ""
"commonpagesdbp.src\n"
"RID_STR_TYPE_QUERY\n"
"string.text"
msgid "Query"
msgstr "Azgoulenn"

#: commonpagesdbp.src
msgctxt ""
"commonpagesdbp.src\n"
"RID_STR_TYPE_COMMAND\n"
"string.text"
msgid "SQL command"
msgstr "Ditour SQL"

#: dbpilots.src
msgctxt ""
"dbpilots.src\n"
"RID_STR_GROUPWIZARD_TITLE\n"
"string.text"
msgid "Group Element Wizard"
msgstr "Skoazeller an elfenn strollad"

#: dbpilots.src
msgctxt ""
"dbpilots.src\n"
"RID_STR_GRIDWIZARD_TITLE\n"
"string.text"
msgid "Table Element Wizard"
msgstr "Skoazeller an elfenn daolenn"

#: dbpilots.src
msgctxt ""
"dbpilots.src\n"
"RID_STR_LISTWIZARD_TITLE\n"
"string.text"
msgid "List Box Wizard"
msgstr "Skoazeller ar maez dre roll"

#: dbpilots.src
msgctxt ""
"dbpilots.src\n"
"RID_STR_COMBOWIZARD_TITLE\n"
"string.text"
msgid "Combo Box Wizard"
msgstr "Skoazeller ar voestad kedaoz"

#: dbpilots.src
msgctxt ""
"dbpilots.src\n"
"RID_STR_COULDNOTOPENTABLE\n"
"string.text"
msgid "The table connection to the data source could not be established."
msgstr "N'haller ket kennaskañ an daolenn ouzh an tarzh roadennoù."

#: gridpages.src
msgctxt ""
"gridpages.src\n"
"RID_STR_DATEPOSTFIX\n"
"string.text"
msgid " (Date)"
msgstr " (Deiziad)"

#: gridpages.src
msgctxt ""
"gridpages.src\n"
"RID_STR_TIMEPOSTFIX\n"
"string.text"
msgid " (Time)"
msgstr " (Eur)"

#: groupboxpages.src
msgctxt ""
"groupboxpages.src\n"
"RID_STR_GROUPWIZ_DBFIELD\n"
"string.text"
msgid "You can either save the value of the option group in a database field or use it for a later action."
msgstr "Gallout a rit pe enrollañ gwerzh ar strollad dibarzhioù en ur vaezienn stlennvon, pe e arverañ evit un gwered diwezhatoc'h."

#: listcombopages.src
msgctxt ""
"listcombopages.src\n"
"RID_STR_FIELDINFO_COMBOBOX\n"
"string.text"
msgid "The contents of the field selected will be shown in the combo box list."
msgstr "Endalc'had ar vaezienn diuzet a vo skrammet war roll ar voestad kedaoz."

#: listcombopages.src
msgctxt ""
"listcombopages.src\n"
"RID_STR_FIELDINFO_LISTBOX\n"
"string.text"
msgid "The contents of the selected field will be shown in the list box if the linked fields are identical."
msgstr "Endalc'had ar vaezienn diuzet a vo skrammet war ar maez dre roll mar bez heñvel ar maeziennoù ereet ouzh ar re all."

#: listcombopages.src
msgctxt ""
"listcombopages.src\n"
"RID_STR_COMBOWIZ_DBFIELD\n"
"string.text"
msgid "You can either save the value of the combo box in a database field or use it for display purposes."
msgstr "Gallout a rit pe enrollañ gwerzh ar maez kedaoz en ur vaezienn stlennvon, pe hec'h arverañ evit ar skrammañ hepken."
