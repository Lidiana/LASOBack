#. extracted from formula/uiconfig/ui
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-02 00:10+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: kn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1462147859.000000\n"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"array\n"
"label\n"
"string.text"
msgid "Array"
msgstr "ಸರಣಿ"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"back\n"
"label\n"
"string.text"
msgid "<< _Back"
msgstr "<< ಹಿಂದಕ್ಕೆ (_B)"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"next\n"
"label\n"
"string.text"
msgid "_Next >>"
msgstr "ಮುಂದಕ್ಕೆ (_N) >>"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"function\n"
"label\n"
"string.text"
msgid "Functions"
msgstr "ಕಾರ್ಯಭಾರಗಳು"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"struct\n"
"label\n"
"string.text"
msgid "Structure"
msgstr "ರಚನೆ"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Function result"
msgstr "ಕಾರ್ಯಭಾರದ ಫಲಿತಾಂಶ"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"formula\n"
"label\n"
"string.text"
msgid "For_mula"
msgstr "ಸೂತ್ರ (_m)"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Result"
msgstr "ಫಲಿತಾಂಶ"

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"ed_formula-atkobject\n"
"AtkObject::accessible-name\n"
"string.text"
msgid "Formula"
msgstr ""

#: formuladialog.ui
msgctxt ""
"formuladialog.ui\n"
"RB_REF\n"
"tooltip_text\n"
"string.text"
msgid "Maximize"
msgstr "ಗರಿಷ್ಠೀಕರಿಸು"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"label_search\n"
"label\n"
"string.text"
msgid "_Search"
msgstr ""

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Category"
msgstr "ವರ್ಗ (_C)"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"category\n"
"0\n"
"stringlist.text"
msgid "Last Used"
msgstr "ಕೊನೆಯ ಬಳಕೆ"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"category\n"
"1\n"
"stringlist.text"
msgid "All"
msgstr "ಎಲ್ಲಾ"

#: functionpage.ui
msgctxt ""
"functionpage.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "_Function"
msgstr "ಕಾರ್ಯಭಾರ (_F)"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"editdesc\n"
"label\n"
"string.text"
msgid "Function not known"
msgstr "ಕಾರ್ಯಭಾರವು ತಿಳಿದಿಲ್ಲ"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG1\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "ಆರಿಸು"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG2\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "ಆರಿಸು"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG3\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "ಆರಿಸು"

#: parameter.ui
msgctxt ""
"parameter.ui\n"
"RB_ARG4\n"
"tooltip_text\n"
"string.text"
msgid "Select"
msgstr "ಆರಿಸು"

#: structpage.ui
msgctxt ""
"structpage.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "_Structure"
msgstr "ರಚನೆ (_S)"
