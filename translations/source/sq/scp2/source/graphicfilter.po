#. extracted from scp2/source/graphicfilter
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2015-02-26 12:31+0000\n"
"Last-Translator: Indrit <indrit.bashkimi@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.5.1\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1424953869.000000\n"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT\n"
"LngText.text"
msgid "Image Filters"
msgstr "Filtrat e figurave"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT\n"
"LngText.text"
msgid "Additional filters required to read alien image formats."
msgstr "Nevojiten filtra shtesë për të lexuar formatet e figurave të panjohura."

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_ADOBE\n"
"LngText.text"
msgid "Adobe Photoshop Import Filter"
msgstr "Filtër importimi Adobe Photoshop"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_ADOBE\n"
"LngText.text"
msgid "Adobe Photoshop Import Filter"
msgstr "Filtër importimi Adobe Photoshop"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_TGA\n"
"LngText.text"
msgid "TGA Import"
msgstr "Importim TGA"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_TGA\n"
"LngText.text"
msgid "TGA TrueVision TARGA Import Filter"
msgstr "Filtër importimi TGA TrueVision TARGA"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_EPS\n"
"LngText.text"
msgid "EPS Import/Export Filter"
msgstr "Filtër importimi/eksportimi EPS"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_EPS\n"
"LngText.text"
msgid "Encapsulated Postscript Import/Export Filter"
msgstr "Filtër importimi/eksportimi Encapsulated Postscript"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_XPM\n"
"LngText.text"
msgid "XPM Export Filter"
msgstr "Filtër eksportimi XPM"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_XPM\n"
"LngText.text"
msgid "XPM Export Filter"
msgstr "Filtër eksportimi XPM"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_PBMP\n"
"LngText.text"
msgid "Portable Bitmap Import/Export"
msgstr "Importim/eksportim Portable Bitmap"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_PBMP\n"
"LngText.text"
msgid "Portable Bitmap Import/Export Filters"
msgstr "Filtër importimi/eksportimi Portable Bitmap"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_SUNRST\n"
"LngText.text"
msgid "SUN Rasterfile Import/Export"
msgstr "Importim/eksportim SUN Rasterfile"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_SUNRST\n"
"LngText.text"
msgid "SUN Rasterfile Import/Export Filters"
msgstr "Filtër importimi/eksportimi SUN Rasterfile"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_ACAD\n"
"LngText.text"
msgid "AutoCAD Import"
msgstr "Importim AutoCAD"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_ACAD\n"
"LngText.text"
msgid "AutoCAD Import Filter"
msgstr "Filtër importimi AutoCAD"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_KODAC\n"
"LngText.text"
msgid "Kodak Photo-CD Import"
msgstr "Importim Kodak Foto-CD"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_KODAC\n"
"LngText.text"
msgid "Kodak Photo-CD Import Filter"
msgstr "Filtër importimi Kodak Foto-CD"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_MACPICT\n"
"LngText.text"
msgid "Mac-Pict Import/Export"
msgstr "Importim/eksportim Mac-Pict"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_MACPICT\n"
"LngText.text"
msgid "Mac-Pict Import/Export Filters"
msgstr "Filtra importimi/eksportimi Mac-Pict"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_OS2META\n"
"LngText.text"
msgid "OS/2 Metafile Import/Export"
msgstr "Importim/eksportim OS/2 Metafile"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_OS2META\n"
"LngText.text"
msgid "OS/2 Metafile Import/Export Filters"
msgstr "Filtra importimi/eksportimi OS/2 Metafile"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_PCX\n"
"LngText.text"
msgid "PCX Import"
msgstr "Importim PCX"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_PCX\n"
"LngText.text"
msgid "Z-Soft PCX Import"
msgstr "Importim Z-Soft PCX"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_TIFF\n"
"LngText.text"
msgid "TIFF Import/Export"
msgstr "Importim /eksportim TIFF"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_TIFF\n"
"LngText.text"
msgid "TIFF Import and Export Filter"
msgstr "Filtër importimi dhe eksportimi TIFF"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_SVG\n"
"LngText.text"
msgid "SVG Export"
msgstr "Eksportim SVG"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_SVG\n"
"LngText.text"
msgid "SVG Export Filter"
msgstr "Filtër eksportimi SVG"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_NAME_MODULE_OPTIONAL_GRFFLT_FLASH\n"
"LngText.text"
msgid "Macromedia Flash (SWF)"
msgstr "Macromedia Flash (SWF)"

#: module_graphicfilter.ulf
msgctxt ""
"module_graphicfilter.ulf\n"
"STR_DESC_MODULE_OPTIONAL_GRFFLT_FLASH\n"
"LngText.text"
msgid "Macromedia Flash (SWF) Export Filter"
msgstr "Filtër Eksportimi Macromedia Flash (SWF)"
