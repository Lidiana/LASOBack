#. extracted from sfx2/source/sidebar
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-09 02:15+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic <translation-team-is@lists.sourceforge.net>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1483928156.000000\n"

#: Sidebar.src
msgctxt ""
"Sidebar.src\n"
"SFX_STR_SIDEBAR_MORE_OPTIONS\n"
"string.text"
msgid "More Options"
msgstr "Fleiri valmöguleikar"

#: Sidebar.src
msgctxt ""
"Sidebar.src\n"
"SFX_STR_SIDEBAR_CLOSE_DECK\n"
"string.text"
msgid "Close Sidebar Deck"
msgstr "Loka hliðarspjaldsramma"

#: Sidebar.src
msgctxt ""
"Sidebar.src\n"
"SFX_STR_SIDEBAR_SETTINGS\n"
"string.text"
msgid "Sidebar Settings"
msgstr "Stillingar hliðarspjalds"

#: Sidebar.src
msgctxt ""
"Sidebar.src\n"
"SFX_STR_SIDEBAR_CUSTOMIZATION\n"
"string.text"
msgid "Customization"
msgstr "Sérsníðing"

#: Sidebar.src
msgctxt ""
"Sidebar.src\n"
"SFX_STR_SIDEBAR_RESTORE\n"
"string.text"
msgid "Restore Default"
msgstr "Endurheimta sjálfgefið"

#: Sidebar.src
msgctxt ""
"Sidebar.src\n"
"SFX_STR_SIDEBAR_HIDE_SIDEBAR\n"
"string.text"
msgid "Close Sidebar"
msgstr "Loka hliðarstiku"
