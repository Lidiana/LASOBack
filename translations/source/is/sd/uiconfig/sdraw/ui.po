#. extracted from sd/uiconfig/sdraw/ui
msgid ""
msgstr ""
"Project-Id-Version: ui\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-09 01:38+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic <openoffice@openoffice.is>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1483925920.000000\n"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"BreakDialog\n"
"title\n"
"string.text"
msgid "Break"
msgstr "Rofstaður"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Processing metafile:"
msgstr "Vinn með lýsiskrá:"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Broken down metaobjects:"
msgstr "Sundurteknir lýsihlutir:"

#: breakdialog.ui
msgctxt ""
"breakdialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Inserted drawing objects:"
msgstr "Innsettir teikningahlutar:"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"BulletsAndNumberingDialog\n"
"title\n"
"string.text"
msgid "Bullets and Numbering"
msgstr "Áherslumerki og tölusetning"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"reset\n"
"label\n"
"string.text"
msgid "Reset"
msgstr "Endurstilla"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"bullets\n"
"label\n"
"string.text"
msgid "Bullets"
msgstr "Áherslumerki"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"singlenum\n"
"label\n"
"string.text"
msgid "Numbering"
msgstr "Tölusetning"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"graphics\n"
"label\n"
"string.text"
msgid "Image"
msgstr "Mynd"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"position\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Staðsetning"

#: bulletsandnumbering.ui
msgctxt ""
"bulletsandnumbering.ui\n"
"customize\n"
"label\n"
"string.text"
msgid "Customize"
msgstr "Sérsníða"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"DuplicateDialog\n"
"title\n"
"string.text"
msgid "Duplicate"
msgstr "Margfalda"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"default\n"
"label\n"
"string.text"
msgid "_Default"
msgstr "_Sjálfgefið"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Number of _copies:"
msgstr "_Fjöldi afrita:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"viewdata\n"
"tooltip_text\n"
"string.text"
msgid "Values from Selection"
msgstr "Gildi úr/frá vali"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"viewdata-atkobject\n"
"AtkObject::accessible-name\n"
"string.text"
msgid "Values from Selection"
msgstr "Gildi úr/frá vali"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "_X axis:"
msgstr "_X-ás:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "_Y axis:"
msgstr "_Y-ás:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label7\n"
"label\n"
"string.text"
msgid "_Angle:"
msgstr "H_orn:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Placement"
msgstr "Staðsetning"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label8\n"
"label\n"
"string.text"
msgid "_Width:"
msgstr "_Breidd:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label9\n"
"label\n"
"string.text"
msgid "_Height:"
msgstr "_Hæð:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Enlargement"
msgstr "Stækkun"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label10\n"
"label\n"
"string.text"
msgid "_Start:"
msgstr "_Byrjar:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"endlabel\n"
"label\n"
"string.text"
msgid "_End:"
msgstr "_Endar:"

#: copydlg.ui
msgctxt ""
"copydlg.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Colors"
msgstr "Litir"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"CrossFadeDialog\n"
"title\n"
"string.text"
msgid "Cross-fading"
msgstr "Krossblöndun"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"orientation\n"
"label\n"
"string.text"
msgid "Same orientation"
msgstr "Sama stefna"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"attributes\n"
"label\n"
"string.text"
msgid "Cross-fade attributes"
msgstr "Eiginleikar krossblöndunar"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Increments:"
msgstr "Vaxtarþrep:"

#: crossfadedialog.ui
msgctxt ""
"crossfadedialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Settings"
msgstr "Stillingar"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"SnapObjectDialog\n"
"title\n"
"string.text"
msgid "New Snap Object"
msgstr "Nýr griphlutur"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"xlabel\n"
"label\n"
"string.text"
msgid "_X:"
msgstr "_X:"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"ylabel\n"
"label\n"
"string.text"
msgid "_Y:"
msgstr "_Y:"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Staðsetning"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"point\n"
"label\n"
"string.text"
msgid "_Point"
msgstr "_Punktur"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"vert\n"
"label\n"
"string.text"
msgid "_Vertical"
msgstr "_Lóðrétt"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"horz\n"
"label\n"
"string.text"
msgid "Hori_zontal"
msgstr "_Lárétt"

#: dlgsnap.ui
msgctxt ""
"dlgsnap.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Type"
msgstr "Tegund"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"DrawCharDialog\n"
"title\n"
"string.text"
msgid "Character"
msgstr "Stafur"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_CHAR_NAME\n"
"label\n"
"string.text"
msgid "Fonts"
msgstr "Letur"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_CHAR_EFFECTS\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "Leturbrellur"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_CHAR_POSITION\n"
"label\n"
"string.text"
msgid "Position"
msgstr "Staðsetning"

#: drawchardialog.ui
msgctxt ""
"drawchardialog.ui\n"
"RID_SVXPAGE_BACKGROUND\n"
"label\n"
"string.text"
msgid "Highlighting"
msgstr "Áherslulitun"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"DrawPageDialog\n"
"title\n"
"string.text"
msgid "Page Setup"
msgstr "Uppsetning síðu"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"RID_SVXPAGE_PAGE\n"
"label\n"
"string.text"
msgid "Page"
msgstr "Síða"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"RID_SVXPAGE_AREA\n"
"label\n"
"string.text"
msgid "Background"
msgstr "Bakgrunnur"

#: drawpagedialog.ui
msgctxt ""
"drawpagedialog.ui\n"
"RID_SVXPAGE_TRANSPARENCE\n"
"label\n"
"string.text"
msgid "Transparency"
msgstr "Gegnsæi"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"DrawParagraphPropertiesDialog\n"
"title\n"
"string.text"
msgid "Paragraph"
msgstr "Málsgrein"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_PARA_STD\n"
"label\n"
"string.text"
msgid "Indents & Spacing"
msgstr "Inndráttur og bil"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_PARA_ASIAN\n"
"label\n"
"string.text"
msgid "Asian Typography"
msgstr "Asísk stafaframsetning"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_TABULATOR\n"
"label\n"
"string.text"
msgid "Tabs"
msgstr "Flipar"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelTP_PARA_ALIGN\n"
"label\n"
"string.text"
msgid "Alignment"
msgstr "Jöfnun"

#: drawparadialog.ui
msgctxt ""
"drawparadialog.ui\n"
"labelNUMBERING\n"
"label\n"
"string.text"
msgid "Numbering"
msgstr "Tölusetning"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"DrawPRTLDialog\n"
"title\n"
"string.text"
msgid "Presentation Layout"
msgstr "Framsetning kynningar"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_LINE\n"
"label\n"
"string.text"
msgid "Line"
msgstr "Lína"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_AREA\n"
"label\n"
"string.text"
msgid "Area"
msgstr "Flötur"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_SHADOW\n"
"label\n"
"string.text"
msgid "Shadow"
msgstr "Skuggi"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_TRANSPARENCE\n"
"label\n"
"string.text"
msgid "Transparency"
msgstr "Gegnsæi"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_CHAR_NAME\n"
"label\n"
"string.text"
msgid "Font"
msgstr "Leturgerð"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_CHAR_EFFECTS\n"
"label\n"
"string.text"
msgid "Font Effects"
msgstr "Leturbrellur"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_STD_PARAGRAPH\n"
"label\n"
"string.text"
msgid "Indents & Spacing"
msgstr "Inndráttur og bil"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_TEXTATTR\n"
"label\n"
"string.text"
msgid "Text"
msgstr "Texti"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PICK_BULLET\n"
"label\n"
"string.text"
msgid "Bullets"
msgstr "Áherslumerki"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PICK_SINGLE_NUM\n"
"label\n"
"string.text"
msgid "Numbering"
msgstr "Tölusetning"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PICK_BMP\n"
"label\n"
"string.text"
msgid "Image"
msgstr "Mynd"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_NUM_OPTIONS\n"
"label\n"
"string.text"
msgid "Customize"
msgstr "Sérsníða"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_ALIGN_PARAGRAPH\n"
"label\n"
"string.text"
msgid "Alignment"
msgstr "Jöfnun"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_PARA_ASIAN\n"
"label\n"
"string.text"
msgid "Asian Typography"
msgstr "Asísk stafaframsetning"

#: drawprtldialog.ui
msgctxt ""
"drawprtldialog.ui\n"
"RID_SVXPAGE_TABULATOR\n"
"label\n"
"string.text"
msgid "Tabs"
msgstr "Flipar"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"InsertLayerDialog\n"
"title\n"
"string.text"
msgid "Insert Layer"
msgstr "Setja inn lag"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "_Name"
msgstr "_Nafn"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "_Title"
msgstr "_Titill"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"description\n"
"label\n"
"string.text"
msgid "_Description"
msgstr "_Lýsing"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"visible\n"
"label\n"
"string.text"
msgid "_Visible"
msgstr "_Sýnileg"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"printable\n"
"label\n"
"string.text"
msgid "_Printable"
msgstr "_Prentanlegt"

#: insertlayer.ui
msgctxt ""
"insertlayer.ui\n"
"locked\n"
"label\n"
"string.text"
msgid "_Locked"
msgstr "_Læst"

#: insertslidesdialog.ui
msgctxt ""
"insertslidesdialog.ui\n"
"InsertSlidesDialog\n"
"title\n"
"string.text"
msgid "Insert Slides/Objects"
msgstr "Setja inn skyggnur/hluti"

#: insertslidesdialog.ui
msgctxt ""
"insertslidesdialog.ui\n"
"backgrounds\n"
"label\n"
"string.text"
msgid "Delete unused backg_rounds"
msgstr "Eyða ónotuðum bakg_runnum"

#: insertslidesdialog.ui
msgctxt ""
"insertslidesdialog.ui\n"
"links\n"
"label\n"
"string.text"
msgid "_Link"
msgstr "Tengi_ll"

#: namedesign.ui
msgctxt ""
"namedesign.ui\n"
"NameDesignDialog\n"
"title\n"
"string.text"
msgid "Name HTML Design"
msgstr "Nefna HTML-hönnun"

#: paranumberingtab.ui
msgctxt ""
"paranumberingtab.ui\n"
"checkbuttonCB_NEW_START\n"
"label\n"
"string.text"
msgid "R_estart at this paragraph"
msgstr "Byrja aftur við þ_essa málsgrein"

#: paranumberingtab.ui
msgctxt ""
"paranumberingtab.ui\n"
"checkbuttonCB_NUMBER_NEW_START\n"
"label\n"
"string.text"
msgid "S_tart with:"
msgstr "_Byrja á:"

#: paranumberingtab.ui
msgctxt ""
"paranumberingtab.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Paragraph Numbering"
msgstr "Tölusetning málsgreina"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"printname\n"
"label\n"
"string.text"
msgid "Page name"
msgstr "Síðuheiti"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"printdatetime\n"
"label\n"
"string.text"
msgid "Date and time"
msgstr "Dagsetning og tími"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Contents"
msgstr "Innihald"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"originalcolors\n"
"label\n"
"string.text"
msgid "Original size"
msgstr "Upprunaleg stærð"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"grayscale\n"
"label\n"
"string.text"
msgid "Grayscale"
msgstr "Grátóna"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"blackandwhite\n"
"label\n"
"string.text"
msgid "Black & white"
msgstr "Svarthvítt"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Color"
msgstr "Litur"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"originalsize\n"
"label\n"
"string.text"
msgid "Original size"
msgstr "Upprunaleg stærð"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"fittoprintable\n"
"label\n"
"string.text"
msgid "Fit to printable page"
msgstr "Aðlaga á prentanlega síðu"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"distributeonmultiple\n"
"label\n"
"string.text"
msgid "Distribute on multiple sheets of paper"
msgstr "Dreifa á mörgum blöðum"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"tilesheet\n"
"label\n"
"string.text"
msgid "Tile sheet of paper with repeated pages"
msgstr "Flísaleggja síður með endurteknum síðum"

#: printeroptions.ui
msgctxt ""
"printeroptions.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Size"
msgstr "Stærð"

#: queryunlinkimagedialog.ui
msgctxt ""
"queryunlinkimagedialog.ui\n"
"QueryUnlinkImageDialog\n"
"title\n"
"string.text"
msgid "Release image's link?"
msgstr "Leysa upp tengingu myndarinnar?"

#: queryunlinkimagedialog.ui
msgctxt ""
"queryunlinkimagedialog.ui\n"
"QueryUnlinkImageDialog\n"
"text\n"
"string.text"
msgid "This image is linked to a document."
msgstr "Þessi mynd er tengd inn í skjal."

#: queryunlinkimagedialog.ui
msgctxt ""
"queryunlinkimagedialog.ui\n"
"QueryUnlinkImageDialog\n"
"secondary_text\n"
"string.text"
msgid "Do you want to unlink the image in order to edit it?"
msgstr " Viltu aftengja myndina til að geta breytt henni?"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"TableDesignDialog\n"
"title\n"
"string.text"
msgid "Table Design"
msgstr "Töfluhönnun"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseFirstRowStyle\n"
"label\n"
"string.text"
msgid "_Header row"
msgstr "_Fyrirsögn raðar"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseLastRowStyle\n"
"label\n"
"string.text"
msgid "Tot_al row"
msgstr "Fjöldi r_aða"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseBandingRowStyle\n"
"label\n"
"string.text"
msgid "_Banded rows"
msgstr "_Mislitar raðir"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseFirstColumnStyle\n"
"label\n"
"string.text"
msgid "Fi_rst column"
msgstr "Fy_rsti dálkur"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseLastColumnStyle\n"
"label\n"
"string.text"
msgid "_Last column"
msgstr "Seinasti dá_lkur"

#: tabledesigndialog.ui
msgctxt ""
"tabledesigndialog.ui\n"
"UseBandingColumnStyle\n"
"label\n"
"string.text"
msgid "Ba_nded columns"
msgstr "M_islitir dálkar"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"VectorizeDialog\n"
"title\n"
"string.text"
msgid "Convert to Polygon"
msgstr "Umbreyta í marghyrning"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"preview\n"
"label\n"
"string.text"
msgid "Preview"
msgstr "Forskoða"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "Number of colors:"
msgstr "Fjöldi lita:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "Point reduction:"
msgstr "Fækkun punkta:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"tilesft\n"
"label\n"
"string.text"
msgid "Tile size:"
msgstr "Stærð flísa:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"fillholes\n"
"label\n"
"string.text"
msgid "_Fill holes"
msgstr "_Fylla holur"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Source image:"
msgstr "Upprunaleg mynd:"

#: vectorize.ui
msgctxt ""
"vectorize.ui\n"
"label6\n"
"label\n"
"string.text"
msgid "Vectorized image:"
msgstr "Línuteiknuð mynd:"
