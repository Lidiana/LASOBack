#. extracted from desktop/uiconfig/ui
msgid ""
msgstr ""
"Project-Id-Version: LibO 40l10n\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-12-01 12:11+0100\n"
"PO-Revision-Date: 2016-12-29 21:24+0000\n"
"Last-Translator: Stanislav Horáček <stanislav.horacek@gmail.com>\n"
"Language-Team: none\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1483046646.000000\n"

#: cmdlinehelp.ui
msgctxt ""
"cmdlinehelp.ui\n"
"CmdLineHelp\n"
"title\n"
"string.text"
msgid "Help Message"
msgstr "Zpráva nápovědy"

#: dependenciesdialog.ui
msgctxt ""
"dependenciesdialog.ui\n"
"Dependencies\n"
"title\n"
"string.text"
msgid "System dependencies check"
msgstr "Kontrola systémových závislostí"

#: dependenciesdialog.ui
msgctxt ""
"dependenciesdialog.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "The extension cannot be installed as the following system dependencies are not fulfilled:"
msgstr "Rozšíření nelze nainstalovat, protože nejsou splněny následující závislosti:"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"ExtensionManagerDialog\n"
"title\n"
"string.text"
msgid "Extension Manager"
msgstr "Správce rozšíření"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"optionsbtn\n"
"label\n"
"string.text"
msgid "_Options"
msgstr "_Možnosti"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"updatebtn\n"
"label\n"
"string.text"
msgid "Check for _Updates"
msgstr "Zkontrolovat _aktualizace"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"addbtn\n"
"label\n"
"string.text"
msgid "_Add"
msgstr "_Přidat"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"removebtn\n"
"label\n"
"string.text"
msgid "_Remove"
msgstr "_Odstranit"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"enablebtn\n"
"label\n"
"string.text"
msgid "_Enable"
msgstr "Po_volit"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"shared\n"
"label\n"
"string.text"
msgid "Installed for all users"
msgstr "Nainstalovaná pro _všechny uživatele"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"user\n"
"label\n"
"string.text"
msgid "Installed for current user"
msgstr "Nainstalovaná pro _aktuálního uživatele"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"bundled\n"
"label\n"
"string.text"
msgid "Bundled with %PRODUCTNAME"
msgstr "_Dodávaná s %PRODUCTNAME"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"label1\n"
"label\n"
"string.text"
msgid "Display Extensions"
msgstr "Zobrazit rozšíření"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"progressft\n"
"label\n"
"string.text"
msgid "Adding %EXTENSION_NAME"
msgstr "Přidává se %EXTENSION_NAME"

#: extensionmanager.ui
msgctxt ""
"extensionmanager.ui\n"
"getextensions\n"
"label\n"
"string.text"
msgid "Get more extensions online..."
msgstr "Další rozšíření získáte na webu..."

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"InstallForAllDialog\n"
"text\n"
"string.text"
msgid "For whom do you want to install the extension?"
msgstr "Pro koho si přejete rozšíření nainstalovat?"

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"InstallForAllDialog\n"
"secondary_text\n"
"string.text"
msgid "Make sure that no further users are working with the same %PRODUCTNAME, when installing an extension for all users in a multi user environment."
msgstr "Když instalujete rozšíření pro všechny uživatele, ujistěte se, že nikdo další s %PRODUCTNAME nepracuje."

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"no\n"
"label\n"
"string.text"
msgid "_For all users"
msgstr "Pro _všechny uživatele"

#: installforalldialog.ui
msgctxt ""
"installforalldialog.ui\n"
"yes\n"
"label\n"
"string.text"
msgid "_Only for me"
msgstr "_Jen pro mě"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"LicenseDialog\n"
"title\n"
"string.text"
msgid "Extension Software License Agreement"
msgstr "Licenční ujednání rozšíření"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"accept\n"
"label\n"
"string.text"
msgid "Accept"
msgstr "Přijmout"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"decline\n"
"label\n"
"string.text"
msgid "Decline"
msgstr "Odmítnout"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"head\n"
"label\n"
"string.text"
msgid "Please follow these steps to proceed with the installation of the extension:"
msgstr "Pro instalaci rozšíření postupujte podle následujících kroků:"

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label2\n"
"label\n"
"string.text"
msgid "1."
msgstr "1."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label3\n"
"label\n"
"string.text"
msgid "2."
msgstr "2."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label4\n"
"label\n"
"string.text"
msgid "Read the complete License Agreement. Use the scroll bar or the 'Scroll Down' button in this dialog to view the entire license text."
msgstr "Přečtěte si licenční ujednání. Pro zobrazení celého textu použijte posuvník nebo tlačítko 'Posunout dolů'."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"label5\n"
"label\n"
"string.text"
msgid "Accept the License Agreement for the extension by pressing the 'Accept' button."
msgstr "Přijměte licenční ujednání rozšíření stisknutím tlačítka 'Přijmout'."

#: licensedialog.ui
msgctxt ""
"licensedialog.ui\n"
"down\n"
"label\n"
"string.text"
msgid "_Scroll Down"
msgstr "Posunout _dolů"

#: showlicensedialog.ui
msgctxt ""
"showlicensedialog.ui\n"
"ShowLicenseDialog\n"
"title\n"
"string.text"
msgid "Extension Software License Agreement"
msgstr "Licenční ujednání rozšíření"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UpdateDialog\n"
"title\n"
"string.text"
msgid "Extension Update"
msgstr "Aktualizace rozšíření"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"INSTALL\n"
"label\n"
"string.text"
msgid "_Install"
msgstr "_Instalovat"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UPDATE_LABEL\n"
"label\n"
"string.text"
msgid "_Available extension updates"
msgstr "_Dostupné aktualizace rozšíření"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UPDATE_CHECKING\n"
"label\n"
"string.text"
msgid "Checking..."
msgstr "Probíhá kontrola..."

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"UPDATE_ALL\n"
"label\n"
"string.text"
msgid "_Show all updates"
msgstr "_Zobrazit všechny aktualizace"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"DESCRIPTION_LABEL\n"
"label\n"
"string.text"
msgid "Description"
msgstr "Popis"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"PUBLISHER_LABEL\n"
"label\n"
"string.text"
msgid "Publisher:"
msgstr "Vydavatel:"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"PUBLISHER_LINK\n"
"label\n"
"string.text"
msgid "button"
msgstr "tlačítko"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"RELEASE_NOTES_LABEL\n"
"label\n"
"string.text"
msgid "What is new:"
msgstr "Co je nového:"

#: updatedialog.ui
msgctxt ""
"updatedialog.ui\n"
"RELEASE_NOTES_LINK\n"
"label\n"
"string.text"
msgid "Release notes"
msgstr "Poznámky k vydání"

#: updateinstalldialog.ui
msgctxt ""
"updateinstalldialog.ui\n"
"UpdateInstallDialog\n"
"title\n"
"string.text"
msgid "Download and Installation"
msgstr "Stažení a instalace"

#: updateinstalldialog.ui
msgctxt ""
"updateinstalldialog.ui\n"
"DOWNLOADING\n"
"label\n"
"string.text"
msgid "Downloading extensions..."
msgstr "Stahují se rozšíření..."

#: updateinstalldialog.ui
msgctxt ""
"updateinstalldialog.ui\n"
"RESULTS\n"
"label\n"
"string.text"
msgid "Result"
msgstr "Výsledek"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"UpdateRequiredDialog\n"
"title\n"
"string.text"
msgid "Extension Update Required"
msgstr "Je vyžadována aktualizace rozšíření"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"updatelabel\n"
"label\n"
"string.text"
msgid "%PRODUCTNAME has been updated to a new version. Some installed %PRODUCTNAME extensions are not compatible with this version and need to be updated before they can be used."
msgstr "%PRODUCTNAME byl aktualizován na novou verzi. Některá nainstalovaná rozšíření %PRODUCTNAME nejsou s touto verzí kompatibilní a před použitím je potřeba je aktualizovat."

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"progresslabel\n"
"label\n"
"string.text"
msgid "Adding %EXTENSION_NAME"
msgstr "Přidává se %EXTENSION_NAME"

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"check\n"
"label\n"
"string.text"
msgid "Check for _Updates..."
msgstr "_Vyhledat aktualizace..."

#: updaterequireddialog.ui
msgctxt ""
"updaterequireddialog.ui\n"
"disable\n"
"label\n"
"string.text"
msgid "Disable all"
msgstr "Zakázat vše"
