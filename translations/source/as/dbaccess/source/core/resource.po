#. extracted from dbaccess/source/core/resource
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-05-11 16:12+0200\n"
"PO-Revision-Date: 2014-02-20 12:59+0000\n"
"Last-Translator: ngoswami <ngoswami@redhat.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: as\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1392901174.0\n"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_TRIED_OPEN_TABLE\n"
"string.text"
msgid "Tried to open the table $name$."
msgstr "$name$ টেবুলখন খুলিবলৈ চেষ্টা কৰা হ'ল ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONNECTION_INVALID\n"
"string.text"
msgid "No connection could be established."
msgstr "কোনো সংযোগ প্ৰতিষ্ঠা কৰিব পৰা নগ'ল ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_TABLE_IS_FILTERED\n"
"string.text"
msgid "The table $name$ already exists. It is not visible because it has been filtered out."
msgstr "$name$ টেবুলখন ইতিমধ্যে বৰ্তি আছে । এইটো দৃশ্যমান নহয় কাৰণ ইয়াক ফিল্টাৰ কৰা হৈছে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NEED_CONFIG_WRITE_ACCESS\n"
"string.text"
msgid "You have no write access to the configuration data the object is based on."
msgstr "বস্তুটো আধাৰিত ৰূপৰেখা ডাটালৈ প্ৰবেশ কৰাবৰ আপোনাৰ কোনো অধিকাৰ নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULDNOTCONNECT_UNSPECIFIED\n"
"string.text"
msgid "The connection to the external data source could not be established. An unknown error occurred. The driver is probably defective."
msgstr "বাহিৰৰ ডাটা উত্সটোলৈ সংযোগটো প্ৰতিষ্ঠা কৰিব পৰা নগ'ল । এটা অজ্ঞাত ভুল ওলাল । ড্ৰাইভাৰটো সম্ভৱতঃ ত্ৰুটিপুৰ্ণ ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULDNOTCONNECT_NODRIVER\n"
"string.text"
msgid "The connection to the external data source could not be established. No SDBC driver was found for the URL '$name$'."
msgstr "বহিৰ্তম তথ্য উৎসলৈ সংযোগ স্থাপন কৰিব পৰা নগল। URL '$name$' ৰ বাবে কোনো SDBC ড্ৰাইভাৰ পোৱা নগল।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COULDNOTLOAD_MANAGER\n"
"string.text"
msgid "The connection to the external data source could not be established. The SDBC driver manager could not be loaded."
msgstr "বাহিৰৰ তথ্য উৎসলৈ সংযোগ প্ৰতিষ্ঠা কৰিব পৰা নগ'ল। SDBC ড্ৰাইভাৰ মেনেজাৰ ল'ড কৰিব পৰা নগ'ল।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FORM\n"
"string.text"
msgid "Form"
msgstr "ফর্ম"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_REPORT\n"
"string.text"
msgid "Report"
msgstr "ৰিপৰ্ট"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_DATASOURCE_NOT_STORED\n"
"string.text"
msgid "The data source was not saved. Please use the interface XStorable to save the data source."
msgstr "ডাটা উত্সটো ছেভ কৰা হোৱা নাছিল ।অনুগ্ৰহ কৰি ডাটা উত্সটো ছেভ কৰিবলৈ ইন্টাৰফেছ Xষ্টৰেবলটো ব্যৱহাৰ কৰক ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ONLY_QUERY\n"
"string.text"
msgid ""
"The given command is not a SELECT statement.\n"
"Only queries are allowed."
msgstr ""
"এই আদেশটো এটা SELECT বাক্য নহয় ।\n"
"অকল প্ৰশ্নৰহে অনুমতি আছে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_VALUE_CHANGED\n"
"string.text"
msgid "No values were modified."
msgstr "কোনো মূল্য ৰূপান্তৰিত হোৱা নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_XROWUPDATE\n"
"string.text"
msgid "Values could not be inserted. The XRowUpdate interface is not supported by ResultSet."
msgstr "মূল্যবোৰ ভৰাব পৰা নগল। XRowUpdate আন্তঃপৃষ্ঠ ResultSet ৰ দ্বাৰা সমৰ্থিত নহয়।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_XRESULTSETUPDATE\n"
"string.text"
msgid "Values could not be inserted. The XResultSetUpdate interface is not supported by ResultSet."
msgstr "মূল্যবোৰ ৰূপান্তৰিত কৰিব পৰা নগল । Xৰিজাল্টছেট আপডেট ইন্টাৰফেছটো ৰিজাল্টছেটৰ দ্বাৰা সমৰ্থিত হোৱা নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_UPDATE_MISSING_CONDITION\n"
"string.text"
msgid "Values could not be modified, due to a missing condition statement."
msgstr "হেৰোৱা চৰ্ত উক্তিৰ বাবে মূল্যবোৰ ৰূপান্তৰিত কৰিব পৰা নগল ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_COLUMN_ADD\n"
"string.text"
msgid "The adding of columns is not supported."
msgstr "স্তম্ভবোৰৰ সংযোজন সমৰ্থিত কৰা নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_COLUMN_DROP\n"
"string.text"
msgid "The dropping of columns is not supported."
msgstr "স্তম্ভবোৰৰ আঁতৰকৰণ সমৰ্থিত কৰা নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_CONDITION_FOR_PK\n"
"string.text"
msgid "The WHERE condition could not be created for the primary key."
msgstr "প্ৰাথমিক চাবিৰ কাৰণে WHERE চৰ্তটো সৃষ্টি কৰিব পৰা নগ'ল ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_UNKNOWN_PROP\n"
"string.text"
msgid "The column does not support the property '%value'."
msgstr "স্তম্ভটোৱে বৈশিষ্ট্য '%value' সমৰ্থন নকৰে।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_NOT_SEARCHABLE\n"
"string.text"
msgid "The column is not searchable!"
msgstr "স্তম্ভটো সন্ধান কৰিব পৰা নহয়!"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NOT_SEQUENCE_INT8\n"
"string.text"
msgid "The value of the columns is not of the type Sequence<sal_Int8>."
msgstr "স্তম্ভবোৰৰ মূল্যটো অনুক্ৰম<sal_Int8> ৰ দৰে নহয় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_NOT_VALID\n"
"string.text"
msgid "The column is not valid."
msgstr "স্তম্ভটো বৈধ নহয় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_MUST_VISIBLE\n"
"string.text"
msgid "The column '%name' must be visible as a column."
msgstr "'%name' স্তম্ভটো এটা স্তম্ভ হিচাপে দৃশ্যমান হব লাগিব।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_XQUERIESSUPPLIER\n"
"string.text"
msgid "The interface XQueriesSupplier is not available."
msgstr "XQueriesSupplier আন্তঃপৃষ্ঠ মজুত নাই।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NOT_SUPPORTED_BY_DRIVER\n"
"string.text"
msgid "The driver does not support this function."
msgstr "এই ফাংকশ্বনটো ড্ৰাইভাৰটোৱে সমৰ্থন নকৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_ABS_ZERO\n"
"string.text"
msgid "An 'absolute(0)' call is not allowed."
msgstr "এটা 'সম্পূৰ্ণ(0)' নিমন্ত্ৰণ অনুমোদিত নহয় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_RELATIVE\n"
"string.text"
msgid "Relative positioning is not allowed in this state."
msgstr "এই অৱস্থাত আপেক্ষিক স্থানকৰণ অনুমোদিত নহয় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_REFESH_AFTERLAST\n"
"string.text"
msgid "A row cannot be refreshed when the ResultSet is positioned after the last row."
msgstr "যেতিয়া শেষৰ শাৰীৰ পাছত ৰিজাল্টছেটটোক অবস্থান দিয়া হয় এটা শাৰী পুনৰ সজীৱ কৰিব পৰা নাযায় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_MOVETOINSERTROW_CALLED\n"
"string.text"
msgid "A new row cannot be inserted when the ResultSet is not first moved to the insert row."
msgstr "যেতিয়া ৰিজাল্টছেটটো শাৰী ভৰাওকলৈ প্ৰথমতে নিয়া নহয় এটা নতুন শাৰী ভৰাব নোৱাৰি।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_UPDATEROW\n"
"string.text"
msgid "A row cannot be modified in this state"
msgstr "এই অৱস্থাত এটা শাৰী ৰূপান্তৰ কৰিব নোৱাৰি"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETEROW\n"
"string.text"
msgid "A row cannot be deleted in this state."
msgstr "এই অৱস্থাত এটা শাৰী ডিলিট কৰিব নোৱাৰি ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_TABLE_RENAME\n"
"string.text"
msgid "The driver does not support table renaming."
msgstr "টেবুলৰ পুনৰ নামকৰণক ড্ৰাইভাৰে সমৰ্থন নকৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_ALTER_COLUMN_DEF\n"
"string.text"
msgid "The driver does not support the modification of column descriptions."
msgstr "স্তম্ভৰ বিৱৰণবোৰৰ ৰূপান্তৰ ড্ৰাইভাৰটোৱে সমৰ্থন নকৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_ALTER_BY_NAME\n"
"string.text"
msgid "The driver does not support the modification of column descriptions by changing the name."
msgstr "নামটো পৰিৱৰ্তনৰ দ্বাৰা স্তম্ভ বিৱৰণৰ ৰূপান্তৰক ড্ৰাইভাৰে সমৰ্থন নকৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COLUMN_ALTER_BY_INDEX\n"
"string.text"
msgid "The driver does not support the modification of column descriptions by changing the index."
msgstr "অনুক্ৰমণিকা পৰিৱৰ্তনৰ দ্বাৰা স্তম্ভ বিৱৰণৰ ৰূপান্তৰক ড্ৰাইভাৰটোৱে সমৰ্থন নকৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_FILE_DOES_NOT_EXIST\n"
"string.text"
msgid "The file \"$file$\" does not exist."
msgstr "\"$file$\" ফাইলটোৰ অস্তিত্ব নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_TABLE_DOES_NOT_EXIST\n"
"string.text"
msgid "There exists no table named \"$table$\"."
msgstr "\"$table$\" নামৰ কোনো টেবুল নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_QUERY_DOES_NOT_EXIST\n"
"string.text"
msgid "There exists no query named \"$table$\"."
msgstr "\"$table$\" নামৰ কোনো প্ৰশ্ন নাই ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONFLICTING_NAMES\n"
"string.text"
msgid "There are tables in the database whose names conflict with the names of existing queries. To make full use of all queries and tables, make sure they have distinct names."
msgstr "তথ্যভঁৰালত এনে কিছু টেবুল আছে যাৰ নামৰ লগত বৰ্ত্তমানৰ প্ৰশ্নৰ সংঘৰ্ষ হৈছে । সকলো প্ৰশ্ন আৰু টেবুলক সম্পূৰ্ণ ৰূপে ব্যৱহাৰ কৰিবলৈ, সিহঁতৰ পৃথক নাম হোৱা সুনিশ্চিত কৰক ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_COMMAND_LEADING_TO_ERROR\n"
"string.text"
msgid ""
"The SQL command leading to this error is:\n"
"\n"
"$command$"
msgstr ""
"এই ভুলৰ মূলতে থকা SQL আদেশ হ'ল:\n"
"\n"
"$command$"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_STATEMENT_WITHOUT_RESULT_SET\n"
"string.text"
msgid "The SQL command does not describe a result set."
msgstr "SQL আদেশে এটা ফলাফলৰ গোটৰ বিৱৰণ নিদিয়ে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NAME_MUST_NOT_BE_EMPTY\n"
"string.text"
msgid "The name must not be empty."
msgstr "নাম ৰিক্ত হ'ব নাপায় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_NULL_OBJECTS_IN_CONTAINER\n"
"string.text"
msgid "The container cannot contain NULL objects."
msgstr "পাত্ৰত NULL বস্তু হ'ব নালাগে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NAME_ALREADY_USED\n"
"string.text"
msgid "There already is an object with the given name."
msgstr "এই নামৰ বস্তু এটা ইতিমধ্যে আছে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_OBJECT_CONTAINER_MISMATCH\n"
"string.text"
msgid "This object cannot be part of this container."
msgstr "এই বস্তুটো এই পাত্ৰৰ অংশ হ'ব নোৱাৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_OBJECT_ALREADY_CONTAINED\n"
"string.text"
msgid "The object already is, with a different name, part of the container."
msgstr "বস্তুটো ইতিমধ্যে আছে, এই পাত্ৰৰ এটা অংশ হৈ, এটা বেলেগ নামেৰে ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NAME_NOT_FOUND\n"
"string.text"
msgid "Unable to find the document '$name$'."
msgstr "'$name$' আলেখ্যন পোৱা নাযায় ।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ERROR_WHILE_SAVING\n"
"string.text"
msgid ""
"Could not save the document to $location$:\n"
"$message$"
msgstr ""
"দস্তাবেজক $location$ লে সঞ্চয় কৰিবলে অক্ষম:\n"
"$message$"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_NO_SUCH_DATA_SOURCE\n"
"string.text"
msgid ""
"Error accessing data source '$name$':\n"
"$error$"
msgstr ""
"তথ্য উৎস '$name$' অভিগম কৰোতে ত্ৰুটি:\n"
"$error$"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_SUB_FOLDER\n"
"string.text"
msgid "There exists no folder named \"$folder$\"."
msgstr "\"$folder$\" নামৰ কোনো ফোল্ডাৰ নাই।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETE_BEFORE_AFTER\n"
"string.text"
msgid "Cannot delete the before-first or after-last row."
msgstr "প্ৰথমৰ-আগত বা শেষৰ-পিছত শাৰীক মচিব নোৱাৰি।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETE_INSERT_ROW\n"
"string.text"
msgid "Cannot delete the insert-row."
msgstr "সোমোৱা-শাৰী মচিব নোৱাৰি।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_RESULT_IS_READONLY\n"
"string.text"
msgid "Result set is read only."
msgstr "পৰিণত সংহতি কেৱল পঢ়িব পাৰি।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_DELETE_PRIVILEGE\n"
"string.text"
msgid "DELETE privilege not available."
msgstr "DELETE সুবিধা উপলব্ধ নহয়।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_ROW_ALREADY_DELETED\n"
"string.text"
msgid "Current row is already deleted."
msgstr "বৰ্তমান ইতিমধ্যে মচি দিয়া হৈছে।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_UPDATE_FAILED\n"
"string.text"
msgid "Current row could not be updated."
msgstr "বৰ্তমান শাৰী আপডেইট কৰিব পৰা নগল।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_INSERT_PRIVILEGE\n"
"string.text"
msgid "INSERT privilege not available."
msgstr "INSERT সুবিধা উপলব্ধ নহয়।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INTERNAL_ERROR\n"
"string.text"
msgid "Internal error: no statement object provided by the database driver."
msgstr "অভ্যন্তৰীক ত্ৰুটি: ডাটাবেইচ ড্ৰাইভাৰৰ দ্বাৰা কোনো বিবৃতি অবজেক্ট যোগান দিয়া হোৱা নাই।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_EXPRESSION1\n"
"string.text"
msgid "Expression1"
msgstr "অভিব্যক্তি১"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_SQL_COMMAND\n"
"string.text"
msgid "No SQL command was provided."
msgstr "কোনো SQL কমান্ড প্ৰদান কৰা হোৱা নাছিল।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INVALID_INDEX\n"
"string.text"
msgid "Invalid column index."
msgstr "অবৈধ স্তম্ভ সূচক।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_INVALID_CURSOR_STATE\n"
"string.text"
msgid "Invalid cursor state."
msgstr "অবৈধ কাৰ্চাৰ অৱস্থা।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CURSOR_BEFORE_OR_AFTER\n"
"string.text"
msgid "The cursor points to before the first or after the last row."
msgstr "কাৰ্চাৰটোৱে প্ৰথমৰ আগত অথবা শেষৰ পিছৰ শাৰীত ইংগিত কৰে।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_BOOKMARK_BEFORE_OR_AFTER\n"
"string.text"
msgid "The rows before the first and after the last row don't have a bookmark."
msgstr "প্ৰথমৰ আগৰ আৰু শেষৰ শাৰীৰ পিছৰ শাৰীসমূহৰ পত্ৰচিহ্ন নাই।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_NO_BOOKMARK_DELETED\n"
"string.text"
msgid "The current row is deleted, and thus doesn't have a bookmark."
msgstr "বৰ্তমান শাৰী মচি দিয়া হৈছে, আৰু কোনো পত্ৰচিহ্ন নাই।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_CONNECTION_REQUEST\n"
"string.text"
msgid "A connection for the following URL was requested \"$name$\"."
msgstr "নিম্নলিখিত URL -ৰ কাৰণে এটা সংযোগৰ অনুৰোধ কৰা হৈছিল \"$name$\"।"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_STR_MISSING_EXTENSION\n"
"string.text"
msgid "The extension is not installed."
msgstr "প্ৰসাৰন ইনস্টল কৰা হোৱা নাই।"
