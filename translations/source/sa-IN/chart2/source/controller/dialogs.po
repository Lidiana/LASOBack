#. extracted from chart2/source/controller/dialogs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2014-09-29 04:44+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sa_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1411965881.000000\n"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DLG_CHART_WIZARD\n"
"string.text"
msgid "Chart Wizard"
msgstr "चार्ट् विजार्ड्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DLG_SMOOTH_LINE_PROPERTIES\n"
"string.text"
msgid "Smooth Lines"
msgstr "मृदुरेखाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DLG_STEPPED_LINE_PROPERTIES\n"
"string.text"
msgid "Stepped Lines"
msgstr "Stepped Lines"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_CHARTTYPE\n"
"string.text"
msgid "Chart Type"
msgstr "कोष्ठकपटप्रकारः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_DATA_RANGE\n"
"string.text"
msgid "Data Range"
msgstr "डाटागोचरः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_CHART_ELEMENTS\n"
"string.text"
msgid "Chart Elements"
msgstr "तालिकातत्त्वानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_CHART_LOCATION\n"
"string.text"
msgid "Chart Location"
msgstr "तालिकास्थानम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_LINE\n"
"string.text"
msgid "Line"
msgstr "रेखा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_BORDER\n"
"string.text"
msgid "Borders"
msgstr "सीमाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_AREA\n"
"string.text"
msgid "Area"
msgstr "क्षेत्रम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_TRANSPARENCY\n"
"string.text"
msgid "Transparency"
msgstr "पारदर्शिता"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_FONT\n"
"string.text"
msgid "Font"
msgstr "अक्षरगणः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_FONT_EFFECTS\n"
"string.text"
msgid "Font Effects"
msgstr "अक्षरगणप्रभावाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_NUMBERS\n"
"string.text"
msgid "Numbers"
msgstr "संख्याः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_POSITION\n"
"string.text"
msgid "Position"
msgstr "अवस्थितिः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_LAYOUT\n"
"string.text"
msgid "Layout"
msgstr "अभिन्यासः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_OPTIONS\n"
"string.text"
msgid "Options"
msgstr "विकल्पाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_SCALE\n"
"string.text"
msgid "Scale"
msgstr "मापनी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_POSITIONING\n"
"string.text"
msgid "Positioning"
msgstr "अवस्थितिः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_TRENDLINE_TYPE\n"
"string.text"
msgid "Type"
msgstr "वर्गः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_XERROR_BARS\n"
"string.text"
msgid "X Error Bars"
msgstr "Y दोषपट्टाः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_YERROR_BARS\n"
"string.text"
msgid "Y Error Bars"
msgstr "Y दोषपट्टाः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_ZERROR_BARS\n"
"string.text"
msgid "Z Error Bars"
msgstr "Y दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_ALIGNMENT\n"
"string.text"
msgid "Alignment"
msgstr "संरेखणम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_PERSPECTIVE\n"
"string.text"
msgid "Perspective"
msgstr "दृश्यछाया"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_APPEARANCE\n"
"string.text"
msgid "Appearance"
msgstr "आलोकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PAGE_ILLUMINATION\n"
"string.text"
msgid "Illumination"
msgstr "प्रदीपनम्"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_PAGE_ASIAN\n"
"string.text"
msgid "Asian Typography"
msgstr "एशियायी-टाइपोग्राफी"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AVERAGE_LINE_WITH_PARAMETERS\n"
"string.text"
msgid "Mean value line with value %AVERAGE_VALUE and standard deviation %STD_DEVIATION"
msgstr "मध्यमूल्यरेखा %AVERAGE_VALUE मूल्येन तथा %STD_DEVIATION प्रामाणिकविचलनेन च सह"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS\n"
"string.text"
msgid "Axis"
msgstr "अक्षः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS_X\n"
"string.text"
msgid "X Axis"
msgstr "X अक्षः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS_Y\n"
"string.text"
msgid "Y Axis"
msgstr "Y अक्षः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXIS_Z\n"
"string.text"
msgid "Z Axis"
msgstr "Z अक्षः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_SECONDARY_X_AXIS\n"
"string.text"
msgid "Secondary X Axis"
msgstr "गौणः Y अक्षः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_SECONDARY_Y_AXIS\n"
"string.text"
msgid "Secondary Y Axis"
msgstr "गौणः Y अक्षः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AXES\n"
"string.text"
msgid "Axes"
msgstr "अक्षाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRIDS\n"
"string.text"
msgid "Grids"
msgstr "जालानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID\n"
"string.text"
msgid "Grid"
msgstr "जालिका"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MAJOR_X\n"
"string.text"
msgid "X Axis Major Grid"
msgstr "X अक्षमुख्यजालम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MAJOR_Y\n"
"string.text"
msgid "Y Axis Major Grid"
msgstr "Y अक्षमुख्यजालम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MAJOR_Z\n"
"string.text"
msgid "Z Axis Major Grid"
msgstr "Z अक्षमुख्यजालम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MINOR_X\n"
"string.text"
msgid "X Axis Minor Grid"
msgstr "X अक्षगौणजालम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MINOR_Y\n"
"string.text"
msgid "Y Axis Minor Grid"
msgstr "Y अक्षगौणजालम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_GRID_MINOR_Z\n"
"string.text"
msgid "Z Axis Minor Grid"
msgstr "Z अक्षगौणजालम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_LEGEND\n"
"string.text"
msgid "Legend"
msgstr "पुष्पिका"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE\n"
"string.text"
msgid "Title"
msgstr "शीर्षकम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLES\n"
"string.text"
msgid "Titles"
msgstr "शीर्षकाणि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_MAIN\n"
"string.text"
msgid "Main Title"
msgstr "मुख्यशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_SUB\n"
"string.text"
msgid "Subtitle"
msgstr "उपशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_X_AXIS\n"
"string.text"
msgid "X Axis Title"
msgstr "X अक्षशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_Y_AXIS\n"
"string.text"
msgid "Y Axis Title"
msgstr "Y अक्षशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_Z_AXIS\n"
"string.text"
msgid "Z Axis Title"
msgstr "Z अक्षशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_SECONDARY_X_AXIS\n"
"string.text"
msgid "Secondary X Axis Title"
msgstr "गौणः X अक्षशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_TITLE_SECONDARY_Y_AXIS\n"
"string.text"
msgid "Secondary Y Axis Title"
msgstr "गौणः X अक्षशीर्षकः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_LABEL\n"
"string.text"
msgid "Label"
msgstr "अंकितकम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATALABELS\n"
"string.text"
msgid "Data Labels"
msgstr "लेखांकितकानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATAPOINT\n"
"string.text"
msgid "Data Point"
msgstr "लेखाबिंदु"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATAPOINTS\n"
"string.text"
msgid "Data Points"
msgstr "डाटाबिन्दवः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_LEGEND_SYMBOL\n"
"string.text"
msgid "Legend Key"
msgstr "लेजण्ड् कुञ्चिका"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATASERIES\n"
"string.text"
msgid "Data Series"
msgstr "लेखाश्रृंखला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DATASERIES_PLURAL\n"
"string.text"
msgid "Data Series"
msgstr "लेखाश्रृंखला"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVE\n"
"string.text"
msgid "Trend Line"
msgstr "ट्रेण्ट् रेखा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVES\n"
"string.text"
msgid "Trend Lines"
msgstr "ट्रेण्ट् रेखाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVE_WITH_PARAMETERS\n"
"string.text"
msgid "Trend line %FORMULA with accuracy R² = %RSQUARED"
msgstr "%FORMULA ट्रेण्ड् रेखा R² = %RSQUARED शुद्धतया सह"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_MOVING_AVERAGE_WITH_PARAMETERS\n"
"string.text"
msgid "Moving average trend line with period = %PERIOD"
msgstr ""

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_AVERAGE_LINE\n"
"string.text"
msgid "Mean Value Line"
msgstr "मध्यमूल्यरेखा"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_CURVE_EQUATION\n"
"string.text"
msgid "Equation"
msgstr "समीकरणम्"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_ERROR_BARS_X\n"
"string.text"
msgid "X Error Bars"
msgstr "Y दोषपट्टाः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_ERROR_BARS_Y\n"
"string.text"
msgid "Y Error Bars"
msgstr "Y दोषपट्टाः"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_ERROR_BARS_Z\n"
"string.text"
msgid "Z Error Bars"
msgstr "Y दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_STOCK_LOSS\n"
"string.text"
msgid "Stock Loss"
msgstr "व्यतिरेकविचलनम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_STOCK_GAIN\n"
"string.text"
msgid "Stock Gain"
msgstr "अन्वयविचलनम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_PAGE\n"
"string.text"
msgid "Chart Area"
msgstr "कोष्ठकपटक्षेत्रम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DIAGRAM\n"
"string.text"
msgid "Chart"
msgstr "तालिका"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DIAGRAM_WALL\n"
"string.text"
msgid "Chart Wall"
msgstr "कोष्ठकपटभित्तिः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_DIAGRAM_FLOOR\n"
"string.text"
msgid "Chart Floor"
msgstr "कोष्ठकपटतलम्"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_OBJECT_SHAPE\n"
"string.text"
msgid "Drawing Object"
msgstr "चित्रणवस्तूनि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATASERIES\n"
"string.text"
msgid "Data Series '%SERIESNAME'"
msgstr "डाटाततिः '%SERIESNAME'"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATAPOINT_INDEX\n"
"string.text"
msgid "Data Point %POINTNUMBER"
msgstr "डाटाबिन्दुः %POINTNUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATAPOINT_VALUES\n"
"string.text"
msgid "Values: %POINTVALUES"
msgstr "मूल्यानि : %POINTVALUES"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TIP_DATAPOINT\n"
"string.text"
msgid "Data Point %POINTNUMBER, data series %SERIESNUMBER, values: %POINTVALUES"
msgstr "%POINTNUMBER डाटाबिन्दुः, %SERIESNUMBER डाटाततिः, मूल्यानि : %POINTVALUES"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_STATUS_DATAPOINT_MARKED\n"
"string.text"
msgid "Data point %POINTNUMBER in data series %SERIESNUMBER selected, values: %POINTVALUES"
msgstr "%POINTNUMBER डाटाबिन्दुः %SERIESNUMBER डाटातत्यां वृतः, मूल्यानि : %POINTVALUES"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_STATUS_OBJECT_MARKED\n"
"string.text"
msgid "%OBJECTNAME selected"
msgstr "%OBJECTNAME वृतम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_STATUS_PIE_SEGMENT_EXPLODED\n"
"string.text"
msgid "Pie exploded by %PERCENTVALUE percent"
msgstr "%PERCENTVALUE प्रतिशतेन पै स्फुटितम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_FOR_SERIES\n"
"string.text"
msgid "%OBJECTNAME for Data Series '%SERIESNAME'"
msgstr "डाटा तति प्रति %OBJECTNAME  '%SERIESNAME'"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_OBJECT_FOR_ALL_SERIES\n"
"string.text"
msgid "%OBJECTNAME for all Data Series"
msgstr "सर्वेषां डाटा तति कृते %OBJECTNAME"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_CHARTTYPE\n"
"string.text"
msgid "Edit chart type"
msgstr "तालिकावर्गम् सम्पादय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_DATA_RANGES\n"
"string.text"
msgid "Edit data ranges"
msgstr "डाटागोचरान् सम्पादय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_3D_VIEW\n"
"string.text"
msgid "Edit 3D view"
msgstr "3D दृश्यं सम्पादय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_CHART_DATA\n"
"string.text"
msgid "Edit chart data"
msgstr "तालिकाडाटा सम्पादय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_TOGGLE_LEGEND\n"
"string.text"
msgid "Legend on/off"
msgstr "लेजण्ड् चालय/पिधत्स्व"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_TOGGLE_GRID_HORZ\n"
"string.text"
msgid "Horizontal grid major/major&minor/off"
msgstr ""

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_TOGGLE_GRID_VERTICAL\n"
"string.text"
msgid "Vertical grid major/major&minor/off"
msgstr ""

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_SCALE_TEXT\n"
"string.text"
msgid "Scale Text"
msgstr "विषयं मापय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_REARRANGE_CHART\n"
"string.text"
msgid "Automatic Layout"
msgstr "स्वचालिततपरिलेखः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ACTION_NOTPOSSIBLE\n"
"string.text"
msgid "This function cannot be completed with the selected objects."
msgstr "वृतवस्तुभिः सह इदं कृत्यं समापयितुं न शक्यते।"

#: Strings.src
#, fuzzy
msgctxt ""
"Strings.src\n"
"STR_ACTION_EDIT_TEXT\n"
"string.text"
msgid "Edit text"
msgstr "%1 अस्य पाठ्यं सम्पादय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_COLUMN_LABEL\n"
"string.text"
msgid "Column %COLUMNNUMBER"
msgstr "स्तम्भः %COLUMNNUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_ROW_LABEL\n"
"string.text"
msgid "Row %ROWNUMBER"
msgstr "पङ्क्तिः %ROWNUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_LABEL\n"
"string.text"
msgid "Name"
msgstr "नाम"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X\n"
"string.text"
msgid "X-Values"
msgstr "X-मूल्यानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y\n"
"string.text"
msgid "Y-Values"
msgstr "Y-मूल्यानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_SIZE\n"
"string.text"
msgid "Bubble Sizes"
msgstr "बब्बल् परिमाणानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X_ERROR\n"
"string.text"
msgid "X-Error-Bars"
msgstr "X-दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X_ERROR_POSITIVE\n"
"string.text"
msgid "Positive X-Error-Bars"
msgstr "अन्वयाः X-दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_X_ERROR_NEGATIVE\n"
"string.text"
msgid "Negative X-Error-Bars"
msgstr "व्यतिरेकाः X-दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y_ERROR\n"
"string.text"
msgid "Y-Error-Bars"
msgstr "Y-दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y_ERROR_POSITIVE\n"
"string.text"
msgid "Positive Y-Error-Bars"
msgstr "अन्वयाः Y-दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_Y_ERROR_NEGATIVE\n"
"string.text"
msgid "Negative Y-Error-Bars"
msgstr "व्यतिरेकाः Y-दोषपट्टाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_FIRST\n"
"string.text"
msgid "Open Values"
msgstr "उद्घाटितमूल्यानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_LAST\n"
"string.text"
msgid "Close Values"
msgstr "पिहितमूल्यानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_MIN\n"
"string.text"
msgid "Low Values"
msgstr "न्यूनमूल्यानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_MAX\n"
"string.text"
msgid "High Values"
msgstr "उच्चमूल्यानि"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_ROLE_CATEGORIES\n"
"string.text"
msgid "Categories"
msgstr "वर्गाः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_UNNAMED_SERIES\n"
"string.text"
msgid "Unnamed Series"
msgstr "नामरहितततिः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_UNNAMED_SERIES_WITH_INDEX\n"
"string.text"
msgid "Unnamed Series %NUMBER"
msgstr "नामरहितततिः %NUMBER"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_SELECT_RANGE_FOR_SERIES\n"
"string.text"
msgid "Select Range for %VALUETYPE of %SERIESNAME"
msgstr "%VALUETYPE of %SERIESNAME कृते गोचरं वृणु"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_SELECT_RANGE_FOR_CATEGORIES\n"
"string.text"
msgid "Select Range for Categories"
msgstr "वर्गेभ्यः गोचरं वृणु"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_SELECT_RANGE_FOR_DATALABELS\n"
"string.text"
msgid "Select Range for data labels"
msgstr "डाटालेबिल्स् कृते गोचरं वृणु"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_DATA_EDITOR_INCORRECT_INPUT\n"
"string.text"
msgid ""
"Your last input is incorrect.\n"
"Ignore this change and close the dialog?"
msgstr ""
"तव अन्तिमनिवेशः अशुद्धः।\n"
"एतं परिणामम् उपेक्ष्य संवादं पिधत्स्व?"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TEXT_DIRECTION_LTR\n"
"string.text"
msgid "Left-to-right"
msgstr "वामतः दक्षिणम्"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TEXT_DIRECTION_RTL\n"
"string.text"
msgid "Right-to-left"
msgstr "दक्षिणतः वामः"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_TEXT_DIRECTION_SUPER\n"
"string.text"
msgid "Use superordinate object settings"
msgstr "अधिअक्षीय-वस्तु-स्थापनायाः प्रयोगं कुरू"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PROPERTY_ROLE_FILLCOLOR\n"
"string.text"
msgid "Fill Color"
msgstr "वर्णं परिपूरय"

#: Strings.src
msgctxt ""
"Strings.src\n"
"STR_PROPERTY_ROLE_BORDERCOLOR\n"
"string.text"
msgid "Border Color"
msgstr "सीमा-वर्णः"

#: Strings_AdditionalControls.src
msgctxt ""
"Strings_AdditionalControls.src\n"
"STR_TEXT_SEPARATOR\n"
"string.text"
msgid "Separator"
msgstr "विभाजकः"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_COLUMN\n"
"string.text"
msgid "Column"
msgstr "स्तम्भः"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_BAR\n"
"string.text"
msgid "Bar"
msgstr "पट्टः"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_AREA\n"
"string.text"
msgid "Area"
msgstr "क्षेत्रम्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_PIE\n"
"string.text"
msgid "Pie"
msgstr "पै"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_PIE_EXPLODED\n"
"string.text"
msgid "Exploded Pie Chart"
msgstr "स्फुटित-पै-तालिका"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_DONUT_EXPLODED\n"
"string.text"
msgid "Exploded Donut Chart"
msgstr "स्फुटित-डोनट्-तालिका"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_DONUT\n"
"string.text"
msgid "Donut"
msgstr "डोनट्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_LINE\n"
"string.text"
msgid "Line"
msgstr "रेखा"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_XY\n"
"string.text"
msgid "XY (Scatter)"
msgstr "XY (प्रसारय)"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_POINTS_AND_LINES\n"
"string.text"
msgid "Points and Lines"
msgstr "बिन्दवः रेखाः च"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_POINTS_ONLY\n"
"string.text"
msgid "Points Only"
msgstr "बिन्दवः एव"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINES_ONLY\n"
"string.text"
msgid "Lines Only"
msgstr "रेखाः एव"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINES_3D\n"
"string.text"
msgid "3D Lines"
msgstr "3D रेखाः3-D Lines"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_COMBI_COLUMN_LINE\n"
"string.text"
msgid "Column and Line"
msgstr "स्तम्भः रेखा च"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINE_COLUMN\n"
"string.text"
msgid "Columns and Lines"
msgstr "स्तम्भाः रेखाः च"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_LINE_STACKEDCOLUMN\n"
"string.text"
msgid "Stacked Columns and Lines"
msgstr "स्टैक् कृताः स्तम्भाः रेखाः च"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_NET\n"
"string.text"
msgid "Net"
msgstr "नेट"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_STOCK\n"
"string.text"
msgid "Stock"
msgstr "स्टोक्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_1\n"
"string.text"
msgid "Stock Chart 1"
msgstr "सङ्ग्रहकोष्ठकपटः 1 "

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_2\n"
"string.text"
msgid "Stock Chart 2"
msgstr "सङ्ग्रहकोष्ठकपटः 2"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_3\n"
"string.text"
msgid "Stock Chart 3"
msgstr "सङ्ग्रहकोष्ठकपटः 3"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STOCK_4\n"
"string.text"
msgid "Stock Chart 4"
msgstr "सङ्ग्रहकोष्ठकपटः 4"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_NORMAL\n"
"string.text"
msgid "Normal"
msgstr "सामान्यम्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_STACKED\n"
"string.text"
msgid "Stacked"
msgstr "राशीकृतम्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_PERCENT\n"
"string.text"
msgid "Percent Stacked"
msgstr "स्टैक् कृतं प्रतिशतम्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_DEEP\n"
"string.text"
msgid "Deep"
msgstr "गहनम्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_FILLED\n"
"string.text"
msgid "Filled"
msgstr "पूरितम्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_BUBBLE\n"
"string.text"
msgid "Bubble"
msgstr "बब्बल्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_BUBBLE_1\n"
"string.text"
msgid "Bubble Chart"
msgstr "बब्बल् चार्ट्"

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_TYPE_GL3D_BAR\n"
"string.text"
msgid "GL3D Bar"
msgstr ""

#: Strings_ChartTypes.src
msgctxt ""
"Strings_ChartTypes.src\n"
"STR_GL3D_BAR\n"
"string.text"
msgid "GL3D Bar Chart"
msgstr ""

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_INVALID_NUMBER\n"
"string.text"
msgid "Numbers are required. Check your input."
msgstr "सङ्ख्याः आवश्यक्यः। तव निवेशं परीक्षस्व।"

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_STEP_GT_ZERO\n"
"string.text"
msgid "The major interval requires a positive number. Check your input."
msgstr "मुख्यान्तरालाय अन्वयसङ्ख्या आवश्यकी। तव निवेशं परीक्षस्व।"

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_BAD_LOGARITHM\n"
"string.text"
msgid "The logarithmic scale requires positive numbers. Check your input."
msgstr "लघुगणनामापकाय अन्वयसङ्ख्याः आवश्यक्यः। तव निवेशं परीक्षस्व।"

#: Strings_Scale.src
msgctxt ""
"Strings_Scale.src\n"
"STR_MIN_GREATER_MAX\n"
"string.text"
msgid "The minimum must be lower than the maximum. Check your input."
msgstr "न्यूनतमम् अधिकतमात् न्यूनं भवितव्यम्। तव निवेशं परीक्षस्व।"

#: Strings_Scale.src
#, fuzzy
msgctxt ""
"Strings_Scale.src\n"
"STR_INVALID_INTERVALS\n"
"string.text"
msgid "The major interval needs to be greater than the minor interval. Check your input."
msgstr "मुख्यान्तरालाय अन्वयसङ्ख्या आवश्यकी। तव निवेशं परीक्षस्व।"

#: Strings_Scale.src
#, fuzzy
msgctxt ""
"Strings_Scale.src\n"
"STR_INVALID_TIME_UNIT\n"
"string.text"
msgid "The major and minor interval need to be greater or equal to the resolution. Check your input."
msgstr "मुख्यान्तरालाय अन्वयसङ्ख्या आवश्यकी। तव निवेशं परीक्षस्व।"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_INDICATE_BOTH\n"
"string.text"
msgid "Negative and Positive"
msgstr "अन्वयव्यतिरेकौ"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_INDICATE_DOWN\n"
"string.text"
msgid "Negative"
msgstr "व्यतिरेकः"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_INDICATE_UP\n"
"string.text"
msgid "Positive"
msgstr "अन्वयः"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_CONTROLTEXT_ERROR_BARS_FROM_DATA\n"
"string.text"
msgid "From Data Table"
msgstr "डाटासारण्याः"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_LINEAR\n"
"string.text"
msgid "Linear"
msgstr "रेखासमबन्धी"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_LOG\n"
"string.text"
msgid "Logarithmic"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_EXP\n"
"string.text"
msgid "Exponential"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_POWER\n"
"string.text"
msgid "Power"
msgstr "घातांकः"

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_POLYNOMIAL\n"
"string.text"
msgid "Polynomial"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_MOVING_AVERAGE\n"
"string.text"
msgid "Moving average"
msgstr ""

#: Strings_Statistic.src
msgctxt ""
"Strings_Statistic.src\n"
"STR_REGRESSION_MEAN\n"
"string.text"
msgid "Mean"
msgstr "मध्यम्"
