#. extracted from svtools/source/dialogs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-08-25 12:33+0200\n"
"PO-Revision-Date: 2014-09-29 06:19+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: sa_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1411971543.000000\n"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_NO_FIELD_SELECTION\n"
"string.text"
msgid "<none>"
msgstr "<न किमपि>"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_COMPANY\n"
"string.text"
msgid "Company"
msgstr "संस्था"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_DEPARTMENT\n"
"string.text"
msgid "Department"
msgstr "विभागः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_FIRSTNAME\n"
"string.text"
msgid "First name"
msgstr "सञ्चिकानाम"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_LASTNAME\n"
"string.text"
msgid "Last name"
msgstr "अन्तिमनाम"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_STREET\n"
"string.text"
msgid "Street"
msgstr "मार्गः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_COUNTRY\n"
"string.text"
msgid "Country"
msgstr "देशः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_ZIPCODE\n"
"string.text"
msgid "ZIP Code"
msgstr "ZIP कूटः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_CITY\n"
"string.text"
msgid "City"
msgstr "नगरम्"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_TITLE\n"
"string.text"
msgid "Title"
msgstr "शीर्षकम्"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_POSITION\n"
"string.text"
msgid "Position"
msgstr "स्थितिः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_ADDRFORM\n"
"string.text"
msgid "Addr. Form"
msgstr "पत्रा. प्रपत्रम्"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_INITIALS\n"
"string.text"
msgid "Initials"
msgstr "आद्यक्षराः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_SALUTATION\n"
"string.text"
msgid "Complimentary close"
msgstr "पूरकं पिधत्स्व"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_HOMETEL\n"
"string.text"
msgid "Tel: Home"
msgstr "दूरवाणी : गृहम् "

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_WORKTEL\n"
"string.text"
msgid "Tel: Work"
msgstr "दूरवाणी : कार्यालयः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_FAX\n"
"string.text"
msgid "FAX"
msgstr "फैक्स"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_EMAIL\n"
"string.text"
msgid "E-mail"
msgstr "ई-मैल्"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_URL\n"
"string.text"
msgid "URL"
msgstr "URL"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_NOTE\n"
"string.text"
msgid "Note"
msgstr "टिप्पणी"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER1\n"
"string.text"
msgid "User 1"
msgstr "प्रयोक्ता 1"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER2\n"
"string.text"
msgid "User 2"
msgstr "प्रयोक्ता 2"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER3\n"
"string.text"
msgid "User 3"
msgstr "प्रयोक्ता 3"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_USER4\n"
"string.text"
msgid "User 4"
msgstr "प्रयोक्ता 4"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_ID\n"
"string.text"
msgid "ID"
msgstr "ID"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_STATE\n"
"string.text"
msgid "State"
msgstr "राज्यम्"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_OFFICETEL\n"
"string.text"
msgid "Tel: Office"
msgstr "दूरभाषः : कार्यालयः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_PAGER\n"
"string.text"
msgid "Pager"
msgstr "पेजर"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_MOBILE\n"
"string.text"
msgid "Mobile"
msgstr "चलभाषः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_TELOTHER\n"
"string.text"
msgid "Tel: Other"
msgstr "दूरभाषः : अन्यः"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_CALENDAR\n"
"string.text"
msgid "Calendar"
msgstr "पञ्चांगम्"

#: addresstemplate.src
msgctxt ""
"addresstemplate.src\n"
"STR_FIELD_INVITE\n"
"string.text"
msgid "Invite"
msgstr "आमन्त्रय "

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_FILEDLG_OPEN\n"
"string.text"
msgid "Open"
msgstr "उद्घाटय"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_FILEDLG_TYPE\n"
"string.text"
msgid "File ~type"
msgstr "सञ्चिकाप्रकारः"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_FILEDLG_SAVE\n"
"string.text"
msgid "Save"
msgstr "सञ्चय"

#: filedlg2.src
msgctxt ""
"filedlg2.src\n"
"STR_SVT_DEFAULT_SERVICE_LABEL\n"
"string.text"
msgid "$user$'s $service$"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_STRING\n"
"string.text"
msgid "Unformatted text"
msgstr "असंरचितपाठः "

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_BITMAP\n"
"string.text"
msgid "Bitmap"
msgstr "बिट्मैप्"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_GDIMETAFILE\n"
"string.text"
msgid "GDI metafile"
msgstr "GDI अधिसञ्चिका"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_RTF\n"
"string.text"
msgid "Formatted text [RTF]"
msgstr "संरचितपाठः [RTF]"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DRAWING\n"
"string.text"
msgid "Drawing format"
msgstr "आलेखनसंरचना"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SVXB\n"
"string.text"
msgid "SVXB (StarView bitmap/animation)"
msgstr "SVXB (StarView बिट्मैप्/सञ्जीवनम्)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_INTERNALLINK_STATE\n"
"string.text"
msgid "Status Info from Svx Internal Link"
msgstr "Svx आन्तरिकसंपर्कात् आस्थितिसूचना"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SOLK\n"
"string.text"
msgid "SOLK (%PRODUCTNAME Link)"
msgstr "SOLK (%PRODUCTNAME संपर्कः)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_NETSCAPE_BOOKMARK\n"
"string.text"
msgid "Netscape Bookmark"
msgstr "Netscapeपुस्तकचिह्नम्"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARSERVER\n"
"string.text"
msgid "Star server format"
msgstr "स्टार्सर्वरसंरचना"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STAROBJECT\n"
"string.text"
msgid "Star object format"
msgstr "स्टार्वस्तुसंरचना"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_APPLETOBJECT\n"
"string.text"
msgid "Applet object"
msgstr "Applet वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_PLUGIN_OBJECT\n"
"string.text"
msgid "Plug-in object"
msgstr "प्लग्-इन् वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_30\n"
"string.text"
msgid "StarWriter 3.0 object"
msgstr "StarWriter 3.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_40\n"
"string.text"
msgid "StarWriter 4.0 object"
msgstr "StarWriter 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_50\n"
"string.text"
msgid "StarWriter 5.0 object"
msgstr "StarWriter 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERWEB_40\n"
"string.text"
msgid "StarWriter/Web 4.0 object"
msgstr "StarWriter/Web 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERWEB_50\n"
"string.text"
msgid "StarWriter/Web 5.0 object"
msgstr "StarWriter/Web 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERGLOB_40\n"
"string.text"
msgid "StarWriter/Master 4.0 object"
msgstr "StarWriter/Master 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERGLOB_50\n"
"string.text"
msgid "StarWriter/Master 5.0 object"
msgstr "StarWriter/Master 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW\n"
"string.text"
msgid "StarDraw object"
msgstr "StarDraw वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW_40\n"
"string.text"
msgid "StarDraw 4.0 object"
msgstr "StarDraw 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMPRESS_50\n"
"string.text"
msgid "StarImpress 5.0 object"
msgstr "StarImpress 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW_50\n"
"string.text"
msgid "StarDraw 5.0 object"
msgstr "StarDraw 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC\n"
"string.text"
msgid "StarCalc object"
msgstr "StarCalc वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC_40\n"
"string.text"
msgid "StarCalc  4.0 object"
msgstr "StarCalc  4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC_50\n"
"string.text"
msgid "StarCalc 5.0 object"
msgstr "StarCalc 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART\n"
"string.text"
msgid "StarChart object"
msgstr "StarChart वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART_40\n"
"string.text"
msgid "StarChart 4.0 object"
msgstr "StarChart 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART_50\n"
"string.text"
msgid "StarChart 5.0 object"
msgstr "StarChart 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMAGE\n"
"string.text"
msgid "StarImage object"
msgstr "StarImage वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMAGE_40\n"
"string.text"
msgid "StarImage 4.0 object"
msgstr "StarImage 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMAGE_50\n"
"string.text"
msgid "StarImage 5.0 object"
msgstr "StarImage 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH\n"
"string.text"
msgid "StarMath object"
msgstr "StarMath वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH_40\n"
"string.text"
msgid "StarMath 4.0 object"
msgstr "StarMath 4.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH_50\n"
"string.text"
msgid "StarMath 5.0 object"
msgstr "StarMath 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STAROBJECT_PAINTDOC\n"
"string.text"
msgid "StarObject Paint object"
msgstr "StarObject चित्रणवस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_HTML\n"
"string.text"
msgid "HTML (HyperText Markup Language)"
msgstr "HTML (हाइपर् टेक्स्ट् मार्क्अप् लैंग्वेज)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_HTML_SIMPLE\n"
"string.text"
msgid "HTML format"
msgstr "HTML संरचना"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_BIFF_5\n"
"string.text"
msgid "Biff5 (Microsoft Excel 5.0/95)"
msgstr "Biff5 (Microsoft Excel 5.0/95)"

#: formats.src
#, fuzzy
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_BIFF_8\n"
"string.text"
msgid "Biff8 (Microsoft Excel 97/2000/XP/2003)"
msgstr "Biff8 (Microsoft Excel 97/2000/XP)"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SYLK\n"
"string.text"
msgid "Sylk"
msgstr "सिल्क"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_LINK\n"
"string.text"
msgid "DDE link"
msgstr "DDE संपर्कः"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DIF\n"
"string.text"
msgid "DIF"
msgstr "DIF"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_MSWORD_DOC\n"
"string.text"
msgid "Microsoft Word object"
msgstr "Microsoft Word वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STAR_FRAMESET_DOC\n"
"string.text"
msgid "StarFrameSet object"
msgstr "StarFrameSet वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_OFFICE_DOC\n"
"string.text"
msgid "Office document object"
msgstr "Office लेख्यपत्रवस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_NOTES_DOCINFO\n"
"string.text"
msgid "Notes document info"
msgstr "टिप्पणीलेख्यपत्रसूचना"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_SFX_DOC\n"
"string.text"
msgid "Sfx document"
msgstr "Sfx लेख्यपत्रम्"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHARTDOCUMENT_50\n"
"string.text"
msgid "StarChart 5.0 object"
msgstr "StarChart 5.0 वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_GRAPHOBJ\n"
"string.text"
msgid "Graphic object"
msgstr "चित्रीयं वस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITER_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Writer object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERWEB_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Writer/Web object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARWRITERGLOB_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Writer/Master object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARDRAW_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Draw object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARIMPRESS_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Impress object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCALC_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Calc object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARCHART_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Chart object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_STARMATH_60\n"
"string.text"
msgid "OpenOffice.org 1.0 Math object"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_WMF\n"
"string.text"
msgid "Windows metafile"
msgstr "Windows अधिसञ्चिका"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DBACCESS_QUERY\n"
"string.text"
msgid "Data source object"
msgstr "लेखामूलःवस्तु"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DBACCESS_TABLE\n"
"string.text"
msgid "Data source table"
msgstr "लेखामूलःसारणी"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DBACCESS_COMMAND\n"
"string.text"
msgid "SQL query"
msgstr "SQL प्रश्न"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_DIALOG_60\n"
"string.text"
msgid "OpenOffice.org 1.0 dialog"
msgstr ""

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_FILEGRPDESCRIPTOR\n"
"string.text"
msgid "Link"
msgstr "संपर्कः"

#: formats.src
msgctxt ""
"formats.src\n"
"STR_FORMAT_ID_HTML_NO_COMMENT\n"
"string.text"
msgid "HTML format without comments"
msgstr "HTML संरचना टिप्पणीभिः विना"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_GENERALERROR&S_MAX\n"
"string.text"
msgid "General OLE error."
msgstr "सामान्या OLE त्रुटिः।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_CANT_BINDTOSOURCE&S_MAX\n"
"string.text"
msgid "The connection to the object cannot be established."
msgstr "वस्तुनः संयोगं स्थापयितुं न शक्यते।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOCACHE_UPDATED&S_MAX\n"
"string.text"
msgid "No cache files were updated."
msgstr "काऽपि गुप्तस्थानीयास्मृतिसञ्चिका न अद्यतनीकृता।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_SOMECACHES_NOTUPDATED&S_MAX\n"
"string.text"
msgid "Some cache files were not updated."
msgstr "काश्चित् गुप्तस्थायीस्मृतिसञ्चिकाः न अद्यतनीकृताः।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_UNAVAILABLE&S_MAX\n"
"string.text"
msgid "Status of object cannot be determined in a timely manner."
msgstr "वस्तुनः स्थितिः समयबद्धरूपेण निर्धारयितुं न शक्यते।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_E_CLASSDIFF&S_MAX\n"
"string.text"
msgid "Source of the OLE link has been converted."
msgstr "OLE सम्पर्कस्य स्थितेः मूलः रूपान्तरितम्।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_OBJECT&S_MAX\n"
"string.text"
msgid "The object could not be found."
msgstr "वस्तु प्राप्तुं न शक्यते।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_EXCEEDED_DEADLINE&S_MAX\n"
"string.text"
msgid "The process could not be completed within the specified time period."
msgstr "कार्यं निर्दिष्टसमयान्तराले समापयितुं न शक्यते।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_CONNECT_MANUALLY&S_MAX\n"
"string.text"
msgid "OLE could not connect to a network device (server)."
msgstr "OLE जालककेन्द्रो संयोगयितुं न शक्तम् (सर्वर्)।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_INTERMEDIATE_INTERFACE_NOT_SUPPORTED&S_MAX\n"
"string.text"
msgid "The object found does not support the interface required for the desired operation."
msgstr "प्राप्तं वस्तु कार्यं चालयितुम् अपेक्षितम् आधाररफलकं न समर्थयति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NO_INTERFACE&S_MAX\n"
"string.text"
msgid "Interface not supported."
msgstr "आधारफलकं न समर्थितम्।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_OUT_OF_MEMORY&S_MAX\n"
"string.text"
msgid "Insufficient memory."
msgstr "अपर्याप्तस्मृतिः ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_SYNTAX&S_MAX\n"
"string.text"
msgid "The connection name could not be processed."
msgstr "संयोगनाम संसाधयितुं न शक्यते।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_REDUCED_TO_SELF&S_MAX\n"
"string.text"
msgid "The connection name could not be reduced further."
msgstr "संयोगनाम अधिकं लघूकर्तुं न शक्यते।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_INVERSE&S_MAX\n"
"string.text"
msgid "The connection name has no inverse."
msgstr "संयोगनाम्नः व्यतिक्रमः नास्ति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_PREFIX&S_MAX\n"
"string.text"
msgid "No common prefix exists."
msgstr "कोऽपि सामान्यः उपसर्गः न वर्तते ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_HIM&S_MAX\n"
"string.text"
msgid "The connection name is contained in the other one."
msgstr "संयोगनाम अन्यस्मिन् समाविष्टमस्ति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_US&S_MAX\n"
"string.text"
msgid "The connection names (the receiver and the other moniker) are identical."
msgstr "संयोगनामनी (प्रापकः अन्यत् उपनाम च) समाने स्तः।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_ME&S_MAX\n"
"string.text"
msgid "The connection name is contained in the other one."
msgstr "संयोगनाम अन्यस्मिन् समाविष्टमस्ति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NOT_BINDABLE&S_MAX\n"
"string.text"
msgid "The connection name cannot be connected. This is a relative name."
msgstr "संयोगनाम संबन्धयितुं न शक्यते।एतत् सापेक्षिकं नाम अस्ति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOT_IMPLEMENTED&S_MAX\n"
"string.text"
msgid "Operation not implemented."
msgstr "संचालनं न कार्यान्वितम् ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NO_STORAGE&S_MAX\n"
"string.text"
msgid "No storage."
msgstr "न भण्डारणम्।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_FALSE&S_MAX\n"
"string.text"
msgid "False."
msgstr "असत्यम् ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_NEED_GENERIC&S_MAX\n"
"string.text"
msgid "Monikers must be composed generically."
msgstr "उपनाम अवश्यमेव प्रगातीयप्रकारेण रचितव्यम्।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_PENDING&S_MAX\n"
"string.text"
msgid "Data not available at this time."
msgstr "आधारसामग्री अधुना न उपलब्धा।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOT_INPLACEACTIVE & S_MAX\n"
"string.text"
msgid "Object could not be activated InPlace."
msgstr "वस्तु योग्यस्थानं सक्रियं कर्तुं न शक्तम्।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_LINDEX & S_MAX\n"
"string.text"
msgid "Invalid index."
msgstr "असिद्धानुक्रमणी।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_CANNOT_DOVERB_NOW & S_MAX\n"
"string.text"
msgid "The action cannot be executed in the object's current state."
msgstr "वस्तुन वर्तमानावस्थायां एषा क्रिया कर्तुं न शक्यते ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_OLEOBJ_INVALIDHWND & S_MAX\n"
"string.text"
msgid "An invalid window was passed when activated."
msgstr "सक्रियकरणेसमये एकम् असिद्धविण्डो पारंगतम् आसीत्।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOVERBS & S_MAX\n"
"string.text"
msgid "The object does not support any actions."
msgstr "वस्तु कान्चिदपि क्रियाः न समर्थयति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_INVALIDVERB & S_MAX\n"
"string.text"
msgid "The action is not defined. The default action will be executed."
msgstr "क्रिया परिभाषिता नास्ति।मूलभूतक्रिया निष्पादिता भविष्यति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_CONNECT & S_MAX\n"
"string.text"
msgid "A link to the network could not be re-established."
msgstr "जालकेन्द्रेण सह संपर्कः पुनः-स्थापयितुं न शक्तः।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_NOTIMPL & S_MAX\n"
"string.text"
msgid "Object does not support this action."
msgstr "वस्तु इमां क्रियां न समर्थयति।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERROR_HANDLER\n"
"ERRCODE_SO_MK_CANTOPENFILE & S_MAX\n"
"string.text"
msgid "The specified file could not be opened."
msgstr "निर्दिष्टा सञ्चिका उद्घाटयितुं न शक्यते ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"RID_SO_ERRCTX\n"
"ERRCTX_SO_DOVERB\n"
"string.text"
msgid "$(ERR) activating object"
msgstr "$(ERR) वस्तु सक्रियं करोति"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_ERROR_OBJNOCREATE\n"
"string.text"
msgid "Object % could not be inserted."
msgstr "वस्तु % समावेशयितुं न शक्यते ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_ERROR_OBJNOCREATE_FROM_FILE\n"
"string.text"
msgid "Object from file % could not be inserted."
msgstr "सञ्चिका % तः वस्तु समावेशयितुं न शक्यते ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_ERROR_OBJNOCREATE_PLUGIN\n"
"string.text"
msgid "Plug-in from document % could not be inserted."
msgstr "लेख्यपत्र % तः प्लग-इन समावेशयितुं न शक्यते ।"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_FURTHER_OBJECT\n"
"string.text"
msgid "Further objects"
msgstr "अधिकानि वस्तूनि"

#: so3res.src
msgctxt ""
"so3res.src\n"
"STR_UNKNOWN_SOURCE\n"
"string.text"
msgid "Unknown source"
msgstr "अज्ञातमूलः"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_FINISH\n"
"string.text"
msgid "~Finish"
msgstr "समाप्तः"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_NEXT\n"
"string.text"
msgid "~Next >>"
msgstr "अग्रिमः >>"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_PREVIOUS\n"
"string.text"
msgid "<< Bac~k"
msgstr "<< पश्चम्"

#: wizardmachine.src
msgctxt ""
"wizardmachine.src\n"
"STR_WIZDLG_ROADMAP_TITLE\n"
"string.text"
msgid "Steps"
msgstr "पदानि "
