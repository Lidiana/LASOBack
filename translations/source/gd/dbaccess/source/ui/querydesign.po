#. extracted from dbaccess/source/ui/querydesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-24 01:09+0000\n"
"Last-Translator: Michael Bauer <fios@akerbeltz.org>\n"
"Language-Team: Akerbeltz\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : (n > 2 && n < 20) ? 2 : 3;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-Project-Style: openoffice\n"
"X-POOTLE-MTIME: 1482541763.000000\n"

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_CONNECTION\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "Sgua~b às"

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_CONNECTION\n"
"ID_QUERY_EDIT_JOINCONNECTION\n"
"menuitem.text"
msgid "Edit..."
msgstr "Deasaich..."

#: query.src
msgctxt ""
"query.src\n"
"RID_MENU_JOINVIEW_TABLE\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "Sgua~b às"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYCOLPOPUPMENU\n"
"ID_BROWSER_COLWIDTH\n"
"menuitem.text"
msgid "Column ~Width..."
msgstr "L~eud a’ chuilbh…"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYCOLPOPUPMENU\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr "Sgua~b às"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABWINSHOW\n"
"string.text"
msgid "Add Table Window"
msgstr "Cuir uinneag clàir ris"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_MOVETABWIN\n"
"string.text"
msgid "Move table window"
msgstr "Gluais uinneag a' chlàir"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_INSERTCONNECTION\n"
"string.text"
msgid "Insert Join"
msgstr "Cuir a-steach alt"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_REMOVECONNECTION\n"
"string.text"
msgid "Delete Join"
msgstr "Sguab às an t-alt"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_SIZETABWIN\n"
"string.text"
msgid "Resize table window"
msgstr "Atharraich meud uinneag a' chlàir"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDDELETE\n"
"string.text"
msgid "Delete Column"
msgstr "Sguab às an colbh"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDMOVED\n"
"string.text"
msgid "Move column"
msgstr "Gluais an colbh"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABFIELDCREATE\n"
"string.text"
msgid "Add Column"
msgstr "Cuir colbh ris"

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_TABLE_DOESNT_EXIST\n"
"string.text"
msgid "Invalid expression, table '$name$' does not exist."
msgstr "Eas-preisean mì-dhligheach, chan eil an clàr \"$name$\" ann."

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_FIELD_DOESNT_EXIST\n"
"string.text"
msgid "Invalid expression, field name '$name$' does not exist."
msgstr "Eas-preisean mì-dhligheach, chan eil an raon $name$ ann."

#: query.src
msgctxt ""
"query.src\n"
"RID_STR_TOMUCHTABLES\n"
"string.text"
msgid "The query covers #num# tables. The selected database type, however, can only process a maximum of #maxnum# table(s) per statement."
msgstr "Tha a' cheist seo a' buntainn ri #num# clàr. Ge-tà, chan eil an seòrsa de stòr-dàta a thagh thu a' cur taic ach ri #maxnum# clàir gach radh air a' char as motha."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_TABWINDELETE\n"
"string.text"
msgid "Delete Table Window"
msgstr "Sguab às uinneag a' chlàir"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_MODIFY_CELL\n"
"string.text"
msgid "Edit Column Description"
msgstr "Deasaich tuairisgeul a' chuilbh"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_UNDO_SIZE_COLUMN\n"
"string.text"
msgid "Adjust column width"
msgstr "Cuir air gleus leud a' chuilbh"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_SORTTEXT\n"
"string.text"
msgid "(not sorted);ascending;descending"
msgstr "(gun seòrsachadh);a' dìreadh;a' teàrnadh"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_FUNCTIONS\n"
"string.text"
msgid "(no function);Group"
msgstr "(gun fhoincsean);Buidhnich"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_NOTABLE\n"
"string.text"
msgid "(no table)"
msgstr "(gun chlàr)"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ORDERBY_UNRELATED\n"
"string.text"
msgid "The database only supports sorting for visible fields."
msgstr "Chan eil an stòr-dàta a' cur taic do sheòrsachadh de raointean do-fhaicsinneach."

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_FUNCTION\n"
"menuitem.text"
msgid "Functions"
msgstr "Foincseanan"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_TABLENAME\n"
"menuitem.text"
msgid "Table Name"
msgstr "Ainm a' chlàir"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_ALIASNAME\n"
"menuitem.text"
msgid "Alias"
msgstr "Ainm-brèige"

#: query.src
msgctxt ""
"query.src\n"
"RID_QUERYFUNCTION_POPUPMENU\n"
"ID_QUERY_DISTINCT\n"
"menuitem.text"
msgid "Distinct Values"
msgstr "Luachan àraidh"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_HANDLETEXT\n"
"string.text"
msgid "Field;Alias;Table;Sort;Visible;Function;Criterion;Or;Or"
msgstr "Raon;Ainm-brèige;Clàr;Seòrsaich;Faicsinneach;Foincsean;No;No"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_LIMIT_ALL\n"
"string.text"
msgid "All"
msgstr "A h-uile"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_MANY_COLUMNS\n"
"string.text"
msgid "There are too many columns."
msgstr "Tha cus cholbhan ann."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_CRITERIA_ON_ASTERISK\n"
"string.text"
msgid "A condition cannot be applied to field [*]"
msgstr "Chan urrainn dhut cumha a chur an sàs san raon [*]"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_LONG_STATEMENT\n"
"string.text"
msgid "The SQL statement created is too long."
msgstr "Tha an aithris SQL a chruthaich thu ro fhada."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOOCOMPLEX\n"
"string.text"
msgid "Query is too complex"
msgstr "Tha a' cheist ro thoinnte"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_NOSELECT\n"
"string.text"
msgid "Nothing has been selected."
msgstr "Cha do thagh thu dad."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOOMANYCOND\n"
"string.text"
msgid "Too many search criteria"
msgstr "Cus cuspair-deuchainnean ann"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_SYNTAX\n"
"string.text"
msgid "SQL syntax error"
msgstr "Mearachd ann an co-chàradh an SQL"

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ORDERBY_ON_ASTERISK\n"
"string.text"
msgid "[*] cannot be used as a sort criterion."
msgstr "Chan urrainn dhut [*] a chleachdadh mar chuspair-deuchainn."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_TOO_MANY_TABLES\n"
"string.text"
msgid "There are too many tables."
msgstr "Tha cus chlàr ann."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_NATIVE\n"
"string.text"
msgid "The statement will not be applied when querying in the SQL dialect of the database."
msgstr "Cha dèid an aithris a chur an sàs nuair a nithear ceist ann an dual-chainnt SQL an stòir-dhàta."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_ILLEGAL_JOIN\n"
"string.text"
msgid "Join could not be processed"
msgstr "Cha do ghabh an t-alt a phròiseadadh"

#: query.src
msgctxt ""
"query.src\n"
"STR_SVT_SQL_SYNTAX_ERROR\n"
"string.text"
msgid "Syntax error in SQL statement"
msgstr "Mearachd co-chàraidh ann an aithris an SQL"

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN_NO_VIEW_SUPPORT\n"
"string.text"
msgid "This database does not support table views."
msgstr "Chan eil an stòr-dàta a' cur taic ri seallaidhean clàir."

#: query.src
msgctxt ""
"query.src\n"
"STR_NO_ALTER_VIEW_SUPPORT\n"
"string.text"
msgid "This database does not support altering of existing table views."
msgstr "Chan eil an stòr-dàta seo a' cur taic ri atharrachadh de sheallaidhean clàir làithreach."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN_NO_VIEW_ASK\n"
"string.text"
msgid "Do you want to create a query instead?"
msgstr "A bheil thu airson ceist a chruthachadh 'na àite?"

#: query.src
msgctxt ""
"query.src\n"
"STR_DATASOURCE_DELETED\n"
"string.text"
msgid "The corresponding data source has been deleted. Therefore, data relevant to that data source cannot be saved."
msgstr "Chaidh an tùs-dàta a tha co-cheangailte ris a sguabadh às. Mar sin dheth, chan urrainn dhut dàta a shàbhaladh a tha co-cheangailte ris an tùs-dàta sin."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_COLUMN_NOT_FOUND\n"
"string.text"
msgid "The column '$name$' is unknown."
msgstr "Chan aithnichear an colbh \"$name$\"."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_JOIN_COLUMN_COMPARE\n"
"string.text"
msgid "Columns can only be compared using '='."
msgstr "Chan urrainn dhut coimeas a dhèanamh eadar colbhan ach le \"=\"."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_LIKE_LEFT_NO_COLUMN\n"
"string.text"
msgid "You must use a column name before 'LIKE'."
msgstr "Feumaidh tu ainm cuilbh a chur air beulaibh \"LIKE\"."

#: query.src
msgctxt ""
"query.src\n"
"STR_QRY_CHECK_CASESENSITIVE\n"
"string.text"
msgid "The column could not be found. Please note that the database is case-sensitive."
msgstr "Cha deach an colbh a lorg. Dèan cinnteach gu bheil aire ri litrichean mòra is beaga san stòr-dàta."

#: query.src
msgctxt ""
"query.src\n"
"STR_QUERYDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: Query Design"
msgstr " - %PRODUCTNAME Base: Dealbhadh cheistean"

#: query.src
msgctxt ""
"query.src\n"
"STR_VIEWDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: View Design"
msgstr " - %PRODUCTNAME Base: Dealbhadh sheallaidhean"

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_QUERY_SAVEMODIFIED\n"
"string.text"
msgid ""
"$object$ has been changed.\n"
"Do you want to save the changes?"
msgstr ""
"Chaidh $object$ atharrachadh.\n"
"A bheil thu airson na h-atharraichean a shàbhaladh?"

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource (except "SQL command", which doesn't make sense here) will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_ERROR_PARSING_STATEMENT\n"
"string.text"
msgid "$object$ is based on an SQL command which could not be parsed."
msgstr "Tha $object$ stèidhichte air àithne SQL nach gabh a pharsadh."

#. For $object$, one of the values of the RSC_QUERY_OBJECT_TYPE resource (except "SQL command", which doesn't make sense here) will be inserted.
#: query.src
msgctxt ""
"query.src\n"
"STR_INFO_OPENING_IN_SQL_VIEW\n"
"string.text"
msgid "$object$ will be opened in SQL view."
msgstr "Thèid $object$ fhosgladh san t-sealladh SQL."

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The table view\n"
"itemlist.text"
msgid "The table view"
msgstr "Sealladh a’ chlàir"

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The query\n"
"itemlist.text"
msgid "The query"
msgstr "A’ cheist"

#: query.src
msgctxt ""
"query.src\n"
"RSC_QUERY_OBJECT_TYPE\n"
"The SQL statement\n"
"itemlist.text"
msgid "The SQL statement"
msgstr "An àithne SQL"

#: query.src
msgctxt ""
"query.src\n"
"STR_STATEMENT_WITHOUT_RESULT_SET\n"
"string.text"
msgid "The query does not create a result set, and thus cannot be part of another query."
msgstr "Cha chruthaich a' cheist seata thoraidhean agus mar sin dheth, chan urrainn dhut a chleachdadh mar phàirt de cheist eile."

#: query.src
msgctxt ""
"query.src\n"
"STR_NO_DATASOURCE_OR_CONNECTION\n"
"string.text"
msgid "Both the ActiveConnection and the DataSourceName parameter are missing or wrong - cannot initialize the query designer."
msgstr "Tha an dà chuid paramadair an ActiveConnection agus DataSourceName a dhìth no cearr - cha ghabh dealbhaiche nan ceist a chur gu dol."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_JOIN_TYPE_HINT\n"
"string.text"
msgid "Please note that some databases may not support this join type."
msgstr "Thoir an aire nach eil cuid de stòir-dhàta a' cur taic ris an t-seòrsa seo de dh'alt."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_INNER_JOIN\n"
"string.text"
msgid "Includes only records for which the contents of the related fields of both tables are identical."
msgstr "Gabhaidh seo a-steach reacordan a-mhàin a tha susbaint nan raointean co-cheangailte co-ionnann san dà chlàr."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_LEFTRIGHT_JOIN\n"
"string.text"
msgid "Contains ALL records from table '%1' but only records from table '%2' where the values in the related fields are matching."
msgstr "Tha a-steach GACH reacord on chlàr \"%1\" ann ach reacordan on chlàr \"%2\" a-mhàin far a bheil na luachan a' freagairt ri chèile sna raointean co-cheangailte."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_FULL_JOIN\n"
"string.text"
msgid "Contains ALL records from '%1' and from '%2'."
msgstr "Tha GACH reacord o \"%1\" agus \"%2\" ann."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_CROSS_JOIN\n"
"string.text"
msgid "Contains the Cartesian product of ALL records from '%1' and from '%2'."
msgstr "Tha toradh Cartesach de GACH reacord o \"%1\" agus \"%2\" ann."

#: querydlg.src
msgctxt ""
"querydlg.src\n"
"STR_QUERY_NATURAL_JOIN\n"
"string.text"
msgid "Contains only one column for each pair of equally-named columns from '%1' and from '%2'."
msgstr "Chan eil ann ach aon cholbh airson gach paidhir de cholbhan o \"%1\" is \"%2\" air a bheil an dearbh-ainm."
