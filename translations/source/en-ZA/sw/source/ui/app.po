#. extracted from sw/source/ui/app
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-07-04 20:21+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en_ZA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1467663690.000000\n"

#: app.src
msgctxt ""
"app.src\n"
"STR_PRINT_MERGE_MACRO\n"
"string.text"
msgid "Print form letters"
msgstr "Print form letters"

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGE_COUNT_MACRO\n"
"string.text"
msgid "Changing the page count"
msgstr "Changing the page count"

#: app.src
msgctxt ""
"app.src\n"
"STR_PARAGRAPHSTYLEFAMILY\n"
"string.text"
msgid "Paragraph Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CHARACTERSTYLEFAMILY\n"
"string.text"
msgid "Character Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_FRAMESTYLEFAMILY\n"
"string.text"
msgid "Frame Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGESTYLEFAMILY\n"
"string.text"
msgid "Page Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_LISTSTYLEFAMILY\n"
"string.text"
msgid "List Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_TABLESTYLEFAMILY\n"
"string.text"
msgid "Table Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"All Styles\n"
"itemlist.text"
msgid "All Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Automatic\n"
"itemlist.text"
msgid "Automatic"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Text Styles\n"
"itemlist.text"
msgid "Text Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Chapter Styles\n"
"itemlist.text"
msgid "Chapter Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"List Styles\n"
"itemlist.text"
msgid "List Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Index Styles\n"
"itemlist.text"
msgid "Index Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Special Styles\n"
"itemlist.text"
msgid "Special Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"HTML Styles\n"
"itemlist.text"
msgid "HTML Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PARAGRAPHSTYLEFAMILY\n"
"Conditional Styles\n"
"itemlist.text"
msgid "Conditional Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_CHARACTERSTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_FRAMESTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_PAGESTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_LISTSTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"All\n"
"itemlist.text"
msgid "All"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"Hidden Styles\n"
"itemlist.text"
msgid "Hidden Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"Applied Styles\n"
"itemlist.text"
msgid "Applied Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"RID_TABLESTYLEFAMILY\n"
"Custom Styles\n"
"itemlist.text"
msgid "Custom Styles"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_ENV_TITLE\n"
"string.text"
msgid "Envelope"
msgstr "Envelope"

#: app.src
msgctxt ""
"app.src\n"
"STR_LAB_TITLE\n"
"string.text"
msgid "Labels"
msgstr "Labels"

#: app.src
msgctxt ""
"app.src\n"
"STR_HUMAN_SWDOC_NAME\n"
"string.text"
msgid "Text"
msgstr "Text"

#: app.src
msgctxt ""
"app.src\n"
"STR_WRITER_DOCUMENT_FULLTYPE\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION Text Document"
msgstr "%PRODUCTNAME %PRODUCTVERSION Text Document"

#: app.src
msgctxt ""
"app.src\n"
"STR_CANTOPEN\n"
"string.text"
msgid "Cannot open document."
msgstr "Cannot open document."

#: app.src
msgctxt ""
"app.src\n"
"STR_CANTCREATE\n"
"string.text"
msgid "Can't create document."
msgstr "Can't create document."

#: app.src
msgctxt ""
"app.src\n"
"STR_DLLNOTFOUND\n"
"string.text"
msgid "Filter not found."
msgstr "Filter not found."

#: app.src
msgctxt ""
"app.src\n"
"STR_UNBENANNT\n"
"string.text"
msgid "Untitled"
msgstr "Untitled"

#: app.src
msgctxt ""
"app.src\n"
"STR_LOAD_GLOBAL_DOC\n"
"string.text"
msgid "Name and Path of Master Document"
msgstr "Name and Path of Master Document"

#: app.src
msgctxt ""
"app.src\n"
"STR_LOAD_HTML_DOC\n"
"string.text"
msgid "Name and Path of the HTML Document"
msgstr "Name and Path of the HTML Document"

#: app.src
msgctxt ""
"app.src\n"
"STR_JAVA_EDIT\n"
"string.text"
msgid "Edit Script"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_REMOVE_WARNING\n"
"string.text"
msgid "The following characters are not valid and have been removed: "
msgstr ""

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_BOOKMARK_NAME\n"
"string.text"
msgid "Name"
msgstr "Name"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_BOOKMARK_TEXT\n"
"string.text"
msgid "Text"
msgstr "Text"

#: app.src
msgctxt ""
"app.src\n"
"SW_STR_NONE\n"
"string.text"
msgid "[None]"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_BEGINNING\n"
"string.text"
msgid "Start"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_END\n"
"string.text"
msgid "End"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_ABOVE\n"
"string.text"
msgid "Above"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_BELOW\n"
"string.text"
msgid "Below"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"SW_STR_READONLY\n"
"string.text"
msgid "read-only"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_READONLY_PATH\n"
"string.text"
msgid "The 'AutoText' directories are read-only. Do you want to call the path settings dialog?"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_ERROR_PASSWD\n"
"string.text"
msgid "Invalid password"
msgstr "Invalid password"

#: app.src
msgctxt ""
"app.src\n"
"STR_FMT_STD\n"
"string.text"
msgid "(none)"
msgstr "(none)"

#: app.src
msgctxt ""
"app.src\n"
"STR_DOC_STAT\n"
"string.text"
msgid "Statistics"
msgstr "Statistics"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_W4WREAD\n"
"string.text"
msgid "Importing document..."
msgstr "Importing document..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_W4WWRITE\n"
"string.text"
msgid "Exporting document..."
msgstr "Exporting document..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SWGREAD\n"
"string.text"
msgid "Loading document..."
msgstr "Loading document..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SWGWRITE\n"
"string.text"
msgid "Saving document..."
msgstr "Saving document..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_REFORMAT\n"
"string.text"
msgid "Repagination..."
msgstr "Re-pagination..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_AUTOFORMAT\n"
"string.text"
msgid "Formatting document automatically..."
msgstr "Formatting document automatically..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_IMPGRF\n"
"string.text"
msgid "Importing images..."
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SEARCH\n"
"string.text"
msgid "Search..."
msgstr "Search..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_FORMAT\n"
"string.text"
msgid "Formatting..."
msgstr "Formatting..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_PRINT\n"
"string.text"
msgid "Printing..."
msgstr "Printing..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_LAYOUTINIT\n"
"string.text"
msgid "Converting..."
msgstr "Converting..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_LETTER\n"
"string.text"
msgid "Letter"
msgstr "Letter"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SPELL\n"
"string.text"
msgid "Spellcheck..."
msgstr "Spellcheck..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_HYPHEN\n"
"string.text"
msgid "Hyphenation..."
msgstr "Hyphenation..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_TOX_INSERT\n"
"string.text"
msgid "Inserting Index..."
msgstr "Inserting Index..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_TOX_UPDATE\n"
"string.text"
msgid "Updating Index..."
msgstr "Updating Index..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SUMMARY\n"
"string.text"
msgid "Creating abstract..."
msgstr "Creating abstract..."

#: app.src
msgctxt ""
"app.src\n"
"STR_STATSTR_SWGPRTOLENOTIFY\n"
"string.text"
msgid "Adapt Objects..."
msgstr "Adapt Objects..."

#: app.src
msgctxt ""
"app.src\n"
"STR_TABLE_DEFNAME\n"
"string.text"
msgid "Table"
msgstr "Table"

#: app.src
msgctxt ""
"app.src\n"
"STR_GRAPHIC_DEFNAME\n"
"string.text"
msgid "Image"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_OBJECT_DEFNAME\n"
"string.text"
msgid "Object"
msgstr "Object"

#: app.src
msgctxt ""
"app.src\n"
"STR_FRAME_DEFNAME\n"
"string.text"
msgid "Frame"
msgstr "Frame"

#: app.src
msgctxt ""
"app.src\n"
"STR_SHAPE_DEFNAME\n"
"string.text"
msgid "Shape"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_REGION_DEFNAME\n"
"string.text"
msgid "Section"
msgstr "Section"

#: app.src
msgctxt ""
"app.src\n"
"STR_NUMRULE_DEFNAME\n"
"string.text"
msgid "Numbering"
msgstr "Numbering"

#: app.src
msgctxt ""
"app.src\n"
"STR_EMPTYPAGE\n"
"string.text"
msgid "blank page"
msgstr "blank page"

#: app.src
msgctxt ""
"app.src\n"
"STR_ABSTRACT_TITLE\n"
"string.text"
msgid "Abstract: "
msgstr "Abstract: "

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_TEMPLATE_BUTTON\n"
"string.text"
msgid "Style"
msgstr "Style"

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_TEMPLATE_NAME\n"
"string.text"
msgid "separated by: "
msgstr "separated by: "

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_OUTLINE_LEVEL\n"
"string.text"
msgid "Outline: Level "
msgstr "Outline: Level "

#: app.src
msgctxt ""
"app.src\n"
"STR_FDLG_STYLE\n"
"string.text"
msgid "Style: "
msgstr "Style: "

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGEOFFSET\n"
"string.text"
msgid "Page number: "
msgstr "Page number: "

#: app.src
msgctxt ""
"app.src\n"
"STR_PAGEBREAK\n"
"string.text"
msgid "Break before new page"
msgstr "Break before new page"

#: app.src
msgctxt ""
"app.src\n"
"STR_WESTERN_FONT\n"
"string.text"
msgid "Western text: "
msgstr "Western text: "

#: app.src
msgctxt ""
"app.src\n"
"STR_CJK_FONT\n"
"string.text"
msgid "Asian text: "
msgstr "Asian text: "

#: app.src
msgctxt ""
"app.src\n"
"STR_REDLINE_UNKNOWN_AUTHOR\n"
"string.text"
msgid "Unknown Author"
msgstr "Unknown Author"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_DELETE_NOTE_AUTHOR\n"
"string.text"
msgid "Delete ~All Comments by $1"
msgstr "Delete ~All Comments by $1"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_HIDE_NOTE_AUTHOR\n"
"string.text"
msgid "H~ide All Comments by $1"
msgstr "Delete ~All Comments by $1"

#: app.src
msgctxt ""
"app.src\n"
"STR_DONT_ASK_AGAIN\n"
"string.text"
msgid "~Do not show warning again"
msgstr "~Do not show warning again"

#: app.src
msgctxt ""
"app.src\n"
"STR_OUTLINE_NUMBERING\n"
"string.text"
msgid "Outline Numbering"
msgstr "Outline Numbering"

#: app.src
msgctxt ""
"app.src\n"
"STR_STATUSBAR_WORDCOUNT_NO_SELECTION\n"
"string.text"
msgid "%1 words, %2 characters"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_STATUSBAR_WORDCOUNT\n"
"string.text"
msgid "%1 words, %2 characters selected"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CONVERT_TEXT_TABLE\n"
"string.text"
msgid "Convert Text to Table"
msgstr "Convert Text to Table"

#: app.src
msgctxt ""
"app.src\n"
"STR_ADD_AUTOFORMAT_TITLE\n"
"string.text"
msgid "Add AutoFormat"
msgstr "Add AutoFormat"

#: app.src
msgctxt ""
"app.src\n"
"STR_ADD_AUTOFORMAT_LABEL\n"
"string.text"
msgid "Name"
msgstr "Name"

#: app.src
msgctxt ""
"app.src\n"
"STR_DEL_AUTOFORMAT_TITLE\n"
"string.text"
msgid "Delete AutoFormat"
msgstr "Delete AutoFormat"

#: app.src
msgctxt ""
"app.src\n"
"STR_DEL_AUTOFORMAT_MSG\n"
"string.text"
msgid "The following AutoFormat entry will be deleted:"
msgstr "The following AutoFormat entry will be deleted:"

#: app.src
msgctxt ""
"app.src\n"
"STR_RENAME_AUTOFORMAT_TITLE\n"
"string.text"
msgid "Rename AutoFormat"
msgstr "Rename AutoFormat"

#: app.src
msgctxt ""
"app.src\n"
"STR_BTN_AUTOFORMAT_CLOSE\n"
"string.text"
msgid "~Close"
msgstr "~Close"

#: app.src
msgctxt ""
"app.src\n"
"STR_JAN\n"
"string.text"
msgid "Jan"
msgstr "Jan"

#: app.src
msgctxt ""
"app.src\n"
"STR_FEB\n"
"string.text"
msgid "Feb"
msgstr "Feb"

#: app.src
msgctxt ""
"app.src\n"
"STR_MAR\n"
"string.text"
msgid "Mar"
msgstr "Mar"

#: app.src
msgctxt ""
"app.src\n"
"STR_NORTH\n"
"string.text"
msgid "North"
msgstr "North"

#: app.src
msgctxt ""
"app.src\n"
"STR_MID\n"
"string.text"
msgid "Mid"
msgstr "Mid"

#: app.src
msgctxt ""
"app.src\n"
"STR_SOUTH\n"
"string.text"
msgid "South"
msgstr "South"

#: app.src
msgctxt ""
"app.src\n"
"STR_SUM\n"
"string.text"
msgid "Sum"
msgstr "Sum"

#: app.src
msgctxt ""
"app.src\n"
"STR_INVALID_AUTOFORMAT_NAME\n"
"string.text"
msgid ""
"You have entered an invalid name.\n"
"The desired AutoFormat could not be created. \n"
"Try again using a different name."
msgstr ""
"You have entered an invalid name.\n"
"The desired AutoFormat could not be created. \n"
"Try again using a different name."

#: app.src
msgctxt ""
"app.src\n"
"STR_NUMERIC\n"
"string.text"
msgid "Numeric"
msgstr "Numeric"

#: app.src
msgctxt ""
"app.src\n"
"STR_ROW\n"
"string.text"
msgid "Rows"
msgstr "Rows"

#: app.src
#, fuzzy
msgctxt ""
"app.src\n"
"STR_COL\n"
"string.text"
msgid "Column"
msgstr "Colu~mn"

#: app.src
msgctxt ""
"app.src\n"
"STR_SIMPLE\n"
"string.text"
msgid "Plain"
msgstr "Plain"

#: app.src
msgctxt ""
"app.src\n"
"STR_AUTHMRK_EDIT\n"
"string.text"
msgid "Edit Bibliography Entry"
msgstr "Edit Bibliography Entry"

#: app.src
msgctxt ""
"app.src\n"
"STR_AUTHMRK_INSERT\n"
"string.text"
msgid "Insert Bibliography Entry"
msgstr "Insert Bibliography Entry"

#: app.src
msgctxt ""
"app.src\n"
"STR_ACCESS_PAGESETUP_SPACING\n"
"string.text"
msgid "Spacing between %1 and %2"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_ACCESS_COLUMN_WIDTH\n"
"string.text"
msgid "Column %1 Width"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_TABLE\n"
"string.text"
msgid "%PRODUCTNAME Writer Table"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_FRAME\n"
"string.text"
msgid "%PRODUCTNAME Writer Frame"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_GRAPHIC\n"
"string.text"
msgid "%PRODUCTNAME Writer Image"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_CAPTION_OLE\n"
"string.text"
msgid "Other OLE Objects"
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_WRONG_TABLENAME\n"
"string.text"
msgid "The name of the table must not contain spaces."
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_ERR_TABLE_MERGE\n"
"string.text"
msgid "Selected table cells are too complex to merge."
msgstr ""

#: app.src
msgctxt ""
"app.src\n"
"STR_SRTERR\n"
"string.text"
msgid "Cannot sort selection"
msgstr ""

#: error.src
msgctxt ""
"error.src\n"
"STR_COMCORE_READERROR\n"
"string.text"
msgid "Read Error"
msgstr ""

#: error.src
msgctxt ""
"error.src\n"
"STR_COMCORE_CANT_SHOW\n"
"string.text"
msgid "Image cannot be displayed."
msgstr ""

#: error.src
msgctxt ""
"error.src\n"
"STR_ERROR_CLPBRD_READ\n"
"string.text"
msgid "Error reading from the clipboard."
msgstr "Error reading from the clipboard."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_FILE_FORMAT_ERROR )\n"
"string.text"
msgid "File format error found."
msgstr "File format error found."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_READ_ERROR )\n"
"string.text"
msgid "Error reading file."
msgstr "Error reading file."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_INPUT_FILE )\n"
"string.text"
msgid "Input file error."
msgstr "Input file error."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_NOWRITER_FILE )\n"
"string.text"
msgid "This is not a %PRODUCTNAME Writer file."
msgstr "This is not a %PRODUCTNAME Writer file."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_UNEXPECTED_EOF )\n"
"string.text"
msgid "Unexpected end of file."
msgstr "Unexpected end of file."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SW6_PASSWD )\n"
"string.text"
msgid "Password-protected files cannot be opened."
msgstr "Password-protected files cannot be opened."

#: error.src
#, fuzzy
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_WW6_NO_WW6_FILE_ERR )\n"
"string.text"
msgid "This is not a valid WinWord6 file."
msgstr "This is not a WinWord6 file."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_WW6_FASTSAVE_ERR )\n"
"string.text"
msgid "This file was saved with WinWord in 'Fast Save' mode. Please unmark the WinWord option 'Allow Fast Saves' and save the file again."
msgstr "This file was saved with WinWord in 'Fast Save' mode. Please unmark the WinWord option 'Allow Fast Saves' and save the file again."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_FORMAT_ROWCOL )\n"
"string.text"
msgid "File format error found at $(ARG1)(row,col)."
msgstr "File format error found at $(ARG1)(row,col)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_NEW_VERSION )\n"
"string.text"
msgid "File has been written in a newer version."
msgstr "File has been written in a newer version."

#: error.src
#, fuzzy
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_WW8_NO_WW8_FILE_ERR )\n"
"string.text"
msgid "This is not a valid WinWord97 file."
msgstr "This is not a WinWord97 file."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_FORMAT_FILE_ROWCOL )\n"
"string.text"
msgid "Format error discovered in the file in sub-document $(ARG1) at $(ARG2)(row,col)."
msgstr "Format error discovered in the file in sub-document $(ARG1) at $(ARG2)(row,col)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_SWG_WRITE_ERROR )\n"
"string.text"
msgid "Error writing file."
msgstr "Error writing file."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_SWG_OLD_GLOSSARY )\n"
"string.text"
msgid "Wrong AutoText document version."
msgstr "Wrong AutoText document version."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_WRITE_ERROR_FILE )\n"
"string.text"
msgid "Error in writing sub-document $(ARG1)."
msgstr "Error in writing sub-document $(ARG1)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_READ , ERR_SWG_INTERNAL_ERROR )\n"
"string.text"
msgid "Internal error in %PRODUCTNAME Writer file format."
msgstr "Internal error in %PRODUCTNAME Writer file format."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , ERR_SWG_INTERNAL_ERROR )\n"
"string.text"
msgid "Internal error in %PRODUCTNAME Writer file format."
msgstr "Internal error in %PRODUCTNAME Writer file format."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_LOCKING , ERR_TXTBLOCK_NEWFILE_ERROR )\n"
"string.text"
msgid "$(ARG1) has changed."
msgstr "$(ARG1) has changed."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_PATH , ERR_AUTOPATH_ERROR )\n"
"string.text"
msgid "$(ARG1) does not exist."
msgstr "$(ARG1) does not exist."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_NONE , ERR_TBLSPLIT_ERROR )\n"
"string.text"
msgid "Cells cannot be further split."
msgstr "Cells cannot be further split."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_NONE , ERR_TBLINSCOL_ERROR )\n"
"string.text"
msgid "Additional columns cannot be inserted."
msgstr "Additional columns cannot be inserted."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_NONE , ERR_TBLDDECHG_ERROR )\n"
"string.text"
msgid "The structure of a linked table cannot be modified."
msgstr "The structure of a linked table cannot be modified."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_NO_DRAWINGS )\n"
"string.text"
msgid "No drawings could be read."
msgstr "No drawings could be read."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_WW6_FASTSAVE_ERR )\n"
"string.text"
msgid "This file was saved with WinWord in 'Fast Save' mode. Please unmark the WinWord option 'Allow Fast Saves' and save the file again."
msgstr "This file was saved with WinWord in 'Fast Save' mode. Please unmark the WinWord option 'Allow Fast Saves' and save the file again."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_FEATURES_LOST )\n"
"string.text"
msgid "Not all attributes could be read."
msgstr "Not all attributes could be read."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_FEATURES_LOST )\n"
"string.text"
msgid "Not all attributes could be recorded."
msgstr "Not all attributes could be recorded."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_OLE )\n"
"string.text"
msgid "Some OLE objects could only be loaded as images."
msgstr ""

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_OLE )\n"
"string.text"
msgid "Some OLE objects could only be saved as images."
msgstr ""

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_READ , WARN_SWG_POOR_LOAD )\n"
"string.text"
msgid "Document could not be completely loaded."
msgstr "Document could not be completely loaded."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_POOR_LOAD )\n"
"string.text"
msgid "Document could not be completely saved."
msgstr "Document could not be completely saved."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"WARN_CODE ( ERRCODE_CLASS_WRITE , WARN_SWG_HTML_NO_MACROS)\n"
"string.text"
msgid ""
"This HTML document contains %PRODUCTNAME Basic macros.\n"
"They were not saved with the current export settings."
msgstr ""
"This HTML document contains %PRODUCTNAME Basic macros.\n"
"They were not saved with the current export settings."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , WARN_WRITE_ERROR_FILE )\n"
"string.text"
msgid "Error in writing sub-document $(ARG1)."
msgstr "Error in writing sub-document $(ARG1)."

#: error.src
msgctxt ""
"error.src\n"
"RID_SW_ERRHDL\n"
"ERR_CODE ( ERRCODE_CLASS_WRITE , WARN_FORMAT_FILE_ROWCOL )\n"
"string.text"
msgid "Format error discovered in the file in sub-document $(ARG1) at $(ARG2)(row,col)."
msgstr "Format error discovered in the file in sub-document $(ARG1) at $(ARG2)(row,col)."

#: mn.src
#, fuzzy
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_REPLY\n"
"menuitem.text"
msgid "Reply"
msgstr "Reply"

#: mn.src
#, fuzzy
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_DELETE_COMMENT\n"
"menuitem.text"
msgid "Delete ~Comment"
msgstr "Delete ~Comment"

#: mn.src
#, fuzzy
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_DELETE_NOTE_AUTHOR\n"
"menuitem.text"
msgid "Delete ~All Comments by $1"
msgstr "Delete ~All Comments by $1"

#: mn.src
#, fuzzy
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_DELETE_ALL_NOTES\n"
"menuitem.text"
msgid "~Delete All Comments"
msgstr "~Delete All Comments"

#: mn.src
msgctxt ""
"mn.src\n"
"MN_ANNOTATION_BUTTON\n"
"FN_FORMAT_ALL_NOTES\n"
"menuitem.text"
msgid "~Format All Comments..."
msgstr ""

#: mn.src
#, fuzzy
msgctxt ""
"mn.src\n"
"MN_HEADERFOOTER_BUTTON\n"
"FN_HEADERFOOTER_BORDERBACK\n"
"menuitem.text"
msgid "Border and Background..."
msgstr "Border / Background"

#: mn.src
msgctxt ""
"mn.src\n"
"MN_PAGEBREAK_BUTTON\n"
"FN_PAGEBREAK_EDIT\n"
"menuitem.text"
msgid "Edit Page Break..."
msgstr ""

#: mn.src
#, fuzzy
msgctxt ""
"mn.src\n"
"MN_PAGEBREAK_BUTTON\n"
"FN_PAGEBREAK_DELETE\n"
"menuitem.text"
msgid "Delete Page Break"
msgstr "Delete Page Breaks"
