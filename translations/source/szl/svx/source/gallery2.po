#. extracted from svx/source/gallery2
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-08-29 10:25+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: szl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1472466300.000000\n"

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ACTUALIZE_PROGRESS\n"
"string.text"
msgid "Update"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_FOPENERROR\n"
"string.text"
msgid "This file cannot be opened"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NOTHEME\n"
"string.text"
msgid "Invalid Theme Name!"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT1_UI\n"
"string.text"
msgid "Wave - Sound File"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT2_UI\n"
"string.text"
msgid "Audio Interchange File Format"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_EXTFORMAT3_UI\n"
"string.text"
msgid "AU - Sound File"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_FILTER\n"
"string.text"
msgid "Graphics filter"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_LENGTH\n"
"string.text"
msgid "Length:"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_SIZE\n"
"string.text"
msgid "Size:"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_DELETEDD\n"
"string.text"
msgid "Do you want to delete the linked file?"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_SGIERROR\n"
"string.text"
msgid ""
"This file cannot be opened.\n"
"Do you want to enter a different search path? "
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_NEWTHEME\n"
"string.text"
msgid "New Theme"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_BROWSER\n"
"string.text"
msgid "~Organizer..."
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_THEMEERR\n"
"string.text"
msgid ""
"This theme name already exists.\n"
"Please choose a different one."
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_IMPORTTHEME\n"
"string.text"
msgid "I~mport..."
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_CREATETHEME\n"
"string.text"
msgid "New Theme..."
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_DIALOGID\n"
"string.text"
msgid "Assign ID"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_TITLE\n"
"string.text"
msgid "Title"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_PATH\n"
"string.text"
msgid "Path"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_ICONVIEW\n"
"string.text"
msgid "Icon View"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXSTR_GALLERY_LISTVIEW\n"
"string.text"
msgid "Detailed View"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_ACTUALIZE\n"
"menuitem.text"
msgid "Update"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_RENAME\n"
"menuitem.text"
msgid "~Rename"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_ASSIGN_ID\n"
"menuitem.text"
msgid "Assign ~ID"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY1\n"
"MN_PROPERTIES\n"
"menuitem.text"
msgid "Propert~ies..."
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_ADD\n"
"menuitem.text"
msgid "~Insert"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_BACKGROUND\n"
"menuitem.text"
msgid "Insert as Bac~kground"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_PREVIEW\n"
"menuitem.text"
msgid "~Preview"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_TITLE\n"
"menuitem.text"
msgid "~Title"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_COPYCLIPBOARD\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: gallery.src
msgctxt ""
"gallery.src\n"
"RID_SVXMN_GALLERY2\n"
"MN_PASTECLIPBOARD\n"
"menuitem.text"
msgid "~Insert"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_3D\n"
"string.text"
msgid "3D Effects"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ANIMATIONS\n"
"string.text"
msgid "Animations"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BULLETS\n"
"string.text"
msgid "Bullets"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_OFFICE\n"
"string.text"
msgid "Office"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FLAGS\n"
"string.text"
msgid "Flags"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FLOWCHARTS\n"
"string.text"
msgid "Flow Charts"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_EMOTICONS\n"
"string.text"
msgid "Emoticons"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PHOTOS\n"
"string.text"
msgid "Pictures"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BACKGROUNDS\n"
"string.text"
msgid "Backgrounds"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_HOMEPAGE\n"
"string.text"
msgid "Homepage"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_INTERACTION\n"
"string.text"
msgid "Interaction"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_MAPS\n"
"string.text"
msgid "Maps"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PEOPLE\n"
"string.text"
msgid "People"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SURFACES\n"
"string.text"
msgid "Surfaces"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMPUTERS\n"
"string.text"
msgid "Computers"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_DIAGRAMS\n"
"string.text"
msgid "Diagrams"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ENVIRONMENT\n"
"string.text"
msgid "Environment"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FINANCE\n"
"string.text"
msgid "Finance"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TRANSPORT\n"
"string.text"
msgid "Transport"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TXTSHAPES\n"
"string.text"
msgid "Textshapes"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SOUNDS\n"
"string.text"
msgid "Sounds"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SYMBOLS\n"
"string.text"
msgid "Symbols"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_MYTHEME\n"
"string.text"
msgid "My Theme"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_ARROWS\n"
"string.text"
msgid "Arrows"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_BALLOONS\n"
"string.text"
msgid "Balloons"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_KEYBOARD\n"
"string.text"
msgid "Keyboard"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TIME\n"
"string.text"
msgid "Time"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_PRESENTATION\n"
"string.text"
msgid "Presentation"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_CALENDAR\n"
"string.text"
msgid "Calendar"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_NAVIGATION\n"
"string.text"
msgid "Navigation"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMMUNICATION\n"
"string.text"
msgid "Communication"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_FINANCES\n"
"string.text"
msgid "Finances"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_COMPUTER\n"
"string.text"
msgid "Computers"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_CLIMA\n"
"string.text"
msgid "Climate"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_EDUCATION\n"
"string.text"
msgid "School & University"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_TROUBLE\n"
"string.text"
msgid "Problem Solving"
msgstr ""

#: galtheme.src
msgctxt ""
"galtheme.src\n"
"RID_GALLERYSTR_THEME_SCREENBEANS\n"
"string.text"
msgid "Screen Beans"
msgstr ""
