#. extracted from dbaccess/source/ext/macromigration
msgid ""
msgstr ""
"Project-Id-Version: LO\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2012-11-19 16:30-0000\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: \n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_TITLE_MACRO_MIGRATION\n"
"string.text"
msgid "Database Document Macro Migration"
msgstr "Aistriú Macraí Bunachair Sonraí"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_CLOSE_SUB_DOCS\n"
"string.text"
msgid "Prepare"
msgstr "Ullmhaigh"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_BACKUP_DBDOC\n"
"string.text"
msgid "Backup Document"
msgstr "Cáipéis Chúltaca"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_MIGRATE\n"
"string.text"
msgid "Migrate"
msgstr "Aistrigh"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_STATE_SUMMARY\n"
"string.text"
msgid "Summary"
msgstr "Achoimre"

#. This refers to a form document inside a database document.
#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_FORM\n"
"string.text"
msgid "Form '$name$'"
msgstr "Foirm '$name$'"

#. This refers to a report document inside a database document.
#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_REPORT\n"
"string.text"
msgid "Report '$name$'"
msgstr "Tuairisc '$name$'"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_OVERALL_PROGRESS\n"
"string.text"
msgid "document $current$ of $overall$"
msgstr "cáipéis $current$ as $overall$"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_DATABASE_DOCUMENT\n"
"string.text"
msgid "Database Document"
msgstr "Cáipéis Bhunachair Sonraí"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_SAVED_COPY_TO\n"
"string.text"
msgid "saved copy to $location$"
msgstr "sábháladh cóip i $location$"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_MOVED_LIBRARY\n"
"string.text"
msgid "migrated $type$ library '$old$' to '$new$'"
msgstr "aistríodh leabharlann $type$ '$old$' go '$new$'"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_LIBRARY_TYPE_AND_NAME\n"
"string.text"
msgid "$type$ library '$library$'"
msgstr "leabharlann $type$ '$library$'"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_MIGRATING_LIBS\n"
"string.text"
msgid "migrating libraries ..."
msgstr "leabharlanna á n-aistriú ..."

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_OOO_BASIC\n"
"string.text"
msgid "%PRODUCTNAME Basic"
msgstr "%PRODUCTNAME Basic"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_JAVA_SCRIPT\n"
"string.text"
msgid "JavaScript"
msgstr "JavaScript"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_BEAN_SHELL\n"
"string.text"
msgid "BeanShell"
msgstr "BeanShell"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_JAVA\n"
"string.text"
msgid "Java"
msgstr "Java"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_PYTHON\n"
"string.text"
msgid "Python"
msgstr "Python"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_DIALOG\n"
"string.text"
msgid "dialog"
msgstr "dialóg"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_ERRORS\n"
"string.text"
msgid "Error(s)"
msgstr "Earráid(í)"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_WARNINGS\n"
"string.text"
msgid "Warnings"
msgstr "Rabhaidh"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_EXCEPTION\n"
"string.text"
msgid "caught exception:"
msgstr "fuarthas eisceacht:"

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_INVALID_BACKUP_LOCATION\n"
"string.text"
msgid "You need to choose a backup location other than the document location itself."
msgstr "Níl cead agat an cúltaca a dhéanamh san áit a bhfuil an cháipéis féin ann.  Roghnaigh suíomh eile."

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_INVALID_NUMBER_ARGS\n"
"string.text"
msgid "Invalid number of initialization arguments. Expected 1."
msgstr "Méid neamhbhailí de pharaiméadair thúsaithe. Bhíothas ag súil le 1."

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_NO_DATABASE\n"
"string.text"
msgid "No database document found in the initialization arguments."
msgstr "Níor aimsíodh cáipéis bhunachair sonraí i measc na n-argóintí túsaithe."

#: macromigration.src
msgctxt ""
"macromigration.src\n"
"STR_NOT_READONLY\n"
"string.text"
msgid "Not applicable to read-only documents."
msgstr "Níl sé oiriúnach do cháipéisí inléite amháin."
