#. extracted from sw/source/uibase/ribbar
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-11-17 15:50+0000\n"
"Last-Translator: Stuart Swales <stuart.swales.croftnuisk@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1479397848.000000\n"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_CALC_SUM\n"
"menuitem.text"
msgid "Sum"
msgstr "Sum"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_CALC_ROUND\n"
"menuitem.text"
msgid "Round"
msgstr "Round"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_CALC_PHD\n"
"menuitem.text"
msgid "Percent"
msgstr "Percent"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_CALC_SQRT\n"
"menuitem.text"
msgid "Square Root"
msgstr "Square Root"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_CALC_POW\n"
"menuitem.text"
msgid "Power"
msgstr "Power"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_LISTSEP\n"
"menuitem.text"
msgid "List Separator"
msgstr "List Separator"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_EQ\n"
"menuitem.text"
msgid "Equal"
msgstr "Equal"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_NEQ\n"
"menuitem.text"
msgid "Not Equal"
msgstr "Not Equal"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_LEQ\n"
"menuitem.text"
msgid "Less Than or Equal"
msgstr "Less Than or Equal"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_GEQ\n"
"menuitem.text"
msgid "Greater Than or Equal"
msgstr "Greater Than or Equal"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_LES\n"
"menuitem.text"
msgid "Less"
msgstr "Less"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_GRE\n"
"menuitem.text"
msgid "Greater"
msgstr "Greater"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_OR\n"
"menuitem.text"
msgid "Boolean Or"
msgstr "Boolean Or"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_XOR\n"
"menuitem.text"
msgid "Boolean Xor"
msgstr "Boolean Xor"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_AND\n"
"menuitem.text"
msgid "Boolean And"
msgstr "Boolean And"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_OPS\n"
"MN_CALC_NOT\n"
"menuitem.text"
msgid "Boolean Not"
msgstr "Boolean Not"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_POP_OPS\n"
"menuitem.text"
msgid "Operators"
msgstr "Operators"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_STATISTICS\n"
"MN_CALC_MEAN\n"
"menuitem.text"
msgid "Mean"
msgstr "Mean"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_STATISTICS\n"
"MN_CALC_MIN\n"
"menuitem.text"
msgid "Minimum"
msgstr "Minimum"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_STATISTICS\n"
"MN_CALC_MAX\n"
"menuitem.text"
msgid "Maximum"
msgstr "Maximum"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_POP_STATISTICS\n"
"menuitem.text"
msgid "Statistical Functions"
msgstr "Statistical Functions"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_FUNC\n"
"MN_CALC_SIN\n"
"menuitem.text"
msgid "Sine"
msgstr "Sine"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_FUNC\n"
"MN_CALC_COS\n"
"menuitem.text"
msgid "Cosine"
msgstr "Cosine"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_FUNC\n"
"MN_CALC_TAN\n"
"menuitem.text"
msgid "Tangent"
msgstr "Tangent"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_FUNC\n"
"MN_CALC_ASIN\n"
"menuitem.text"
msgid "Arcsine"
msgstr "Arc sine"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_FUNC\n"
"MN_CALC_ACOS\n"
"menuitem.text"
msgid "Arccosine"
msgstr "Arc cosine"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP.MN_POP_FUNC\n"
"MN_CALC_ATAN\n"
"menuitem.text"
msgid "Arctangent"
msgstr "Arc tangent"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"MN_CALC_POPUP\n"
"MN_POP_FUNC\n"
"menuitem.text"
msgid "Functions"
msgstr "Functions"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"STR_FORMULA_CALC\n"
"string.text"
msgid "Functions"
msgstr "Functions"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"STR_FORMULA_CANCEL\n"
"string.text"
msgid "Cancel"
msgstr "Cancel"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"STR_FORMULA_APPLY\n"
"string.text"
msgid "Apply"
msgstr "Apply"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"STR_ACCESS_FORMULA_TOOLBAR\n"
"string.text"
msgid "Formula Tool Bar"
msgstr "Formula Tool Bar"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"STR_ACCESS_FORMULA_TYPE\n"
"string.text"
msgid "Formula Type"
msgstr "Formula Type"

#: inputwin.src
msgctxt ""
"inputwin.src\n"
"STR_ACCESS_FORMULA_TEXT\n"
"string.text"
msgid "Formula Text"
msgstr "Formula Text"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_TBL\n"
"string.text"
msgid "Table"
msgstr "Table"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_FRM\n"
"string.text"
msgid "Text Frame"
msgstr "Text Frame"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_PGE\n"
"string.text"
msgid "Page"
msgstr "Page"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_DRW\n"
"string.text"
msgid "Drawing"
msgstr "Drawing"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_CTRL\n"
"string.text"
msgid "Control"
msgstr "Control"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_REG\n"
"string.text"
msgid "Section"
msgstr "Section"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_BKM\n"
"string.text"
msgid "Bookmark"
msgstr "Bookmark"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_GRF\n"
"string.text"
msgid "Graphics"
msgstr "Graphics"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_OLE\n"
"string.text"
msgid "OLE object"
msgstr "OLE object"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_OUTL\n"
"string.text"
msgid "Headings"
msgstr "Headings"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_SEL\n"
"string.text"
msgid "Selection"
msgstr "Selection"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_FTN\n"
"string.text"
msgid "Footnote"
msgstr "Footnote"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_MARK\n"
"string.text"
msgid "Reminder"
msgstr "Reminder"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_POSTIT\n"
"string.text"
msgid "Comment"
msgstr "Comment"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_SRCH_REP\n"
"string.text"
msgid "Repeat search"
msgstr "Repeat search"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_INDEX_ENTRY\n"
"string.text"
msgid "Index entry"
msgstr "Index entry"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_TABLE_FORMULA\n"
"string.text"
msgid "Table formula"
msgstr "Table formula"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"ST_TABLE_FORMULA_ERROR\n"
"string.text"
msgid "Wrong table formula"
msgstr "Wrong table formula"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_TBL_DOWN\n"
"string.text"
msgid "Next table"
msgstr "Next table"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_FRM_DOWN\n"
"string.text"
msgid "Next text frame"
msgstr "Next text frame"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_PGE_DOWN\n"
"string.text"
msgid "Next page"
msgstr "Next page"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_DRW_DOWN\n"
"string.text"
msgid "Next drawing"
msgstr "Next drawing"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_CTRL_DOWN\n"
"string.text"
msgid "Next control"
msgstr "Next control"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_REG_DOWN\n"
"string.text"
msgid "Next section"
msgstr "Next section"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_BKM_DOWN\n"
"string.text"
msgid "Next bookmark"
msgstr "Next bookmark"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_GRF_DOWN\n"
"string.text"
msgid "Next graphic"
msgstr "Next graphic"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_OLE_DOWN\n"
"string.text"
msgid "Next OLE object"
msgstr "Next OLE object"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_OUTL_DOWN\n"
"string.text"
msgid "Next heading"
msgstr "Next heading"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_SEL_DOWN\n"
"string.text"
msgid "Next selection"
msgstr "Next selection"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_FTN_DOWN\n"
"string.text"
msgid "Next footnote"
msgstr "Next footnote"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_MARK_DOWN\n"
"string.text"
msgid "Next Reminder"
msgstr "Next Reminder"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_POSTIT_DOWN\n"
"string.text"
msgid "Next Comment"
msgstr "Next Comment"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_SRCH_REP_DOWN\n"
"string.text"
msgid "Continue search forward"
msgstr "Continue search forwards"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_INDEX_ENTRY_DOWN\n"
"string.text"
msgid "Next index entry"
msgstr "Next index entry"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_TBL_UP\n"
"string.text"
msgid "Previous table"
msgstr "Previous table"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_FRM_UP\n"
"string.text"
msgid "Previous text frame"
msgstr "Previous text frame"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_PGE_UP\n"
"string.text"
msgid "Previous page"
msgstr "Previous page"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_DRW_UP\n"
"string.text"
msgid "Previous drawing"
msgstr "Previous drawing"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_CTRL_UP\n"
"string.text"
msgid "Previous control"
msgstr "Previous control"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_REG_UP\n"
"string.text"
msgid "Previous section"
msgstr "Previous section"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_BKM_UP\n"
"string.text"
msgid "Previous bookmark"
msgstr "Previous bookmark"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_GRF_UP\n"
"string.text"
msgid "Previous graphic"
msgstr "Previous graphic"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_OLE_UP\n"
"string.text"
msgid "Previous OLE object"
msgstr "Previous OLE object"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_OUTL_UP\n"
"string.text"
msgid "Previous heading"
msgstr "Previous heading"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_SEL_UP\n"
"string.text"
msgid "Previous selection"
msgstr "Previous selection"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_FTN_UP\n"
"string.text"
msgid "Previous footnote"
msgstr "Previous footnote"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_MARK_UP\n"
"string.text"
msgid "Previous Reminder"
msgstr "Previous Reminder"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_POSTIT_UP\n"
"string.text"
msgid "Previous Comment"
msgstr "Previous Comment"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_SRCH_REP_UP\n"
"string.text"
msgid "Continue search backwards"
msgstr "Continue search backwards"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_INDEX_ENTRY_UP\n"
"string.text"
msgid "Previous index entry"
msgstr "Previous index entry"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_TBLFML_UP\n"
"string.text"
msgid "Previous table formula"
msgstr "Previous table formula"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_TBLFML_DOWN\n"
"string.text"
msgid "Next table formula"
msgstr "Next table formula"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_TBLFML_ERR_UP\n"
"string.text"
msgid "Previous faulty table formula"
msgstr "Previous faulty table formula"

#: workctrl.src
msgctxt ""
"workctrl.src\n"
"STR_IMGBTN_TBLFML_ERR_DOWN\n"
"string.text"
msgid "Next faulty table formula"
msgstr "Next faulty table formula"
