#. extracted from sfx2/source/doc
msgid ""
msgstr ""
"Project-Id-Version: doc\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2017-01-06 18:17+0000\n"
"Last-Translator: bormant <bormant@mail.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1483726627.000000\n"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_TEMPLATE_FILTER\n"
"string.text"
msgid "Templates"
msgstr "Шаблоны"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_SAVEDOC\n"
"string.text"
msgid "~Save"
msgstr "Сохранить"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_SAVEASDOC\n"
"string.text"
msgid "Save ~As..."
msgstr "Сохранить как..."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_SAVEACOPY\n"
"string.text"
msgid "Save a Copy..."
msgstr "Сохранить копию..."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CLOSEDOC\n"
"string.text"
msgid "~Close"
msgstr "~Закрыть"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_OPEN\n"
"string.text"
msgid "Open"
msgstr "Открыть"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_EDIT_TEMPLATE\n"
"string.text"
msgid "Edit"
msgstr "Правка"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DEFAULT_TEMPLATE\n"
"string.text"
msgid "Set As Default"
msgstr "Установить по умолчанию"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_RESET_DEFAULT\n"
"string.text"
msgid "Reset Default"
msgstr "По умолчанию"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DELETE\n"
"string.text"
msgid "Delete"
msgstr "Удалить"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_RENAME\n"
"string.text"
msgid "Rename"
msgstr "Переименовать"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_RENAME\n"
"string.text"
msgid "Rename Category"
msgstr "Переименовать категорию"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_PROPERTIES\n"
"string.text"
msgid "Properties"
msgstr "Свойства"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_RENAME_TEMPLATE\n"
"string.text"
msgid "Enter New Name: "
msgstr "Введите новое имя: "

#: doc.src
msgctxt ""
"doc.src\n"
"STR_TEMPLATE_TOOLTIP\n"
"string.text"
msgid ""
"Title: $1\n"
"Category: $2"
msgstr ""
"Заглавие: $1\n"
"Категория: $2"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_TEMPLATE_SELECTION\n"
"string.text"
msgid "Select a Template"
msgstr "Выберите шаблон"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_AUTOMATICVERSION\n"
"string.text"
msgid "Automatically saved version"
msgstr "Автоматически сохранённая версия"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SW\n"
"string.text"
msgid "Text Document"
msgstr "Текстовый документ"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SWWEB\n"
"string.text"
msgid "HTML Document"
msgstr "Документ HTML"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SWGLOB\n"
"string.text"
msgid "Master Document"
msgstr "Составной документ"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SC\n"
"string.text"
msgid "Spreadsheet"
msgstr "Электронная таблица"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SI\n"
"string.text"
msgid "Presentation"
msgstr "Презентация"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_SD\n"
"string.text"
msgid "Drawing"
msgstr "Рисунок"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCTYPENAME_MESSAGE\n"
"string.text"
msgid "Message"
msgstr "Сообщение"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_EXPORTBUTTON\n"
"string.text"
msgid "Export"
msgstr "Экспорт"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_LABEL_FILEFORMAT\n"
"string.text"
msgid "File format:"
msgstr "Формат файла:"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTAINS\n"
"string.text"
msgid ""
"This document contains:\n"
"\n"
msgstr ""
"Этот документ содержит:\n"
"\n"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_RECORDCHANGES\n"
"string.text"
msgid "Recorded changes"
msgstr "Запись изменений"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_NOTES\n"
"string.text"
msgid "Notes"
msgstr "Примечания"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_DOCVERSIONS\n"
"string.text"
msgid "Document versions"
msgstr "Версии документа"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_FIELDS\n"
"string.text"
msgid "Fields"
msgstr "Поля"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_LINKDATA\n"
"string.text"
msgid "Linked data..."
msgstr "Связанные данные..."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_SAVING\n"
"string.text"
msgid "Do you want to continue saving the document?"
msgstr "Вы хотите продолжить сохранение документа?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_PRINTING\n"
"string.text"
msgid "Do you want to continue printing the document?"
msgstr "Вы хотите продолжить печать документа?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_SIGNING\n"
"string.text"
msgid "Do you want to continue signing the document?"
msgstr "Вы хотите продолжить подписывание документа цифровой подписью?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_HIDDENINFO_CONTINUE_CREATEPDF\n"
"string.text"
msgid "Do you want to continue creating a PDF file?"
msgstr "Вы хотите продолжить создание файла в формате PDF?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_NEW_FILENAME_SAVE\n"
"string.text"
msgid "If you do not want to overwrite the original document, you should save your work under a new filename."
msgstr "Если Вы не хотите сохранять изменения в первоначальном документе, сохраните свою работу в новом файле."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ERROR_DELETE_TEMPLATE_DIR\n"
"string.text"
msgid "Some template files are protected and can not be deleted."
msgstr "Некоторые файлы шаблонов защищены и не могут быть удалены."

#. pb: %1 == a number [1-4]
#: doc.src
msgctxt ""
"doc.src\n"
"STR_DOCINFO_INFOFIELD\n"
"string.text"
msgid "Info %1"
msgstr "Поле %1"

#. Used in the title of a shared document.
#: doc.src
msgctxt ""
"doc.src\n"
"STR_SHARED\n"
"string.text"
msgid " (shared)"
msgstr " (общий доступ)"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_XMLSEC_ODF12_EXPECTED\n"
"string.text"
msgid "The document format version is set to ODF 1.1 (OpenOffice.org 2.x) in Tools-Options-Load/Save-General. Signing documents requires ODF 1.2 (OpenOffice.org 3.x)."
msgstr "Версия формата документа установлена в ODF 1.1 (OpenOffice.org 2.x) в Сервис -Параметры - Загрузка/сохранение - Общие. Для подписи документов необходима версия ODF 1.2 (OpenOffice.org 3.x)."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_XMLSEC_QUERY_SAVESIGNEDBEFORESIGN\n"
"string.text"
msgid ""
"The document has to be saved before it can be signed. Saving the document removes all present signatures.\n"
"Do you want to save the document?"
msgstr ""
"Документ необходимо сохранить перед добавлением подписей. Сохранение документа приведёт к удалению всех текущих подписей.\n"
"Сохранить документ?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QRYTEMPL_MESSAGE\n"
"string.text"
msgid "The template '$(ARG1)' on which this document is based, has been modified. Do you want to update style based formatting according to the modified template?"
msgstr "Шаблон «$(ARG1)», на котором основан данный документ, был изменён. Хотите обновить стили документа из изменённого шаблона?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QRYTEMPL_UPDATE_BTN\n"
"string.text"
msgid "~Update Styles"
msgstr "Обновить стили"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QRYTEMPL_KEEP_BTN\n"
"string.text"
msgid "~Keep Old Styles"
msgstr "Сохранить старые стили"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ACTION_SORT_NAME\n"
"string.text"
msgid "Sort by name"
msgstr "Сортировать по имени"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ACTION_REFRESH\n"
"string.text"
msgid "Refresh"
msgstr "Обновить"

#. leave ending space
#: doc.src
msgctxt ""
"doc.src\n"
"STR_ACTION_DEFAULT\n"
"string.text"
msgid "Reset Default Template "
msgstr "Восстановить шаблон по умолчанию "

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MOVE_NEW\n"
"string.text"
msgid "New folder"
msgstr "Новая папка"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_NEW\n"
"string.text"
msgid "New Category"
msgstr "Создать категорию"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_DELETE\n"
"string.text"
msgid "Delete Category"
msgstr "Удалить категорию"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CATEGORY_SELECT\n"
"string.text"
msgid "Select Category"
msgstr "Выбрать категорию"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_EXPORT_SUCCESS\n"
"string.text"
msgid "$1 templates successfully exported."
msgstr "Успешно экспортировано шаблонов: $1."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_QUERY_COPY\n"
"string.text"
msgid "$1 could not be moved to the category \"$2\". Do you want to copy the template instead?"
msgstr "Переместить $1 в категорию «$2» невозможно. Хотите скопировать?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_CREATE_ERROR\n"
"string.text"
msgid "Cannot create category: $1"
msgstr "Невозможно создать категорию: $1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_ERROR_SAVEAS\n"
"string.text"
msgid "Cannot save template: $1"
msgstr "Невозможно сохранить шаблон: $1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_INPUT_NEW\n"
"string.text"
msgid "Enter category name:"
msgstr "Введите имя категории:"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_REPOSITORY_LOCAL\n"
"string.text"
msgid "Local"
msgstr "Локальное хранилище"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_REPOSITORY_NEW\n"
"string.text"
msgid "New Repository"
msgstr "Новое хранилище"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_LOCAL_MOVE\n"
"string.text"
msgid ""
"Error moving the following templates to $1.\n"
"$2"
msgstr ""
"Ошибка перемещения следующих шаблонов в $1.\n"
"$2"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_REMOTE_MOVE\n"
"string.text"
msgid ""
"Error moving the following templates from repository $1 to folder $2.\n"
"$3"
msgstr ""
"Ошибка перемещения следующих шаблонов из хранилища $1 в папку $2.\n"
"$3"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_EXPORT\n"
"string.text"
msgid ""
"Error exporting the following templates:\n"
"$1"
msgstr ""
"Ошибка экспорта следующих шаблонов:\n"
"$1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_IMPORT\n"
"string.text"
msgid ""
"Error importing the following templates to $1:\n"
"$2"
msgstr ""
"Ошибка импорта следующих шаблонов в $1:\n"
"$2"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_DELETE_TEMPLATE\n"
"string.text"
msgid ""
"The following templates cannot be deleted:\n"
"$1"
msgstr ""
"Невозможно удалить следующие шаблоны:\n"
"$1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_DELETE_FOLDER\n"
"string.text"
msgid ""
"The following folders cannot be deleted:\n"
"$1"
msgstr ""
"Невозможно удалить следующие папки:\n"
"$1"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_REPOSITORY_NAME\n"
"string.text"
msgid ""
"Failed to create repository \"$1\".\n"
"A repository with this name may already exist."
msgstr ""
"Ошибка создания хранилища \"$1\".\n"
"Хранилище с таким именем уже существует."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_MSG_ERROR_SELECT_FOLDER\n"
"string.text"
msgid "Select the destination folder(s) to save the template."
msgstr "Выберите папку (папки) для сохранения шаблона."

#: doc.src
msgctxt ""
"doc.src\n"
"STR_INPUT_TEMPLATE_NEW\n"
"string.text"
msgid "Enter template name:"
msgstr "Введите имя шаблона:"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QMSG_SEL_FOLDER_DELETE\n"
"string.text"
msgid "Do you want to delete the selected folders?"
msgstr "Удалить выбранные папки?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QMSG_TEMPLATE_OVERWRITE\n"
"string.text"
msgid "A template named $1 already exist in $2. Do you want to overwrite it?"
msgstr "Шаблон с именем $1 уже существует в $2. Перезаписать?"

#: doc.src
msgctxt ""
"doc.src\n"
"STR_QMSG_SEL_TEMPLATE_DELETE\n"
"string.text"
msgid "Do you want to delete the selected templates?"
msgstr "Удалить выбранные шаблоны?"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"My Templates\n"
"itemlist.text"
msgid "My Templates"
msgstr "Мои шаблоны"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Business Correspondence\n"
"itemlist.text"
msgid "Business Correspondence"
msgstr "Деловая корреспонденция"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Other Business Documents\n"
"itemlist.text"
msgid "Other Business Documents"
msgstr "Прочие деловые документы"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Personal Correspondence and Documents\n"
"itemlist.text"
msgid "Personal Correspondence and Documents"
msgstr "Личная корреспонденция и документы"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Forms and Contracts\n"
"itemlist.text"
msgid "Forms and Contracts"
msgstr "Формы и контакты"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Finances\n"
"itemlist.text"
msgid "Finances"
msgstr "Финансы"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Education\n"
"itemlist.text"
msgid "Education"
msgstr "Образование"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Presentation Backgrounds\n"
"itemlist.text"
msgid "Presentation Backgrounds"
msgstr "Фоны презентаций"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Presentations\n"
"itemlist.text"
msgid "Presentations"
msgstr "Презентации"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Miscellaneous\n"
"itemlist.text"
msgid "Miscellaneous"
msgstr "Прочее"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Labels\n"
"itemlist.text"
msgid "Labels"
msgstr "Этикетки"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"TEMPLATE_LONG_NAMES_ARY\n"
"Styles\n"
"itemlist.text"
msgid "Styles"
msgstr "Стили"

#: doctempl.src
msgctxt ""
"doctempl.src\n"
"RID_CNT_STR_WAITING\n"
"string.text"
msgid "The templates are being initialized for first-time usage."
msgstr "Перед первым использованием выполняется инициализация шаблонов."

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME1\n"
"string.text"
msgid "Abstract Green"
msgstr "Абстрактный зелёный"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME2\n"
"string.text"
msgid "Abstract Red"
msgstr "Абстрактный красный"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME3\n"
"string.text"
msgid "Abstract Yellow"
msgstr "Абстрактный жёлтый"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME4\n"
"string.text"
msgid "Bright Blue"
msgstr "Ярко синий"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME5\n"
"string.text"
msgid "DNA"
msgstr "ДНК"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME6\n"
"string.text"
msgid "Inspiration"
msgstr "Вдохновение"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME7\n"
"string.text"
msgid "Lush Green"
msgstr "Сочный зелёный"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME8\n"
"string.text"
msgid "Metropolis"
msgstr "Урбанистический"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME9\n"
"string.text"
msgid "Sunset"
msgstr "Закат"

#: templatelocnames.src
msgctxt ""
"templatelocnames.src\n"
"STR_TEMPLATE_NAME10\n"
"string.text"
msgid "Vintage"
msgstr "Старомодный"
