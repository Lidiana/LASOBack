#. extracted from cui/source/tabpages
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2015-12-11 17:18+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: lo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1449854315.000000\n"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_NONE\n"
"string.text"
msgid "Set No Borders"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_ONLYOUTER\n"
"string.text"
msgid "Set Outer Border Only"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERHORI\n"
"string.text"
msgid "Set Outer Border and Horizontal Lines"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERALL\n"
"string.text"
msgid "Set Outer Border and All Inner Lines"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERINNER\n"
"string.text"
msgid "Set Outer Border Without Changing Inner Lines"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_DIAGONAL\n"
"string.text"
msgid "Set Diagonal Lines Only"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ALL\n"
"string.text"
msgid "Set All Four Borders"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_LEFTRIGHT\n"
"string.text"
msgid "Set Left and Right Borders Only"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_TOPBOTTOM\n"
"string.text"
msgid "Set Top and Bottom Borders Only"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ONLYLEFT\n"
"string.text"
msgid "Set Left Border Only"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_HOR_PRESET_ONLYHOR\n"
"string.text"
msgid "Set Top and Bottom Borders, and All Inner Lines"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_VER_PRESET_ONLYVER\n"
"string.text"
msgid "Set Left and Right Borders, and All Inner Lines"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_NONE\n"
"string.text"
msgid "No Shadow"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMRIGHT\n"
"string.text"
msgid "Cast Shadow to Bottom Right"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPRIGHT\n"
"string.text"
msgid "Cast Shadow to Top Right"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMLEFT\n"
"string.text"
msgid "Cast Shadow to Bottom Left"
msgstr ""

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPLEFT\n"
"string.text"
msgid "Cast Shadow to Top Left"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_LTR\n"
"string.text"
msgid "Left-to-right (LTR)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_RTL\n"
"string.text"
msgid "Right-to-left (RTL)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_SUPER\n"
"string.text"
msgid "Use superordinate object settings"
msgstr "ນຳໃຊ້ການຕິດຕັ້ງຜູ້ສັ່ງການລະດັບສູງ"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_HORI\n"
"string.text"
msgid "Left-to-right (horizontal)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_HORI\n"
"string.text"
msgid "Right-to-left (horizontal)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_VERT\n"
"string.text"
msgid "Right-to-left (vertical)"
msgstr "ຂວາຫາຊ້າຍ(ທາງຕັ້ງ)"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_VERT\n"
"string.text"
msgid "Left-to-right (vertical)"
msgstr ""

#: paragrph.src
#, fuzzy
msgctxt ""
"paragrph.src\n"
"STR_EXAMPLE\n"
"string.text"
msgid "Example"
msgstr "ຕົວຢາງ"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_GRADIENT\n"
"string.text"
msgid "Please enter a name for the gradient:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_GRADIENT\n"
"string.text"
msgid ""
"The gradient was modified without saving. \n"
"Modify the selected gradient or add a new gradient."
msgstr ""

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_BITMAP\n"
"string.text"
msgid "Please enter a name for the bitmap:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_EXT_BITMAP\n"
"string.text"
msgid "Please enter a name for the external bitmap:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_PATTERN\n"
"string.text"
msgid "Please enter a name for the pattern:"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_BITMAP\n"
"string.text"
msgid ""
"The bitmap was modified without saving. \n"
"Modify the selected bitmap or add a new bitmap."
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_PATTERN\n"
"string.text"
msgid ""
"The pattern was modified without saving. \n"
"Modify the selected pattern or add a new pattern"
msgstr ""

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINESTYLE\n"
"string.text"
msgid "Please enter a name for the line style:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_LINESTYLE\n"
"string.text"
msgid ""
"The line style was modified without saving. \n"
"Modify the selected line style or add a new line style."
msgstr ""

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_HATCH\n"
"string.text"
msgid "Please enter a name for the hatching:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_HATCH\n"
"string.text"
msgid ""
"The hatching type was modified but not saved. \n"
"Modify the selected hatching type or add a new hatching type."
msgstr ""

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHANGE\n"
"string.text"
msgid "Modify"
msgstr ""
"#-#-#-#-#  index.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ປັບປຸງ\n"
"#-#-#-#-#  appl.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ປັບປຸງ\n"
"#-#-#-#-#  UI.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ປັບປຸ່ງ"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ADD\n"
"string.text"
msgid "Add"
msgstr ""
"#-#-#-#-#  table.po (PACKAGE VERSION)  #-#-#-#-#\n"
"~ເພີ້ມເຂົ້າ\n"
"#-#-#-#-#  fldui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"~ເພີ່ມເຂົ້າ\n"
"#-#-#-#-#  dlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"~ເພີ່ມ\n"
"#-#-#-#-#  dbgui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"~ເພີ່ມເຂົ້າ\n"
"#-#-#-#-#  source.po (PACKAGE VERSION)  #-#-#-#-#\n"
"~ເພີ່ມເຂົ້າ\n"
"#-#-#-#-#  textconversiondlgs.po (PACKAGE VERSION)  #-#-#-#-#\n"
"~ເພີ່ມ"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_COLOR\n"
"string.text"
msgid "Please enter a name for the new color:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_COLOR\n"
"string.text"
msgid ""
"The color was modified without saving.\n"
"Modify the selected color or add a new color."
msgstr ""

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_TABLE\n"
"string.text"
msgid "Table"
msgstr "ຕາຕະລາງ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINEEND\n"
"string.text"
msgid "Please enter a name for the new arrowhead:"
msgstr "ກະລຸນາປ້ອນຊື່ສຳຫລັບຫວົລູກສອນໃຫມ່"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_NOSTYLE\n"
"string.text"
msgid "No %1"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FAMILY\n"
"string.text"
msgid "Family"
msgstr ""

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FONT\n"
"string.text"
msgid "Font"
msgstr ""
"#-#-#-#-#  dlg.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຕົວອັກສອນ\n"
"#-#-#-#-#  styleui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ແບບຕົວອັກສອນ\n"
"#-#-#-#-#  animations.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຕົວອັກສອນ\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ແບບຕົວອັກສອນ\n"
"#-#-#-#-#  inc.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຂ້າງໜ້າ\n"
"#-#-#-#-#  form.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຕົວອັກສອນ"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_STYLE\n"
"string.text"
msgid "Style"
msgstr ""
"#-#-#-#-#  fldui.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຮູບແບບ\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຮູບແບບ\n"
"#-#-#-#-#  src.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຮູບແບບ\n"
"#-#-#-#-#  formwizard.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ແບບ\n"
"#-#-#-#-#  propctrlr.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ຮູບແບບ\n"
"#-#-#-#-#  accessibility.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ແບບ"

#: strings.src
#, fuzzy
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_TYPEFACE\n"
"string.text"
msgid "Typeface"
msgstr ""
"#-#-#-#-#  animations.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ແບບຕົວອັກສອນ\n"
"#-#-#-#-#  app.po (PACKAGE VERSION)  #-#-#-#-#\n"
"ແບບພິມ"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_HIGHLIGHTING\n"
"string.text"
msgid "Highlight Color"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USE_REPLACE\n"
"string.text"
msgid "Use replacement table"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_WORD\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_SENT\n"
"string.text"
msgid "Capitalize first letter of every sentence"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BOLD_UNDER\n"
"string.text"
msgid "Automatic *bold* and _underline_"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NO_DBL_SPACES\n"
"string.text"
msgid "Ignore double spaces"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DETECT_URL\n"
"string.text"
msgid "URL Recognition"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DASH\n"
"string.text"
msgid "Replace dashes"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CORRECT_ACCIDENTAL_CAPS_LOCK\n"
"string.text"
msgid "Correct accidental use of cAPS LOCK key"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NON_BREAK_SPACE\n"
"string.text"
msgid "Add non-breaking space before specific punctuation marks in French text"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ORDINAL\n"
"string.text"
msgid "Format ordinal numbers suffixes (1st -> 1^st)"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_EMPTY_PARA\n"
"string.text"
msgid "Remove blank paragraphs"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USER_STYLE\n"
"string.text"
msgid "Replace Custom Styles"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BULLET\n"
"string.text"
msgid "Replace bullets with: "
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_RIGHT_MARGIN\n"
"string.text"
msgid "Combine single line paragraphs if length greater than"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NUM\n"
"string.text"
msgid "Bulleted and numbered lists. Bullet symbol: "
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BORDER\n"
"string.text"
msgid "Apply border"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CREATE_TABLE\n"
"string.text"
msgid "Create table"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_REPLACE_TEMPLATES\n"
"string.text"
msgid "Apply Styles"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_AT_STT_END\n"
"string.text"
msgid "Delete spaces and tabs at beginning and end of paragraph"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_BETWEEN_LINES\n"
"string.text"
msgid "Delete spaces and tabs at end and start of line"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CONNECTOR\n"
"string.text"
msgid "Connector"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DIMENSION_LINE\n"
"string.text"
msgid "Dimension line"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_READ_DATA_ERROR\n"
"string.text"
msgid "The file could not be loaded!"
msgstr "ເອກະສານນີ້ບໍ່ສາມາດໂຫລດໄດ້"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_LOAD_ERROR\n"
"string.text"
msgid "The selected module could not be loaded."
msgstr ""
