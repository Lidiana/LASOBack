#. extracted from xmlsecurity/source/dialogs
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-05-23 21:36+0200\n"
"PO-Revision-Date: 2016-01-20 17:14+0000\n"
"Last-Translator: mahir <mahirfarook@gmail.com>\n"
"Language-Team: American English <kde-i18n-doc@kde.org>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1453310069.000000\n"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_CERTIFICATE_NOT_VALIDATED\n"
"string.text"
msgid "The certificate could not be validated."
msgstr "സര്‍ട്ടിഫിക്കേറ്റ് പരിശോധിക്കുവാന്‍ സാധ്യമായില്ല."

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_HEADERBAR\n"
"string.text"
msgid "Field\tValue"
msgstr "ഫീള്‍ഡ്\tമൂല്ല്യം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_VERSION\n"
"string.text"
msgid "Version"
msgstr "പതിപ്പു്"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_SERIALNUM\n"
"string.text"
msgid "Serial Number"
msgstr "സീരിയല്‍ നമ്പര്‍"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_SIGALGORITHM\n"
"string.text"
msgid "Signature Algorithm"
msgstr "കൈയൊപ്പ് അല്‍ഗോരിതം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_ISSUER\n"
"string.text"
msgid "Issuer"
msgstr "നല്‍കുന്നവര്‍"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_VALIDFROM\n"
"string.text"
msgid "Valid From"
msgstr "കാലാവധി ആരംഭം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_VALIDTO\n"
"string.text"
msgid "Valid to"
msgstr "കാലാവധി അവസാനം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_SUBJECT\n"
"string.text"
msgid "Subject"
msgstr "വിഷയം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_SUBJECT_PUBKEY_ALGO\n"
"string.text"
msgid "Subject Algorithm"
msgstr "വിഷയത്തിനുള്ള അല്‍ഗോരിതം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_SUBJECT_PUBKEY_VAL\n"
"string.text"
msgid "Public Key"
msgstr "പബ്ലിക് കീ"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_SIGNATURE_ALGO\n"
"string.text"
msgid "Signature Algorithm"
msgstr "കൈയൊപ്പ് അല്‍ഗോരിതം"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_USE\n"
"string.text"
msgid "Certificate Use"
msgstr ""

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_THUMBPRINT_SHA1\n"
"string.text"
msgid "Thumbprint SHA1"
msgstr "വിരലടയാളം SHA1"

#: certificateviewer.src
msgctxt ""
"certificateviewer.src\n"
"STR_THUMBPRINT_MD5\n"
"string.text"
msgid "Thumbprint MD5"
msgstr "വിരലടയാളം MD5"

#: digitalsignaturesdialog.src
msgctxt ""
"digitalsignaturesdialog.src\n"
"STR_XMLSECDLG_OLD_ODF_FORMAT\n"
"string.text"
msgid ""
"This document contains signatures in ODF 1.1 (OpenOffice.org 2.x) format. Signing documents in %PRODUCTNAME %PRODUCTVERSION requires ODF 1.2 format version. Thus no signatures can be added or removed to this document.\n"
"\n"
"Save document in ODF 1.2 format and add all desired signatures again."
msgstr ""
"ഈ രേഖയിലെ സിഗ്നേച്ചറുകള്‍ ODF 1.1 (OpenOffice.org 2.x) രീതിയിലാണു്. %PRODUCTNAME %PRODUCTVERSION-ലുള്ള രേഖകളില്‍ ഒപ്പിടുന്നതിനായി ODF 1.2 രീതിയിലുള്ള പതിപ്പു് ആവശ്യമുണ്ടു്. അതിനാല്‍, ഈ രേഖയിലേക്കു് ഒപ്പുകള്‍ ചേര്‍ക്കുവാനോ നീക്കം ചെയ്യുവാനോ സാധ്യമല്ല.\n"
"\n"
"ODF 1.2 രീതിയില്‍ രേഖ സൂക്ഷിച്ചു്, ആവശ്യമുള്ള ഏല്ലാ ഒപ്പുകളും ചേര്‍ക്കുക."

#: digitalsignaturesdialog.src
msgctxt ""
"digitalsignaturesdialog.src\n"
"STR_XMLSECDLG_QUERY_REMOVEDOCSIGNBEFORESIGN\n"
"string.text"
msgid ""
"Adding or removing a macro signature will remove all document signatures.\n"
"Do you really want to continue?"
msgstr ""
"ഒരു മാക്രോ സിഗ്നേച്ചര്‍ ചേര്‍ക്കുകയോ നീക്കം ചെയ്യുകയോ ചെയ്യുന്നതു്, എല്ലാ രേഖകളിലുനുള്ള സിഗ്നേച്ചര്‍ നീക്കം ചെയ്യുന്നു.\n"
"നിങ്ങള്‍ക്കു് തുടരണമോ?"
