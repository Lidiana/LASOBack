#. extracted from sw/source/ui/docvw
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2015-01-10 11:26+0000\n"
"Last-Translator: Mihovil <libreoffice@miho.im>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1420889176.000000\n"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_DOC_NAME\n"
"string.text"
msgid "Document view"
msgstr "Pogled dokumenta"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_DOC_DESC\n"
"string.text"
msgid "Document view"
msgstr "Pogled dokumenta"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_HEADING_WITH_NUM_DESC\n"
"string.text"
msgid "Heading number $(ARG2): $(ARG1)"
msgstr "Broj naslova $(ARG2): $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_HEADER_NAME\n"
"string.text"
msgid "Header $(ARG1)"
msgstr "Zaglavlje $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_HEADER_DESC\n"
"string.text"
msgid "Header page $(ARG1)"
msgstr "Stranica zaglavlja $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_FOOTER_NAME\n"
"string.text"
msgid "Footer $(ARG1)"
msgstr "Podnožje $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_FOOTER_DESC\n"
"string.text"
msgid "Footer page $(ARG1)"
msgstr "Stranica podnožja $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_FOOTNOTE_NAME\n"
"string.text"
msgid "Footnote $(ARG1)"
msgstr "Fusnota $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_FOOTNOTE_DESC\n"
"string.text"
msgid "Footnote $(ARG1)"
msgstr "Fusnota $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_ENDNOTE_NAME\n"
"string.text"
msgid "Endnote $(ARG1)"
msgstr "Završna bilješka $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_ENDNOTE_DESC\n"
"string.text"
msgid "Endnote $(ARG1)"
msgstr "Završna bilješka $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_TABLE_DESC\n"
"string.text"
msgid "$(ARG1) on page $(ARG2)"
msgstr "$(ARG1) na stranici $(ARG2)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_PAGE_NAME\n"
"string.text"
msgid "Page $(ARG1)"
msgstr "Stranica $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_PAGE_DESC\n"
"string.text"
msgid "Page: $(ARG1)"
msgstr "Stranica: $(ARG1)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_ANNOTATION_AUTHOR_NAME\n"
"string.text"
msgid "Author"
msgstr "Autor"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_ANNOTATION_DATE_NAME\n"
"string.text"
msgid "Date"
msgstr "Datum"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_ANNOTATION_BUTTON_NAME\n"
"string.text"
msgid "Actions"
msgstr "Djelovanja"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_ANNOTATION_BUTTON_DESC\n"
"string.text"
msgid "Activate this button to open a list of actions which can be performed on this comment and other comments"
msgstr "Aktivirajte ovaj gumb kako biste otvorili popis djelovanja koja se mogu odraditi na ovom komentaru ili drugim komentarima"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_PREVIEW_DOC_NAME\n"
"string.text"
msgid "Document preview"
msgstr "Pregled dokumenta"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_PREVIEW_DOC_SUFFIX\n"
"string.text"
msgid "(Preview mode)"
msgstr "(Pregled)"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_DOC_WORDPROCESSING\n"
"string.text"
msgid "%PRODUCTNAME Document"
msgstr "Dokument %PRODUCTNAME-a"

#: access.src
msgctxt ""
"access.src\n"
"STR_ACCESS_DOC_WORDPROCESSING_READONLY\n"
"string.text"
msgid "(read-only)"
msgstr "(samo za čitanje)"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_POSTIT_TODAY\n"
"string.text"
msgid "Today,"
msgstr "Danas,"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_POSTIT_YESTERDAY\n"
"string.text"
msgid "Yesterday,"
msgstr "Jučer,"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_DELETE_ALL_NOTES\n"
"string.text"
msgid "All Comments"
msgstr "Svi komentari"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_FORMAT_ALL_NOTES\n"
"string.text"
msgid "All Comments"
msgstr "Svi komentari"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_DELETE_AUTHOR_NOTES\n"
"string.text"
msgid "Comments by "
msgstr "Komentari od "

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_NODATE\n"
"string.text"
msgid "(no date)"
msgstr "(nema datuma)"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_NOAUTHOR\n"
"string.text"
msgid "(no author)"
msgstr "(nema autora)"

#: annotation.src
msgctxt ""
"annotation.src\n"
"STR_REPLY\n"
"string.text"
msgid "Reply to $1"
msgstr "Odgovoriti $1"
