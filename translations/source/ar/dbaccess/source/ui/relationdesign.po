#. extracted from dbaccess/source/ui/relationdesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2016-06-17 08:00+0000\n"
"Last-Translator: صفا الفليج <safaalfulaij@hotmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1466150455.000000\n"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_EDIT_RELATION\n"
"string.text"
msgid "This relation already exists. Do you want to edit it or create a new one?"
msgstr "هذه العلاقة موجودة بالفعل. أتريد تحريرها أم إنشاء جديدة؟"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_EDIT\n"
"string.text"
msgid "Edit..."
msgstr "حرّر..."

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_CREATE\n"
"string.text"
msgid "Create..."
msgstr "أنشئ..."

#: relation.src
msgctxt ""
"relation.src\n"
"STR_RELATIONDESIGN\n"
"string.text"
msgid " - %PRODUCTNAME Base: Relation design"
msgstr " - %PRODUCTNAME بيز: تصميم العلاقات"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_RELATIONDESIGN_NOT_AVAILABLE\n"
"string.text"
msgid "The database does not support relations."
msgstr "قاعدة البيانات لا تدعم العلاقات."

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_DELETE_WINDOW\n"
"string.text"
msgid "When you delete this table all corresponding relations will be deleted as well. Continue?"
msgstr "عندما تحذف هذا الجدول ستُحذف أيضًا كلّ العلاقات المقابلة. أأتابع؟"

#: relation.src
msgctxt ""
"relation.src\n"
"STR_QUERY_REL_COULD_NOT_CREATE\n"
"string.text"
msgid ""
"The database could not create the relation. Maybe foreign keys for this kind of table aren't supported.\n"
"Please check your documentation of the database."
msgstr ""
"تعذر على قاعدة البيانات إنشاء العلاقة. لربما المفاتيح الخارجية لهذا النوع من الجداول غير مدعومة.\n"
"فضلا طالع توثيق قاعدة البيانات."
