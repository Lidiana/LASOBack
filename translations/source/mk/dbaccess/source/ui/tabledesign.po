#. extracted from dbaccess/source/ui/tabledesign
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-03-11 11:51+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n==1 || n%10==1 ? 0 : 1;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1457697091.000000\n"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_DBFIELDTYPES\n"
"string.text"
msgid "Unknown;Text;Number;Date/Time;Date;Time;Yes/No;Currency;Memo;Counter;Image;Text (fix);Decimal;Binary (fix);Binary;BigInt;Double;Float;Real;Integer;Small Integer;Tiny Integer;SQL Null;Object;Distinct;Structure;Field;BLOB;CLOB;REF;OTHER;Bit (fix)"
msgstr "Непознато;Текст;Број;Датум/Време;Датум;Време;Да/Не;Валута;Мемо;Бројач;Слика;Текст (фиксен);Децимален;Бинарен (фиксен);Бинарен;Голем цел број;Двојна;Подвижна запирка;Реален;Цел број;Мал цел број;Многу мал цел број;SQL Null;Објект;Уникатно;Структура;Поле;BLOB;CLOB;РЕФ;ДРУГО"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_UNDO_PRIMKEY\n"
"string.text"
msgid "Insert/remove primary key"
msgstr "Вметни/отстрани примарен клуч"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_YES\n"
"string.text"
msgid "Yes"
msgstr "Да"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_NO\n"
"string.text"
msgid "No"
msgstr "Не"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_ASC\n"
"string.text"
msgid "Ascending"
msgstr "Растечки"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_DESC\n"
"string.text"
msgid "Descending"
msgstr "Опаѓачки"

#: table.src
msgctxt ""
"table.src\n"
"STR_VALUE_NONE\n"
"string.text"
msgid "<none>"
msgstr "<нема>"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_NAME\n"
"string.text"
msgid "Field name"
msgstr "Име на полето"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_COLUMN_NAME\n"
"string.text"
msgid "Field Name"
msgstr "Име на поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_DATATYPE\n"
"string.text"
msgid "Field ~type"
msgstr "Тип на полето"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_COLUMN_DATATYPE\n"
"string.text"
msgid "Field Type"
msgstr "Тип на поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_LENGTH\n"
"string.text"
msgid "Field length"
msgstr "Должина на поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_HELP_TEXT\n"
"string.text"
msgid "Description"
msgstr "Опис"

#: table.src
#, fuzzy
msgctxt ""
"table.src\n"
"STR_COLUMN_DESCRIPTION\n"
"string.text"
msgid "Column Description"
msgstr "Уреди опис на колона"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_FIELD_NULLABLE\n"
"string.text"
msgid "Input required"
msgstr "Задолжителен внес"

#: table.src
msgctxt ""
"table.src\n"
"STR_FIELD_AUTOINCREMENT\n"
"string.text"
msgid "~AutoValue"
msgstr "~Автом. вредност"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_PROPERTIES\n"
"string.text"
msgid "Field Properties"
msgstr "Својства на полето"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABPAGE_GENERAL\n"
"string.text"
msgid "General"
msgstr "Општо"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_TABLE_DESCRIPTION\n"
"string.text"
msgid "Description:"
msgstr "Опис:"

#: table.src
msgctxt ""
"table.src\n"
"STR_TAB_TABLE_PROPERTIES\n"
"string.text"
msgid "Table properties"
msgstr "Својства на табелата"

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_CUT\n"
"menuitem.text"
msgid "Cu~t"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_COPY\n"
"menuitem.text"
msgid "~Copy"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_PASTE\n"
"menuitem.text"
msgid "~Paste"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_DELETE\n"
"menuitem.text"
msgid "~Delete"
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_TABLEDESIGN_INSERTROWS\n"
"menuitem.text"
msgid "Insert Rows"
msgstr "Вметни редови"

#: table.src
msgctxt ""
"table.src\n"
"RID_TABLEDESIGNROWPOPUPMENU\n"
"SID_TABLEDESIGN_TABED_PRIMARYKEY\n"
"menuitem.text"
msgid "Primary Key"
msgstr "Примарен клуч"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_CELLMODIFIED\n"
"string.text"
msgid "Modify cell"
msgstr "Измени ќелија"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_ROWDELETED\n"
"string.text"
msgid "Delete row"
msgstr "Избриши ред"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_TYPE_CHANGED\n"
"string.text"
msgid "Modify field type"
msgstr "Измени тип на поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_ROWINSERTED\n"
"string.text"
msgid "Insert row"
msgstr "Вметни ред"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_NEWROWINSERTED\n"
"string.text"
msgid "Insert new row"
msgstr "Вметни нов ред"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABED_UNDO_PRIMKEY\n"
"string.text"
msgid "Insert/remove primary key"
msgstr "Вметни/отстрани примарен клуч"

#: table.src
msgctxt ""
"table.src\n"
"STR_DEFAULT_VALUE\n"
"string.text"
msgid "~Default value"
msgstr "~Стандардна вредност"

#: table.src
msgctxt ""
"table.src\n"
"STR_FIELD_REQUIRED\n"
"string.text"
msgid "~Entry required"
msgstr "~Задолжителен внес"

#: table.src
msgctxt ""
"table.src\n"
"STR_TEXT_LENGTH\n"
"string.text"
msgid "~Length"
msgstr "~Должина"

#: table.src
msgctxt ""
"table.src\n"
"STR_NUMERIC_TYPE\n"
"string.text"
msgid "~Type"
msgstr "~Тип"

#: table.src
msgctxt ""
"table.src\n"
"STR_LENGTH\n"
"string.text"
msgid "~Length"
msgstr "~Должина"

#: table.src
msgctxt ""
"table.src\n"
"STR_SCALE\n"
"string.text"
msgid "Decimal ~places"
msgstr "Де~цимални места"

#: table.src
msgctxt ""
"table.src\n"
"STR_FORMAT\n"
"string.text"
msgid "Format example"
msgstr "Пример за формат"

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_BOOL_DEFAULT\n"
"string.text"
msgid ""
"Select a value that is to appear in all new records as default.\n"
"If the field is not to have a default value, select the empty string."
msgstr ""
"Изберете вредност што ќе се појавува како стандардна во сите нови записи.\n"
"Ако полето нема стандардна вредност, изберете <нема>."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_DEFAULT_VALUE\n"
"string.text"
msgid ""
"Enter a default value for this field.\n"
"\n"
"When you later enter data in the table, this string will be used in each new record for the field selected. It should, therefore, correspond to the cell format that needs to be entered below."
msgstr ""
"Внесете стандардна вредност за полето.\n"
"\n"
"Кога подоцна ќе внесувате податоци во табелата, оваа вредност ќе се користи во секој нов запис за избраното поле. Затоа треба да одговара на форматот на ќелија што треба да се внесе подолу."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_FIELD_REQUIRED\n"
"string.text"
msgid "Activate this option if this field cannot contain NULL values, i.e. the user must always enter data."
msgstr "Активирајте ја оваа опција ако ова поле не смее да содржи NULL-вредности, т.е. корисникот секогаш мора да внесе податоци."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_TEXT_LENGTH\n"
"string.text"
msgid "Enter the maximum text length permitted."
msgstr "Внесете ја максимално дозволената должина на текстот."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_NUMERIC_TYPE\n"
"string.text"
msgid "Enter the number format."
msgstr "Внесете го форматот на број."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_LENGTH\n"
"string.text"
msgid ""
"Determine the length data can have in this field.\n"
"\n"
"If decimal fields, then the maximum length of the number to be entered, if binary fields, then the length of the data block.\n"
"The value will be corrected accordingly when it exceeds the maximum for this database."
msgstr ""
"Одредете ја должината што може да ја имаат податоците во ова поле.\n"
"Кај децимални полиња, треба да се внесе максималната должина на бројот, а кај бинарни полиња должината на блокот на податоци.\n"
"Вредноста ќе биде коригирана соодветно кога ќе го надмине максимумот за оваа база на податоци."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_SCALE\n"
"string.text"
msgid "Specify the number of decimal places permitted in this field."
msgstr "Одредете го бројот на децимални места дозволени за ова поле."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_FORMAT_CODE\n"
"string.text"
msgid "This is where you see how the data would be displayed in the current format (use the button on the right to modify the format)."
msgstr "Тука може да видите како ќе се прикажуваат податоците во тековниот формат (користете го копчето од десната страна за да го измените форматот)."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_FORMAT_BUTTON\n"
"string.text"
msgid "This is where you determine the output format of the data."
msgstr "Тука го одредувате излезниот формат на податоците."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_AUTOINCREMENT\n"
"string.text"
msgid ""
"Choose if this field should contain AutoIncrement values.\n"
"\n"
"You can not enter data in fields of this type. An intrinsic value will be assigned to each new record automatically (resulting from the increment of the previous record)."
msgstr ""
"Изберете дали ова поле треба да содржи вредности што се зголемуваат автоматски (AutoIncrement).\n"
"\n"
"Не може да внесувате податоци во овој тип полиња. Нова вредност ќе се додели на секој нов запис автоматски (како резултат на зголемување на вредноста од претходниот запис)."

#: table.src
msgctxt ""
"table.src\n"
"STR_BUTTON_FORMAT\n"
"string.text"
msgid "~..."
msgstr ""

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_DUPLICATE_NAME\n"
"string.text"
msgid "The table cannot be saved because column name \"$column$\" was assigned twice."
msgstr "Табелата не може да се зачува бидејќи името на колоната „$column$“ се појавува двапати."

#: table.src
msgctxt ""
"table.src\n"
"STR_TBL_COLUMN_IS_KEYCOLUMN\n"
"string.text"
msgid "The column \"$column$\" belongs to the primary key. If the column is deleted, the primary key will also be deleted. Do you really want to continue?"
msgstr "Колоната „$column$“ му припаѓа на примарниот клуч. Ако се избрише колоната, ќе биде избришан и примарниот клуч. Дали навистина сакате да продолжите?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TBL_COLUMN_IS_KEYCOLUMN_TITLE\n"
"string.text"
msgid "Primary Key Affected"
msgstr "Влијае врз примарниот клуч"

#: table.src
msgctxt ""
"table.src\n"
"STR_COLUMN_NAME\n"
"string.text"
msgid "Column"
msgstr "Колона"

#: table.src
msgctxt ""
"table.src\n"
"STR_QRY_CONTINUE\n"
"string.text"
msgid "Continue anyway?"
msgstr "Дали сепак продолжувате?"

#: table.src
msgctxt ""
"table.src\n"
"STR_STAT_WARNING\n"
"string.text"
msgid "Warning!"
msgstr "Предупредување!"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_CONNECTION_MISSING\n"
"string.text"
msgid "The table could not be saved due to problems connecting to the database."
msgstr "Табелата не можеше да се зачува поради проблеми при поврзувањето со базата на податоци."

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_DATASOURCE_DELETED\n"
"string.text"
msgid "The table filter could not be adjusted because the data source has been deleted."
msgstr "Филтерот за табелата не можеше да биде приспособен бидејќи изворот на податоци беше избришан."

#: table.src
#, fuzzy
msgctxt ""
"table.src\n"
"STR_QUERY_SAVE_TABLE_EDIT_INDEXES\n"
"string.text"
msgid ""
"Before you can edit the indexes of a table, you have to save it.\n"
"Do you want to save the changes now?"
msgstr ""
"Пред да може да ги уредувате индексите на некоја табела, мора да ја зачувате.\n"
"Дали сакате да ги зачувате измените сега?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_NO_PRIM_KEY_HEAD\n"
"string.text"
msgid "No primary key"
msgstr "Нема примарен клуч"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_NO_PRIM_KEY\n"
"string.text"
msgid ""
"A unique index or primary key is required for data record identification in this database.\n"
"You can only enter data into this table when one of these two structural conditions has been met.\n"
"\n"
"Should a primary key be created now?"
msgstr ""
"Потребен е единствен индекс или примарен клуч за идентификација на записите со податоци во табелата.\n"
"Можете да внесувате податоци во табелата само ако е исполнет еден од овие два структурни услова.\n"
"\n"
"Дали треба да се креира примарен клуч сега?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_TITLE\n"
"string.text"
msgid " - %PRODUCTNAME Base: Table Design"
msgstr " - %PRODUCTNAME Base: Дизајнирање на табели"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_ALTER_ERROR\n"
"string.text"
msgid "The column \"$column$\" could not be changed. Should the column instead be deleted and the new format appended?"
msgstr "Колоната „$column$“ не може да се измени. Дали место тоа колоната да се избрише и да се додаде новиот формат?"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_SAVE_ERROR\n"
"string.text"
msgid "Error while saving the table design"
msgstr "Грешка при зачувувањето на дизајнот на табелата"

#: table.src
msgctxt ""
"table.src\n"
"STR_TABLEDESIGN_COULD_NOT_DROP_COL\n"
"string.text"
msgid "The column $column$ could not be deleted."
msgstr "Колоната $column$ не можеше да биде избришана."

#: table.src
msgctxt ""
"table.src\n"
"STR_AUTOINCREMENT_VALUE\n"
"string.text"
msgid "A~uto-increment statement"
msgstr "Израз за а~втом. зголем."

#: table.src
msgctxt ""
"table.src\n"
"STR_HELP_AUTOINCREMENT_VALUE\n"
"string.text"
msgid ""
"Enter an SQL statement for the auto-increment field.\n"
"\n"
"This statement will be directly transferred to the database when the table is created."
msgstr ""
"Внесете SQL-израз за полето со автом. зголемување.\n"
"\n"
"Овој израз ќе биде директно пренесен во базата на податоци кога ќе се креира табелата."

#: table.src
msgctxt ""
"table.src\n"
"STR_NO_TYPE_INFO_AVAILABLE\n"
"string.text"
msgid ""
"No type information could be retrieved from the database.\n"
"The table design mode is not available for this data source."
msgstr ""
"Не можеше да се добијат информации за типови од базата на податоци.\n"
"Режимот на дизајнирање табела не е достапен за овој извор на податоци."

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_NAME\n"
"string.text"
msgid "change field name"
msgstr "измени име на поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_TYPE\n"
"string.text"
msgid "change field type"
msgstr "измени тип поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_DESCRIPTION\n"
"string.text"
msgid "change field description"
msgstr "измени опис на поле"

#: table.src
msgctxt ""
"table.src\n"
"STR_CHANGE_COLUMN_ATTRIBUTE\n"
"string.text"
msgid "change field attribute"
msgstr "измени својства на поле"
