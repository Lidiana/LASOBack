#. extracted from sw/source/ui/envelp
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2014-11-18 11:23+0100\n"
"PO-Revision-Date: 2011-04-27 19:43+0200\n"
"Last-Translator: sedric <sedric@translate.org.za>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_DATABASE_NOT_OPENED\n"
"string.text"
msgid "Database could not be opened."
msgstr "Isiqu solwazi asivulekenga."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_NO_DRIVERS\n"
"string.text"
msgid "No database drivers installed."
msgstr "Akukho bashayeli besiqu solwazi abafakiwe."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_BTN_NEWDOC\n"
"string.text"
msgid "~New Doc."
msgstr "~Ushicilelo Olusha."

#: envelp.src
msgctxt ""
"envelp.src\n"
"STR_SENDER_TOKENS\n"
"string.text"
msgid "COMPANY;CR;FIRSTNAME; ;LASTNAME;CR;ADDRESS;CR;CITY; ;STATEPROV; ;POSTALCODE;CR;COUNTRY;CR;"
msgstr "INKAMPANI;CR;IGAMA; ;ISIBONGO;CR;IKHELI;CR;IDOLOBHA; ;ISIFUNDA; ;IKHODI;CR;IZWE;CR;"

#: label.src
msgctxt ""
"label.src\n"
"STR_CUSTOM\n"
"string.text"
msgid "[User]"
msgstr "[Umsebenzisi]"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_HDIST\n"
"string.text"
msgid "H. Pitch"
msgstr "Ummango ovundlile"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_VDIST\n"
"string.text"
msgid "V. Pitch"
msgstr "Ummango omile"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_WIDTH\n"
"string.text"
msgid "Width"
msgstr "Ububanzi"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_HEIGHT\n"
"string.text"
msgid "Height"
msgstr "Ubude"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_LEFT\n"
"string.text"
msgid "Left margin"
msgstr "Usebe langakwesokunxele"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_UPPER\n"
"string.text"
msgid "Top margin"
msgstr "Usebe lwaphezulu"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_COLS\n"
"string.text"
msgid "Columns"
msgstr "Imifantu"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_ROWS\n"
"string.text"
msgid "Rows"
msgstr "Imigqa"

#: labfmt.src
msgctxt ""
"labfmt.src\n"
"STR_PWIDTH\n"
"string.text"
msgid "Page Width"
msgstr "Ububanzi Bekhasi"

#: labfmt.src
#, fuzzy
msgctxt ""
"labfmt.src\n"
"STR_PHEIGHT\n"
"string.text"
msgid "Page Height"
msgstr "Ubude ~bekhasi"
