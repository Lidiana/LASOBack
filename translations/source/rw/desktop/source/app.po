#. extracted from desktop/source/app
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2015-05-13 11:21+0000\n"
"Last-Translator: system user <>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: rw\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Pootle 2.7\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1431516118.000000\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_RECOVER_QUERY\n"
"string.text"
msgid "Should the file \"$1\" be restored?"
msgstr "Ni ngombwa ko dosiye \"$1\" igaruka?"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_RECOVER_TITLE\n"
"string.text"
msgid "File Recovery"
msgstr "Gukiza dosiye"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_CANNOT_START\n"
"string.text"
msgid "The application cannot be started. "
msgstr "iyi porogaramu ntishobora gutangira."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_DIR_MISSING\n"
"string.text"
msgid "The configuration directory \"$1\" could not be found."
msgstr "Iboneza ry'ububiko \"$1\" ntirishoboye kuboneka."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_PATH_INVALID\n"
"string.text"
msgid "The installation path is invalid."
msgstr "Inzira y'iyinjizaporogaramu siyo."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_PATH\n"
"string.text"
msgid "The installation path is not available."
msgstr "Inzira y'iyinjizaporogaramu ntibonetse."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_INTERNAL\n"
"string.text"
msgid "An internal error occurred."
msgstr "Ikosa ry'imbere ryagaragaye."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_FILE_CORRUPT\n"
"string.text"
msgid "The configuration file \"$1\" is corrupt."
msgstr "Dosiye y'iboneza \"$1\" ifite ikosa."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_FILE_MISSING\n"
"string.text"
msgid "The configuration file \"$1\" was not found."
msgstr "Dosiye y'iboneza \"$1\" ntiyabonetse."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_SUPPORT\n"
"string.text"
msgid "The configuration file \"$1\" does not support the current version."
msgstr "Dosiye y'iboneza \"$1\" ntiyemera verisiyo igezweho."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_LANGUAGE_MISSING\n"
"string.text"
msgid "The user interface language cannot be determined."
msgstr "Ururimi rw'irebero ry'ukoresha ntabwo rushobora kugenwa."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_USERINSTALL_FAILED\n"
"string.text"
msgid "User installation could not be completed. "
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_CFG_SERVICE\n"
"string.text"
msgid "The configuration service is not available."
msgstr "Serivise y'iboneza ntibonetse."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_MULTISESSION\n"
"string.text"
msgid "You have another instance running in a different terminal session. Close that instance and then try again."
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_ASK_START_SETUP_MANUALLY\n"
"string.text"
msgid "Start the setup application to repair the installation from the CD or the folder containing the installation packages."
msgstr "Tangiza iboneza rya porogaramu kugirango ukosore iyinjizaporogaramu uhereye kuri CD cyangwaububiko bufite porogaramu y'itangizaporogaramu"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_SETTINGS_INCOMPLETE\n"
"string.text"
msgid "The startup settings for accessing the central configuration are incomplete. "
msgstr "Igenamiterere ryo kugera ku iboneza-shingiro ntiryuzuye."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_CANNOT_CONNECT\n"
"string.text"
msgid "A connection to the central configuration could not be established. "
msgstr "Kwihuza n'iboneza-shingiro ntibishobotse."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_RIGHTS_MISSING\n"
"string.text"
msgid "You cannot access the central configuration because of missing access rights. "
msgstr "Ntushobora kugera ku iboneza fatizo kubera ko utabifitiye uburenganzira."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_ACCESS_GENERAL\n"
"string.text"
msgid "A general error occurred while accessing your central configuration. "
msgstr "Hagaragaye ikosa rusange igihe washaka kugera ku iboneza fatizo ryawe."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_NO_WRITE_ACCESS\n"
"string.text"
msgid "The changes to your personal settings cannot be stored centrally because of missing access rights. "
msgstr "Ibyahindutse ku igenamikorere ryawe ntibishobora kubikwa mo imbere kukonta burenganzira."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_CFG_DATAACCESS\n"
"string.text"
msgid ""
"%PRODUCTNAME cannot be started due to an error in accessing the %PRODUCTNAME configuration data.\n"
"\n"
"Please contact your system administrator."
msgstr ""
"%PRODUCTNAME ntishoboye gutangizwa kuko hari ikosa mu kugera ku byatanze by'iboneza rya %PRODUCTNAME.\n"
"Baza umugenzuzi wa sisitemu yawe."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_INTERNAL_ERRMSG\n"
"string.text"
msgid "The following internal error has occurred: "
msgstr "Dore ikosa ry'imbere ryabayeho: "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_LO_MUST_BE_RESTARTED\n"
"string.text"
msgid "%PRODUCTNAME must unfortunately be manually restarted once after installation or update."
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_QUERY_USERDATALOCKED\n"
"string.text"
msgid ""
"Either another instance of %PRODUCTNAME is accessing your personal settings or your personal settings are locked.\n"
"Simultaneous access can lead to inconsistencies in your personal settings. Before continuing, you should make sure user '$u' closes %PRODUCTNAME on host '$h'.\n"
"\n"
"Do you really want to continue?"
msgstr ""

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_TITLE_USERDATALOCKED\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_ERR_PRINTDISABLED\n"
"string.text"
msgid "Printing is disabled. No documents can be printed."
msgstr "Gucapa byahagaritswe. Nta nyandiko zishobora gucapwa."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_TITLE_EXPIRED\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_PATHSET_SERVICE\n"
"string.text"
msgid "The path manager is not available.\n"
msgstr "Mucunganzira ntayiriho. \n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOSTRAP_ERR_NOTENOUGHDISKSPACE\n"
"string.text"
msgid ""
"%PRODUCTNAME user installation could not be completed due to insufficient free disk space. Please free more disc space at the following location and restart %PRODUCTNAME:\n"
"\n"
msgstr ""
"Kwinjizamo ukoresha %PRODUCTNAME ntibyaboye kurangira kubera kubura umwanya uhagije kuri disiki. Usabwe kongera umwanya kuri disiki ahakurikira maze ukongera gutangiza %PRODUCTNAME:\n"
"\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOSTRAP_ERR_NOACCESSRIGHTS\n"
"string.text"
msgid ""
"%PRODUCTNAME user installation could not be processed due to missing access rights. Please make sure that you have sufficient access rights for the following location and restart %PRODUCTNAME:\n"
"\n"
msgstr ""
"Kwinjizamo ukoresha %PRODUCTNAME ntibyashoboye gutunganywa kubera kubura uburenganzira bwo kubigeraho. Usabwe kugenzura neza ko ufite uburenganzira buhagije bwo kugera ahakurikira maze ukongera ugatangiza %PRODUCTNAME:\n"
"\n"
