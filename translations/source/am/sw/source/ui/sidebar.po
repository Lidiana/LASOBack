#. extracted from sw/source/ui/sidebar
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-12-02 15:35+0000\n"
"Last-Translator: Samson B <sambelet@yahoo.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: am\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1480692916.000000\n"

#: PagePropertyPanel.src
msgctxt ""
"PagePropertyPanel.src\n"
"STR_MARGIN_TOOLTIP_LEFT\n"
"string.text"
msgid "Left: "
msgstr "በ ግራ: "

#: PagePropertyPanel.src
msgctxt ""
"PagePropertyPanel.src\n"
"STR_MARGIN_TOOLTIP_RIGHT\n"
"string.text"
msgid ". Right: "
msgstr ". በ ቀኝ: "

#: PagePropertyPanel.src
msgctxt ""
"PagePropertyPanel.src\n"
"STR_MARGIN_TOOLTIP_INNER\n"
"string.text"
msgid "Inner: "
msgstr "ውስጥ: "

#: PagePropertyPanel.src
msgctxt ""
"PagePropertyPanel.src\n"
"STR_MARGIN_TOOLTIP_OUTER\n"
"string.text"
msgid ". Outer: "
msgstr ". ውጪ: "

#: PagePropertyPanel.src
msgctxt ""
"PagePropertyPanel.src\n"
"STR_MARGIN_TOOLTIP_TOP\n"
"string.text"
msgid ". Top: "
msgstr ". ከ ላይ: "

#: PagePropertyPanel.src
msgctxt ""
"PagePropertyPanel.src\n"
"STR_MARGIN_TOOLTIP_BOT\n"
"string.text"
msgid ". Bottom: "
msgstr ". ከ ታች: "
