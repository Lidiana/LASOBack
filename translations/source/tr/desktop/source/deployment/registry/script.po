#. extracted from desktop/source/deployment/registry/script
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2016-12-30 09:21+0000\n"
"Last-Translator: Necdet Yucel <necdetyucel@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1483089669.000000\n"

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_BASIC_LIB\n"
"string.text"
msgid "%PRODUCTNAME Basic Library"
msgstr "%PRODUCTNAME Basic Kütüphanesi"

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_DIALOG_LIB\n"
"string.text"
msgid "Dialog Library"
msgstr "İletişim Ögesi Kütüphanesi"

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_CANNOT_DETERMINE_LIBNAME\n"
"string.text"
msgid "The library name could not be determined."
msgstr "Kütüphane adı belirlenemedi."

#: dp_script.src
msgctxt ""
"dp_script.src\n"
"RID_STR_LIBNAME_ALREADY_EXISTS\n"
"string.text"
msgid "This library name already exists. Please choose a different name."
msgstr "Bu kütüphane adı mevcut. Lütfen farklı bir isim seçin."
