#. extracted from desktop/source/app
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-04-22 23:40+0200\n"
"PO-Revision-Date: 2017-03-15 06:13+0000\n"
"Last-Translator: VenetoABC <veneto.abc@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: vec\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.8\n"
"X-POOTLE-MTIME: 1489558392.000000\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_RECOVER_QUERY\n"
"string.text"
msgid "Should the file \"$1\" be restored?"
msgstr "Vuto recuparar el file \"$1\"?"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_RECOVER_TITLE\n"
"string.text"
msgid "File Recovery"
msgstr "Recuparo file"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_CANNOT_START\n"
"string.text"
msgid "The application cannot be started. "
msgstr "Inposìbiłe aviar l'aplegasion. "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_DIR_MISSING\n"
"string.text"
msgid "The configuration directory \"$1\" could not be found."
msgstr "Inposìbiłe trovar ła carteła de configurasion \"$1\"."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_PATH_INVALID\n"
"string.text"
msgid "The installation path is invalid."
msgstr "El parcorso de instałasion no'l ze mìa vàłido."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_PATH\n"
"string.text"
msgid "The installation path is not available."
msgstr "Inposìbiłe definir el parcorso de instałasion."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_INTERNAL\n"
"string.text"
msgid "An internal error occurred."
msgstr "Se ga verifegà un eror interno."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_FILE_CORRUPT\n"
"string.text"
msgid "The configuration file \"$1\" is corrupt."
msgstr "El file de configurasion \"$1\" el ze sasinà."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_FILE_MISSING\n"
"string.text"
msgid "The configuration file \"$1\" was not found."
msgstr "Inposìbiłe trovar el file de configurasion \"$1\"."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_SUPPORT\n"
"string.text"
msgid "The configuration file \"$1\" does not support the current version."
msgstr "El file de configurasion prinsipałe \"$1\" no'l suporta mìa 'sta version."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_LANGUAGE_MISSING\n"
"string.text"
msgid "The user interface language cannot be determined."
msgstr "No ze mìa stà posìbiłe determinar ła łengua de l'intarfasa utente."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_USERINSTALL_FAILED\n"
"string.text"
msgid "User installation could not be completed. "
msgstr "No ze mìa stà posìbiłe conpletar l'instałasion utente. "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_CFG_SERVICE\n"
"string.text"
msgid "The configuration service is not available."
msgstr "El servisio de configurasion no'l ze mìa desponìbiłe."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_MULTISESSION\n"
"string.text"
msgid "You have another instance running in a different terminal session. Close that instance and then try again."
msgstr "Te ghe n'altra istansa in ezecusion inte na sesion de terminałe difarente. Sara su che l'istansa e reprova."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_ASK_START_SETUP_MANUALLY\n"
"string.text"
msgid "Start the setup application to repair the installation from the CD or the folder containing the installation packages."
msgstr "Pa reparar l'instałasion, avìa el programa de setup da el CD-ROM o da ła carteła che ga rento i pacheti de instałasion."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_SETTINGS_INCOMPLETE\n"
"string.text"
msgid "The startup settings for accessing the central configuration are incomplete. "
msgstr "Łe inpostasion de inviamento, par aver aceso a ła configurasion sentrałe, no łe ze mìa conplete. "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_CANNOT_CONNECT\n"
"string.text"
msgid "A connection to the central configuration could not be established. "
msgstr "Inposìbiłe stabiłir na conesion co ła configurasion sentrałe."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_RIGHTS_MISSING\n"
"string.text"
msgid "You cannot access the central configuration because of missing access rights. "
msgstr "Inposìbiłe acédere a ła configurasion sentrałe parché manca i deriti nesesari. "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_ACCESS_GENERAL\n"
"string.text"
msgid "A general error occurred while accessing your central configuration. "
msgstr "Fin che se acedéa a ła configurasion sentrałe se ga verifegà un eror zenarałe. "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_CONFIG_ERR_NO_WRITE_ACCESS\n"
"string.text"
msgid "The changes to your personal settings cannot be stored centrally because of missing access rights. "
msgstr "Inposìbiłe memorizar łe modìfeghe a łe inpostasion parsonaizà parché manca i deriti de aceso."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_CFG_DATAACCESS\n"
"string.text"
msgid ""
"%PRODUCTNAME cannot be started due to an error in accessing the %PRODUCTNAME configuration data.\n"
"\n"
"Please contact your system administrator."
msgstr ""
"Inposìbiłe aviar %PRODUCTNAME par un eror de aceso a i dati de configurasion de %PRODUCTNAME.\n"
"\n"
"Contata el to aministrador de sistema."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_INTERNAL_ERRMSG\n"
"string.text"
msgid "The following internal error has occurred: "
msgstr "Se ga verifegà l'eror interno seguente: "

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_LO_MUST_BE_RESTARTED\n"
"string.text"
msgid "%PRODUCTNAME must unfortunately be manually restarted once after installation or update."
msgstr "Sfortunatamente %PRODUCTNAME el ga da èsar reavià manualmente dopo na instałasion o un ajornamento."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_QUERY_USERDATALOCKED\n"
"string.text"
msgid ""
"Either another instance of %PRODUCTNAME is accessing your personal settings or your personal settings are locked.\n"
"Simultaneous access can lead to inconsistencies in your personal settings. Before continuing, you should make sure user '$u' closes %PRODUCTNAME on host '$h'.\n"
"\n"
"Do you really want to continue?"
msgstr ""
"N'altra istansa de %PRODUCTNAME ła ga aceso a łe inpostasion parsonaizà opur łe inpostasion parsonaizà łe ze blocà.\n"
"L'aceso simultaneo el pol cauzar incongruense a łe inpostasion. Prima de ndar vanti, te ghe da èsar seguro che l'utente '$u' el sara sù %PRODUCTNAME so'l computer host '$h'.\n"
"\n"
"Vuto davero continuar?"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_TITLE_USERDATALOCKED\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_ERR_PRINTDISABLED\n"
"string.text"
msgid "Printing is disabled. No documents can be printed."
msgstr "Ła stanpa ła ze dezabiłità. Inposìbiłe stanpar."

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_TITLE_EXPIRED\n"
"string.text"
msgid "%PRODUCTNAME %PRODUCTVERSION"
msgstr "%PRODUCTNAME %PRODUCTVERSION"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOTSTRAP_ERR_NO_PATHSET_SERVICE\n"
"string.text"
msgid "The path manager is not available.\n"
msgstr "El jestor de parcorsi no'l ze mìa desponìbiłe.\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOSTRAP_ERR_NOTENOUGHDISKSPACE\n"
"string.text"
msgid ""
"%PRODUCTNAME user installation could not be completed due to insufficient free disk space. Please free more disc space at the following location and restart %PRODUCTNAME:\n"
"\n"
msgstr ""
"L'instałasion utente de %PRODUCTNAME no ła ze sta' conpletada parché no A ghe ze spasio che basta so'l disco. Łìbara pì spasio so'l disco so ła pozision seguente e retaca %PRODUCTNAME:\n"
"\n"

#: desktop.src
msgctxt ""
"desktop.src\n"
"STR_BOOSTRAP_ERR_NOACCESSRIGHTS\n"
"string.text"
msgid ""
"%PRODUCTNAME user installation could not be processed due to missing access rights. Please make sure that you have sufficient access rights for the following location and restart %PRODUCTNAME:\n"
"\n"
msgstr ""
"Inposìbiłe ełaborar l'instałasion utente de %PRODUCTNAME parché A no te ghe autorizasion che basta. Controła de aver łe juste autorizasion par ła pozision seguente e retaca %PRODUCTNAME:\n"
"\n"
