#. extracted from dictionaries/ru_RU/dialog/registry/data/org/openoffice/Office
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2013-05-23 12:05+0200\n"
"PO-Revision-Date: 2012-11-23 08:54+0000\n"
"Last-Translator: Cor <cor.nouws@documentfoundation.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: LibreOffice\n"
"X-Accelerator-Marker: ~\n"
"X-POOTLE-MTIME: 1353660871.0\n"

#: OptionsDialog.xcu
msgctxt ""
"OptionsDialog.xcu\n"
"..OptionsDialog.Nodes.LanguageSettings\n"
"Label\n"
"value.text"
msgid "Language Settings"
msgstr "Taalinstellingen"

#: OptionsDialog.xcu
msgctxt ""
"OptionsDialog.xcu\n"
"..OptionsDialog.Nodes.LanguageSettings.Leaves.org.openoffice.lightproof.ru_RU\n"
"Label\n"
"value.text"
msgid "Grammar checking (Russian)"
msgstr "Grammatica-controle (Russisch)"
