#. extracted from sc/source/ui/StatisticsDialogs
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2015-11-10 19:32+0100\n"
"PO-Revision-Date: 2016-09-19 10:01+0000\n"
"Last-Translator: Modestas Rimkus <modestas.rimkus@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1474279295.000000\n"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_MOVING_AVERAGE_UNDO_NAME\n"
"string.text"
msgid "Moving Average"
msgstr "Slankusis vidurkis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_EXPONENTIAL_SMOOTHING_UNDO_NAME\n"
"string.text"
msgid "Exponential Smoothing"
msgstr "Eksponentinis glotninimas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANALYSIS_OF_VARIANCE_UNDO_NAME\n"
"string.text"
msgid "Analysis of Variance"
msgstr "Dispersinė analizė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_SINGLE_FACTOR_LABEL\n"
"string.text"
msgid "ANOVA - Single Factor"
msgstr "Dispersinė analizė – vienas faktorius"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_TWO_FACTOR_LABEL\n"
"string.text"
msgid "ANOVA - Two Factor"
msgstr "Dispersinė analizė – du faktoriai"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_GROUPS\n"
"string.text"
msgid "Groups"
msgstr "Grupės"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_BETWEEN_GROUPS\n"
"string.text"
msgid "Between Groups"
msgstr "Tarp grupių"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_WITHIN_GROUPS\n"
"string.text"
msgid "Within Groups"
msgstr "Grupėse"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_SOURCE_OF_VARIATION\n"
"string.text"
msgid "Source of Variation"
msgstr "Dispersijos šaltinis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_SS\n"
"string.text"
msgid "SS"
msgstr "SS"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_DF\n"
"string.text"
msgid "df"
msgstr "df"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_MS\n"
"string.text"
msgid "MS"
msgstr "MS"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_F\n"
"string.text"
msgid "F"
msgstr "F"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_P_VALUE\n"
"string.text"
msgid "P-value"
msgstr "P-reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_F_CRITICAL\n"
"string.text"
msgid "F critical"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ANOVA_LABEL_TOTAL\n"
"string.text"
msgid "Total"
msgstr "Iš viso"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_CORRELATION_UNDO_NAME\n"
"string.text"
msgid "Correlation"
msgstr "Koreliacija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_CORRELATION_LABEL\n"
"string.text"
msgid "Correlations"
msgstr "Koreliacijos"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_COVARIANCE_UNDO_NAME\n"
"string.text"
msgid "Covariance"
msgstr "Kovariacija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_COVARIANCE_LABEL\n"
"string.text"
msgid "Covariances"
msgstr "Kovariacijos"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DESCRIPTIVE_STATISTICS_UNDO_NAME\n"
"string.text"
msgid "Descriptive Statistics"
msgstr "Aprašomoji statistika"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_MEAN\n"
"string.text"
msgid "Mean"
msgstr "Vidurkis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_STD_ERROR\n"
"string.text"
msgid "Standard Error"
msgstr "Standartinė paklaida"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_MODE\n"
"string.text"
msgid "Mode"
msgstr "Veiksena"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_MEDIAN\n"
"string.text"
msgid "Median"
msgstr "Mediana"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_VARIANCE\n"
"string.text"
msgid "Variance"
msgstr "Dispersija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_STD_DEVIATION\n"
"string.text"
msgid "Standard Deviation"
msgstr "Standartinis nuokrypis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_KURTOSIS\n"
"string.text"
msgid "Kurtosis"
msgstr "Ekscesas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_SKEWNESS\n"
"string.text"
msgid "Skewness"
msgstr "Asimetrija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_RANGE\n"
"string.text"
msgid "Range"
msgstr "Diapazonas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_MIN\n"
"string.text"
msgid "Minimum"
msgstr "Minimumas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_MAX\n"
"string.text"
msgid "Maximum"
msgstr "Maksimumas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_SUM\n"
"string.text"
msgid "Sum"
msgstr "Suma"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_COUNT\n"
"string.text"
msgid "Count"
msgstr "Kiekis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_FIRST_QUARTILE\n"
"string.text"
msgid "First Quartile "
msgstr "Pirmasis kvartilis "

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STRID_CALC_THIRD_QUARTILE\n"
"string.text"
msgid "Third Quartile"
msgstr "Trečiasis kvartilis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_UNDO_DISTRIBUTION_TEMPLATE\n"
"string.text"
msgid "Random ($(DISTRIBUTION))"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_UNIFORM_REAL\n"
"string.text"
msgid "Uniform"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_UNIFORM_INTEGER\n"
"string.text"
msgid "Uniform Integer"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_NORMAL\n"
"string.text"
msgid "Normal"
msgstr "Normalusis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_CAUCHY\n"
"string.text"
msgid "Cauchy"
msgstr "Koši"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_BERNOULLI\n"
"string.text"
msgid "Bernoulli"
msgstr "Bernulio"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_BINOMIAL\n"
"string.text"
msgid "Binomial"
msgstr "Binominis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_NEGATIVE_BINOMIAL\n"
"string.text"
msgid "Negative Binomial"
msgstr "Neigiamas binominis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_CHI_SQUARED\n"
"string.text"
msgid "Chi Squared"
msgstr "Chi kvadrato"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DISTRIBUTION_GEOMETRIC\n"
"string.text"
msgid "Geometric"
msgstr "Geometrinis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_MINIMUM\n"
"string.text"
msgid "Minimum"
msgstr "Minimumas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_MAXIMUM\n"
"string.text"
msgid "Maximum"
msgstr "Maksimumas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_MEAN\n"
"string.text"
msgid "Mean"
msgstr "Vidurkis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_STANDARD_DEVIATION\n"
"string.text"
msgid "Standard Deviation"
msgstr "Standartinis nuokrypis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_STANDARD_MEDIAN\n"
"string.text"
msgid "Median"
msgstr "Mediana"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_STANDARD_SIGMA\n"
"string.text"
msgid "Sigma"
msgstr "Sigma"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_STANDARD_PROBABILITY\n"
"string.text"
msgid "p Value"
msgstr "p reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_STANDARD_NUMBER_OF_TRIALS\n"
"string.text"
msgid "Number of Trials"
msgstr "Bandymų skaičius"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_RNG_PARAMETER_STANDARD_NU_VALUE\n"
"string.text"
msgid "nu Value"
msgstr "nu reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_SAMPLING_UNDO_NAME\n"
"string.text"
msgid "Sampling"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST\n"
"string.text"
msgid "F-test"
msgstr "F kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_UNDO_NAME\n"
"string.text"
msgid "F-test"
msgstr "F kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST\n"
"string.text"
msgid "t-test"
msgstr "t kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_UNDO_NAME\n"
"string.text"
msgid "t-test"
msgstr "t kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST\n"
"string.text"
msgid "z-test"
msgstr "z kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_UNDO_NAME\n"
"string.text"
msgid "z-test"
msgstr "z kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_CHI_SQUARE_TEST\n"
"string.text"
msgid "Test of Independence (Chi-Square)"
msgstr "Nepriklausomumo kriterijus (Chi kvadrato)"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_REGRESSION_UNDO_NAME\n"
"string.text"
msgid "Regression"
msgstr "Regresija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_REGRESSION\n"
"string.text"
msgid "Regression"
msgstr "Regresija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_COLUMN_LABEL_TEMPLATE\n"
"string.text"
msgid "Column %NUMBER%"
msgstr "%NUMBER% stulpelis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ROW_LABEL_TEMPLATE\n"
"string.text"
msgid "Row %NUMBER%"
msgstr "%NUMBER% eilutė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_ALPHA\n"
"string.text"
msgid "Alpha"
msgstr "Alfa"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_VARIABLE_1_LABEL\n"
"string.text"
msgid "Variable 1"
msgstr "1 kintamasis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_VARIABLE_2_LABEL\n"
"string.text"
msgid "Variable 2"
msgstr "2 kintamasis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_HYPOTHESIZED_MEAN_DIFFERENCE_LABEL\n"
"string.text"
msgid "Hypothesized Mean Difference"
msgstr "Prognozuojamas vidurkių skirtumas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_OBSERVATIONS_LABEL\n"
"string.text"
msgid "Observations"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_OBSERVED_MEAN_DIFFERENCE_LABEL\n"
"string.text"
msgid "Observed Mean Difference"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_DEGREES_OF_FREEDOM_LABEL\n"
"string.text"
msgid "df"
msgstr "df"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_P_VALUE_LABEL\n"
"string.text"
msgid "P-value"
msgstr "P-reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_CRITICAL_VALUE_LABEL\n"
"string.text"
msgid "Critical Value"
msgstr "Kritinė reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TEST_STATISTIC_LABEL\n"
"string.text"
msgid "Test Statistic"
msgstr "Kriterijaus statistika"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_LINEAR\n"
"string.text"
msgid "Linear"
msgstr "Tiesinė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_LOGARITHMIC\n"
"string.text"
msgid "Logarithmic"
msgstr "Logaritminė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_POWER\n"
"string.text"
msgid "Power"
msgstr "Laipsninė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_REGRESSION_MODEL\n"
"string.text"
msgid "Regression Model"
msgstr "Regresijos modelis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_RSQUARED\n"
"string.text"
msgid "R^2"
msgstr "R^2"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_SLOPE\n"
"string.text"
msgid "Slope"
msgstr "Krypties koeficientas"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_LABEL_INTERCEPT\n"
"string.text"
msgid "Intercept"
msgstr "Bendrasis vidurkis"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_P_RIGHT_TAIL\n"
"string.text"
msgid "P (F<=f) right-tail"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_F_CRITICAL_RIGHT_TAIL\n"
"string.text"
msgid "F Critical right-tail"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_P_LEFT_TAIL\n"
"string.text"
msgid "P (F<=f) left-tail"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_F_CRITICAL_LEFT_TAIL\n"
"string.text"
msgid "F Critical left-tail"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_P_TWO_TAIL\n"
"string.text"
msgid "P two-tail"
msgstr "dvipusė p reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_FTEST_F_CRITICAL_TWO_TAIL\n"
"string.text"
msgid "F Critical two-tail"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_PEARSON_CORRELATION\n"
"string.text"
msgid "Pearson Correlation"
msgstr "Pirsono koreliacija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_VARIANCE_OF_THE_DIFFERENCES\n"
"string.text"
msgid "Variance of the Differences"
msgstr ""

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_T_STAT\n"
"string.text"
msgid "t Stat"
msgstr "Stjudento kriterijus"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_P_ONE_TAIL\n"
"string.text"
msgid "P (T<=t) one-tail"
msgstr "P (T<=t), vienpusė reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_T_CRITICAL_ONE_TAIL\n"
"string.text"
msgid "t Critical one-tail"
msgstr "vienpusė kritinė t reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_P_TWO_TAIL\n"
"string.text"
msgid "P (T<=t) two-tail"
msgstr "P (T<=t), dvipusė reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_TTEST_T_CRITICAL_TWO_TAIL\n"
"string.text"
msgid "t Critical two-tail"
msgstr "dvipusė kritinė t reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_Z_VALUE\n"
"string.text"
msgid "z"
msgstr "z"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_KNOWN_VARIANCE\n"
"string.text"
msgid "Known Variance"
msgstr "Žinoma dispersija"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_P_ONE_TAIL\n"
"string.text"
msgid "P (Z<=z) one-tail"
msgstr "P (Z<=z) vienpusė reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_Z_CRITICAL_ONE_TAIL\n"
"string.text"
msgid "z Critical one-tail"
msgstr "vienpusė kritinė z reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_P_TWO_TAIL\n"
"string.text"
msgid "P (Z<=z) two-tail"
msgstr "P (Z<=z) dvipusė reikšmė"

#: StatisticsDialogs.src
msgctxt ""
"StatisticsDialogs.src\n"
"RID_STATISTICS_DLGS\n"
"STR_ZTEST_Z_CRITICAL_TWO_TAIL\n"
"string.text"
msgid "z Critical two-tail"
msgstr "dvipusė kritinė z reikšmė"
