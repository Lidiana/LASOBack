#. extracted from cui/source/tabpages
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: https://bugs.libreoffice.org/enter_bug.cgi?product=LibreOffice&bug_status=UNCONFIRMED&component=UI\n"
"POT-Creation-Date: 2016-11-09 14:10+0100\n"
"PO-Revision-Date: 2016-05-04 07:15+0000\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Accelerator-Marker: ~\n"
"X-Generator: LibreOffice\n"
"X-POOTLE-MTIME: 1462346109.000000\n"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_NONE\n"
"string.text"
msgid "Set No Borders"
msgstr "אל תסמן גבולות"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_ONLYOUTER\n"
"string.text"
msgid "Set Outer Border Only"
msgstr "סימון גבול חיצוני בלבד"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERHORI\n"
"string.text"
msgid "Set Outer Border and Horizontal Lines"
msgstr "סימון הגבול החיצוני והקווים האופקיים"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERALL\n"
"string.text"
msgid "Set Outer Border and All Inner Lines"
msgstr "סימון הגבול החיצוני וכל הקווים הפנימיים"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_TABLE_PRESET_OUTERINNER\n"
"string.text"
msgid "Set Outer Border Without Changing Inner Lines"
msgstr "סימון הגבול החיצוני בלי לשנות את הקווים הפנימיים"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_DIAGONAL\n"
"string.text"
msgid "Set Diagonal Lines Only"
msgstr "סימון קווים אלכסוניים בלבד"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ALL\n"
"string.text"
msgid "Set All Four Borders"
msgstr "סימון כל ארבעת הגבולות"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_LEFTRIGHT\n"
"string.text"
msgid "Set Left and Right Borders Only"
msgstr "סימון הגבול השמאלי והימני בלבד"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_TOPBOTTOM\n"
"string.text"
msgid "Set Top and Bottom Borders Only"
msgstr "סימון הגבול העליון והתחתון בלבד"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_PARA_PRESET_ONLYLEFT\n"
"string.text"
msgid "Set Left Border Only"
msgstr "סימון הגבול השמאלי בלבד"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_HOR_PRESET_ONLYHOR\n"
"string.text"
msgid "Set Top and Bottom Borders, and All Inner Lines"
msgstr "סימון גבולות עליונים ותחתונים, וגם כל הקווים פנימיים"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_VER_PRESET_ONLYVER\n"
"string.text"
msgid "Set Left and Right Borders, and All Inner Lines"
msgstr "סימון גבולות ימינים ושמאלים, וגם כל הקווים פנימיים"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_NONE\n"
"string.text"
msgid "No Shadow"
msgstr "ללא הצללה"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMRIGHT\n"
"string.text"
msgid "Cast Shadow to Bottom Right"
msgstr "הטלת צל לעבר הצד הימני התחתון"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPRIGHT\n"
"string.text"
msgid "Cast Shadow to Top Right"
msgstr "הטלת צל לעבר הצד הימני העליון"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_BOTTOMLEFT\n"
"string.text"
msgid "Cast Shadow to Bottom Left"
msgstr "הטלת צל לעבר הצד השמאלי התחתון"

#: border.src
msgctxt ""
"border.src\n"
"RID_SVXSTR_SHADOW_STYLE_TOPLEFT\n"
"string.text"
msgid "Cast Shadow to Top Left"
msgstr "הטלת צל לעבר הצד השמאלי העליון"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_LTR\n"
"string.text"
msgid "Left-to-right (LTR)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_RTL\n"
"string.text"
msgid "Right-to-left (RTL)"
msgstr ""

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_FRAMEDIR_SUPER\n"
"string.text"
msgid "Use superordinate object settings"
msgstr "שימוש בהגדרות אובייקט ברמת העל"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_HORI\n"
"string.text"
msgid "Left-to-right (horizontal)"
msgstr "משמאל לימין (אופקי)‏"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_HORI\n"
"string.text"
msgid "Right-to-left (horizontal)"
msgstr "מימין לשמאל (אופקי)‏"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_RTL_VERT\n"
"string.text"
msgid "Right-to-left (vertical)"
msgstr "מימין לשמאל (אנכי)‏"

#: frmdirlbox.src
msgctxt ""
"frmdirlbox.src\n"
"RID_SVXSTR_PAGEDIR_LTR_VERT\n"
"string.text"
msgid "Left-to-right (vertical)"
msgstr "שמאל לימין (אנכי)‏"

#: paragrph.src
msgctxt ""
"paragrph.src\n"
"STR_EXAMPLE\n"
"string.text"
msgid "Example"
msgstr "דוגמה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_GRADIENT\n"
"string.text"
msgid "Please enter a name for the gradient:"
msgstr "יש להזין שם עבור המידרג:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_GRADIENT\n"
"string.text"
msgid ""
"The gradient was modified without saving. \n"
"Modify the selected gradient or add a new gradient."
msgstr ""
"המדרג שונה אך לא נשמר. \n"
"יש לשנות את המדרג שנבחר או להוסיף מדרג חדש.‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_BITMAP\n"
"string.text"
msgid "Please enter a name for the bitmap:"
msgstr "יש להזין שם עבור מפת הסיביות:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_EXT_BITMAP\n"
"string.text"
msgid "Please enter a name for the external bitmap:"
msgstr "יש להזין שם עבור מפת הסיביות החיצונית:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_NEW_PATTERN\n"
"string.text"
msgid "Please enter a name for the pattern:"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_BITMAP\n"
"string.text"
msgid ""
"The bitmap was modified without saving. \n"
"Modify the selected bitmap or add a new bitmap."
msgstr ""
"מפת הסיביות שונתה אבל לא נשמרה.\n"
"יש לשנות את מפת הסיביות שנבחר או להוסיף חדשה.‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_PATTERN\n"
"string.text"
msgid ""
"The pattern was modified without saving. \n"
"Modify the selected pattern or add a new pattern"
msgstr ""

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINESTYLE\n"
"string.text"
msgid "Please enter a name for the line style:"
msgstr "יש להזין שם עבור סגנון הקו:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_LINESTYLE\n"
"string.text"
msgid ""
"The line style was modified without saving. \n"
"Modify the selected line style or add a new line style."
msgstr ""
"סגנון הקו שונה אבל לא נשמר.\n"
"יש לשנות את סגנון הקו שנבחר או להוסיף סגנון קו חדש.‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_HATCH\n"
"string.text"
msgid "Please enter a name for the hatching:"
msgstr "יש להזין שם עבור הקווקו:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_HATCH\n"
"string.text"
msgid ""
"The hatching type was modified but not saved. \n"
"Modify the selected hatching type or add a new hatching type."
msgstr ""
"סגנון הקיווקוו שונה אבל לא נשמר.\n"
"יש לשנות את סגנון הקיווקוו שנבחר או להוסיף חדש.‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHANGE\n"
"string.text"
msgid "Modify"
msgstr "שינוי"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ADD\n"
"string.text"
msgid "Add"
msgstr "הוספה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_COLOR\n"
"string.text"
msgid "Please enter a name for the new color:"
msgstr "יש להזין שם עבור הצבע החדש:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ASK_CHANGE_COLOR\n"
"string.text"
msgid ""
"The color was modified without saving.\n"
"Modify the selected color or add a new color."
msgstr ""
"הצבע שונה אבל לא נשמר.\n"
"יש לשנות את הצבע שנבחר או להוסיף צבע חדש.‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_TABLE\n"
"string.text"
msgid "Table"
msgstr "טבלה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DESC_LINEEND\n"
"string.text"
msgid "Please enter a name for the new arrowhead:"
msgstr "יש להזין שם עבור ראש החץ החדש:‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_NOSTYLE\n"
"string.text"
msgid "No %1"
msgstr "מספר %1"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FAMILY\n"
"string.text"
msgid "Family"
msgstr "משפחה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_FONT\n"
"string.text"
msgid "Font"
msgstr "גופן"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_STYLE\n"
"string.text"
msgid "Style"
msgstr "סגנון"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_TYPEFACE\n"
"string.text"
msgid "Typeface"
msgstr "סוג הגופן"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CHARNAME_HIGHLIGHTING\n"
"string.text"
msgid "Highlight Color"
msgstr "צבע ההדגשה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USE_REPLACE\n"
"string.text"
msgid "Use replacement table"
msgstr "שימוש בטבלת החלפות"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_WORD\n"
"string.text"
msgid "Correct TWo INitial CApitals"
msgstr "תיקון שתי אותיות רישיות עוקבות"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CPTL_STT_SENT\n"
"string.text"
msgid "Capitalize first letter of every sentence"
msgstr "הפיכת אות ראשונה לרישית בכל משפט"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BOLD_UNDER\n"
"string.text"
msgid "Automatic *bold* and _underline_"
msgstr "*הדגשה* ו_קו תחתון_ אוטומטיים"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NO_DBL_SPACES\n"
"string.text"
msgid "Ignore double spaces"
msgstr "התעלמות מרווחים כפולים"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DETECT_URL\n"
"string.text"
msgid "URL Recognition"
msgstr "זיהוי ‏‪URL‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DASH\n"
"string.text"
msgid "Replace dashes"
msgstr "החלפת מקפים"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CORRECT_ACCIDENTAL_CAPS_LOCK\n"
"string.text"
msgid "Correct accidental use of cAPS LOCK key"
msgstr "תיקון שימוש לא מכוון ב־cAPS LOCK"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NON_BREAK_SPACE\n"
"string.text"
msgid "Add non-breaking space before specific punctuation marks in French text"
msgstr "הוספת רווח בלתי מפריד לפני סימני פיסוק מסוימים בטקסט צרפתי"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_ORDINAL\n"
"string.text"
msgid "Format ordinal numbers suffixes (1st -> 1^st)"
msgstr "עיצוב סיומות מספרים סודרים (1st -> 1^st)"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_EMPTY_PARA\n"
"string.text"
msgid "Remove blank paragraphs"
msgstr "הסרת פסקאות ריקות"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_USER_STYLE\n"
"string.text"
msgid "Replace Custom Styles"
msgstr "החלפת סגנונות מותאמים אישית"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BULLET\n"
"string.text"
msgid "Replace bullets with: "
msgstr "החלפת תבליטים ב־: "

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_RIGHT_MARGIN\n"
"string.text"
msgid "Combine single line paragraphs if length greater than"
msgstr "שילוב פסקאות שורה בודדת אם האורך גדול מ-"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_NUM\n"
"string.text"
msgid "Bulleted and numbered lists. Bullet symbol: "
msgstr "רשימות תבליטים וממוספרות: סמל התבליט: "

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_BORDER\n"
"string.text"
msgid "Apply border"
msgstr "החלת גבול"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CREATE_TABLE\n"
"string.text"
msgid "Create table"
msgstr "יצירת טבלה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_REPLACE_TEMPLATES\n"
"string.text"
msgid "Apply Styles"
msgstr "החלת סגנונות"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_AT_STT_END\n"
"string.text"
msgid "Delete spaces and tabs at beginning and end of paragraph"
msgstr "מחיקת רווחים וטאבים בתחילת פיסקה ובסופה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DEL_SPACES_BETWEEN_LINES\n"
"string.text"
msgid "Delete spaces and tabs at end and start of line"
msgstr "מחיקת רווחים וטאבים בתחילת שורה ובסופה"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_CONNECTOR\n"
"string.text"
msgid "Connector"
msgstr "מחבר"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_DIMENSION_LINE\n"
"string.text"
msgid "Dimension line"
msgstr "קו ממד"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_READ_DATA_ERROR\n"
"string.text"
msgid "The file could not be loaded!"
msgstr "לא ניתן היה לטעון את הקובץ!‏"

#: strings.src
msgctxt ""
"strings.src\n"
"RID_SVXSTR_LOAD_ERROR\n"
"string.text"
msgid "The selected module could not be loaded."
msgstr "לא ניתן לטעון את המודול הנבחר.‏"
